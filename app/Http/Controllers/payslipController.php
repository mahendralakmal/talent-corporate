<?php

namespace App\Http\Controllers;

use App\Allowance;
use App\company;
use App\Employee;
use App\paysheet;
use App\Salary;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class payslipController extends Controller {
    /**
     * payslipController constructor.
     */
    public function __construct () { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $company = session('company');
        $employees = null;
        if (isset($company))
            $employees = company::findOrFail($company)->employeesWithoutResign;
        else
            \alert()->error('Please select a company');
        
        $currentMonth = date('F');
        $last_month = Date('F', strtotime($currentMonth . " last month"));
        $year = date('Y');
        $payslips = paysheet::where('month', '=', Date('F', strtotime($currentMonth . " last month")))->where('year', '=', date("Y"))->where('company_id', session('company_id'))->get();
        
        if (Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
        }
        return view('reports.payslip', compact('employees', 'payslips', 'last_month', 'year', 'companies'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create () {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request) {
        $this->validate($request, [
            'emp_id' => 'required|not_in:0',
            'year' => 'required|not_in:0',
            'month' => 'required|not_in:0'
        ]);
        
        $mon = $request->month;
        $month = Carbon::parse($mon)->month;
        $company = company::where('regno', session('company_id'))->first();
        
        $allowance = Allowance::where('company_id', session('company_id'))->first();
        
        $employee = Employee::where('user_id', '=', $request->emp_id)->where('company_id', session('company_id'))->first();
        
        $salary = Salary::where('employee_id', '=', $request->emp_id)->where('company_id', session('company_id'))->first();
        
        $count = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('emp_id', '=', $request->emp_id)->where('company_id', session('company_id'))->count();
        
        $logo = company::where('regno', session('company_id'))->value('logo');
    
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
        }
        if ($count == 0) {
            
            return redirect(route('payslip.index'))->with('warning', 'No Paysheet Found !');
            
        } else {
            $payslip = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('emp_id', '=', $request->emp_id)->where('company_id', session('company_id'))->first();
            
            return view('reports.payslip_report', compact('companies','salary', 'payslip', 'employee', 'company', 'allowance', 'logo', 'mon'));
        }
        
    }
    
    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id) {
        $payslip = paysheet::findOrFail($id);
        $company = company::findOrFail(session('company'));
        $logo = $company->logo;
        $mon = $payslip->month;
        $allowance = $company->allowance;
        $employee = Employee::where([['user_id',$payslip->emp_id],['company_id',$payslip->company_id]])->first();
//        dd($employee);
        if (Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
        }
        return view('reports.payslip_report', compact('companies','salary', 'payslip', 'employee', 'company', 'allowance', 'logo', 'mon'));
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id) {
        //
    }
    
    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id) {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id) {
        //
    }
    
    public function employee_payslip () {
        
        $currentMonth = date('F');
        
        $employees = company::findOrFail(session('company'))->employeesWithoutResign;
        $last_month = Date('F', strtotime($currentMonth . " last month"));
        $year = (Carbon::parse($last_month)->month == 12)? Date('Y', strtotime(date('Y') . " last year")): date('y');
        
//        dd(paysheet::where([['month', '=', Carbon::parse($last_month)->month],['year', $year],['company_id', session('company_id')]])->get());

//        dd(Auth::user()->user_id);
        
        $payslips = paysheet::where([
//            ['month', '=', Carbon::parse($last_month)->month],
//            ['year', $year],
            ['company_id', session('company_id')],
            ['emp_id', Auth::user()->user_id]
        ])->get();
        
        return view('reports.employee_payslip', compact('employees', 'payslips', 'last_month', 'year'));
        
    }
}
