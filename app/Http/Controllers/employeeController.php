<?php
    
    namespace App\Http\Controllers;
    
    
    use App\Allowance;
    use App\company;
    use App\Country_list;
    use App\Department;
    use App\Electorate;
    use App\Employee;
    use App\Employee_Attendance;
    use App\Employee_History;
    use App\bank;
    use App\Employee_Kids;
    use App\Bank_Branch;
    use App\Leave_balance;
    use App\LeaveType;
    use App\Salary;
    use App\Employee_Loan;
    use App\Employee_skills;
    use App\SkillCategory;
    use App\User;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Mockery\Exception;
    use Validator;
    use Illuminate\Http\Request;
    use phpDocumentor\Reflection\Types\Integer;
    use RealRashid\SweetAlert\Facades\Alert;
    use function GuzzleHttp\Promise\all;
    
    /**
     * Class employeeController
     * @package App\Http\Controllers
     */
    class employeeController extends Controller {
        /**
         * employeeController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index () {
            $company = session('company');
            $employees = null;
            if (isset($company))
                $employees = company::findOrFail($company)->employees;
            else
                \alert()->error('Please select a company');
            
            return view('employee.index', compact('employees'));
        }
        
        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create () {
            $company = company::findOrFail(session('company'));
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            $employees = $company->employees;
            $skill_categoryies = SkillCategory::orderBy('cat_name', 'asc')->get();
            $departments = $company->departments;
            $electorates = Electorate::orderBy('polling_division', 'asc')->get();
            $allowance = $company->allowance;
            $countries = Country_list::orderBy('name', 'asc')->get();
            
            return view('employee.create', compact('banks', 'bank_branches', 'employees', 'allowance', 'skill_categoryies', 'departments', 'electorates', 'countries'));
        }
        
        /**
         * Store a newly created resource in storage.
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store (Request $request) {
            $validator = Validator::make($request->all(), [
                'company_id' => 'required',
                'user_id' => 'required',
                'epf_no' => 'required|max:5',
                'title' => 'required|not_in:0',
                'fname' => 'required|alpha|alpha_dash',
                'lname' => 'required|alpha|alpha_dash',
                'full_name' => 'required|max:500',
                'name_with_initials' => 'required|max:200',
                'p_address_line1' => 'required',
                'p_city' => 'required',
                'p_country' => 'required',
                'p_country_code' => 'required',
                'c_address_line1' => 'required',
                'c_city' => 'required',
                'c_country' => 'required',
                'c_country_code' => 'required',
                'nationality' => 'required',
                'nic' => 'required|max:12|min:10',
                'personal_email' => 'required|email|unique:employees',
                'personalmobileno' => 'required',
                'homeno' => 'required',
                'bday' => 'required',
                'civil_status' => 'required|not_in:0',
                'gender' => 'required|not_in:0',
                'designation' => 'required',
                'designated_date' => 'required',
                'department' => 'required|not_in:0',
                'employee_cat' => 'required|not_in:0',
                'skill_cat' => 'required|not_in:0',
                'basic_salary' => 'required',
//                'bank_code' => 'required|not_in:0',
//                'branch_code' => 'required|not_in:0',
//                'account_no' => 'required|max:12',
//                'bankaccount_name' => 'required',
                'emp_agreement' => 'required',
            ]);
            
            if ($validator->fails()) {
                return redirect(route('employees.create'))
                    ->withErrors($validator)
                    ->with('warning', 'Error')
                    ->withInput();
            }
            if ($request->hasFile('emp_agreement')) {
                $filenameWithExt_agreement = $request->file('emp_agreement')->getClientOriginalName();
                $filename_agreement = pathinfo($filenameWithExt_agreement, PATHINFO_FILENAME);
                $extension_agreement = $request->file('emp_agreement')->getClientOriginalExtension();
                $fileNameToStore_agreement = $filename_agreement . '_' . time() . '.' . $extension_agreement;
                $path_agreement = $request->file('emp_agreement')->storeAs('public/company/employees', $fileNameToStore_agreement);
            } else {
                
                $fileNameToStore_agreement = null;
            }
            if ($request->hasFile('nic_copy')) {
                $filenameWithExt_nic = $request->file('nic_copy')->getClientOriginalName();
                $filename_nic = pathinfo($filenameWithExt_nic, PATHINFO_FILENAME);
                $extension_nic = $request->file('nic_copy')->getClientOriginalExtension();
                $fileNameToStore_nic = $filename_nic . '_' . time() . '.' . $extension_nic;
                $path_nic = $request->file('nic_copy')->storeAs('public/company/employees', $fileNameToStore_nic);
            } else {
                $fileNameToStore_nic = null;
            }
            if ($request->hasFile('birth_copy')) {
                $filenameWithExt_birth = $request->file('birth_copy')->getClientOriginalName();
                $filename_birth = pathinfo($filenameWithExt_birth, PATHINFO_FILENAME);
                $extension_birth = $request->file('birth_copy')->getClientOriginalExtension();
                $fileNameToStore_birth = $filename_birth . '_' . time() . '.' . $extension_birth;
                $path_birth = $request->file('birth_copy')->storeAs('public/company/employees', $fileNameToStore_birth);
            } else {
                $fileNameToStore_birth = null;
            }
            if ($request->hasFile('passport_copy')) {
                $filenameWithExt_passport = $request->file('passport_copy')->getClientOriginalName();
                $filename_passport = pathinfo($filenameWithExt_passport, PATHINFO_FILENAME);
                $extension_passport = $request->file('passport_copy')->getClientOriginalExtension();
                $fileNameToStore_passport = $filename_passport . '_' . time() . '.' . $extension_passport;
                $path_passport = $request->file('passport_copy')->storeAs('public/company/employees', $fileNameToStore_passport);
            } else {
                $fileNameToStore_passport = null;
            }
            if ($request->hasFile('spouse_nic_copy')) {
                $filenameWithExt_spouse_nic = $request->file('spouse_nic_copy')->getClientOriginalName();
                $filename_spouse_nic = pathinfo($filenameWithExt_spouse_nic, PATHINFO_FILENAME);
                $extension_spouse_nic = $request->file('spouse_nic_copy')->getClientOriginalExtension();
                $fileNameToStore_spouse_nic = $filename_spouse_nic . '_' . time() . '.' . $extension_spouse_nic;
                $path_spouse_nic = $request->file('spouse_nic_copy')->storeAs('public/company/employees', $fileNameToStore_spouse_nic);
            } else {
                $fileNameToStore_spouse_nic = null;
            }
            if ($request->hasFile('bank_copy')) {
                $filenameWithExt_bank = $request->file('bank_copy')->getClientOriginalName();
                $filename_bank = pathinfo($filenameWithExt_bank, PATHINFO_FILENAME);
                $extension_bank = $request->file('bank_copy')->getClientOriginalExtension();
                $fileNameToStore_bank = $filename_bank . '_' . time() . '.' . $extension_bank;
                $path_bank = $request->file('bank_copy')->storeAs('public/company/employees', $fileNameToStore_bank);
            } else {
                $fileNameToStore_bank = null;
            }
            if ($request->hasFile('additional_doc2')) {
                $filenameWithExt_additional2 = $request->file('additional_doc2')->getClientOriginalName();
                $filename_additional2 = pathinfo($filenameWithExt_additional2, PATHINFO_FILENAME);
                $extension_additional2 = $request->file('additional_doc2')->getClientOriginalExtension();
                $fileNameToStore_additional2 = $filename_additional2 . '_' . time() . '.' . $extension_additional2;
                //$path_bank = $request->file('additional_doc1')->storeAs('public/company/employees', $fileNameToStore_additional);
            } else {
                $fileNameToStore_additional2 = null;
            }
            if ($request->hasFile('additional_doc1')) {
                $filenameWithExt_additional1 = $request->file('additional_doc1')->getClientOriginalName();
                $filename_additional1 = pathinfo($filenameWithExt_additional1, PATHINFO_FILENAME);
                $extension_additional1 = $request->file('additional_doc1')->getClientOriginalExtension();
                $fileNameToStore_additional1 = $filename_additional1 . '_' . time() . '.' . $extension_additional1;
                //$path_bank = $request->file('additional_doc2')->storeAs('public/company/employees', $fileNameToStore_additional1);
            } else {
                $fileNameToStore_additional1 = null;
            }
            if ($request->hasFile('additional_doc3')) {
                $filenameWithExt_additional3 = $request->file('additional_doc3')->getClientOriginalName();
                $filename_additional3 = pathinfo($filenameWithExt_additional3, PATHINFO_FILENAME);
                $extension_additional3 = $request->file('additional_doc3')->getClientOriginalExtension();
                $fileNameToStore_additional3 = $filename_additional3 . '_' . time() . '.' . $extension_additional3;
                //$path_bank = $request->file('additional_doc3')->storeAs('public/company/employees', $fileNameToStore_additional);
            } else {
                $fileNameToStore_additional3 = null;
            }
            if ($request->hasFile('joining_letter')) {
                $filenameWithExt_joining = $request->file('joining_letter')->getClientOriginalName();
                $filename_joining = pathinfo($filenameWithExt_joining, PATHINFO_FILENAME);
                $extension_joining = $request->file('joining_letter')->getClientOriginalExtension();
                $fileNameToStore_joining = $filename_joining . '_' . time() . '.' . $extension_joining;
                $path_bank = $request->file('joining_letter')->storeAs('public/company/employees', $fileNameToStore_joining);
            } else {
                $fileNameToStore_joining = null;
            }
            
            DB::beginTransaction();
            
            $employee = new Employee();
            $employee->user_id = $request->user_id;
            $employee->epf_no = $request->epf_no;
            
            if (Auth::user()->id == 1)
                $employee->company_id = session('company_id');
            else {
                if (Auth::user()->company->count() > 1)
                    $employee->company_id = session('company_id');
                else
                    $employee->company_id = $request->company_id;
            }
            
            $employee->title = $request->title;
            $employee->fname = $request->fname;
            $employee->lname = $request->lname;
            $employee->full_name = $request->full_name;
            $employee->name_with_initials = $request->name_with_initials;
            $employee->bday = $request->bday;
            $employee->nic = $request->nic;
            $employee->religion = $request->religion;
            $employee->reporting_manager = $request->reporting_manager;
            $employee->department = $request->department;
            $employee->electorate = $request->electorate;
            $employee->employee_cat = $request->employee_cat;
            $employee->passportNo = $request->passNo;
            $employee->off_email = $request->official_email;
            $employee->personal_email = $request->personal_email;
            
            if ($request->spouse_teleNo != "__ ___-____") {
                $employee->off_mobileno = $request->officialmobileno;
            }
            
            if ($request->spouse_teleNo != "__ ___-____") {
                $employee->per_mobileno = $request->personalmobileno;
            }
            
            if ($request->spouse_teleNo != "__ ___-____") {
                $employee->homeno = $request->homeno;
            }
            
            $employee->nationality = $request->nationality;
            $employee->civil_status = $request->civil_status;
            $employee->gender = $request->gender;
            $employee->blod = $request->blod;
            $employee->designation = $request->designation;
            $employee->date_of_join = $request->designated_date;
            $employee->p_line01 = $request->p_address_line1;
            $employee->p_line02 = $request->p_address_line2;
            $employee->p_street = $request->p_street;
            $employee->p_city = $request->p_city;
            $employee->p_postal_code = $request->p_postal_code;
            $splitName1 = explode('-', $request->p_country);
            $employee->p_country = $splitName1[0];
            $employee->p_country_code = $request->p_country_code;
            $employee->c_line01 = $request->c_address_line1;
            $employee->c_line02 = $request->c_address_line2;
            $employee->c_street = $request->c_street;
            $employee->c_city = $request->c_city;
            $employee->c_postal_code = $request->c_postal_code;
            $splitName2 = explode('-', $request->c_country);
            $employee->c_country = $splitName2[0];
            $employee->c_country_code = $request->c_country_code;
            $employee->bank_code = $request->bank_code;
            $employee->branch_code = $request->branch_code;
            $original_acc_no = str_replace('_', '', $request->account_no);
            $acc_length = strlen($original_acc_no);
            $required_zeros = 12 - $acc_length;
            $new_zeros = str_repeat("0", $required_zeros);
            $new_account_no = $new_zeros . $original_acc_no;
            $employee->account_no = $new_account_no;
            $employee->account_name = $request->bankaccount_name;
            $employee->spouse_name = $request->spouse_name;
            $employee->spouse_nic = $request->spouse_nic;
            $employee->spouse_dob = $request->spouse_dob;
            $employee->spouse_gender = $request->spouse_gender;
            if ($request->spouse_teleNo != "__ ___-____") {
                $employee->spouse_teleNo = $request->spouse_teleNo;
            }
            
            $employee->spouse_blood = $request->spouse_blood;
            $employee->attendance = 0;
            $employee->skill_cate = $request->skill_cat;
            $employee->last_salary = 0;
            $employee->agreement = $fileNameToStore_agreement;
            $employee->nic_copy = $fileNameToStore_nic;
            $employee->dob_copy = $fileNameToStore_birth;
            $employee->passport_copy = $fileNameToStore_passport;
            $employee->spouse_nic_copy = $fileNameToStore_spouse_nic;
            $employee->account_copy = $fileNameToStore_bank;
            $employee->joining_copy = $fileNameToStore_joining;
            $employee->additional_doc1 = $fileNameToStore_additional1;
            $employee->additional_doc2 = $fileNameToStore_additional2;
            $employee->additional_doc3 = $fileNameToStore_additional3;
            $employee->user_img = "profile.png";
            $employee->status = 1;
            $count = Employee::where('company_id', '=', $request->company_id)->where('epf_no', '!=', null)->count();
            if ($request->epf_calc == "on") {
                $employee->epf_no = ++$count;
            }
            
            $history = new Employee_History();
//        $history->company_id = $request->company_id;
            if (Auth::user()->id == 1)
                $history->company_id = session('company_id');
            else {
                if (Auth::user()->company->count() > 1)
                    $history->company_id = session('company_id');
                else
                    $history->company_id = $request->company_id;
            }
            $history->employee_id = $request->user_id;
            $history->designation = $request->designation;
            $history->description = "Designated as " . $request->designation;
            $history->date = $request->designated_date;
            
            if ($request->kid1_name != null) {
                $kid1 = new Employee_Kids();
//            $kid1->company_id = $request->company_id;
                if (Auth::user()->id == 1)
                    $kid1->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $kid1->company_id = session('company_id');
                    else
                        $kid1->company_id = $request->company_id;
                }
                $kid1->employee_id = $request->user_id;
                $kid1->name = $request->kid1_name;
                $kid1->dob = $request->kid1_dob;
                $kid1->blood = $request->kid1_blood;
                $kid1->save();
            }
            if ($request->kid2_name != null) {
                $kid2 = new Employee_Kids();
                if (Auth::user()->id == 1)
                    $kid2->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $kid2->company_id = session('company_id');
                    else
                        $kid2->company_id = $request->company_id;
                }
                $kid2->employee_id = $request->user_id;
                $kid2->name = $request->kid2_name;
                $kid2->dob = $request->kid2_dob;
                $kid2->blood = $request->kid2_blood;
                $kid2->save();
            }
            if ($request->kid3_name != null) {
                $kid3 = new Employee_Kids();
                if (Auth::user()->id == 1)
                    $kid3->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $kid3->company_id = session('company_id');
                    else
                        $kid3->company_id = $request->company_id;
                }
                $kid3->employee_id = $request->user_id;
                $kid3->name = $request->kid2_name;
                $kid3->dob = $request->kid2_dob;
                $kid3->blood = $request->kid2_blood;
                $kid3->save();
            }
            
            $salary = new Salary();
            if (Auth::user()->id == 1)
                $salary->company_id = session('company_id');
            else {
                if (Auth::user()->company->count() > 1)
                    $salary->company_id = session('company_id');
                else
                    $salary->company_id = $request->company_id;
            }
            $salary->employee_id = $request->user_id;
            $basic_salary = str_replace(',', '', $request->basic_salary);
            $salary->basic_salary = (float)$basic_salary;
            if ($request->paye_calc == "on") {
                $salary->basic_PAYE = 1;
            } else {
                $salary->basic_PAYE = 0;
            }
            if ($request->epf_calc == "on") {
                $salary->basic_EPF = 1;
            } else {
                $salary->basic_EPF = 0;
            }
            
            $allowance = Allowance::where('company_id', company::findOrFail(session('company'))->regno)->first();
//        $allowance = Allowance::where('company_id', '=', $request->company_id)->first();
            if ($allowance->allowance01 != null) {
                $salary->allowance01 = str_replace(',', '', $request->allow01);
            }
            if ($allowance->allowance02 != null) {
                $salary->allowance02 = str_replace(',', '', $request->allow02);
            }
            if ($allowance->allowance03 != null) {
                $salary->allowance03 = str_replace(',', '', $request->allow03);
            }
            if ($allowance->allowance04 != null) {
                $salary->allowance04 = str_replace(',', '', $request->allow04);
            }
            if ($allowance->allowance05 != null) {
                $salary->allowance05 = str_replace(',', '', $request->allow05);
            }
            if ($allowance->allowance06 != null) {
                $salary->allowance06 = str_replace(',', '', $request->allow06);
            }
            if ($allowance->allowance07 != null) {
                $salary->allowance07 = str_replace(',', '', $request->allow07);
            }
            if ($allowance->allowance08 != null) {
                $salary->allowance08 = str_replace(',', '', $request->allow08);
            }
            if ($allowance->allowance09 != null) {
                $salary->allowance09 = str_replace(',', '', $request->allow09);
            }
            if ($allowance->allowance10 != null) {
                $salary->allowance10 = str_replace(',', '', $request->allow10);
            }
            
            
            $user_count = User::where('email', '=', $request->personal_email)->count();
            if ($request->personal_email == auth()->user()->email && auth()->user()->user_id == null) {
                $user = User::where('email', '=', $request->personal_email)->first();
                $user->user_id = $request->user_id;
                $user->update();
                $employee->save();
                $history->save();
                $salary->save();
            } else if ($request->personal_email == auth()->user()->email && auth()->user()->user_id != null) {
                return redirect(route('employees.create'))
                    ->with('warning', 'Already company admin added as a employee')
                    ->withInput();
            } else if ($request->personal_email != auth()->user()->email && $user_count != 0) {
                return redirect(route('employees.create'))
                    ->with('warning', 'Already employee added')
                    ->withInput();
            } else {
                $user = new User();
                $user->name = $request->fname;
                $user->email = $request->personal_email;
                $user->password = $request->nic;// Do not put bcrypt method. bcrypt method add user model. it will automatically  bcrypted.
                if (Auth::user()->id == 1)
                    $user->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $user->company_id = session('company_id');
                    else
                        $user->company_id = $request->company_id;
                }
                $user->user_id = $request->user_id;
                $user->image = 'profile.png';
                $user->assignRole('employee');
                $user->save();
                $employee->save();
                $history->save();
                $salary->save();
            }
            
            if ($request->employee_cat == "Permanent") {
                $types = LeaveType::where('company_id', '=', $request->company_id)->where('type', '=', 'permanent')->orWhere('company_id', '=', 'All')->where('type', '=', 'permanent')->get();
                foreach ($types as $type) {
                    $leave_balance = new Leave_balance();
                    if (Auth::user()->id == 1)
                        $leave_balance->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $leave_balance->company_id = session('company_id');
                        else
                            $leave_balance->company_id = $request->company_id;
                    }
                    $leave_balance->user_id = $request->user_id;
                    $leave_balance->leave_type = $type->id;
                    $leave_balance->used = 0;
                    if ($type->name == 'Annual') {
                        $date = explode('-', $request->designated_date);
                        if ((integer)$date[1] < 4) {
                            $leave_balance->remaining = 14;
                        } else if ((integer)$date[1] < 7) {
                            $leave_balance->remaining = 10;
                        } else if ((integer)$date[1] < 11) {
                            $leave_balance->remaining = 7;
                            
                        } else {
                            $leave_balance->remaining = 4;
                        }
                    } else {
                        $leave_balance->remaining = $type->entitlement;
                    }
                    
                    
                    $leave_balance->save();
                }
            } else {
                $types = LeaveType::where('company_id', '=', $request->company_id)->where('type', '=', 'probation')->orWhere('company_id', '=', 'All')->where('type', '=', 'probation')->get();
                foreach ($types as $type) {
                    $leave_balance = new Leave_balance();
                    if (Auth::user()->id == 1)
                        $leave_balance->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $leave_balance->company_id = session('company_id');
                        else
                            $leave_balance->company_id = $request->company_id;
                    }
                    $leave_balance->user_id = $request->user_id;
                    $leave_balance->leave_type = $type->id;
                    $leave_balance->used = 0;
                    $leave_balance->remaining = $type->entitlement;
                    $leave_balance->save();
                }
            }
            
            DB::commit();
            
            alert()->success("Employee Added Successfully!");
            return redirect(route('employees.index'))->with('success', 'Employee Added Successfully!');
        }
        
        /**
         * Display the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show ($id) {
            $employee = Employee::findOrFail($id);
//        $d = Employee_Attendance::where([['companyId', session('company_id')],['user_id', $employee->user_id]])->get();
//        dd($d);
            $attendYears = Employee_Attendance::where([['companyId', session('company_id')], ['user_id', $employee->user_id]])->select(DB::raw("DATE_FORMAT(today,'%Y') as year"))->distinct()->get();
            $attendMonth = Employee_Attendance::where([['companyId', session('company_id')], ['user_id', $employee->user_id]])->select(DB::raw("DATE_FORMAT(today,'%Y') as year"), DB::raw("DATE_FORMAT(today,'%M') as month"))->distinct()->get();
            $employee_history = Employee_History::all();
            $loan = Employee_Loan::all();
            $skills = SkillCategory::all();
            $kids = Employee_Kids::all();
            $salaries = $employee->salary;
            $departments = company::findOrFail(session('company'))->departments;
            $employees = company::findOrFail(session('company'))->employees;
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $allowance = Company::where('regno', $employee->company_id)->first()->allowance; //Allowance::where('company_id','=',$employee->company_id)->first();
            return view('employee.show', compact('attendYears', 'attendMonth', 'employee', 'employee_history', 'employees', 'allowance', 'loan', 'skills', 'kids', 'salaries', 'bank_branches', 'banks', 'departments'));
        }
        
        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit ($id) {
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            }
            $employee = Employee::findOrFail($id);
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $kids = Employee_Kids::all();
            $salary = $employee->salary;
            $employees = company::findOrFail(session('company'))->employees;
            $skills = SkillCategory::all();
            $departments = company::findOrFail(session('company'))->departments;
            $electorates = Electorate::orderBy('polling_division', 'asc')->get();
            $countries = Country_list::orderBy('name', 'asc')->get();
            $employee_history = Employee_History::all();
            $allowance = Allowance::where('company_id', '=', $employee->company_id)->first();
            
            return view('employee.edit', compact('companies', 'employee', 'departments', 'electorates', 'countries', 'employee_history', 'employees', 'allowance', 'skills', 'bank_branches', 'banks', 'kids', 'salary'));
        }
        
        /**
         * Update the specified resource in storage.
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update (Request $request, $id) {
            if (sizeof($request->all()) == 5) {
                $employee = Employee::findOrFail($request->employee_id);
                $employee->date_of_resign = $request->separation_date;
                $employee->remarks_for_separation = $request->remarks_for_separation;
                $employee->update();
            } else {
                $employee = Employee::findOrFail($request->emp_id);
                $employee->user_id = $request->user_id;
                if (Auth::user()->id == 1)
                    $employee->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $employee->company_id = session('company_id');
                    else
                        $employee->company_id = $request->company_id;
                }
                $employee->title = $request->title;
                $employee->fname = $request->fname;
                $employee->lname = $request->lname;
                $employee->full_name = $request->full_name;
                $employee->name_with_initials = $request->name_with_initials;
                $employee->bday = $request->bday;
                $employee->religion = $request->religion;
                $employee->reporting_manager = $request->reporting_manager;
                $employee->department = $request->department;
                $employee->electorate = $request->electorate;
                $employee->employee_cat = $request->employee_cat;
                $employee->nic = $request->nic;
                $employee->passportNo = $request->passNo;
                $employee->off_email = $request->official_email;
                $employee->personal_email = $request->personal_email;
                $employee->off_mobileno = $request->officialmobileno;
                $employee->per_mobileno = $request->personalmobileno;
                $employee->skill_cate = $request->skill_cat;
                $employee->homeno = $request->homeno;
                $employee->nationality = $request->nationality;
                $employee->civil_status = $request->civil_status;
                $employee->gender = $request->gender;
                $employee->blod = $request->blod;
                $employee->designation = $request->designation;
                $employee->p_line01 = $request->p_address_line1;
                $employee->p_line02 = $request->p_address_line2;
                $employee->p_street = $request->p_street;
                $employee->p_city = $request->p_city;
                $employee->p_postal_code = $request->p_postal_code;
                $splitName1 = explode('-', $request->p_country);
                $employee->p_country = $splitName1[0];
                $employee->p_country_code = $request->p_country_code;
                $employee->c_line01 = $request->c_address_line1;
                $employee->c_line02 = $request->c_address_line2;
                $employee->c_street = $request->c_street;
                $employee->c_city = $request->c_city;
                $employee->c_postal_code = $request->c_postal_code;
                $splitName2 = explode('-', $request->c_country);
                $employee->c_country = $splitName2[0];
                $employee->c_country_code = $request->c_country_code;
                $employee->bank_code = $request->bank_code;
                $employee->branch_code = $request->branch_code;
                $employee->account_no = $request->account_no;
                $employee->account_name = $request->bankaccount_name;
                $employee->spouse_name = $request->spouse_name;
                $employee->spouse_nic = $request->spouse_nic;
                $employee->spouse_dob = $request->spouse_dob;
                $count = Employee::where('company_id', '=', $request->company_id)->where('epf_no', '!=', null)->count();
                if ($employee->emp_no != null) {
                    if ($request->epf_calc == "on") {
                        $employee->epf_no = ++$count;
                    }
                }
                $employee->spouse_teleNo = $request->spouse_teleNo;
                $employee->spouse_blood = $request->spouse_blood;
                $employee->shift = $request->shift;
                $employee->update();
                
                if ($request->new_kid_name != null) {
                    $kid1 = new Employee_Kids();
                    $kid1->company_id = $request->company_id;
                    if (Auth::user()->id == 1)
                        $kid1->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $kid1->company_id = session('company_id');
                        else
                            $kid1->company_id = $request->company_id;
                    }
                    $kid1->employee_id = $request->user_id;
                    $kid1->name = $request->new_kid_name;
                    $kid1->dob = $request->new_kid_dob;
                    $kid1->blood = $request->new_kid_blood;
                    $kid1->save();
                }
                if ($request->new_kid_name != null) {
                    $kid2 = new Employee_Kids();
                    $kid2->company_id = $request->company_id;
                    if (Auth::user()->id == 1)
                        $kid2->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $kid2->company_id = session('company_id');
                        else
                            $kid2->company_id = $request->company_id;
                    }
                    $kid2->employee_id = $request->user_id;
                    $kid2->name = $request->new_kid_name;
                    $kid2->dob = $request->new_kid_dob;
                    $kid2->blood = $request->new_kid_blood;
                    $kid2->save();
                }
                if ($request->new_kid_name != null) {
                    $kid3 = new Employee_Kids();
                    $kid3->company_id = $request->company_id;
                    if (Auth::user()->id == 1)
                        $kid3->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $kid3->company_id = session('company_id');
                        else
                            $kid3->company_id = $request->company_id;
                    }
                    $kid3->employee_id = $request->user_id;
                    $kid3->name = $request->new_kid_name;
                    $kid3->dob = $request->new_kid_dob;
                    $kid3->blood = $request->new_kid_blood;
                    $kid3->save();
                }
                if ($request->designation != $request->previous_designation) {
                    $history = new Employee_History();
                    $history->company_id = $request->company_id;
                    
                    if (Auth::user()->id == 1)
                        $history->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $history->company_id = session('company_id');
                        else
                            $history->company_id = $request->company_id;
                    }
                    
                    $history->employee_id = $request->user_id;
                    $history->designation = $request->designation;
                    $history->description = "Designated as " . $request->designation;
                    $history->date = $request->designated_date;
                    $history->save();
                }
                if ($request->previous_basic_salary != $request->basic_salary) {
                    $history = new Employee_History();
                    $history->company_id = $request->company_id;
                    
                    if (Auth::user()->id == 1)
                        $history->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $history->company_id = session('company_id');
                        else
                            $history->company_id = $request->company_id;
                    }
                    
                    $history->employee_id = $request->user_id;
                    $history->designation = "Salary increment";
                    $history->description = "Salary is increment from " . $request->previous_basic_salary . " to " . $request->basic_salary;
                    $history->date = date('Y-m-d');
                    $history->save();
                }
                if ($request->previous_bank_code != $request->bank_code | $request->previous_branch_code != $request->branch_code) {
                    $history = new Employee_History();
                    $history->company_id = $request->company_id;
                    
                    if (Auth::user()->id == 1)
                        $history->company_id = session('company_id');
                    else {
                        if (Auth::user()->company->count() > 1)
                            $history->company_id = session('company_id');
                        else
                            $history->company_id = $request->company_id;
                    }
                    
                    $history->employee_id = $request->user_id;
                    $history->designation = "Bank details has been changed";
                    $banks = bank::where('bank_code', '=', $request->previous_bank_code)->get();
                    $branches = Bank_Branch::where('branch_code', '=', $request->previous_branch_code)->get();
                    foreach ($banks as $bank) {
                        foreach ($branches as $branch) {
                            $history->description = "Previous bank details. Bank - " . $bank->bank_name . " Branch - " . $branch->bank_branch;
                            $history->date = date('Y-m-d');
                            $history->save();
                        }
                    }
                }
                
                $salary = Salary::findOrFail($request->salaryId);
                $salary->company_id = $request->company_id;
                
                if (Auth::user()->id == 1)
                    $salary->company_id = session('company_id');
                else {
                    if (Auth::user()->company->count() > 1)
                        $salary->company_id = session('company_id');
                    else
                        $salary->company_id = $request->company_id;
                }
                
                $salary->employee_id = $request->user_id;
                $basic_salary = str_replace(',', '', $request->basic_salary);
                $salary->basic_salary = (float)$basic_salary;
                if ($request->paye_calc == "on") {
                    $salary->basic_PAYE = 1;
                } else {
                    $salary->basic_PAYE = 0;
                }
                
                if ($request->epf_calc == "on") {
                    $salary->basic_EPF = 1;
                    $contribution = company::where('regno', '=', $request->company_id)->first();
                    $con = $contribution->epf_contribution;
                    $con++;
                    $contribution->epf_contribution = $con;
                    $contribution->update();
                } else {
                    $salary->basic_EPF = 0;
                }
                $allowance = Allowance::where('company_id', '=', $request->company_id)->first();
                if ($allowance->allowance01 != null)
                    $salary->allowance01 = str_replace(',', '', $request->allow01);
                if ($allowance->allowance02 != null)
                    $salary->allowance02 = str_replace(',', '', $request->allow02);
                if ($allowance->allowance03 != null)
                    $salary->allowance03 = str_replace(',', '', $request->allow03);
                if ($allowance->allowance04 != null)
                    $salary->allowance04 = str_replace(',', '', $request->allow04);
                if ($allowance->allowance05 != null)
                    $salary->allowance05 = str_replace(',', '', $request->allow05);
                if ($allowance->allowance06 != null)
                    $salary->allowance06 = str_replace(',', '', $request->allow06);
                if ($allowance->allowance07 != null)
                    $salary->allowance07 = str_replace(',', '', $request->allow07);
                if ($allowance->allowance08 != null)
                    $salary->allowance08 = str_replace(',', '', $request->allow08);
                if ($allowance->allowance09 != null)
                    $salary->allowance09 = str_replace(',', '', $request->allow09);
                if ($allowance->allowance10 != null)
                    $salary->allowance10 = str_replace(',', '', $request->allow10);
                $salary->update();
                
                if ($request->employee_cat == "Permanent") {
                    $types = LeaveType::where([['company_id', $request->company_id], ['type', 'permanent']])
                        ->orWhere([['company_id', 'All'], ['type', 'permanent']])->get();
                    if (empty($types[0])) {
                        foreach ($types as $type) {
                            $leave_balance = new Leave_balance();
                            if (Auth::user()->id == 1)
                                $leave_balance->company_id = session('company_id');
                            else {
                                if (Auth::user()->company->count() > 1)
                                    $leave_balance->company_id = session('company_id');
                                else
                                    $leave_balance->company_id = $request->company_id;
                            }
                            $leave_balance->user_id = $request->user_id;
                            $leave_balance->leave_type = $type->id;
                            $leave_balance->used = 0;
                            if ($type->name == 'Annual') {
                                $date = explode('-', $request->date_of_join);
                                if ((integer)$date[1] < 4) {
                                    $leave_balance->remaining = 14;
                                } else if ((integer)$date[1] < 7) {
                                    $leave_balance->remaining = 10;
                                } else if ((integer)$date[1] < 11) {
                                    $leave_balance->remaining = 7;
                                } else {
                                    $leave_balance->remaining = 4;
                                }
                            } else {
                                $leave_balance->remaining = $type->entitlement;
                            }
                            $leave_balance->save();
                        }
                    }
                } else {
                    $types = LeaveType::where('company_id', $request->company_id)->where('type', 'probation')->orWhere('company_id', '=', 'All')->where('type', '=', 'probation')->get();
                    if (empty($types[0])) {
                        foreach ($types as $type) {
                            $leave_balance = new Leave_balance();
                            if (Auth::user()->id == 1)
                                $leave_balance->company_id = session('company_id');
                            else {
                                if (Auth::user()->company->count() > 1)
                                    $leave_balance->company_id = session('company_id');
                                else
                                    $leave_balance->company_id = $request->company_id;
                            }
                            $leave_balance->user_id = $request->user_id;
                            $leave_balance->leave_type = $type->id;
                            $leave_balance->used = 0;
                            $leave_balance->remaining = $type->entitlement;
                            $leave_balance->save();
                        }
                    }
                }
            }
            
            alert()->success('Update Successfull');
            return redirect(route('employees.index'))->with('success', 'Employee Updated Successfully!');
        }
        
        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy (Request $request) {
            $employee = Employee::findOrFail($request->employee_id);
            $employee->status = '0';
            $employee->update();
            alert()->success('Employee Deleted Successfully!');
            return redirect(route('employees.index'))->with('success', 'Employee Deleted Successfully!');
        }
        
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         * @throws \Illuminate\Validation\ValidationException
         */
        public function profile (Request $request, $id) {
            
            $this->validate($request, [
                'profile_image' => 'required'
            ]);
            
            if ($request->hasFile('profile_image')) {
                $filenameWithExt = $request->file('profile_image')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                $path = $request->file('profile_image')->storeAs('public/company/employees', $fileNameToStore);
            } else {
                $fileNameToStore = 'noimage.jpg';
            }
            
            $employee = Employee::findOrfail($id);
            $employee->user_img = $fileNameToStore;
            $employee->update();
            return redirect(route('employees.show', $id))->with('success', 'Employee Image Updated Successfully!');
        }
        
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         */
        public function getbranches (Request $request) {
            $html = '';
            $branchs = Bank_Branch::where('bank_code', '=', $request->input('bank_code'))->orderBy('bank_branch', 'asc')->get();
            $html .= '<option value="">Select Branch</option>';
            foreach ($branchs as $branch) {
                $html .= '<option value="' . $branch->branch_code . '">' . $branch->bank_branch . '</option>';
            }
            
            return response()->json(['html' => $html]);
        }
        
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         */
        public function filter (Request $request) {
            
            $html = '';
            $employee = Employee::where('company_id', session('company_id'))->get();
            $html .= '<option value="">' . $request->previous_branch_code . '</option>';
            foreach ($employee as $emp) {
                $html .= '<tr> <td>' . $employee->user_id . '</td><td>' . $employee->sname . $employee->fname . $employee->lname . '</td><td>' . $employee->off_email . '</td><td>' . $employee->nic . '</td><td>' . $employee->off_mobileno . '</td> <td>' . $employee->gender . '</td>
                            <td><a class="btn btn-circle btn-info btn-sm" href="{{ route(\'employees.show\',' . $employee->id . ') }}" data-toggle="tooltip" title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                            <a class="btn btn-circle btn-success btn-sm" href="{{ route(\'employees.edit\',' . $employee->id . ') }}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <button class="btn btn-danger btn-sm" data-employeeid="{{' . $employee->id . '}}" data-toggle="modal" data-target="#delete"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button></td>
                        </tr>';
            }
            
            return response()->json(['html' => $html]);
        }
        
        /**
         * @param Request $request
         */
        public function export (Request $request) {
            echo "export funtion";
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function resign_list () {
            $employees = Employee::where('status', '=', false)->get();
            
            return view('employee.resign', compact('employees'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
         */
        public function emp_profile () {
            $employee = Employee::where('company_id', session('company_id'))->where('user_id', '=', auth()->user()->user_id)->first();
            $attendYears = Employee_Attendance::where([['companyId', session('company_id')], ['user_id', auth()->user()->user_id]])->select(DB::raw("DATE_FORMAT(today,'%Y') as year"))->distinct()->get();
            $attendMonth = Employee_Attendance::where([['companyId', session('company_id')], ['user_id', auth()->user()->user_id]])->select(DB::raw("DATE_FORMAT(today,'%Y') as year"), DB::raw("DATE_FORMAT(today,'%M') as month"))->distinct()->get();
    
            $employee_history = Employee_History::all();
            $loan = Employee_Loan::all();
            $skills = SkillCategory::all();
            $kids = Employee_Kids::all();
            $salaries = Salary::all();
            $employees = Employee::all();
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $allowance = Allowance::where('company_id', session('company_id'))->first();
            if ($employee == null) {
                return redirect('home')->with('warning', 'Your Profile Not Found Please Add Employee First');
            } else {
                return view('employee.profile', compact('attendYears', 'attendMonth', 'employee', 'employee_history', 'employees', 'allowance', 'loan', 'skills', 'kids', 'salaries', 'bank_branches', 'banks'));
            }
        }
        
        /**
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function emp_list ($id) {
            $employees = Employee::where('company_id', '=', $id)->get();
            
            return view('employee.company_employee', compact('employees'));
            
        }
        
        public function separateEmployee (Request $request) {
        
        }
    }
