<?php

namespace App\Http\Controllers;

use App\company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:super-admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('roles.index', compact('roles','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'role_name' => 'required',
//            'permissions' => 'required|array|min:1',
        ]);

        $role = new Role();
        $role->name = strtolower(preg_replace('/\s+/','-',$request->role_name));
        $role->role_name = $request->role_name;
        $permissions = $request['permissions'];
        $role->save();

//        foreach ($permissions as $permission){
//
//            $p = Permission::where('id', '=', $permission)->firstOrFail();
//
//            $role = Role::where('name', '=', $request->role_name)->first();
//
//            $role->givePermissionTo($p);
//
//        }

//        $role->syncPermissions($permissions);
        alert()->success('Role Created Successfully!');
        return redirect(route('roles.index'))->with('success', 'Role Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        $permissions = Permission::all();

        return view('roles.edit', compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'role_name' => 'required',
            'permissions' => 'required|array|min:1',
        ]);

        $role = Role::findOrFail($id);

        $role = Role::where('name', '=', $request->role_name)->first();

        $role->revokePermissionTo(Permission::all());

        $permissions = $request['permissions'];

        foreach ($permissions as $permission){

            $p = Permission::where('id', '=', $permission)->firstOrFail();

            $role = Role::where('name', '=', $request->role_name)->first();

            $role->givePermissionTo($p);
        }

        $role->syncPermissions($permissions);

        return redirect(route('roles.index'))->with('success', 'Role Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();

        return redirect(route('roles.index'))->with('success', 'Role Deleted Successfully!');
    }
}
