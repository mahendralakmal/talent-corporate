<?php

namespace App\Http\Controllers;

use App\company;
use App\Employee;
use App\Employee_Loan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    /**
     * LoanController constructor.
     */
    public function __construct() { $this->middleware('auth'); }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loans = Employee_Loan::all();

        $employee = Employee::where('company_id',session('company_id'))->get();
    
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
        }
        return view('loan.index',compact('loans','employee', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
        }
        return view('loan.create',compact('companies','employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'emp_id' => 'required|not_in:0',
            'emp_name' => 'required',
            'loan_desc' => 'required',
            'loan_amount' => 'required',
            'no_of_months' => 'required',
            'monthly_inst' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);


        $loan = new Employee_Loan();

        $loan->company_id = $request->company_id;

        $splitName = explode('-', $request->emp_id);

        $loan->user_id = $splitName[0];

        $loan->loan_type = $request->loan_desc;

        $amount = str_replace(',','',$request->loan_amount);

        $loan->loan_total = (float)$amount;

        $loan->no_installments = $request->no_of_months;

        $loan->interest_rate = $request->interest_rate;

        $monthly_installment = str_replace(',','',$request->monthly_inst);

        $loan->monthly_installment = (float)$monthly_installment;

        $loan->payed_installments = $request->payed_installments;

        $loan->start_date = $request->start_date;

        $loan->end_date = $request->end_date;

        $loan->save();

        return redirect(route('employee_loans.index'))->with('success','Loan Added Successfully');

        //dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
