<?php
    
    namespace App\Http\Controllers;
    
    use App\company;
    use App\Employee;
    use App\Leave;
    use App\Leave_balance;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    
    class LeaveController extends Controller {
        /**
         * LeaveController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index () {
            $leaves = null;
            $employees = null;
            $company = null;
            $role = auth()->user()->getRoleNames()->first();
//            dd(is_null(session('company')));
            if (!is_null(session('company'))) {
                $company = company::findOrFail(session('company'));
    
                if ($role == "employee") {
                    $leaves = Leave::where('company_id', session('company_id'))->where('reporting_Manager', '=', auth()->user()->employee->reporting_manager)->get();
                    $employees = auth()->user()->employee;
                } else {
                    $leaves = $company->leaves;
                }
                
            }else
                alert()->error('Please select a company');
            
            
            return view('leave.index', compact('leaves', 'employees'));
        }
        
        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create () {
            //
        }
        
        /**
         * Store a newly created resource in storage.
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store (Request $request) {
            $this->validate($request, [
                'user_id' => 'required',
                'type' => 'required',
                'reason' => 'required',
                'date' => 'required',
            ]);
            $repManagerId = Employee::where([['company_id', session('company_id')], ['user_id', $request->user_id]])->first()->reporting_manager;
            if (!is_null($repManagerId)) {
                $leave = new Leave();
                $leave->user_id = $request->user_id;
                $leave->company_id = $request->company_id;
                $leave->date = $request->date;
                $leave->type = $request->type;
                $leave->reporting_Manager = $repManagerId;
                $leave->reason = $request->reason;
                $leave->approved_status = 'pending';
                $leave->calc_status = 'pending';
                $leave->save();
                $manager_email = Employee::where('company_id', session('company_id'))->where('user_id', $request->reporting_manager)->value('off_email');
                alert()->success('Leave created successfully');
                return redirect(route('leaves.index'))->with('success', 'Leave created successfully');
            } else {
                alert()->error('Cannot create a Leave.<br> This employee must have a Reporting Manager');
                return redirect(route('leaves.index'));
            }
        }
        
        /**
         * Display the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show ($id) {
            $leaves = Leave::where([['company_id', session('company_id')],['user_id', $id]])->get();
            $leave_balance = Leave_balance::where('company_id', session('company_id'))->where('user_id', '=', $id)->get();
//            dd($leave_balance);
            $remainingLeaves = 0;
            $usedLeaves = 0;
            foreach ($leave_balance as $lBalance){
                $remainingLeaves += $lBalance->remaining;
                $usedLeaves += $lBalance->used;
            }
            return view('leave.show', compact('leaves', 'leave_balance', 'remainingLeaves', 'usedLeaves'));
        }
        
        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit ($id) {
            //
        }
        
        /**
         * Update the specified resource in storage.
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update (Request $request, $id) {
            DB::beginTransaction();
            $leaves = Leave::findOrFail($id);
            $leaves->approved_status = 'accepted';
            $userid = $leaves->user_id;
            $leaves->update();
            
            $balance_id = Leave_balance::where([['company_id', session('company_id')], ['user_id', $userid], ['leave_type', $leaves->type]])->first()->id;
            $balance = Leave_balance::findOrFail($balance_id);
            $balance->used = $balance->used + 1;
            $balance->remaining = $balance->remaining - 1;
            $balance->update();
            DB::commit();
            alert()->success('Leave Approved successfully');
            return back();
        }
        
        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy (Request $request, $id) {
            $leaves = Leave::findOrFail($id);
            $leaves->approved_status = 'rejected';
            $leaves->update();
            return redirect(route('leaves.index'))->with('success', 'Leave deleted successfully');
        }
    }
