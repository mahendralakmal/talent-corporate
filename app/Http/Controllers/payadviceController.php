<?php

namespace App\Http\Controllers;

use App\Allowance;
use App\company;
use App\Employee;
use App\paysheet;
use App\Salary;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class payadviceController extends Controller {
    /**
     * payadviceController constructor.
     */
    public function __construct () { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index () {
        if (Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
        }
        return view('reports.payadvice', compact('companies'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create () {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request) {
        $this->validate($request, [
            'year' => 'required|not_in:0',
            'month' => 'required|not_in:0'
        ]);
        $year = $request->year;
        $mon = $request->month;
        $month = Carbon::parse($request->month)->format('m');
        $company = company::where('regno', session('company_id'))->first();
        $allowance = Allowance::where('company_id', session('company_id'))->first();
        $salary = Salary::where('company_id', session('company_id'))->get();
        $count = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('company_id', session('company_id'))->count();
        $logo = company::where('regno', session('company_id'))->value('logo');
        if (Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
        }
        if ($count == 0) {
            return redirect(route('payadvice.index'))->with('warning', 'No Pay Advice Found !');
        } else {
            $payadvice = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('company_id', session('company_id'))->get();
            return view('reports.payadvice_report', compact('companies','salary', 'payadvice', 'company', 'logo', 'allowance', 'month', 'mon','year'));
        }
    }
    
    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id) {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id) {
        //
    }
    
    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id) {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id) {
        //
    }
}
