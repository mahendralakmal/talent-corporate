<?php
    
    namespace App\Http\Controllers;
    
    use App\company;
    use App\Department;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Storage;
    
    class SettingController extends Controller {
        /**
         * SettingController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        public function department () {
            $departments = null;
            if (!empty(session('company')))
                $departments = company::findOrFail(session('company'))->first()->departments;
            else {
                alert()->error("please select a company");
                return back();
            }
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
            
            return view('settings.department', compact('departments', 'companies'));
        }
        
        public function store_department (Request $request) {
            
            $this->validate($request, [
                'company' => 'required',
                'department_id' => 'required',
                'department_name' => 'required'
            ]);
            
            $department = new Department();
            $department->company_id = $request->company;
            $department->department_name = $request->department_name;
            $department->department_code = $request->department_id;
            $department->save();
            
            return redirect(route('settings.department'))->with('success', 'Department Added Successfully!');
        }
        
        public function edit_department (Request $request) {
            
            $id = $request->input('id');
            
            $department = Department::find($id);
            
            $output = array(
                'department_code' => $department->department_code,
                'department_name' => $department->department_name
            );
            echo json_encode($output);
        }
        
        public function update_department (Request $request) {
            
            $department = Department::findOrFail($request->dept_id);
            
            $department->department_name = $request->department_name;
            
            $department->department_code = $request->department_id;
            
            $department->update();
            
            return redirect(route('settings.department'))->with('success', 'Department Update Successfully!');
            
        }
        
        public function delete_department (Request $request) {
            
            $department = Department::findOrFail($request->dept_id);
            
            $department->delete();
            
            return redirect(route('settings.department'))->with('success', 'Department Delete Successfully!');
            
        }
        
        public function theme () {
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
                $users = User::all();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
                $users = User::all();
            }
            return view('settings.theme', compact('companies'));
        }
        
        public function temp($id){
            Auth::loginUsingId($id);
            return redirect(route('home'));
        }
    }
