<?php

namespace App\Http\Controllers;

use App\Mail\employee_creation;
use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{

    public function index()
    {
        return view('email.index');
    }
    public function send_mail()
    {
       Mail::send(new employee_creation());

       return redirect(route('Mail.index'))->with('success', 'Company Added Successfully!');
    }
}
