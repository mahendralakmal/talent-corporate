<?php
    
    namespace App\Http\Controllers;
    
    use App\Allowance;
    use App\company;
    use App\Department;
    use App\Employee;
    use App\Employee_Attendance;
    use App\Employee_Loan;
    use App\Events;
    use App\Imports\AttendanceImport;
    use App\Leave;
    use App\Leave_balance;
    use App\paysheet;
    use App\Salary;
    use App\User;
    use Carbon\Carbon;
    use Carbon\CarbonPeriod;
    use Illuminate\Routing\RouteGroup;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Response;
    use Illuminate\Support\Facades\Storage;
    use Maatwebsite\Excel\Facades\Excel;
    use Maatwebsite\Excel\Importer;
    use Validator;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    
    class  AttendanceController extends Controller {
        /**
         * AttendanceController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index () {
            $filtered = false;
            $attendance = null;
            $attendYears = Employee_Attendance::where('companyId', session('company_id'))->select(DB::raw("DATE_FORMAT(today,'%Y') as year"))->distinct()->get();
            $attendMonth = Employee_Attendance::where('companyId', session('company_id'))->select(DB::raw("DATE_FORMAT(today,'%Y') as year"), DB::raw("DATE_FORMAT(today,'%M') as month"))->distinct()->get();
            if ((Auth::user()->getRoleNames()->first() == "super-admin") || (Auth::user()->getRoleNames()->first() == "company-admin")) {
                $company = session('company');
                if (isset($company))
                    $employees = company::findOrFail($company)->employeesWithoutResign;
                else
                    $employees = null;
                
                $companies = company::where('status', '=', true)->get();
                $users = User::all();
            } else {
                $employees = company::findOrFail(session('company'))->employeesWithoutResign;
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
                $users = User::all();
            }
            return view('payrole.attendance', compact('employees', 'attendance', 'filtered', 'companies', 'attendYears', 'attendMonth'));
        }
        
        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create () {
            //
        }
        
        /**
         * Store a newly created resource in storage.
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store (Request $request) {
            $this->validate($request, [
                'year' => 'required|not_in:0',
                'month' => 'required|not_in:0',
                'file' => 'required',
            ]);
            
            $file = $request->file('file');
            Excel::import(new AttendanceImport, $file);
            
            alert()->success('Successfully Imported');
            return redirect(route('attendances.index'))->with('success', 'Calculated');
        }
        
        /**
         * Display the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show ($id) {
            //
        }
        
        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit ($id) {
            //
        }
        
        /**
         * Update the specified resource in storage.
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update (Request $request, $id) {
            //
        }
        
        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy ($id) {
            //
        }
        
        public function importExport () {
            return view('attendance.importExport');
        }
        
        public function downloadExcel ($type) {
            $data = Item::get()->toArray();
            
            return Excel::create('itsolutionstuff_example', function ($excel) use ($data) {
                $excel->sheet('mySheet', function ($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->download($type);
        }
        
        public function importExcel (Request $request) {
            $validator = Validator::make($request->all(), [
                'file' => 'required|max:5000|mimes:xlsx,xls,csv'
            ]);
            
            if ($validator->passes()) {
                $dataTime = date('Ymd_His');
                $file = $request->file('file');
                $filename = $dataTime . '-' . $file->getClientOriginalName();
                $savePath = public_path('/upload/');
                $file->move($savePath, $filename);
                
                
                $excel = Importer::make('Excel');
                $excel->load($savePath . $filename);
                $collection = $excel->getCollection();
                
                $attendance_arr = [];
                $days = sizeof($collection) - 1;
                
                $employees = Employee::all();
                
                
                if (sizeof($collection[1]) == 11) {
                    $myarray = array();
                    for ($row = 1; $row < sizeof($collection); $row++) {
                        $attendance_count = 0;
                        $absent_count = 0;
                        for ($col = 1; $col < $collection->count(); $col++) {
                            if ($collection[$row][$col] == 'pst') {
                                $attendance_count = $attendance_count + 1;
                            } else {
                                $leaves = Leave::where('company_id', session('company_id'))->where('user_id', '=', $collection[$row][0])->where('calc_status', '=', 'pending')->where('approved_status', '=', 'accepted')->get();
                                $status = false;
                                foreach ($leaves as $leave) {
                                    if ($leave->date === $collection[0][$col]) {
                                        if ($leave->approved_status == "accepted") {
                                            $status = true;
                                        }
                                    }
                                }
                                if ($status) {
                                    $attendance_count = $attendance_count + 1;
                                } else {
                                    $absent_count = $absent_count + 1;
                                }
                                
                            }
                        }
                        
                        $myarray[] = array("id" => $collection[$row][0], "attendance" => $attendance_count, "absent" => $absent_count, "total" => ($collection->count()) - 1);
                        
                    }
                    foreach ($employees as $employee) {
                        if ($employee->company_id == auth()->user()->company_id) {
                            foreach ($myarray as $attendance) {
                                if ($employee->user_id == $attendance['id']) {
                                    
                                    $salaries = Salary::where('company_id', '=', $employee->company_id)->where('employee_id', '=', $employee->user_id)->get();
                                    
                                    foreach ($salaries as $salary) {
                                        
                                        $total_salary = $attendance['attendance'] * ($salary->basic_salary / $attendance['total']);
                                        
                                        
                                        if ($attendance['attendance'] != $attendance['total']) {
                                            
                                            
                                            if ($salary->allow01_desc == 'Attendance' | $salary->allow01_desc == 'attendance') {
                                                $total_salary = $total_salary + $salary->allow02_amount + $salary->allow03_amount + $salary->allow04_amount;
                                            } else if ($salary->allow02_desc == 'Attendance' | $salary->allow02_desc == 'attendance') {
                                                $total_salary = $total_salary + $salary->allow01_amount + $salary->allow03_amount + $salary->allow04_amount;
                                            } else if ($salary->allow03_desc == 'Attendance' | $salary->allow03_desc == 'attendance') {
                                                $total_salary = $total_salary + $salary->allow01_amount + $salary->allow02_amount + $salary->allow04_amount;
                                            } elseif ($salary->allow04_desc == 'Attendance' | $salary->allow04_desc == 'attendance') {
                                                $total_salary = $total_salary + $salary->allow01_amount + $salary->allow02_amount + $salary->allow03_amount;
                                            } else {
                                                $total_salary = $total_salary + $salary->allow01_amount + $salary->allow02_amount + $salary->allow03_amount + $salary->allow04_amount;
                                            }
                                            
                                        } else {
                                            
                                            $total_salary = $total_salary + $salary->allow01_amount + $salary->allow02_amount + $salary->allow03_amount + $salary->allow04_amount;
                                            
                                        }
                                        
                                        
                                        $loans = Employee_Loan::where('company_id', '=', $employee->company_id)->where('user_id', '=', $employee->user_id)->get();
                                        
                                        foreach ($loans as $loan) {
                                            if ($loan->no_installments != $loan->payed_installments) {
                                                $total_salary = $total_salary - $loan->monthly_installment;
                                                
                                                $employee_loan = Employee_Loan::findOrFail($loan->id);
                                                
                                                $employee_loan->payed_installments = $loan->payed_installments + 1;
                                                
                                                $employee_loan->update();
                                            }
                                        }
                                        
                                        
                                        $employee = Employee::findOrFail($employee->id);
                                        $employee->attendance = $attendance['attendance'];
                                        $employee->last_salary = $total_salary;
                                        $employee->update();
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                    // dd($myarray);
                    return view('home');
                } else {
                    return redirect()->back()
                        ->with(['errors' => [0 => 'Please provide date in file according to sample file']]);
                    
                }
                
                
            } else {
                return redirect()->back()
                    ->with(['errors' => $validator->errors()->all()]);
            }
        }
        
        public function payRole (Request $request) {
            
            $this->validate($request, [
                'year1' => 'required|not_in:0',
                'month1' => 'required|not_in:0',
                'attendance1' => 'required',
                'payexcel' => 'required',
            ]);
            
            $paysheet = paysheet::where('company_id', session('company_id'))->where('month', '=', $request->month1)->where('year', '=', $request->year1)->count();
            
            if ($paysheet != 0) {
                
                return redirect(route('attendances.index'))->with('warning', 'All ready calculated');
                
            } else if ($request->year1 > date("Y")) {
                return redirect(route('attendances.index'))->with('warning', 'Incorrect year');
                
            } else if ($request->year1 == date("Y") && date('n', strtotime(date('F'))) <= date('n', strtotime($request->month1))) {
                
                return redirect(route('attendances.index'))->with('warning', 'Incorrect month');
            } else {
                $validator = Validator::make($request->all(), [
                    'attendance1' => 'required|max:5000|mimes:xlsx,xls,csv',
                    'payexcel' => 'required|max:5000|mimes:xlsx,xls,csv',
                ]);
                
                if ($validator->passes()) {
                    $dataTime = date('Ymd_His');
                    $file = $request->file('attendance1');
                    $filename = $dataTime . '-' . $file->getClientOriginalName();
                    $savePath = public_path('/upload/');
                    $file->move($savePath, $filename);
                    
                    
                    $excel = Importer::make('Excel');
                    $excel->load($savePath . $filename);
                    $collection = $excel->getCollection();
                    
                    
                    $dataTime = date('Ymd_His');
                    $file = $request->file('payexcel');
                    $filename = $dataTime . '-' . $file->getClientOriginalName();
                    $savePath = public_path('/upload/');
                    $file->move($savePath, $filename);
                    
                    $excel = Importer::make('Excel');
                    $excel->load($savePath . $filename);
                    $payroleArray = $excel->getCollection();
                    
                    //+
                    //dd($collection,$array);
                    
                    $company_id = auth()->user()->company_id;
                    $salaries = Salary::where('company_id', '=', $company_id)->get();
                    $SN = 1;
                    
                    
                    $shortleaveCount = 5;
                    $shortLeaveTime = 90;
                    $startTime = company::where("regno", '=', $company_id)->value('startTime');
                    $endTime = company::where("regno", '=', $company_id)->value('endTime');
                    
                    $totalHours = (strtotime($endTime) - strtotime($startTime)) / (60 * $shortLeaveTime);
                    
                    foreach ($salaries as $salary) {
                        $emp_id = $salary->employee_id;
                        $employee = Employee::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->first();
                        
                        switch ($request->month1) {
                            case("January"):
                                $workDays = 31;
                                $month = '01';
                                break;
                            case("February"):
                                $workDays = 28;
                                $month = '02';
                                break;
                            case("March"):
                                $workDays = 31;
                                $month = '03';
                                break;
                            case("April"):
                                $month = '04';
                                $workDays = 30;
                                break;
                            case("May"):
                                $month = '05';
                                $workDays = 31;
                                break;
                            case("June"):
                                $month = '06';
                                $workDays = 30;
                                break;
                            case("July"):
                                $month = '07';
                                $workDays = 31;
                                break;
                            case("August"):
                                $month = '08';
                                $workDays = 31;
                                break;
                            case("September"):
                                $month = '09';
                                $workDays = 30;
                                break;
                            case("October"):
                                $month = '10';
                                $workDays = 31;
                                break;
                            case("November"):
                                $month = '11';
                                $workDays = 30;
                                break;
                            case("December"):
                                $month = '12';
                                $workDays = 31;
                                break;
                        }
                        
                        $days = 0;
                        $totalTime = 0;
                        $absent = 0;
                        $perDay = $salary->basic_salary / 30;
                        $perhour = $salary->basic_salary / (30 * $totalHours);
                        //dd($perDay,$perhour);
                        $nopayAmount = 0;
                        $count = 0;
                        
                        for ($row = 1; $row < sizeof($collection); $row++) {
                            
                            if ($collection[$row][1] == $emp_id) {
                                if ($collection[$row][3] == "-") {
                                    
                                    $absent++;
                                    
                                    $count = Leave::where("company_id", '=', $company_id)->where('user_id', '=', $emp_id)->where('approved_status', '=', 'accepted')->where('date', '=', $collection[$row][0])->where('calc_status', '=', 'pending')->count();
                                    
                                    $holidays = Events::where('company_id', '=', $company_id)->where('event_date', '=', $collection[$row][0])->orWhere('company_id', '=', 'All')->where('event_date', '=', $collection[$row][0])->count();
                                    
                                    if ($count == 0 && $holidays == 0) {
                                        
                                        $nopayAmount += $perDay;
                                        
                                    } else if ($count != 0 && $holidays == 0) {
                                        
                                        $leave = Leave::where("company_id", '=', $company_id)->where('user_id', '=', $emp_id)->where('approved_status', '=', 'accepted')->where('date', '=', $collection[$row][0])->where('calc_status', '=', 'pending')->first();
                                        
                                        $leaveType = $leave->type;
                                        
                                        $leave->calc_status = 'calculated';
                                        
                                        $leave->update();
                                        
                                        $leaveBalance = Leave_balance::where("company_id", '=', $company_id)->where('user_id', '=', $emp_id)->where('leave_type', '=', $leaveType)->first();
                                        
                                        $leaveBalance->used = $leaveBalance->used + 1;
                                        
                                        $leaveBalance->update();
                                    }
                                    
                                } else {
                                    $days++;
                                    //$difference = strtotime($startTime)->diff();
                                    //$time = $difference->h * 60 + $difference->m;
                                    
                                    //$startDif = strtotime($startTime)-strtotime(date_format($collection[$row][3],'h:m:s'));
                                    //$endDiff = (strtotime(date_format($collection[$row][4],'h:m:s'))+60*60*12)-strtotime($endTime);


//                                if ($startDif/(60)>=0 && $endDiff/60>=0) {
//                                    //no no pay
//                                } else{
//                                    if($endDiff/(60)>=0){
//                                        if($startDif/(60) < $shortLeaveTime){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count++;
//                                        }else if($startDif/(60) < ($shortLeaveTime*2)){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour*2;
//                                            }else if($count<$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count+=2;
//                                        }else{
//                                            //half day(check in the leave table)
//                                            $nopayAmount+=$perDay/2;
//                                        }
//                                    }else if($startDif/60>=0){
//                                        if($endDiff/(60) < $shortLeaveTime){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count++;
//                                        }else if($endDiff/(60) < ($shortLeaveTime*2)){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour*2;
//                                            }else if($count<$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count+=2;
//                                        }else{
//                                            //half day(check in the leave table)
//                                            $nopayAmount+=$perDay/2;
//                                        }
//                                    }else{
//                                        if($startDif/(60) < $shortLeaveTime){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count++;
//                                        }else if($startDif/(60) < ($shortLeaveTime*2)){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour*2;
//                                            }else if($count<$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count+=2;
//                                        }else{
//                                            //half day(check in the leave table)
//                                            $nopayAmount+=$perDay/2;
//                                        }
//
//                                        if($endDiff/(60) < $shortLeaveTime){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count++;
//                                        }else if($endDiff/(60) < ($shortLeaveTime*2)){
//                                            if($count<=$shortleaveCount){
//                                                $nopayAmount+=$perhour*2;
//                                            }else if($count<$shortleaveCount){
//                                                $nopayAmount+=$perhour;
//                                            }
//                                            $count+=2;
//                                        }else{
//                                            //half day(check in the leave table)
//                                            $nopayAmount+=$perDay/2;
//                                        }
//                                    }
//                                }
                                    
                                    
                                    //$totalTime += $time;
                                    
                                }
                            }
                            
                        }
                        //dd($totalTime,$days,$totalTime/(60*($days-$absent)));
                        
                        
                        $name = $employee->name_with_initials;
                        $designation = $employee->designation;
                        $epfno = $employee->epf_no;
                        $nic = $employee->nic;
                        $DOJ = $employee->date_of_join;
                        $DOR = null;
                        $department = $employee->department;
                        
                        
                        $basic = $salary->basic_salary;
                        $allowance01 = 0;
                        $allowance02 = 0;
                        $allowance03 = 0;
                        $allowance04 = 0;
                        $allowance05 = 0;
                        $allowance06 = 0;
                        $allowance07 = 0;
                        $allowance08 = 0;
                        $allowance09 = 0;
                        $allowance10 = 0;
                        
                        $performance = 0;
                        $commission = 0;
                        
                        $ot = 0;
                        $leave_pay = 0;
                        $arrears = 0;
                        $adjustments = 0;
                        $paye = 0;
                        $epf = 0;
                        $allow = 0;
                        $advance = 0;
                        $deduct01 = 0;
                        $deduct02 = 0;
                        
                        for ($row = 1; $row < sizeof($payroleArray); $row++) {
                            
                            
                            if ($payroleArray[$row][0] == $emp_id) {
                                //dd($collection[$row]);
                                $performance = $payroleArray[$row][1];
                                $commission = $payroleArray[$row][2];
                                $ot = $payroleArray[$row][3];
                                $leave_pay = $payroleArray[$row][4];
                                $arrears = $payroleArray[$row][5];
                                $adjustments = $payroleArray[$row][6];
                                $advance = $payroleArray[$row][7];
                                $deduct01 = $payroleArray[$row][8];
                                $deduct02 = $payroleArray[$row][9];
                                //dd($performance,$commission);
                                break;
                            }
                        }
                        
                        if ($salary->basic_PAYE == 1) {
                            $paye = $paye + $basic;
                        } else {
                            $payeAmount = 0;
                        }
                        if ($salary->basic_EPF == 1) {
                            $epf = $epf + $basic;
                        } else {
                            $epfAmount = 0;
                        }
                        
                        $allowance = Allowance::where('company_id', session('company_id'))->first();
                        
                        //dd($allowance);
                        if ($allowance->allowance01 != null) {
                            
                            //dd($salary->allowance01);
                            $allow01 = $salary->allowance01;
                            $allowance01 = $salary->allowance01;
                            $allow = $allow + $allow01;
                            
                            
                            if ($allowance->allowance01Paye == true) {
                                $paye = $paye + $allow01;
                            }
                            if ($allowance->allowance01Epf == true) {
                                $epf = $epf + $allow01;
                            }
                        }
                        if ($allowance->allowance02 != null) {
                            $allow02 = $salary->allowance02;
                            $allowance02 = $salary->allowance02;
                            $allow = $allow + $allow02;
                            
                            if ($allowance->allowance02Paye == 1) {
                                $paye = $paye + $allow02;
                            }
                            if ($allowance->allowance02Epf == 1) {
                                $epf = $epf + $allow02;
                            }
                        }
                        if ($allowance->allowance03 != null) {
                            $allow03 = $salary->allowance03;
                            $allowance03 = $salary->allowance03;
                            $allow = $allow + $allow03;
                            
                            if ($allowance->allowance03Paye == 1) {
                                $paye = $paye + $allow03;
                            }
                            if ($allowance->allowance03Epf == 1) {
                                $epf = $epf + $allow03;
                            }
                        }
                        if ($allowance->allowance04 != null) {
                            $allow04 = $salary->allowance04;
                            $allowance04 = $salary->allowance04;
                            $allow = $allow + $allow04;
                            
                            if ($allowance->allowance04Paye == 1) {
                                $paye = $paye + $allow04;
                            }
                            if ($allowance->allowance04Epf == 1) {
                                $epf = $epf + $allow04;
                            }
                        }
                        if ($allowance->allowance05 != null) {
                            $allow05 = $salary->allowance05;
                            $allowance05 = $salary->allowance05;
                            $allow = $allow + $allow05;
                            
                            if ($allowance->allowance05Paye == 1) {
                                $paye = $paye + $allow05;
                            }
                            if ($allowance->allowance05Epf == 1) {
                                $epf = $epf + $allow05;
                            }
                        }
                        if ($allowance->allowance06 != null) {
                            $allow06 = $salary->allowance06;
                            $allowance06 = $salary->allowance06;
                            $allow = $allow + $allow06;
                            
                            if ($allowance->allowance06Paye == 1) {
                                $paye = $paye + $allow06;
                            }
                            if ($allowance->allowance06Epf == 1) {
                                $epf = $epf + $allow06;
                            }
                        }
                        if ($allowance->allowance07 != null) {
                            $allow07 = $salary->allowance07;
                            $allowance07 = $salary->allowance07;
                            $allow = $allow + $allow07;
                            
                            if ($allowance->allowance07Paye == 1) {
                                $paye = $paye + $allow07;
                            }
                            if ($allowance->allowance07Epf == 1) {
                                $epf = $epf + $allow07;
                            }
                        }
                        if ($allowance->allowance08 != null) {
                            $allow08 = $salary->allowance08;
                            $allowance08 = $salary->allowance08;
                            $allow = $allow + $allow08;
                            
                            if ($allowance->allowance08Paye == 1) {
                                $paye = $paye + $allow08;
                            }
                            if ($allowance->allowance08Epf == 1) {
                                $epf = $epf + $allow08;
                            }
                        }
                        if ($allowance->allowance09 != null) {
                            $allow09 = $salary->allowance09;
                            $allowance09 = $salary->allowance09;
                            $allow = $allow + $allow09;
                            
                            if ($allowance->allowance09Paye == 1) {
                                $paye = $paye + $allow09;
                            }
                            if ($allowance->allowance09Epf == 1) {
                                $epf = $epf + $allow09;
                            }
                        }
                        if ($allowance->allowance10 != null) {
                            $allow10 = $salary->allowance10;
                            $allowance10 = $salary->allowance10;
                            $allow = $allow + $allow10;
                            
                            if ($allowance->allowance10Paye == 1) {
                                $paye = $paye + $allow10;
                            }
                            if ($allowance->allowance10Epf == 1) {
                                $epf = $epf + $allow10;
                            }
                        }
                        
                        if ($paye < 100000) {
                            $calc_paye = 0;
                        } else if ($paye >= 100000 && $paye < 150000) {
                            $calc_paye = $paye * (4 / 100) - 4000;
                        } else if ($paye >= 150000 && $paye < 200000) {
                            $calc_paye = $paye * (8 / 100) - 10000;
                        } else if ($paye >= 200000 && $paye < 250000) {
                            $calc_paye = $paye * (12 / 100) - 18000;
                        } else if ($paye >= 250000 && $paye < 300000) {
                            $calc_paye = $paye * (16 / 100) - 28000;
                        } else if ($paye >= 300000 && $paye < 350000) {
                            $calc_paye = $paye * (20 / 100) - 40000;
                        } else {
                            $calc_paye = $paye * (24 / 100) - 54000;
                        }
                        
                        if ($salary->basic_EPF == 0) {
                            $epf = 0;
                        }
                        $payeAmount = $calc_paye;
                        $calc_epf = $epf * 8 / 100;
                        $epfAmount = $calc_epf;
                        $calc_loans = 0;
                        $date = $request->year1 . '-' . $month . '-01';
                        $loans = Employee_Loan::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->where('loan_status', '=', 'pending')->where('start_date', '<', date($date))->get();
                        //dd($loans);
                        foreach ($loans as $loan) {
                            
                            
                            $calc_loans = $calc_loans + $loan->monthly_installment;
                            
                            $newLoan = Employee_Loan::findOrFail($loan->id);
                            
                            $newLoan->payed_installments = $newLoan->payed_installments + 1;
                            
                            
                            if ($newLoan->no_installments == $newLoan->payed_installments) {
                                
                                $newLoan->loan_status = "completed";
                            }
                            
                            $newLoan->update();
                        }
                        $loan = $calc_loans;
                        $totalDeduction = $epfAmount + $payeAmount + $advance + $loan + $deduct01 + $deduct02 + $nopayAmount;
                        $gross_remuneration = $allowance01 + $allowance02 + $allowance03 + $allowance04 + $allowance05 + $allowance06 + $allowance07 + $allowance08 + $allowance09 + $allowance10 + $basic + $performance + $commission + $ot + $leave_pay + $arrears + $adjustments;
                        $net_sallary = $gross_remuneration - $totalDeduction;
                        $SD = 0;
                        
                        $stampDuty = company::where('regno', '=', $company_id)->value('stamp_duty');
                        if ($stampDuty == 'on') {
                            if ($net_sallary > 25000) {
                                $net_sallary = $net_sallary - 25;
                                $SD += 25;
                            }
                        }
                        //dd($name);
                        $payrecord = new paysheet();
                        
                        $payrecord->SN = $SN;
                        $payrecord->company_id = $company_id;
                        $payrecord->emp_id = $emp_id;
                        $payrecord->month = $request->month1;
                        $payrecord->year = $request->year1;
                        $payrecord->epf_no = $epfno;
                        $payrecord->name = $name;
                        $payrecord->designation = $designation;
                        $payrecord->nic = $nic;
                        $payrecord->doj = $DOJ;
                        $payrecord->department = $department;
                        $payrecord->noofdays_work = $days;
                        $payrecord->basic_salary = $basic;
                        $payrecord->allowance01 = $allowance01;
                        $payrecord->allowance02 = $allowance02;
                        $payrecord->allowance03 = $allowance03;
                        $payrecord->allowance04 = $allowance04;
                        $payrecord->allowance05 = $allowance05;
                        $payrecord->allowance06 = $allowance06;
                        $payrecord->allowance07 = $allowance07;
                        $payrecord->allowance08 = $allowance08;
                        $payrecord->allowance09 = $allowance09;
                        $payrecord->allowance10 = $allowance10;
                        $payrecord->performanceBonus = $performance;
                        $payrecord->commission = $commission;
                        $payrecord->OT = $ot;
                        $payrecord->leave_pay = $leave_pay;
                        $payrecord->arrears = $arrears;
                        $payrecord->adjustments = $adjustments;
                        $payrecord->noPay = $nopayAmount;
                        $payrecord->epf08 = number_format((float)$epfAmount, 2, '.', '');
                        $payrecord->paye = number_format((float)$payeAmount, 2, '.', '');
                        $payrecord->loan = $loan;
                        $payrecord->advance = $advance;
                        $payrecord->otherDeduction01 = $deduct01;
                        $payrecord->otherDeduction02 = $deduct02;
                        $payrecord->loan = $loan;
                        $payrecord->netPay = number_format((float)$net_sallary, 2, '.', '');
                        $payrecord->SD = $SD;
                        $payrecord->totalDeduction = number_format((float)$totalDeduction + $SD, 2, '.', '');
                        $payrecord->gross_remuneration = number_format((float)$gross_remuneration, 2, '.', '');
                        $payrecord->epf12 = number_format((float)$epf * 12 / 100, 2, '.', '');
                        $payrecord->etf = number_format((float)$epf * 3 / 100, 2, '.', '');
                        $payrecord->CTC = number_format((float)$net_sallary + ($epf * 12 / 100) + ($epf * 3 / 100), 2, '.', '');
                        $payrecord->save();
                    }
                    return redirect(route('attendances.index'))->with('success', 'Calculated');
                } else {
                    return redirect()->back()->with(['errors' => $validator->errors()->all()]);
                }
            }
        }
        
        public function filterAttendance (Request $request) {
            $filtered = false;
            $attendance = array();
            $employees = null;
            if (is_null($request->from) && empty($request->from) && is_null($request->to) && empty($request->to)) {
                alert()->error("Please select date range");
            } else {
                $filtered = true;
                $attendance = array();
                $company = session('company');
                if (!is_null($company)) {
                    $employees = company::findOrFail($company)->employees;
                    foreach ($employees as $key => $employee) {
                        $attendance[$key]['employee'] = $employee;
                        $attendance[$key]['attendance'] = Employee_Attendance::whereBetween('today', [$request->from, $request->to])->where('user_id', $employee->user_id)->get();
                    }
                } else
                    alert()->warning('Please select the company');
            }
            return view('payrole.attendance', compact('employees', 'attendance', 'filtered'));
        }
        
        public function getSampleAttendance () {
            if (Storage::disk('public')->exists('samples/attendance.csv')) {
                return Storage::download('public/samples/attendance.csv');
            }
        }
        
        public function getAttendanceReport () {
            $company_id = session('company');
            $frm = null;
            $too = null;
            $dep = null;
            $emp = null;
            $typ = null;
            $attendance = array();
            
            if (!is_null($company_id)) {
                $company = company::findOrFail($company_id);
                return view('reports.attendance', compact('company', 'attendance', 'frm', 'too', 'dep', 'emp', 'typ'));
            } else {
                alert()->warning('Please select the company');
                $company = null;
                return back();
            }
        }
        
        public function generateAttendanceReport (Request $request) {
            $frm = $request->from;
            $too = $request->to;
            $dep = $request->department;
            $emp = $request->employee;
            $typ = $request->type;
            
            $attendance = null;
            $company = null;
            $period = CarbonPeriod::create($frm . ' ' . '00:00:00', $too . ' ' . '23:59:59');
            $company_id = session('company');
            
            if (is_null($company_id)) {
                alert()->warning('Please select the company');
            } elseif (is_null($typ)) {
                alert()->warning('Please select the attendance type.');
            } elseif (is_null($request->from) && is_null($request->to)) {
                alert()->warning('Please select the date range');
            } else {
                $company = company::findOrFail($company_id);
                if ($request->type == 'a')
                    $attendance = $this->getAttendance($request, $company);
                if ($request->type == 'l')
                    $attendance = $this->getAttendance($request, $company);
                if ($request->type == 'e')
                    $attendance = $this->getAttendance($request, $company);
            }
            return view('reports.attendance', compact('company', 'period', 'attendance', 'frm', 'too', 'dep', 'emp', 'typ'));
        }
        
        public function getAttendance ($param, $company) {
            $attendance = array();
            $period = CarbonPeriod::create($param->from . ' ' . '00:00:00', $param->to . ' ' . '23:59:59');
            if (!is_null($param->department)) {
                $department = $company->departments->find($param->department);
                $attendance = $this->getAttendanceDetails($department->employees, $period);
            } elseif (!is_null($param->employee)) {
                $employee = Employee::findOrFail($param->employee);
                $attendance[0]['employee'] = $employee;
                foreach ($period as $keys => $date) {
                    $empAttendance = Employee_Attendance::where('today', $date->format('Y-m-d'))->where('user_id', $employee->user_id)->get()->toArray();
                    if (empty($empAttendance))
                        $empAttendance = [0 => [
                            "user_id" => $employee->user_id,
                            "uin_string" => "AB",
                            "uout_string" => "AB",
                            "today" => $date->format('Y-m-d')
                        ]
                        ];
                    foreach ($empAttendance as $empAttend) {
                        $attendance[0]['attendance'][$keys]['user_id'] = $empAttend['user_id'];
                        $attendance[0]['attendance'][$keys]['uin_string'] = $empAttend['uin_string'];
                        $attendance[0]['attendance'][$keys]['uout_string'] = $empAttend['uout_string'];
                        $attendance[0]['attendance'][$keys]['today'] = $empAttend['today'];
                    }
                }
            } else {
                $attendance = $this->getAttendanceDetails($company->employees, $period);
            }
            return $attendance;
        }
        
        public function getAttendanceDetails ($employees, $period) {
            foreach ($employees as $key => $employee) {
                $attendance[$key]['employee'] = $employee;
                foreach ($period as $keys => $date) {
                    $empAttendance = Employee_Attendance::where('today', $date->format('Y-m-d'))->where('user_id', $employee->user_id)->get()->toArray();
                    if (empty($empAttendance))
                        $empAttendance = [0 => [
                            "user_id" => $employee->user_id,
                            "uin_string" => "AB",
                            "uout_string" => "AB",
                            "today" => $date->format('Y-m-d')
                        ]
                        ];
                    foreach ($empAttendance as $empAttend) {
                        $attendance[$key]['attendance'][$keys]['user_id'] = $empAttend['user_id'];
                        $attendance[$key]['attendance'][$keys]['uin_string'] = $empAttend['uin_string'];
                        $attendance[$key]['attendance'][$keys]['uout_string'] = $empAttend['uout_string'];
                        $attendance[$key]['attendance'][$keys]['today'] = $empAttend['today'];
                    }
                }
            }
            return $attendance;
        }
        
    }
