<?php
    
    namespace App\Http\Controllers;
    
    use App\Allowance;
    use App\company;
    use App\Employee;
    use App\Employee_Loan;
    use App\Leave_balance;
    use App\paysheet;
    use App\Salary;
    use App\User;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Validator;
    use Importer;
    use Excel;
    use Illuminate\Http\Request;
    use RealRashid\SweetAlert\Facades\Alert;
    
    class paysheetController extends Controller {
        /**
         * paysheetController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index () {
            $currentMonth = date('F');
            $last_month = Date('F', strtotime($currentMonth . " last month"));
            $year = date('Y');
            $paysheets = paysheet::where('month', '=', Date('F', strtotime($currentMonth . " last month")))->where('year', '=', date("Y"))->where('company_id', session('company_id'))->get();
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            }
            return view('reports.paysheet', compact('paysheets', 'last_month', 'year', 'companies'));
        }
        
        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create () {
        
        }
        
        
        /**
         * @param $allowance
         * @param $employee
         * @param $year
         * @param $month
         * @param $workDays
         * @param $allow
         * @param bool $allowancePaye
         * @param $paye
         * @param bool $allowanceEpf
         * @param $epf
         * @return array
         */
        public function getAllowance ($allowance, $employee, $year, $month, $workDays, $allow, $allowancePaye = false,
                                      $paye,
                                      $allowanceEpf = false, $epf, $forFullMonth = false) {
            $allowanceArray = [];
            if ($employee->date_of_resign != NULL) {
                $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                $allowanceArray['ALLOWANCE'] = ($allowance / 30) * $dateDiff;
            } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month && !$forFullMonth) {
                $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                $allowanceArray['ALLOWANCE'] = ($allowance / 30) * $dateDiff;
            } else
                $allowanceArray['ALLOWANCE'] = $allowance;
            
            $allowanceArray['ALLOW'] = $allow + $allowanceArray['ALLOWANCE'];
            
            $allowanceArray['PAYE'] = ($allowancePaye == true) ? $paye + $allowanceArray['ALLOWANCE'] : 0;
            
            $allowanceArray['EPF'] = ($allowanceEpf == true) ? $epf + $allowanceArray['ALLOWANCE'] : 0;
            
            
            return $allowanceArray;
        }
        
        public function calculatePaye ($paye) {
            if ($paye < 100000) {
                $calc_paye = 0;
            } else if ($paye >= 100000 && $paye < 150000) {
                $calc_paye = $paye * (4 / 100) - 4000;
            } else if ($paye >= 150000 && $paye < 200000) {
                $calc_paye = $paye * (8 / 100) - 10000;
            } else if ($paye >= 200000 && $paye < 250000) {
                $calc_paye = $paye * (12 / 100) - 18000;
            } else if ($paye >= 250000 && $paye < 300000) {
                $calc_paye = $paye * (16 / 100) - 28000;
            } else if ($paye >= 300000 && $paye < 350000) {
                $calc_paye = $paye * (20 / 100) - 40000;
            } else {
                $calc_paye = $paye * (24 / 100) - 54000;
            }
            return $calc_paye;
        }
        
        public function getWorkDays ($month) {
            $workDays = [];
            switch ($month) {
                case("January"):
                    $workDays['WORKDAYS'] = 31;
                    $workDays['MONTH'] = '01';
                    break;
                case("February"):
                    $workDays['WORKDAYS'] = 28;
                    $workDays['MONTH'] = '02';
                    break;
                case("March"):
                    $workDays['WORKDAYS'] = 31;
                    $workDays['MONTH'] = '03';
                    break;
                case("April"):
                    $workDays['MONTH'] = '04';
                    $workDays['WORKDAYS'] = 30;
                    break;
                case("May"):
                    $workDays['MONTH'] = '05';
                    $workDays['WORKDAYS'] = 31;
                    break;
                case("June"):
                    $workDays['MONTH'] = '06';
                    $workDays['WORKDAYS'] = 30;
                    break;
                case("July"):
                    $workDays['MONTH'] = '07';
                    $workDays['WORKDAYS'] = 31;
                    break;
                case("August"):
                    $workDays['MONTH'] = '08';
                    $workDays['WORKDAYS'] = 31;
                    break;
                case("September"):
                    $workDays['MONTH'] = '09';
                    $workDays['WORKDAYS'] = 30;
                    break;
                case("October"):
                    $workDays['MONTH'] = '10';
                    $workDays['WORKDAYS'] = 31;
                    break;
                case("November"):
                    $workDays['MONTH'] = '11';
                    $workDays['WORKDAYS'] = 30;
                    break;
                case("December"):
                    $workDays['MONTH'] = '12';
                    $workDays['WORKDAYS'] = 31;
                    break;
            }
            return $workDays;
        }
        
        
        /**
         * Store a newly created resource in storage.
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store (Request $request) {
            $this->validate($request, [
                'year1' => 'required|not_in:0',
                'month1' => 'required|not_in:0'
            ]);
            
            $paysheet = paysheet::where([['company_id', session('company_id')], ['month', Carbon::parse($request->month1)->format('m')], ['year', $request->year1]])->count();
            if ($paysheet != 0) {
                alert()->warning('Warning', 'All ready calculated');
                return redirect(route('salary.index'))->with('warning', 'All ready calculated');
            } else if ($request->year1 > date("Y")) {
                alert()->warning('Warning', 'Incorrect year');
                return redirect(route('salary.index'))->with('warning', 'Incorrect year');
//        }else if(($request->year1 == date("Y")) && (date('n', strtotime(date('F')))<= date('n', strtotime($request->month1)))) {
//            return redirect(route('salary.index'))->with('warning', 'Incorrect month');
            } else {
                $company_id = session('company_id');
                $company = company::findOrFail(session('company'));
                $salaries = $company->salaries;
                
                $workDaysArray = $this->getWorkDays($request->month1);
                $month = $workDaysArray['MONTH'];
                $workDays = $workDaysArray['WORKDAYS'];
                
                
                foreach ($salaries as $key => $salary) {
                    $emp_id = $salary->employee_id;
                    
                    $employee = Employee::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->first();
                    $DOR = null;
                    if (!is_null($employee)) {
                        if (Carbon::parse($employee->date_of_join) <= Carbon::parse($request->year1 . '-' . $month . '-' .
                                $workDays)) {
                            if ($employee->date_of_resign != NULL) {
                                if ((Carbon::parse($employee->date_of_resign)->format('Y/m')) >= (Carbon::parse
                                    ($request->year1 . '-' . $month . '-' . $workDays)->format('Y/m'))) {
                                    $this->createPaysheet($employee, $request->year1, $month, $salary, $workDays, $key);
                                }
                            } else {
                                $this->createPaysheet($employee, $request->year1, $month, $salary, $workDays, $key);
                            }
                        }
                    }
                }
                alert()->success('Success', 'Generate Paysheet Process');
                return redirect(route('salary.index'))->with('success', 'Calculated');
            }
        }
        
        
        /**
         * @param $employee
         * @param $year
         * @param $month
         * @param $salary
         * @param $workDays
         * @param $serianNumber
         */
        public function createPaysheet ($employee, $year, $month, $salary, $workDays, $serianNumber) {
            if ($employee->date_of_resign != NULL) {
                $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                $basic = ($salary->basic_salary / 30) * $dateDiff;
            } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                $basic = ($salary->basic_salary / 30) * ($dateDiff + 1);
            } else {
                $basic = $salary->basic_salary;
            }
            
            $allowance01 = 0;
            $allowance02 = 0;
            $allowance03 = 0;
            $allowance04 = 0;
            $allowance05 = 0;
            $allowance06 = 0;
            $allowance07 = 0;
            $allowance08 = 0;
            $allowance09 = 0;
            $allowance10 = 0;
            
            $performance = 0;
            $commission = 0;
            $nopayAmount = 0;
            $ot = 0;
            $leave_pay = 0;
            $arrears = 0;
            $adjustments = 0;
            $paye = 0;
            $epf = 0;
            $allow = 0;
            $advance = 0;
            $deduct01 = 0;
            $deduct02 = 0;
            
            
            if ($salary->basic_PAYE == 1) {
                $paye = $paye + $basic;
            } else {
                $payeAmount = 0;
            }
            if ($salary->basic_EPF == 1) {
                $epf = $epf + $basic;
            } else {
                $epfAmount = 0;
            }
            
            $allowance = Allowance::where('company_id', session('company_id'))->first();
            
            // if ($allowance->allowance01 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance01, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance01Paye, $paye, $allowance->allowance01Epf, $epf);
//
            //     $allowance01 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance02 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance02, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance02Paye, $paye, $allowance->allowance02Epf, $epf);
            
            //     $allowance02 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance03 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance03, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance03Paye, $paye, $allowance->allowance03Epf, $epf);
            
            //     $allowance03 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance04 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance04, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance04Paye, $paye, $allowance->allowance04Epf, $epf);
            
            //     $allowance04 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance05 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance05, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance05Paye, $paye, $allowance->allowance05Epf, $epf);
            
            //     $allowance05 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance06 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance06, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance06Paye, $paye, $allowance->allowance06Epf, $epf, true);
            
            //     $allowance06 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance07 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance07, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance07Paye, $paye, $allowance->allowance07Epf, $epf, true);
            
            //     $allowance07 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance08 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance09, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance09Paye, $paye, $allowance->allowance09Epf, $epf, true);
            
            //     $allowance09 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance09 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance09, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance09Paye, $paye, $allowance->allowance09Epf, $epf, true);
            
            //     $allowance09 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            // if ($allowance->allowance10 != null) {
            //     $allowances = $this->getAllowance($salary->$allowance10, $employee, $year, $month,
            //         $workDays, $allow, $allowance->allowance10Paye, $paye, $allowance->allowance10Epf, $epf, true);
            
            //     $allowance10 = $allowances['ALLOWANCE'];
            //     $allow = $allowances['ALLOW'];
            //     $paye = $allowances['PAYE'];
            //     $epf = $allowances['EPF'];
            // }
            
            if ($allowance->allowance01 != null) {
                
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow01 = ($salary->allowance01 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow01 = ($salary->allowance01 / 30) * $dateDiff;
                } else {
                    $allow01 = $salary->allowance01;
                }
                
                $allowance01 = $allow01;
                $allow = $allow + $allow01;
                
                
                if ($allowance->allowance01Paye == true) {
                    $paye = $paye + $allow01;
                }
                if ($allowance->allowance01Epf == true) {
                    $epf = $epf + $allow01;
                }
            } else{
                $allowance01 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance02 != null) {
                
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow02 = ($salary->allowance02 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow02 = ($salary->allowance02 / 30) * $dateDiff;
                } else {
                    $allow02 = $salary->allowance02;
                }
                
                $allowance02 = $allow02;
                $allow = $allow + $allow02;
                
                if ($allowance->allowance02Paye == 1) {
                    $paye = $paye + $allow02;
                }
                if ($allowance->allowance02Epf == 1) {
                    $epf = $epf + $allow02;
                }
            }else{
                $allowance02 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance03 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow03 = ($salary->allowance03 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow03 = ($salary->allowance03 / 30) * $dateDiff;
                } else {
                    $allow03 = $salary->allowance03;
                }
                
                $allowance03 = $allow03;
                $allow = $allow + $allow03;
                
                if ($allowance->allowance03Paye == 1) {
                    $paye = $paye + $allow03;
                }
                if ($allowance->allowance03Epf == 1) {
                    $epf = $epf + $allow03;
                }
            }else{
                $allowance03 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance04 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow04 = ($salary->allowance04 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow04 = ($salary->allowance04 / 30) * $dateDiff;
                } else {
                    $allow04 = $salary->allowance04;
                }
                
                $allowance04 = $allow04;
                $allow = $allow + $allow04;
                
                if ($allowance->allowance04Paye == 1) {
                    $paye = $paye + $allow04;
                }
                if ($allowance->allowance04Epf == 1) {
                    $epf = $epf + $allow04;
                }
            }else{
                $allowance04 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance05 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow05 = ($salary->allowance05 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow05 = ($salary->allowance05 / 30) * $dateDiff;
                } else {
                    $allow05 = $salary->allowance05;
                }
                
                $allowance05 = $allow05;
                $allow = $allow + $allow05;
                
                if ($allowance->allowance05Paye == 1) {
                    $paye = $paye + $allow05;
                }
                if ($allowance->allowance05Epf == 1) {
                    $epf = $epf + $allow05;
                }
            }else{
                $allowance05 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance06 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow06 = ($salary->allowance06 / 30) * $dateDiff;
                } else {
                    $allow06 = $salary->allowance06;
                }
                
                $allowance06 = $allow06;
                $allow = $allow + $allow06;
                
                if ($allowance->allowance06Paye == 1) {
                    $paye = $paye + $allow06;
                }
                if ($allowance->allowance06Epf == 1) {
                    $epf = $epf + $allow06;
                }
            }else{
                $allowance06 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance07 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow07 = ($salary->allowance07 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow07 = ($salary->allowance07 / 30) * $dateDiff;
                } else {
                    $allow07 = $salary->allowance07;
                }
                
                $allowance07 = $allow07;
                $allow = $allow + $allow07;
                
                if ($allowance->allowance07Paye == 1) {
                    $paye = $paye + $allow07;
                }
                if ($allowance->allowance07Epf == 1) {
                    $epf = $epf + $allow07;
                }
            }else{
                $allowance07 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance08 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow08 = ($salary->allowance08 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow08 = ($salary->allowance08 / 30) * $dateDiff;
                } else {
                    $allow08 = $salary->allowance08;
                }
                
                $allowance08 = $allow08;
                $allow = $allow + $allow08;
                
                if ($allowance->allowance08Paye == 1) {
                    $paye = $paye + $allow08;
                }
                if ($allowance->allowance08Epf == 1) {
                    $epf = $epf + $allow08;
                }
            }else{
                $allowance08 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance09 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow09 = ($salary->allowance09 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow09 = ($salary->allowance09 / 30) * $dateDiff;
                } else {
                    $allow09 = $salary->allowance09;
                }
                
                $allowance09 = $allow09;
                $allow = $allow + $allow09;
                
                if ($allowance->allowance09Paye == 1) {
                    $paye = $paye + $allow09;
                }
                if ($allowance->allowance09Epf == 1) {
                    $epf = $epf + $allow09;
                }
            }else{
                $allowance09 = 0.00;
                $allow = $allow + 0.00;
            }
            if ($allowance->allowance10 != null) {
                if ($employee->date_of_resign != NULL) {
                    $dateDiff = Carbon::parse($year . '-' . $month . '-01')->diffInDays($employee->date_of_resign) + 1;
                    $allow10 = ($salary->allowance10 / 30) * $dateDiff;
                } elseif (Carbon::parse($employee->date_of_join)->Format('m') == $month) {
                    $dateDiff = $workDays - Carbon::parse($employee->date_of_join)->Format('d');
                    $allow10 = ($salary->allowance10 / 30) * $dateDiff;
                } else {
                    $allow10 = $salary->allowance10;
                }
                
                $allowance10 = $allow10;
                $allow10 = $allow + $allow10;
                
                if ($allowance->allowance10Paye == 1) {
                    $paye = $paye + $allow10;
                }
                if ($allowance->allowance10Epf == 1) {
                    $epf = $epf + $allow10;
                }
            }else{
                $allowance10 = 0.00;
                $allow = $allow + 0.00;
            }
            
            if ($salary->basic_EPF == 0) {
                $epf = 0;
            }
            $payeAmount = $this->calculatePaye($paye);
            $epfAmount = $epf * 8 / 100;
            $calc_loans = 0;
            $date = $year . '-' . $month . '-01';
            
            $loans = Employee_Loan::where('company_id', '=', $employee->company_id)->where('user_id', '=',
                $employee->user_id)->where
            ('loan_status',
                '=', 'pending')->where('start_date', '<', date($date))->get();
            foreach ($loans as $loan) {
                
                
                $calc_loans = $calc_loans + $loan->monthly_installment;
                $newLoan = Employee_Loan::findOrFail($loan->id);
                $newLoan->payed_installments = $newLoan->payed_installments + 1;
                if ($newLoan->no_installments == $newLoan->payed_installments) {
                    $newLoan->loan_status = "completed";
                }
                
                $newLoan->update();
            }
            
            $loan = $calc_loans;
            $totalDeduction = $epfAmount + $payeAmount + $advance + $loan + $deduct01 + $deduct02 + $nopayAmount;
            $gross_remuneration = $allowance01 + $allowance02 + $allowance03 + $allowance04 + $allowance05 + $allowance06 + $allowance07 + $allowance08 + $allowance09 + $allowance10 + $basic + $performance + $commission + $ot + $leave_pay + $arrears + $adjustments;
            
            $net_sallary = $gross_remuneration - $totalDeduction;
            $SD = 0;

//        $stampDuty = company::where('regno', '=', $employee->company_id)->value('stamp_duty');
//                        if($stampDuty == 'on'){
            if ($net_sallary > 25000) {
                $net_sallary = $net_sallary - 25;
                $SD += 25;
            }
//                        }
            $payrecord = new paysheet();
            
            $payrecord->SN = $serianNumber;
            $payrecord->company_id = $employee->company_id;
            $payrecord->emp_id = $employee->user_id;
            $payrecord->month = $month;
            $payrecord->year = $year;
            $payrecord->epf_no = $employee->epf_no;
            $payrecord->name = $employee->fname . " " . $employee->mname . " " . $employee->lname;;
            $payrecord->designation = $employee->designation;
            $payrecord->nic = $employee->nic;
            $payrecord->doj = $employee->date_of_join;
            $payrecord->department = $employee->department;
            $payrecord->noofdays_work = $workDays;
            $payrecord->basic_salary = $basic;
            $payrecord->allowance01 = (!is_null($allowance01))? $allowance01: 0.00;
            $payrecord->allowance02 = (!is_null($allowance02))? $allowance02: 0.00;
            $payrecord->allowance03 = (!is_null($allowance03))? $allowance03: 0.00;
            $payrecord->allowance04 = (!is_null($allowance04))? $allowance04: 0.00;
            $payrecord->allowance05 = (!is_null($allowance05))? $allowance05: 0.00;
            $payrecord->allowance06 = (!is_null($allowance06))? $allowance06: 0.00;
            $payrecord->allowance07 = (!is_null($allowance07))? $allowance07: 0.00;
            $payrecord->allowance08 = (!is_null($allowance08))? $allowance08: 0.00;
            $payrecord->allowance09 = (!is_null($allowance09))? $allowance09: 0.00;
            $payrecord->allowance10 = (!is_null($allowance10))? $allowance10: 0.00;
            $payrecord->performanceBonus = $performance;
            $payrecord->commission = $commission;
            $payrecord->OT = $ot;
            $payrecord->leave_pay = $leave_pay;
            $payrecord->arrears = $arrears;
            $payrecord->adjustments = $adjustments;
            $payrecord->noPay = $nopayAmount;
            $payrecord->epf08 = number_format((float)$epfAmount, 2, '.', '');
            $payrecord->paye = number_format((float)$payeAmount, 2, '.', '');
            $payrecord->advance = $advance;
            $payrecord->otherDeduction01 = $deduct01;
            $payrecord->otherDeduction02 = $deduct02;
            $payrecord->loan = $loan;
            $payrecord->netPay = number_format((float)$net_sallary, 2, '.', '');
            $payrecord->SD = $SD;
            $payrecord->totalDeduction = number_format((float)$totalDeduction + $SD, 2, '.', '');
            $payrecord->gross_remuneration = number_format((float)$gross_remuneration, 2, '.', '');
            $payrecord->epf12 = number_format((float)$epf * 12 / 100, 2, '.', '');
            $payrecord->etf = number_format((float)$epf * 3 / 100, 2, '.', '');
            $payrecord->CTC = number_format((float)$net_sallary + ($epf * 12 / 100) + ($epf * 3 / 100), 2, '.', '');
            $payrecord->save();
        }
        
        
        /**
         * Display the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function show ($id) {
        
        }
        
        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function edit ($id) {
            $paysheet = paysheet::findOrFail($id);
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            }
            $allowance = Allowance::where('company_id', session('company_id'))->first();
            
            return view('payrole.edit', compact('companies', 'paysheet', 'allowance'));
        }
        
        /**
         * Update the specified resource in storage.
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function update (Request $request, $id) {
            
            $payrecord = paysheet::findOrFail($id);
            
            $payrecord->noofdays_work = $request->noofdays_work;
            
            $payrecord->basic_salary = $request->basic_salary;
            
            $allowance = Allowance::where('company_id', '=', $request->company_id)->first();
            
            
            if ($allowance->allowance01 != null) {
                $payrecord->allowance01 = $request->allowance01;
            }
            if ($allowance->allowance02 != null) {
                $payrecord->allowance02 = $request->allowance02;
            }
            if ($allowance->allowance03 != null) {
                $payrecord->allowance03 = $request->allowance03;
            }
            if ($allowance->allowance04 != null) {
                $payrecord->allowance04 = $request->allowance04;
            }
            if ($allowance->allowance05 != null) {
                $payrecord->allowance05 = $request->allowance05;
            }
            if ($allowance->allowance06 != null) {
                $payrecord->allowance06 = $request->allowance06;
            }
            if ($allowance->allowance07 != null) {
                $payrecord->allowance07 = $request->allowance07;
            }
            if ($allowance->allowance08 != null) {
                $payrecord->allowance08 = $request->allowance08;
            }
            if ($allowance->allowance09 != null) {
                $payrecord->allowance09 = $request->allowance09;
            }
            if ($allowance->allowance10 != null) {
                $payrecord->allowance10 = $request->allowance10;
            }
            
            $payrecord->performanceBonus = $request->performanceBonus;
            
            $payrecord->commission = $request->commission;
            
            $payrecord->OT = $request->OT;
            
            $payrecord->leave_pay = $request->leave_pay;
            
            $payrecord->arrears = $request->arrears;
            
            $payrecord->adjustments = $request->adjustments;
            
            $payrecord->noPay = $request->noPay;
            
            $payrecord->epf08 = $request->epf08;
            
            $payrecord->paye = $request->paye;
            
            $payrecord->loan = $request->loan;
            
            $payrecord->advance = $request->advance;
            
            $payrecord->otherDeduction01 = $request->otherDeduction01;
            
            $payrecord->otherDeduction02 = $request->otherDeduction02;
            
            $payrecord->loan = $request->loan;
            
            $payrecord->netPay = $request->netPay;
            
            $payrecord->SD = $request->SD;
            
            $payrecord->totalDeduction = $request->totalDeduction;
            
            $payrecord->gross_remuneration = $request->gross_remuneration;
            
            $payrecord->epf12 = $request->epf12;
            
            $payrecord->etf = $request->etf;
            
            $payrecord->gratuity = $request->gratuity;
            
            $payrecord->CTC = $request->CTC;
            
            $payrecord->update();
            
            return redirect(route('payroll.approval'))->with('success', 'Updated');
            
        }
        
        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function destroy ($id) {
            //
        }
        
        
        public
        function print_sheet (Request $request) {
            
            $this->validate($request, [
                'month' => 'required',
                'year' => 'required'
            ]);
            $mon = $request->month;
            $month = Carbon::parse($request->month)->month;
            $count = paysheet::where('month', '=', $month)->where('year', '=',
                $request->year)
                ->where
                ('company_id', session('company_id'))->where('approvalStatus', '=', true)->count();
            
            if ($count == 0) {
                return redirect(route('paysheet.index'))->with('warning', 'No record found');
            }
            
            $allowance = Allowance::where('company_id', session('company_id'))->first();
            $paysheets = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('company_id', '=',
                company::find(session('company'))->regno)->where('approvalStatus', '=', true)->get();
            $comapany = company::where('regno', session('company_id'))->first();
            $year = $request->year;
            $filename = 'Paysheet_' . $year . '_' . $month;
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            }
            return view('reports.paysheet_report', compact('mon', 'companies', 'paysheets', 'allowance', 'comapany', 'month', 'year', 'filename'));
        }
        
        
        public
        function payrole_with_file (Request $request) {
            $this->validate($request, [
                'year' => 'required|not_in:0',
                'month' => 'required|not_in:0',
                'file' => 'required',
            ]);
            
            $paysheet = paysheet::where('company_id', session('company_id'))->where('month', '=', $request->month)->where('year', '=', $request->year)->count();
            
            if ($paysheet != 0) {
                
                return redirect(route('salary.index'))->with('warning', 'All ready calculated');
                
            } else if ($request->year > date("Y")) {
                return redirect(route('salary.index'))->with('warning', 'Incorrect year');
                
            } else if ($request->year == date("Y") && date('n', strtotime(date('F'))) <= date('n', strtotime($request->month))) {
                
                return redirect(route('salary.index'))->with('warning', 'Incorrect month');
            } else {
                $validator = Validator::make($request->all(), [
                    'file' => 'required|max:5000|mimes:xlsx,xls,csv'
                ]);
                
                if ($validator->passes()) {
                    $dataTime = date('Ymd_His');
                    $file = $request->file('file');
                    $filename = $dataTime . '-' . $file->getClientOriginalName();
                    $savePath = public_path('/upload/');
                    $file->move($savePath, $filename);
                    
                    
                    $excel = Importer::make('Excel');
                    $excel->load($savePath . $filename);
                    $collection = $excel->getCollection();
                    
                    //            dd($collection);
                    
                    $company_id = auth()->user()->company_id;
                    $salaries = Salary::where('company_id', '=', $company_id)->get();
                    $SN = 1;
                    
                    foreach ($salaries as $salary) {
                        $emp_id = $salary->employee_id;
                        $employee = Employee::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->first();
                        
                        switch ($request->month) {
                            case("January"):
                                $workDays = 31;
                                $month = '01';
                                break;
                            case("February"):
                                $workDays = 28;
                                $month = '02';
                                break;
                            case("March"):
                                $workDays = 31;
                                $month = '03';
                                break;
                            case("April"):
                                $month = '04';
                                $workDays = 30;
                                break;
                            case("May"):
                                $month = '05';
                                $workDays = 31;
                                break;
                            case("June"):
                                $month = '06';
                                $workDays = 30;
                                break;
                            case("July"):
                                $month = '07';
                                $workDays = 31;
                                break;
                            case("August"):
                                $month = '08';
                                $workDays = 31;
                                break;
                            case("September"):
                                $month = '09';
                                $workDays = 30;
                                break;
                            case("October"):
                                $month = '10';
                                $workDays = 31;
                                break;
                            case("November"):
                                $month = '11';
                                $workDays = 30;
                                break;
                            case("December"):
                                $month = '12';
                                $workDays = 31;
                                break;
                        }
                        
                        $basic = $salary->basic_salary;
                        $allowance01 = 0;
                        $allowance02 = 0;
                        $allowance03 = 0;
                        $allowance04 = 0;
                        $allowance05 = 0;
                        $allowance06 = 0;
                        $allowance07 = 0;
                        $allowance08 = 0;
                        $allowance09 = 0;
                        $allowance10 = 0;
                        $performance = 0;
                        $commission = 0;
                        $ot = 0;
                        $leave_pay = 0;
                        $arrears = 0;
                        $adjustments = 0;
                        $nopayAmount = 0;
                        $advance = 0;
                        $deduct01 = 0;
                        $deduct02 = 0;
                        
                        
                        $paye = 0;
                        $epf = 0;
                        $allow = 0;
                        
                        
                        for ($row = 1; $row < sizeof($collection); $row++) {
                            
                            if ($collection[$row][0] == $emp_id) {
                                $performance = $collection[$row][1];
                                $commission = $collection[$row][2];
                                $ot = $collection[$row][3];
                                $leave_pay = $collection[$row][4];
                                $arrears = $collection[$row][5];
                                $adjustments = $collection[$row][6];
                                $nopayAmount = $collection[$row][7] * ($basic / $workDays);

//                            $leaveBalance = Leave_balance::where("company_id",'=',$company_id)->where('user_id','=',$emp_id)->where('leave_type','=',$leaveType)->first();
//
//                            $leaveBalance->used = $leaveBalance->used + $collection[$row][7];
//
//                            $leaveBalance->update();
                                $advance = $collection[$row][8];
                                $deduct01 = $collection[$row][9];
                                $deduct02 = $collection[$row][10];
                                break;
                            }
                        }
                        
                        $name = $employee->name_with_initials;
                        $designation = $employee->designation;
                        $epfno = $employee->epf_no;
                        $nic = $employee->nic;
                        $DOJ = $employee->date_of_join;
                        $DOR = null;
                        $department = $employee->department;
                        
                        if ($salary->basic_PAYE == 1) {
                            $paye = $paye + $basic;
                        } else {
                            $payeAmount = 0;
                        }
                        if ($salary->basic_EPF == 1) {
                            $epf = $epf + $basic;
                        } else {
                            $epfAmount = 0;
                        }
                        
                        $allowance = Allowance::where('company_id', session('company_id'))->first();
                        
                        if ($allowance->allowance01 != null) {
                            $allow01 = $salary->allowance01;
                            $allowance01 = $salary->allowance01;
                            $allow = $allow + $allow01;
                            
                            if ($allowance->allowance01Paye == true) {
                                $paye = $paye + $allow01;
                            }
                            if ($allowance->allowance01Epf == true) {
                                $epf = $epf + $allow01;
                            }
                        } else {
                            $allowance01 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance02 != null) {
                            $allow02 = $salary->allowance02;
                            $allowance02 = $salary->allowance02;
                            $allow = $allow + $allow02;
                            
                            if ($allowance->allowance02Paye == 1) {
                                $paye = $paye + $allow02;
                            }
                            if ($allowance->allowance02Epf == 1) {
                                $epf = $epf + $allow02;
                            }
                        } else {
                            $allowance02 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance03 != null) {
                            $allow03 = $salary->allowance03;
                            $allowance03 = $salary->allowance03;
                            $allow = $allow + $allow03;
                            
                            if ($allowance->allowance03Paye == 1) {
                                $paye = $paye + $allow03;
                            }
                            if ($allowance->allowance03Epf == 1) {
                                $epf = $epf + $allow03;
                            }
                        } else {
                            $allowance03 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance04 != null) {
                            $allow04 = $salary->allowance04;
                            $allowance04 = $salary->allowance04;
                            $allow = $allow + $allow04;
                            
                            if ($allowance->allowance04Paye == 1) {
                                $paye = $paye + $allow04;
                            }
                            if ($allowance->allowance04Epf == 1) {
                                $epf = $epf + $allow04;
                            }
                        } else {
                            $allowance04 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance05 != null) {
                            $allow05 = $salary->allowance05;
                            $allowance05 = $salary->allowance05;
                            $allow = $allow + $allow05;
                            
                            if ($allowance->allowance05Paye == 1) {
                                $paye = $paye + $allow05;
                            }
                            if ($allowance->allowance05Epf == 1) {
                                $epf = $epf + $allow05;
                            }
                        } else {
                            $allowance05 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance06 != null) {
                            $allow06 = $salary->allowance06;
                            $allowance06 = $salary->allowance06;
                            $allow = $allow + $allow06;
                            
                            if ($allowance->allowance06Paye == 1) {
                                $paye = $paye + $allow06;
                            }
                            if ($allowance->allowance06Epf == 1) {
                                $epf = $epf + $allow06;
                            }
                        } else {
                            $allowance06 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance07 != null) {
                            $allow07 = $salary->allowance07;
                            $allowance07 = $salary->allowance07;
                            $allow = $allow + $allow07;
                            
                            if ($allowance->allowance07Paye == 1) {
                                $paye = $paye + $allow07;
                            }
                            if ($allowance->allowance07Epf == 1) {
                                $epf = $epf + $allow07;
                            }
                        } else {
                            $allowance07 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance08 != null) {
                            $allow08 = $salary->allowance08;
                            $allowance08 = $salary->allowance08;
                            $allow = $allow + $allow08;
                            
                            if ($allowance->allowance08Paye == 1) {
                                $paye = $paye + $allow08;
                            }
                            if ($allowance->allowance08Epf == 1) {
                                $epf = $epf + $allow08;
                            }
                        } else {
                            $allowance08 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance09 != null) {
                            $allow09 = $salary->allowance09;
                            $allowance09 = $salary->allowance09;
                            $allow = $allow + $allow09;
                            
                            if ($allowance->allowance09Paye == 1) {
                                $paye = $paye + $allow09;
                            }
                            if ($allowance->allowance09Epf == 1) {
                                $epf = $epf + $allow09;
                            }
                        } else {
                            $allowance09 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        if ($allowance->allowance10 != null) {
                            $allow10 = $salary->allowance10;
                            $allowance10 = $salary->allowance10;
                            $allow = $allow + $allow10;
                            
                            if ($allowance->allowance10Paye == 1) {
                                $paye = $paye + $allow10;
                            }
                            if ($allowance->allowance10Epf == 1) {
                                $epf = $epf + $allow10;
                            }
                        } else {
                            $allowance10 = 0.00;
                            $allow = $allow + 0.00;
                        }
                        
                        if ($paye < 100000) {
                            $calc_paye = 0;
                        } else if ($paye >= 100000 && $paye < 150000) {
                            $calc_paye = $paye * (4 / 100) - 4000;
                        } else if ($paye >= 150000 && $paye < 200000) {
                            $calc_paye = $paye * (8 / 100) - 10000;
                        } else if ($paye >= 200000 && $paye < 250000) {
                            $calc_paye = $paye * (12 / 100) - 18000;
                        } else if ($paye >= 250000 && $paye < 300000) {
                            $calc_paye = $paye * (16 / 100) - 28000;
                        } else if ($paye >= 300000 && $paye < 350000) {
                            $calc_paye = $paye * (20 / 100) - 40000;
                        } else {
                            $calc_paye = $paye * (24 / 100) - 54000;
                        }
                        if ($salary->basic_EPF == 0) {
                            $epf = 0;
                        }
                        
                        $payeAmount = $calc_paye;
                        $calc_epf = $epf * 8 / 100;
                        $epfAmount = $calc_epf;
                        $calc_loans = 0;
                        $date = $request->year . '-' . $month . '-01';
                        $loans = Employee_Loan::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->where('loan_status', '=', 'pending')->where('start_date', '<', date($date))->get();
                        foreach ($loans as $loan) {
                            
                            
                            $calc_loans = $calc_loans + $loan->monthly_installment;
                            
                            $newLoan = Employee_Loan::findOrFail($loan->id);
                            
                            $newLoan->payed_installments = $newLoan->payed_installments + 1;
                            
                            
                            if ($newLoan->no_installments == $newLoan->payed_installments) {
                                
                                $newLoan->loan_status = "completed";
                            }
                            
                            $newLoan->update();
                        }
                        $loan = $calc_loans;
                        $totalDeduction = $epfAmount + $payeAmount + $advance + $loan + $deduct01 + $deduct02 + $nopayAmount;
                        $gross_remuneration = $allowance01 + $allowance02 + $allowance03 + $allowance04 + $allowance05 + $allowance06 + $allowance07 + $allowance08 + $allowance09 + $allowance10 + $basic + $performance + $commission + $ot + $leave_pay + $arrears + $adjustments;
                        $net_sallary = $gross_remuneration - $totalDeduction;
                        $SD = 0;
                        
                        $stampDuty = company::where('regno', '=', $company_id)->value('stamp_duty');
                        if ($stampDuty == 'on') {
                            if ($net_sallary > 25000) {
                                $net_sallary = $net_sallary - 25;
                                $SD += 25;
                            }
                        }
                        $payrecord = new paysheet();
                        
                        $payrecord->SN = $SN;
                        $payrecord->company_id = $company_id;
                        $payrecord->emp_id = $emp_id;
                        $payrecord->month = $request->month;
                        $payrecord->year = $request->year;
                        $payrecord->epf_no = $epfno;
                        $payrecord->name = $name;
                        $payrecord->designation = $designation;
                        $payrecord->nic = $nic;
                        $payrecord->doj = $DOJ;
                        $payrecord->department = $department;
                        $payrecord->noofdays_work = $workDays;
                        $payrecord->basic_salary = $basic;
                        $payrecord->allowance01 = (!is_null($allowance01)) ? $allowance01 : 0.00;
                        $payrecord->allowance02 = (!is_null($allowance02)) ? $allowance02 : 0.00;
                        $payrecord->allowance03 = (!is_null($allowance03)) ? $allowance03 : 0.00;
                        $payrecord->allowance04 = (!is_null($allowance04)) ? $allowance04 : 0.00;
                        $payrecord->allowance05 = (!is_null($allowance05)) ? $allowance05 : 0.00;
                        $payrecord->allowance06 = (!is_null($allowance06)) ? $allowance06 : 0.00;
                        $payrecord->allowance07 = (!is_null($allowance07)) ? $allowance07 : 0.00;
                        $payrecord->allowance08 = (!is_null($allowance08)) ? $allowance08 : 0.00;
                        $payrecord->allowance09 = (!is_null($allowance09)) ? $allowance09 : 0.00;
                        $payrecord->allowance10 = (!is_null($allowance10)) ? $allowance10 : 0.00;
                        $payrecord->performanceBonus = $performance;
                        $payrecord->commission = $commission;
                        $payrecord->OT = $ot;
                        $payrecord->leave_pay = $leave_pay;
                        $payrecord->arrears = $arrears;
                        $payrecord->adjustments = $adjustments;
                        $payrecord->noPay = $nopayAmount;
                        $payrecord->epf08 = number_format((float)$epfAmount, 2, '.', '');
                        $payrecord->paye = number_format((float)$payeAmount, 2, '.', '');
                        $payrecord->loan = $loan;
                        $payrecord->advance = $advance;
                        $payrecord->otherDeduction01 = $deduct01;
                        $payrecord->otherDeduction02 = $deduct02;
                        $payrecord->loan = $loan;
                        $payrecord->netPay = number_format((float)$net_sallary, 2, '.', '');
                        $payrecord->SD = $SD;
                        $payrecord->totalDeduction = number_format((float)$totalDeduction + $SD, 2, '.', '');
                        $payrecord->gross_remuneration = number_format((float)$gross_remuneration, 2, '.', '');
                        $payrecord->epf12 = number_format((float)$epf * 12 / 100, 2, '.', '');
                        $payrecord->etf = number_format((float)$epf * 3 / 100, 2, '.', '');
                        $payrecord->CTC = number_format((float)$net_sallary + ($epf * 12 / 100) + ($epf * 3 / 100), 2, '.', '');
                        $payrecord->save();
                    }
                    return redirect(route('salary.index'))->with('success', 'Calculated');
                } else {
                    return redirect()->back()->with(['errors' => $validator->errors()->all()]);
                }
            }
        }
        
        /**
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function paysheetApproveAll (Request $request) {
            for ($i = 0; $i < sizeof($request->paysheet_id); $i++) {
                $check = 'check_' . $i;
                if (isset($request->$check)) {
                    $this->ApprovePaysheet($request->paysheet_id[$i]);
                }
            }
    
            $paysheets = paysheet::where('company_id', session('company_id'))->where('approvalStatus', '=', 0)
                ->get();
            return view('payrole.payrole_approval', compact('paysheets'));
        }
        
        /**
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function paysheetCancelAll (Request $request) {
            for ($i = 0; $i < sizeof($request->paysheet_id); $i++) {
                $check = 'check_' . $i;
                if (isset($request->$check)) {
                    $payrecord = paysheet::findOrFail($request->paysheet_id[$i]);
                    $payrecord->forceDelete();
                }
            }
    
            $paysheets = paysheet::where('company_id', session('company_id'))->where('approvalStatus', '=', 0)
                ->get();
            return view('payrole.payrole_approval', compact('paysheets'));
        }
        
        /**
         * @param $param
         * @return bool
         */
        public function ApprovePaysheet ($param) {
            $payrecord = paysheet::findOrFail($param);
            $payrecord->approvalStatus = 1;
            $payrecord->approvedOn = date('Y-m-d');
            $payrecord->update();
            return true;
        }
        
        /**
         * @param $id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function paysheetApprove ($id) {
            $this->ApprovePaysheet($id);
            alert()->success('Paysheet record Approved.');
            return redirect(route('payroll.approval'))->with('success', 'Paysheet record Approved');
        }
        
        /**
         * @param $id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function paysheetCancel ($id) {
            $payrecord = paysheet::findOrFail($id);
            $payrecord->forceDelete();
            alert()->success('Paysheet record canceled.');
            return redirect(route('payroll.approval'))->with('success', 'Paysheet record canceled.');
        }
        
    }
