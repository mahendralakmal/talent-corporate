<?php

namespace App\Http\Controllers;

use App\Bank_Branch;
use App\company;
use App\Employee;
use App\Leave;
use App\Leave_balance;
use App\LeaveType;
use App\User;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class LeaveBalanceController extends Controller
{
    /**
     * LeaveBalanceController constructor.
     */
    public function __construct() { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = company::findOrFail(session('company'));
//        $leaves_balance = $company->leaveBalance;
        $leaves_balance = Leave_balance::where('company_id',session('company_id'))->get();
        $leaves_type = LeaveType::where('company_id',session('company_id'))->orWhere('company_id','=','All')->orderBy('name')->get();
        $employees = Employee::where('company_id',session('company_id'))->get();
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
            $users = User::all();
        }
        return view('leave.leave_balance',compact('leaves_balance','employees','leaves_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $html = '';
        //$id = $request->input('id');


        $balance = Leave_balance::where('leave_type','=',$request->input('type'))->where('company_id',session('company_id'))->get();

        dd($balance);
        foreach ($balance as $b) {
            $html .= '<tr><td>' . $b->user_id . '</td><td></td><td>' . $b->remaining . '</td><td>' . $b->used . '</td><td>' . $b->remaining-$b->used . '</td></tr>';
        }

        //dd($html);
        return response()->json(['html' => $html]);


//        $leave = new Leave_balance();
//
//        $leave->user_id = $request->emp_name;
//
//        $leave->company_id = auth()->user()->company_id;
//
//        $leave->available_leaves = (Integer)$request->total_leaves;
//
//        $leave->approved_leaves = (Integer)$request->approved_leaves;
//
//        $leave->remaining_leaves = $request->total_leaves-$request->approved_leaves;
//
//
//
//        $leave->save();
//
//        $leaves_balance = Leave_balance::where('company_id',session('company_id'))->get();
//
//        $employees = Employee::where('company_id',session('company_id'))->get();
//
//        return view('leave.leave_balance',compact('leaves_balance','employees'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Leave_balance  $leave_balance
     * @return \Illuminate\Http\Response
     */
    public function show(Leave_balance $leave_balance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave_balance  $leave_balance
     * @return \Illuminate\Http\Response
     */
    public function edit(Leave_balance $leave_balance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave_balance  $leave_balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leave_balance $leave_balance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Leave_balance  $leave_balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leave_balance $leave_balance)
    {
        //
    }
    public function getBalance(Request $request){

//        dd($request->all());
        
        $company = company::findOrFail(session('company'));
        $leaves_balance = $company->leaveBalance->where('leave_type', $request->type);
//        dump($leaves_balance[0]);
        $employees = $company->employees;
        $leaves_type =  LeaveType::all();
        return view('leave.leave_balance',compact('leaves_balance','employees','leaves_type'));
        
//        $html = '';
//
//        $leavetype = LeaveType::findOrFail($request->input('type'));
//        $balance = Leave_balance::where('leave_type','=',$request->input('type'))->where('company_id',session('company_id'))->get();
//        $count = Leave_balance::where('leave_type','=',$request->input('type'))->where('company_id',session('company_id'))->count();
//
//        if ($count == 0)
//        {
//            $html .= '<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>';
//        }
//        else {
//            foreach ($balance as $b) {
//                $available = $leavetype->entitlement - $b->used;
//
//                $emp = Employee::where('company_id',session('company_id'))->where('user_id','=',$b->user_id)->first();
//
//                $name = $emp->fname . " " . $emp->lname;
//                $html .= '<tr><td>' . $b->user_id . '</td><td>' . $name . '</td><td>' . $leavetype->entitlement . '</td><td>' . $b->used . '</td><td>' . $available . '</td></tr>';
//            }
//        }
//        return response()->json(['html' => $html]);
    }
}
