<?php

namespace App\Http\Controllers;

use App\company;
use App\User;
use Illuminate\Http\Request;
use App\Events;
use Illuminate\Support\Facades\Auth;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;


class EventsController extends Controller {
    /**
     * EventsController constructor.
     */
    public function __construct () { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index () {
        $events = [];
        if (Auth::user()->company_id == 'All')
            $data = Events::all();
        else
            $data = Events::whereIn('company_id', [session('company_id'), 'All'])->get();
        
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->event_name, true, new \DateTime($value->event_date), new \DateTime($value->event_date . ' +1 day'), null,
                    ['color' => $value->event_color]
                );
            }
        }
        
        if (Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            $users = User::all();
        }
        $calendar = Calendar::addEvents($events);
        return view('calendar.index', compact('calendar', 'companies'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create () {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request) {
        $this->validate($request, [
            'event_name' => 'required',
            'date' => 'required',
            'color' => 'required',
        ]);
        
        $event = new Events();
        $event->event_name = $request->event_name;
        $event->company_id = auth()->user()->company_id;
        $event->event_date = $request->date;
        $event->event_color = $request->color;
        $$event->save();
        
        return redirect(route('events.index'))->with('success', 'event added successfully');
    }
    
    /**
     * Display the specified resource.
     * @param \App\events $events
     * @return \Illuminate\Http\Response
     */
    public function show (events $events) {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param \App\events $events
     * @return \Illuminate\Http\Response
     */
    public function edit (events $events) {
        //
    }
    
    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param \App\events $events
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, events $events) {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     * @param \App\events $events
     * @return \Illuminate\Http\Response
     */
    public function destroy (events $events) {
        //
    }
}
