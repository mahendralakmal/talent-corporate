<?php

namespace App\Http\Controllers;

use App\company;
use App\Employee;
use App\paysheet;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class epfcformController extends Controller {
    /**
     * epfcformController constructor.
     */
    public function __construct () { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index () {
        return view('reports.epfcform');
    }
    
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create () {
    }
    
    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request) {
//        return "hi";
        $this->validate($request, [
            'year' => 'required|not_in:0',
            'month' => 'required|not_in:0',
        ]);
        
        $mon = $request->month;
        $month = date("m", mktime(0, 0, 0, date('m',strtotime($request->month)), 1));//Carbon::parse($request->month)->month;
        $company = company::find(session('company'));
        $count = paysheet::where('month', $month)->where('year', $request->year)->where('company_id', $company->regno)->where('epf_no', "!=", null)->count();
        $logo = company::where('regno', session('company_id'))->value('logo');
        $year = $request->year;

        if ($count == 0) {
        } else {
            $epfData = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('company_id', session('company_id'))->get();
            return view('reports.epfcform_report', compact('mon', 'epfData', 'company', 'logo', 'year', 'month', 'count'));
        }
    }
    
    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id) {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id) {
        //
    }
    
    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id) {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id) {
        //
    }
    
    
    public function getEpfLiableSalary ($employee) {
        $epfLiableSalary = ($employee->salary->basic_EPF) ? (double)$employee->salary->basic_salary : 00.0;
        $epfLiableSalary += ($employee->salary->allowance01 != 0.0) ? (double)$employee->salary->allowance01 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance02 != 0.0) ? (double)$employee->salary->allowance02 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance03 != 0.0) ? (double)$employee->salary->allowance03 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance04 != 0.0) ? (double)$employee->salary->allowance04 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance05 != 0.0) ? (double)$employee->salary->allowance05 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance06 != 0.0) ? (double)$employee->salary->allowance06 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance07 != 0.0) ? (double)$employee->salary->allowance07 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance08 != 0.0) ? (double)$employee->salary->allowance08 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance09 != 0.0) ? (double)$employee->salary->allowance09 : 00.0;
        $epfLiableSalary += ($employee->salary->allowance10 != 0.0) ? (double)$employee->salary->allowance10 : 00.0;
        return $epfLiableSalary;
    }
    
    public function getWorkDays ($month) {
        $workDays = [];
        switch ($month) {
            case("January"):
                $workDays['WORKDAYS'] = 31;
                $workDays['MONTH'] = '01';
                break;
            case("February"):
                $workDays['WORKDAYS'] = 28;
                $workDays['MONTH'] = '02';
                break;
            case("March"):
                $workDays['WORKDAYS'] = 31;
                $workDays['MONTH'] = '03';
                break;
            case("April"):
                $workDays['MONTH'] = '04';
                $workDays['WORKDAYS'] = 30;
                break;
            case("May"):
                $workDays['MONTH'] = '05';
                $workDays['WORKDAYS'] = 31;
                break;
            case("June"):
                $workDays['MONTH'] = '06';
                $workDays['WORKDAYS'] = 30;
                break;
            case("July"):
                $workDays['MONTH'] = '07';
                $workDays['WORKDAYS'] = 31;
                break;
            case("August"):
                $workDays['MONTH'] = '08';
                $workDays['WORKDAYS'] = 31;
                break;
            case("September"):
                $workDays['MONTH'] = '09';
                $workDays['WORKDAYS'] = 30;
                break;
            case("October"):
                $workDays['MONTH'] = '10';
                $workDays['WORKDAYS'] = 31;
                break;
            case("November"):
                $workDays['MONTH'] = '11';
                $workDays['WORKDAYS'] = 30;
                break;
            case("December"):
                $workDays['MONTH'] = '12';
                $workDays['WORKDAYS'] = 31;
                break;
        }
        return $workDays;
    }
    
    public function getEmployeeStatus ($employee, $year, $month) {
        if ((Carbon::parse($employee->date_of_join)->year . '-' . Carbon::parse($employee->date_of_join)->month) == (Carbon::parse($year . '-' . $month)->year . '-' . Carbon::parse($year . '-' . $month)->month)) {
            return "N";
        } else if ((Carbon::parse($employee->date_of_resign)->year . '-' . Carbon::parse($employee->date_of_resign)->month) == (Carbon::parse($year . '-' . $month)->year . '-' . Carbon::parse($year . '-' . $month)->month)) {
            return "V";
        } else {
            return "E";
        }
    }
    
    public function downloadEpfFileText (Request $request) {
        $month = date("m", mktime(0, 0, 0, $request->month, 1));
        $company = company::findOrFail(session('company'));
        $count = paysheet::where([['month',$month],['year', $request->year],['company_id', session('company_id')],['epf_no', "!=", null]])->count();
        if (!$count == 0) {
            $epfData = paysheet::where([
                ['month', $month],
                ['year', $request->year],
                ['company_id', session('company_id')]
            ])->get();
            $data = null;
            $file = null;
            if (!empty($epfData)) {
                foreach ($epfData as $key => $value) {
                    $emp = Employee::where('user_id', $value->emp_id)->first();
                    $data .= str_pad($value->nic, 20, " ", STR_PAD_RIGHT);
                    $data .= str_pad($emp->lname, 40, " ", STR_PAD_RIGHT);
                    $data .= str_pad(str_replace(" " . $emp->lname, "", $emp->name_with_initials), 20, " ", STR_PAD_RIGHT);
                    $data .= str_pad($emp->epf_no, 6, "0", STR_PAD_LEFT);
                    $data .= str_pad($value->epf12 + $value->epf08, 10, "0", STR_PAD_LEFT);
                    $data .= str_pad($value->epf12, 10, "0", STR_PAD_LEFT);
                    $data .= str_pad($value->epf08, 10, "0", STR_PAD_LEFT);
                    $data .= str_pad(round($this->getEpfLiableSalary($emp), 0) . ".00", 10, "0", STR_PAD_LEFT);
                    $data .= $this->getEmployeeStatus($emp, $request->year, $request->month);
                    $data .= $company->EPFno;
                    $data .= Carbon::now()->year . (Carbon::now()->format('m'));
                    $data .= '01'; //submission code - hard coded
                    $data .= str_pad($this->getWorkDays(date("F", mktime(0, 0, 0, $request->month, 1)))['WORKDAYS'] . ".00", 5, "0", STR_PAD_LEFT);// number of days attend to work - hard coded
                    $data .= str_pad($emp->skill_cate, 3, "0", STR_PAD_LEFT);
                    $data .= "\r\n";
                }
            }
            $file = '/public/files/EPF-C-Forms/' . Carbon::now()->toDateTimeString() . str_replace(" ", "_", $company->name) . '_EPF_file.txt';
            Storage::disk('local')->put($file, $data);
            return Storage::download($file);
        }
    }
    
    public function downloadEtfFileText (Request $request) {
        $month = date("m", mktime(0, 0, 0, $request->month, 1));
        $company = company::findOrFail(session('company'));
        $count = paysheet::where([['month',$month],['year', $request->year],['company_id', session('company_id')],['epf_no', "!=", null]])->count();
        if (!$count == 0) {
            $epfData = paysheet::where([
                ['month', $month],
                ['year', $request->year],
                ['company_id', session('company_id')]
            ])->get();
            $totalEtf = paysheet::where([
                ['month', $month],
                ['year', $request->year],
                ['company_id', session('company_id')]
            ])->sum('etf');
            $data = null;
            if (!empty($epfData)) {
                foreach ($epfData as $key => $value) {
                    $emp = Employee::where('user_id', $value->emp_id)->first();
                    $data .= "D";
                    $data .= substr($company->EPFno, 0,1). " ".substr($company->EPFno, 1,6);
                    $data .= str_pad($emp->epf_no, 6, "0", STR_PAD_LEFT);
                    $data .= str_pad(str_replace(" " . $emp->lname, "", $emp->name_with_initials), 20, " ", STR_PAD_RIGHT);
                    $data .= str_pad($emp->lname, 30, " ", STR_PAD_RIGHT);
                    $data .= str_pad($value->nic, 12, "0", STR_PAD_LEFT);
                    $data .= $request->year.$month;
                    $data .= $request->year.$month;
                    $data .= str_pad(round($value->etf,0)."00", 9, "0", STR_PAD_LEFT);
                    $data .= "\r\n";
                }
                $data .= "H";
                $data .= substr($company->EPFno, 0,1). " ".substr($company->EPFno, 1,6);
                $data .= Carbon::now()->year . (Carbon::now()->format('m'));
                $data .= Carbon::now()->year . (Carbon::now()->format('m'));
                $data .= str_pad(count($epfData),6,"0",STR_PAD_LEFT);
                $data .= str_pad(round($totalEtf,0)."00",13,"0",STR_PAD_LEFT);
                $data .= "024"; //Transaction code
            }
            $file = '/public/files/ETF-C-Forms/' . Carbon::now()->toDateTimeString() . str_replace(" ", "_", $company->name) . '_etf_file.txt';
            Storage::disk('local')->put($file, $data);
            return Storage::download($file);
        }
    }
}
