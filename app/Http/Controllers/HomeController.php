<?php
    
    namespace App\Http\Controllers;
    
    use App\company;
    use App\Employee;
    use App\Leave;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use mysql_xdevapi\Session;
    
    class HomeController extends Controller {
        /**
         * Create a new controller instance.
         * @return void
         */
        public function __construct () {
            $this->middleware('auth');
        }
        
        /**
         * Show the application dashboard.
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function index () {
            $leaves = Leave::where('approved_status', '=', 'pending')->get();
            $birthdays = Employee::where('bday', '=', Carbon::now('+5:30')->format('Y-m-d'))->get();
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
            } elseif (Auth::user()->getRoleNames()->first() == "company-admin") {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
                if($companies->count() == 1){
                    session(['company' => $companies[0]->id]);
                    session(['company_id' => $companies[0]->regno]);
                }
            } else {
//                dd(Auth::user());
                $company = company::where('regno', Auth::user()->company_id)->first();
                session(['company' => $company->id]);
                session(['company_id' => $company->regno]);
            }
            
            return view('home', compact('birthdays', 'leaves', 'birthdays', 'companies'));
        }
        
        public function setCompany (Request $request) {
            $company = company::findOrFail($request->company_id);
            session(['company' => $company->id]);
            session(['company_id' => $company->regno]);
            return back();
        }
    }
