<?php

namespace App\Http\Controllers;

use App\company;
use App\Employee;
use App\Employee_Loan;
use App\Leave;
use App\paysheet;
use App\Salary;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Importer;
use Excel;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    /**
     * SalaryController constructor.
     */
    public function __construct() { $this->middleware('auth'); }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('company_id', session('company_id'))->get();
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
            $users = User::all();
        }
        return view('payrole.index', compact('employees', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id = auth()->user()->company_id;
        $salaries = Salary::where('company_id', '=', $company_id)->get();
        foreach ($salaries as $salary) {
            $emp_id = $salary->employee_id;
            $basic = $salary->basic_salary;
            $paye = 0;
            $epf = 0;
            $allow = 0;
            if ($salary->basic_PAYE == 1) {
                $paye = $paye + $basic;
            }
            if ($salary->basic_EPF == 1) {
                $epf = $epf + $basic;
            }
            if ($salary->allow01_desc != null) {
                $allow01 = $salary->allow01_amount;
                $allow = $allow + $allow01;
                if ($salary->allow01_PAYE == 1) {
                    $paye = $paye + $allow01;
                }
                if ($salary->allow01_EPF == 1) {
                    $epf = $epf + $allow01;
                }
            }
            if ($salary->allow02_desc != null) {
                $allow02 = $salary->allow02_amount;
                $allow = $allow + $allow02;
                if ($salary->allow02_PAYE == 1) {
                    $paye = $paye + $allow02;
                }
                if ($salary->allow02_EPF == 1) {
                    $epf = $epf + $allow02;
                }
            }
            if ($salary->allow03_desc != null) {
                $allow03 = $salary->allow03_amount;
                $allow = $allow + $allow03;
                if ($salary->allow03_PAYE == 1) {
                    $paye = $paye + $allow03;
                }
                if ($salary->allow03_EPF == 1) {
                    $epf = $epf + $allow03;
                }
            }
            if ($salary->allow04_desc != null) {
                $allow04 = $salary->allow04_amount;
                $allow = $allow + $allow04;
                if ($salary->allow04_PAYE == 1) {
                    $paye = $paye + $allow04;
                }
                if ($salary->allow04_EPF == 1) {
                    $epf = $epf + $allow04;
                }
            }
            $calc_paye = 0;
            if ($paye >= 100000 && $paye < 150000) {
                $calc_paye = $paye * (4 / 100) - 4000;
            } else if ($paye >= 150000 && $paye < 200000) {
                $calc_paye = $paye * (8 / 100) - 10000;
            } else if ($paye >= 200000 && $paye < 250000) {
                $calc_paye = $paye * (12 / 100) - 18000;
            } else if ($paye >= 250000 && $paye < 300000) {
                $calc_paye = $paye * (16 / 100) - 28000;
            } else if ($paye >= 300000 && $paye < 350000) {
                $calc_paye = $paye * (20 / 100) - 40000;
            } else {
                $calc_paye = $paye * (24 / 100) + 54000;
            }
            $calc_epf = $epf * 8 / 100;
            $calc_loans = 0;
            $loans = Employee_Loan::where('company_id', '=', $company_id)->where('user_id', '=', auth()->user()->user_id)->get();
            foreach ($loans as $loan) {
                $calc_loans = $calc_loans + $loan->monthly_installment;
            }
            $net_sallary = $basic + $allow - $calc_loans - $calc_epf - $calc_paye;
            if ($net_sallary > 25000) {
                $net_sallary = $net_sallary - 25;
            }

            //dd($company_id,$emp_id);
            $users = Employee::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->get();


            foreach ($users as $user) {

                $emp = Employee::findOrFail($user->id);

                $emp->last_salary = $net_sallary;

                $emp->update();


                break;
            }
        }
        $employees = Employee::where('company_id', session('company_id'))->get();

        return view('payrole.index', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


//        $validator = Validator::make($request->all(),[
//            'file' => 'required|max:5000|mimes:xlsx,xls,csv'
//        ]);
//
//        if($validator->passes()) {
        $dataTime = date('Ymd_His');
        $file = $request->file('file');
        $month = $request->month;
        $filename = auth()->user()->company_id . $month;
        $savePath = public_path('/upload/');
        $file->move($savePath, $filename);

        $excel = Importer::make('Excel');
        $excel->load($savePath . $filename);
        $collection = $excel->getCollection();

        $company_id = auth()->user()->company_id;
        $salaries = Salary::where('company_id', '=', $company_id)->get();

        //$columnSize = max( array_map('count', $collection) );
        foreach ($salaries as $salary) {
            for ($row = 2; $row < sizeof($collection); $row++) {
                if ($collection[$row][0] == $salary->employee_id) {
                    $emp_id = $salary->employee_id;
                    $basic = $salary->basic_salary;
                    $paye = 0;
                    $epf = 0;
                    $allow = 0;

                    $ad001 = 0;
                    $ad002 = 0;
                    $ad003 = 0;
                    $ad004 = 0;
                    $ad005 = 0;
                    $ad006 = 0;
                    $ad007 = 0;
                    $ad008 = 0;
                    $ad009 = 0;

                    for ($col = 1; $col < 6; $col++) {
                        if ($collection[0][$col] == 'ad001') {
                            $ad001 = $collection[$row][$col];
                        } else if ($collection[0][$col] == 'ad002') {
                            $ad002 = $collection[$row][$col];
                        } else if ($collection[0][$col] == 'ad003') {
                            $ad003 = $collection[$row][$col];
                        } else if ($collection[0][$col] == 'ad004') {
                            $ad004 = $collection[$row][$col];
                        } else if ($collection[0][$col] == 'ad005') {
                            $ad005 = $collection[$row][$col];
                        } else if ($collection[0][$col] == 'ad006') {
                            $ad006 = $collection[$row][$col];
                        }
                    }

                    $basic = $basic + $ad001;


                    if ($salary->basic_PAYE == 1) {
                        $paye = $paye + $basic;
                    }
                    if ($salary->basic_EPF == 1) {
                        $epf = $epf + $basic;
                    }

                    if ($salary->allow01_desc != null) {
                        $allow01 = $salary->allow01_amount;
                        $allow = $allow + $allow01;
                        if ($salary->allow01_PAYE == 1) {
                            $paye = $paye + $allow01;
                        }
                        if ($salary->allow01_EPF == 1) {
                            $epf = $epf + $allow01;
                        }
                    }
                    if ($salary->allow02_desc != null) {
                        $allow02 = $salary->allow02_amount;
                        $allow = $allow + $allow02;
                        if ($salary->allow02_PAYE == 1) {
                            $paye = $paye + $allow02;
                        }
                        if ($salary->allow02_EPF == 1) {
                            $epf = $epf + $allow02;
                        }
                    }
                    if ($salary->allow03_desc != null) {
                        $allow03 = $salary->allow03_amount;
                        $allow = $allow + $allow03;
                        if ($salary->allow03_PAYE == 1) {
                            $paye = $paye + $allow03;
                        }
                        if ($salary->allow03_EPF == 1) {
                            $epf = $epf + $allow03;
                        }
                    }
                    if ($salary->allow04_desc != null) {
                        $allow04 = $salary->allow04_amount;
                        $allow = $allow + $allow04;
                        if ($salary->allow04_PAYE == 1) {
                            $paye = $paye + $allow04;
                        }
                        if ($salary->allow04_EPF == 1) {
                            $epf = $epf + $allow04;
                        }
                    }
                    $calc_paye = 0;
                    if ($paye < 100000) {

                    } else if ($paye >= 100000 && $paye < 150000) {
                        $calc_paye = $paye * (4 / 100) - 4000;
                    } else if ($paye >= 150000 && $paye < 200000) {
                        $calc_paye = $paye * (8 / 100) - 10000;
                    } else if ($paye >= 200000 && $paye < 250000) {
                        $calc_paye = $paye * (12 / 100) - 18000;
                    } else if ($paye >= 250000 && $paye < 300000) {
                        $calc_paye = $paye * (16 / 100) - 28000;
                    } else if ($paye >= 300000 && $paye < 350000) {
                        $calc_paye = $paye * (20 / 100) - 40000;
                    } else {
                        $calc_paye = $paye * (24 / 100) - 54000;
                    }
                    $calc_epf = $epf * 8 / 100;
                    $calc_loans = 0;
                    $loans = Employee_Loan::where('company_id', '=', $company_id)->where('user_id', '=', auth()->user()->user_id)->get();
                    foreach ($loans as $loan) {
                        $calc_loans = $calc_loans + $loan->monthly_installment;
                    }
                    $net_sallary = $basic + $allow - $calc_loans - $calc_epf - $calc_paye;


                    $net_sallary = $net_sallary + $ad004 + $ad005 + $ad006 + $ad007 + $ad008 + $ad009 - $ad002 - $ad003;


                    if ($net_sallary > 25000) {
                        $net_sallary = $net_sallary - 25;
                    }


                    //dd($net_sallary);

                    $users = Employee::where('company_id', '=', $company_id)->where('user_id', '=', $emp_id)->get();


                    foreach ($users as $user) {

                        $emp = Employee::findOrFail($user->id);

                        $emp->last_salary = $net_sallary;

                        $emp->update();


                        break;
                    }


                }


            }

        }
        // }

        $companies = company::where('regno', session('company_id'))->get();


        //$user_image = DB::table('employees')->select('user_img')->where('company_id', session('company_id'))->get();

        return view('company.show', compact('companies'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paysheet = paysheet::where('company_id', session('company_id'))->first();

        return view('payrole.edit', compact('paysheet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payrole_approval()
    {

        $paysheets = paysheet::where('company_id', session('company_id'))->where('approvalStatus', '=', 0)
            ->get();
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
        } else {
            $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
            $users = User::all();
        }
        return view('payrole.payrole_approval', compact('paysheets', 'companies'));

    }

    public function approve(Request $request)
    {


    }
}
