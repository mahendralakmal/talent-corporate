<?php
    
    namespace App\Http\Controllers;
    
    use App\Allowance;
    use App\bank;
    use App\Bank_Branch;
    use App\company;
    use App\Leave;
    use App\LeaveType;
    use App\Mail\employee_creation;
    use App\Shift;
    use App\User;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Mail;
    use Validator;
    use Illuminate\Http\Request;
    
    class CompanyController extends Controller {
        /**
         * CompanyController constructor.
         */
        public function __construct () { $this->middleware('auth'); }
        
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index () {
            if (Auth::user()->getRoleNames()->first() == "super-admin") {
                $companies = company::where('status', '=', true)->get();
                $users = User::all();
            } else {
                $companies = company::where([['status', true], ['user_id', Auth::user()->id]])->get();
                $users = User::all();
            }
            return view("company.index", compact('companies', 'users'));
        }
        
        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create () {
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            return view("company.create", compact('banks', 'bank_branches'));
        }
        
        public function getCompanyId ($entity) {
            $company_id = "";
            if ($entity == 'Proprietorship') {
                $company_id = 'PR' . date("Y") . rand(1000, 9999);
            } else if ($entity == 'Partnership') {
                $company_id = 'PA' . date("Y") . rand(1000, 9999);
            } else if ($entity == 'Company') {
                $company_id = 'CO' . date("Y") . rand(1000, 9999);
            } else if ($entity == 'Other') {
                $company_id = 'OT' . date("Y") . rand(1000, 9999);
            }
            return $company_id;
        }
        
        /**
         * Store a newly created resource in storage.
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store (Request $request) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'company_name' => 'required',
                'address' => 'required',
                'business_name' => 'required',
                'entity_type' => 'required|not_in:0',
                'telephone_no' => 'required',
                'company_email' => 'required|email',
                'contact_person' => 'required',
                'contact_person_designation' => 'required',
                'contact_email' => 'required|email',
                'mobile' => 'required',
                'fixed' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'bank_code' => 'required',
                'branch_code' => 'required',
                'barnch_address' => 'required',
                'account_number' => 'required|max:12|min:1',
                'account_name' => 'required',
                'salaryDate' => 'required|not_in:0',
                'br_copy' => 'required|file',
            
            ]);
            
            if ($validator->fails()) {
                return redirect(route('companies.create'))
                    ->withErrors($validator)
                    ->withInput();
            }
            
            if ($request->hasFile('br_copy')) {
                $filenameWithExt_br = $request->file('br_copy')->getClientOriginalName();
                $filename_br = pathinfo($filenameWithExt_br, PATHINFO_FILENAME);
                $extension_br = $request->file('br_copy')->getClientOriginalExtension();
                $fileNameToStore_br = $filename_br . '_' . time() . '.' . $extension_br;
                $request->file('br_copy')->storeAs('public/company', $fileNameToStore_br);
            } else {
                $fileNameToStore_br = null;
            }
            
            if ($request->hasFile('tin_document')) {
                $filenameWithExt_tin = $request->file('tin_document')->getClientOriginalName();
                $filename_tin = pathinfo($filenameWithExt_tin, PATHINFO_FILENAME);
                $extension_tin = $request->file('tin_document')->getClientOriginalExtension();
                $fileNameToStore_tin = $filename_tin . '_' . time() . '.' . $extension_tin;
                $request->file('tin_document')->storeAs('public/company', $fileNameToStore_tin);
            } else {
                $fileNameToStore_tin = null;
            }
            
            if ($request->hasFile('epf_document')) {
                $filenameWithExt_epf = $request->file('epf_document')->getClientOriginalName();
                $filename_epf = pathinfo($filenameWithExt_epf, PATHINFO_FILENAME);
                $extension_epf = $request->file('epf_document')->getClientOriginalExtension();
                $fileNameToStore_epf = $filename_epf . '_' . time() . '.' . $extension_epf;
                $request->file('epf_document')->storeAs('public/company', $fileNameToStore_epf);
            } else {
                $fileNameToStore_epf = null;
            }
            
            DB::beginTransaction();
            $company = new company();
            $company->name = $request->company_name;
            $company_id = $this->getCompanyId($request->entity_type);
            $company->regno = $company_id;
            $company->address = $request->address;
            $company->business_nature = $request->business_name;
            $company->entity_type = $request->entity_type;
            $company->telephone = $request->telephone_no;
            $company->email = $request->company_email;
            $company->website = $request->website;
            $company->shift_apl = $request->shift_apl;
            if (!$request->shift_apl) {
                $company->shift_apl = false;
                $company->startTime = $request->start_time;
                $company->endTime = $request->end_time;
            } else {
                $company->shift_apl = $request->shift_apl;
            }
            $company->bank_code = $request->bank_code;
            $company->branch_code = $request->branch_code;
            $company->branch_address = $request->barnch_address;
            $company->account_number = $request->account_number;
            $company->account_name = $request->account_name;
            $company->contactperson_name = $request->contact_person;
            $company->contactperson_telephone = $request->mobile;
            $company->contactperson_fixedphone = $request->fixed;
            $company->landline_extention = $request->ext;
            $company->contactperson_email = $request->contact_email;
            $company->contactperson_designation = $request->contact_person_designation;
            $company->TINno = $request->tin_no;
            $company->logo = 'logo-here.png';
            $company->EPFno = $request->epf_registration_no;
            $company->EPFrate = $request->epf_rate;
            $company->PAYEno = $request->tin_no;
            $company->epf_contribution = 0;
            $company->stamp_duty = $request->stamp_duty;
            $company->br_copy = $fileNameToStore_br;
            $company->tin_copy = $fileNameToStore_tin;
            $company->epf_copy = $fileNameToStore_epf;
            $company->status = 1;
            $company->salaryDate = $request->salaryDate;
            if ($request->leaves) {
                $company->leaveStatus = $request->leaves;
            } else {
                $company->leaveStatus = 0;
            }
            $company->save();
            
            $allowance = new Allowance();
            $allowance->company_id = $company_id;
            $allowance->allowance01 = $request->allowance01;
            $allowance->allowance01Paye = $request->allowance01Paye;
            $allowance->allowance01Epf = $request->allowance01Epf;
            $allowance->allowance02 = $request->allowance02;
            $allowance->allowance02Paye = $request->allowance02Paye;
            $allowance->allowance02Epf = $request->allowance02Epf;
            $allowance->allowance03 = $request->allowance03;
            $allowance->allowance03Paye = $request->allowance03Paye;
            $allowance->allowance03Epf = $request->allowance03Epf;
            $allowance->allowance04 = $request->allowance04;
            $allowance->allowance04Paye = $request->allowance04Paye;
            $allowance->allowance04Epf = $request->allowance04Epf;
            $allowance->allowance05 = $request->allowance05;
            $allowance->allowance05Paye = $request->allowance05Paye;
            $allowance->allowance05Epf = $request->allowance05Epf;
            $allowance->allowance06 = $request->allowance06;
            $allowance->allowance06Paye = $request->allowance06Paye;
            $allowance->allowance06Epf = $request->allowance06Epf;
            $allowance->allowance07 = $request->allowance07;
            $allowance->allowance07Paye = $request->allowance07Paye;
            $allowance->allowance07Epf = $request->allowance07Epf;
            $allowance->allowance08 = $request->allowance08;
            $allowance->allowance08Paye = $request->allowance08Paye;
            $allowance->allowance08Epf = $request->allowance08Epf;
            $allowance->allowance09 = $request->allowance09;
            $allowance->allowance09Paye = $request->allowance09Paye;
            $allowance->allowance09Epf = $request->allowance09Epf;
            $allowance->allowance10 = $request->allowance10;
            $allowance->allowance10Paye = $request->allowance10Paye;
            $allowance->allowance10Epf = $request->allowance10Epf;
            $allowance->save();

//        $user = new User();
//        $user->name = $request->contact_person;
//        $user->email = $request->contact_email;
//        $user->company_id = $company_id;
//        $user->password = $company_id; // Do not put bcrypt method. bcrypt method add user model. it will automatically  bcrypted.
//        $user->image = 'profile.png';
//        $user->assignRole('company-admin');
//        $user->save();
            
            if ($request->leaves == 1) {
                if ($request->shortLeave1 != null) {
                    $leave1 = new LeaveType();
                    $leave1->company_id = $company_id;
                    $leave1->name = 'Short Leave';
                    $leave1->entitlement = $request->shortLeave1;
                    $leave1->type = 'permanent';
                    $leave1->save();
                }
                if ($request->medical1 != null) {
                    $leave2 = new LeaveType();
                    $leave2->company_id = $company_id;
                    $leave2->name = 'Medical';
                    $leave2->entitlement = $request->medical1;
                    $leave2->type = 'permanent';
                    $leave2->save();
                }
                if ($request->lieuLeave != null) {
                    $leave3 = new LeaveType();
                    $leave3->company_id = $company_id;
                    $leave3->name = 'Lieu Leave';
                    $leave3->entitlement = $request->lieuLeave;
                    $leave3->type = 'permanent';
                    $leave3->save();
                }
                if ($request->shortLeave2 != null) {
                    $leave4 = new LeaveType();
                    $leave4->company_id = $company_id;
                    $leave4->name = 'Short Leave';
                    $leave4->entitlement = $request->shortLeave2;
                    $leave4->type = 'probation';
                    $leave4->save();
                }
                if ($request->medical2 != null) {
                    $leave5 = new LeaveType();
                    $leave5->company_id = $company_id;
                    $leave5->name = 'Medical';
                    $leave5->entitlement = $request->medical2;
                    $leave5->type = 'probation';
                    $leave5->save();
                }
                if ($request->annual != null) {
                    $leave6 = new LeaveType();
                    $leave6->company_id = $company_id;
                    $leave6->name = 'Annual';
                    $leave6->entitlement = $request->annual;
                    $leave6->type = 'permanent';
                    $leave6->save();
                }
                if ($request->casual1 != null) {
                    $leave7 = new LeaveType();
                    $leave7->company_id = $company_id;
                    $leave7->name = 'Casual';
                    $leave7->entitlement = $request->casual1;
                    $leave7->type = 'permanent';
                    $leave7->save();
                }
                if ($request->casual2 != null) {
                    $leave8 = new LeaveType();
                    $leave8->company_id = $company_id;
                    $leave8->name = 'Casual';
                    $leave8->entitlement = $request->casual2;
                    $leave8->type = 'probation';
                    $leave8->save();
                }
            }
            
            if ($request->shift_apl) {
                for ($i = 0; $i < sizeof($request->start_time); $i++) {
                    $shift = new Shift();
                    $shift->company_id = $company_id;
                    $shift->start = $request->start_time[$i];
                    $shift->end = $request->end_time[$i];
                    $shift->save();
                }
            }
            DB::commit();
            
            alert()->success("Company Added Successfully!");
//        Mail::send(new employee_creation($company_id));
            
            return redirect(route('companies.index'))->with('success', 'Company Added Successfully!');
        }
        
        /**
         * Display the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function show ($id) {
            $company = company::where('regno', '=', $id)->first();
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            return view('company.show', compact('company', 'banks', 'bank_branches'));
        }
        
        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function edit ($id) {
            $company = company::findOrFail($id);
            
            $shifts = $company->shifts;
            $leaves = $company->leaveTypes;
            $banks = bank::orderBy('bank_name', 'asc')->get();
            $bank_branches = Bank_Branch::orderBy('bank_branch', 'asc')->get();
            $allowance = Allowance::where('company_id', '=', $company->regno)->first();
            return view('company.edit', compact('banks', 'bank_branches', 'allowance', 'company', 'leaves', 'shifts'));
        }
        
        /**
         * Update the specified resource in storage.
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function update (Request $request, $id) {
        
//            return $request->all();
            $this->validate($request, [
                'company_name' => 'required',
                'address' => 'required',
                'business_name' => 'required',
                'entity_type' => 'required|not_in:0',
                'telephone_no' => 'required',
                'company_email' => 'required|email',
                'contact_person' => 'required',
                'contact_person_designation' => 'required',
                'contact_email' => 'required|email',
                'mobile' => 'required',
                'fixed' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'bank_code' => 'required',
                'branch_code' => 'required',
                'barnch_address' => 'required',
                'account_number' => 'required|max:12|min:1',
                'account_name' => 'required',
                'salaryDate' => 'required|not_in:0',
            ]);
            
            if ($request->hasFile('tin_document')) {
                $filenameWithExt_tin = $request->file('tin_document')->getClientOriginalName();
                $filename_tin = pathinfo($filenameWithExt_tin, PATHINFO_FILENAME);
                $extension_tin = $request->file('tin_document')->getClientOriginalExtension();
                $fileNameToStore_tin = $filename_tin . '_' . time() . '.' . $extension_tin;
                $request->file('tin_document')->storeAs('public/company', $fileNameToStore_tin);
            } else {
                $fileNameToStore_tin = null;
            }
            
            if ($request->hasFile('epf_document')) {
                $filenameWithExt_epf = $request->file('epf_document')->getClientOriginalName();
                $filename_epf = pathinfo($filenameWithExt_epf, PATHINFO_FILENAME);
                $extension_epf = $request->file('epf_document')->getClientOriginalExtension();
                $fileNameToStore_epf = $filename_epf . '_' . time() . '.' . $extension_epf;
                $request->file('epf_document')->storeAs('public/company', $fileNameToStore_epf);
            } else {
                $fileNameToStore_epf = null;
            }
    
            DB::beginTransaction();
            $company = Company::findOrFail($id);
            
            $company->name = $request->company_name;
            $company->address = $request->address;
            $company->business_nature = $request->business_name;
            $company->entity_type = $request->entity_type;
            $company->telephone = $request->telephone_no;
            $company->email = $request->company_email;
            $company->website = $request->website;
            $company->bank_code = $request->bank_code;
            $company->branch_code = $request->branch_code;
            $company->branch_address = $request->barnch_address;
            $company->account_number = $request->account_number;
            $company->account_name = $request->account_name;
            $company->contactperson_name = $request->contact_person;
            $company->contactperson_telephone = $request->mobile;
            $company->contactperson_fixedphone = $request->fixed;
            $company->landline_extention = $request->ext;
            $company->contactperson_email = $request->contact_email;
            $company->contactperson_designation = $request->contact_person_designation;
            $company->TINno = $request->tin_no;
            $company->logo = 'logo-here.png';
            $company->EPFno = $request->epf_registration_no;
            $company->EPFrate = $request->epf_rate;
            $company->PAYEno = $request->paye_no;
            $company->epf_contribution = 0;
            $company->stamp_duty = $request->stamp_duty;
            $company->tin_copy = $fileNameToStore_tin;
            $company->epf_copy = $fileNameToStore_epf;
            
            if ($request->leaves) {
                $company->leaveStatus = $request->leaves;
            } else {
                $company->leaveStatus = false;
            }
            
            if (!$request->shift_apl) {
                $company->shift_apl = false;
                $company->startTime = $request->start_time[0];
                $company->endTime = $request->end_time[0];
            } else {
                $company->shift_apl = $request->shift_apl;
            }
            
            $company->update();
            
            $allowance = Allowance::where('company_id', '=', $company->regno)->first();
            $allowance->allowance01 = $request->allowance01;
            $allowance->allowance01Paye = $request->allowance01Paye;
            $allowance->allowance01Epf = $request->allowance01Epf;
            $allowance->allowance02 = $request->allowance02;
            $allowance->allowance02Paye = $request->allowance02Paye;
            $allowance->allowance02Epf = $request->allowance02Epf;
            $allowance->allowance03 = $request->allowance03;
            $allowance->allowance03Paye = $request->allowance03Paye;
            $allowance->allowance03Epf = $request->allowance03Epf;
            $allowance->allowance04 = $request->allowance04;
            $allowance->allowance04Paye = $request->allowance04Paye;
            $allowance->allowance04Epf = $request->allowance04Epf;
            $allowance->allowance05 = $request->allowance05;
            $allowance->allowance05Paye = $request->allowance05Paye;
            $allowance->allowance05Epf = $request->allowance05Epf;
            $allowance->allowance06 = $request->allowance06;
            $allowance->allowance06Paye = $request->allowance06Paye;
            $allowance->allowance06Epf = $request->allowance06Epf;
            $allowance->allowance07 = $request->allowance07;
            $allowance->allowance07Paye = $request->allowance07Paye;
            $allowance->allowance07Epf = $request->allowance07Epf;
            $allowance->allowance08 = $request->allowance08;
            $allowance->allowance08Paye = $request->allowance08Paye;
            $allowance->allowance08Epf = $request->allowance08Epf;
            $allowance->allowance09 = $request->allowance09;
            $allowance->allowance09Paye = $request->allowance09Paye;
            $allowance->allowance09Epf = $request->allowance09Epf;
            $allowance->allowance10 = $request->allowance10;
            $allowance->allowance10Paye = $request->allowance10Paye;
            $allowance->allowance10Epf = $request->allowance10Epf;
            $allowance->update();
            
            if ($request->leaves == 1) {
                $company_id = $company->regno;
                if (LeaveType::where('company_id', $company_id)->count() > 0) {
                    foreach (LeaveType::where('company_id', $company_id)->get() as $key => $leaveType) {
                        if (($leaveType->name == 'Short Leave') && ($request->shortLeave1 != null)) {
                            $sl1 = LeaveType::findOrFail($leaveType->id);
                            $sl1->entitlement = $request->shortLeave1;
                            $sl1->update();
                        }
                        if (($leaveType->name == 'Short Leave') && ($request->shortLeave2 != null)) {
                            if ($request->shortLeave2 != null) {
                                $sl2 = LeaveType::findOrFail($leaveType->id);
                                $sl2->entitlement = $request->shortLeave2;
                                $sl2->update();
                            }
                        }
                        if (($leaveType->name == 'Medical') && ($request->medical1 != null)) {
                            $ml1 = LeaveType::findOrFail($leaveType->id);
                            $ml1->entitlement = $request->medical1;
                            $ml1->update();
                        }
                        if (($leaveType->name == 'Medical') && ($request->medical2 != null)) {
                            $ml2 = LeaveType::findOrFail($leaveType->id);
                            $ml2->entitlement = $request->medical2;
                            $ml2->update();
                        }
                        if ($leaveType->name == 'Lieu Leave') {
                            if ($request->lieuLeave != null) {
                                $ll1 = LeaveType::findOrFail($leaveType->id);
                                $ll1->entitlement = $request->lieuLeave;
                                $ll1->update();
                            }
                        }
                        if ($leaveType->name == 'Annual Leave') {
                            if ($request->annualLeave != null) {
                                $al1 = LeaveType::findOrFail($leaveType->id);
                                $al1->entitlement = $request->annualLeave;
                                $al1->update();
                            }
                        }
                        if ($leaveType->name == 'Casual Leave') {
                            if ($request->casualLeave1 != null) {
                                $cl1 = LeaveType::findOrFail($leaveType->id);
                                $cl1->entitlement = $request->casualLeave1;
                                $cl1->update();
                            }
                        }
                        if ($leaveType->name == 'Casual Leave') {
                            if ($request->casualLeave2 != null) {
                                $cl2 = LeaveType::findOrFail($leaveType->id);
                                $cl2->entitlement = $request->casualLeave2;
                                $cl2->update();
                            }
                        }
                    }
                } else {
                    if ($request->shortLeave1 != null) {
                        $leave1 = new LeaveType();
                        $leave1->company_id = $company_id;
                        $leave1->name = 'Short Leave';
                        $leave1->entitlement = $request->shortLeave1;
                        $leave1->type = 'permanent';
                        $leave1->save();
                    }
                    if ($request->medical1 != null) {
                        $leave2 = new LeaveType();
                        $leave2->company_id = $company_id;
                        $leave2->name = 'Medical';
                        $leave2->entitlement = $request->medical1;
                        $leave2->type = 'permanent';
                        $leave2->save();
                    }
                    if ($request->lieuLeave != null) {
                        $leave3 = new LeaveType();
                        $leave3->company_id = $company_id;
                        $leave3->name = 'Lieu Leave';
                        $leave3->entitlement = $request->lieuLeave;
                        $leave3->type = 'permanent';
                        $leave3->save();
                    }
                    if ($request->shortLeave2 != null) {
                        $leave4 = new LeaveType();
                        $leave4->company_id = $company_id;
                        $leave4->name = 'Short Leave';
                        $leave4->entitlement = $request->shortLeave2;
                        $leave4->type = 'probation';
                        $leave4->save();
                    }
                    if ($request->medical2 != null) {
                        $leave5 = new LeaveType();
                        $leave5->company_id = $company_id;
                        $leave5->name = 'Medical';
                        $leave5->entitlement = $request->medical2;
                        $leave5->type = 'probation';
                        $leave5->save();
                    }
                    if ($request->annualLeave != null) {
                        $leave6 = new LeaveType();
                        $leave6->company_id = $company_id;
                        $leave6->name = 'Annual';
                        $leave6->entitlement = $request->annualLeave;
                        $leave6->type = 'permanent';
                        $leave6->save();
                    }
                    if ($request->casualLeave1 != null) {
                        $leave7 = new LeaveType();
                        $leave7->company_id = $company_id;
                        $leave7->name = 'Casual';
                        $leave7->entitlement = $request->casualLeave1;
                        $leave7->type = 'permanent';
                        $leave7->save();
                    }
                    if ($request->casual2 != null) {
                        $leave8 = new LeaveType();
                        $leave8->company_id = $company_id;
                        $leave8->name = 'Casual';
                        $leave8->entitlement = $request->casual2;
                        $leave8->type = 'probation';
                        $leave8->save();
                    }
                }
            }
            
            if ($request->shift_apl) {
                if ($company->shifts->count() > 0) {
                    if (sizeof($request->start_time) == $company->shifts->count())
                        for ($i = 0; $i < $company->shifts->count(); $i++) {
                            $shift = Shift::findOrFail($request->shift_id[$i]);
                            $shift->start = $request->start_time[$i];
                            $shift->end = $request->end_time[$i];
                            $shift->update();
                        }
                    elseif (sizeof($request->start_time) > $company->shifts->count()) {
                        $diff = sizeof($request->start_time) - $company->shifts->count();
                        for ($i = 0; $i < $company->shifts->count(); $i++) {
                            $shift = Shift::findOrFail($request->shift_id[$i]);
                            $shift->start = $request->start_time[$i];
                            $shift->end = $request->end_time[$i];
                            $shift->update();
                        }
                        for ($i = $company->shifts->count(); $i < sizeof($request->start_time); $i++) {
                            $shift = new Shift();
                            $shift->company_id = $company_id;
                            $shift->start = $request->start_time[$i];
                            $shift->end = $request->end_time[$i];
                            $shift->save();
                        }
                    } elseif (sizeof($request->start_time) < $company->shifts->count()) {
                        $count = $company->shifts->count();
                        for ($i = 0; $i < $company->shifts->count(); $i++) {
                            if ((int)$request->shift_id[$i] == $company->shifts[$i]->id) {
                                $shift = Shift::findOrFail($request->shift_id[$i]);
                                $shift->start = $request->start_time[$i];
                                $shift->end = $request->end_time[$i];
                                $shift->update();
                            } else {
                                $shift = Shift::findOrFail($company->shifts[$i]->id);
                                $shift->forceDelete();
                            }
                            $count--;
                        }
                    } else {
                    }
                } else {
                    for ($i = 0; $i < sizeof($request->start_time); $i++) {
                        $shift = new Shift();
                        $shift->company_id = $company_id;
                        $shift->start = $request->start_time[$i];
                        $shift->end = $request->end_time[$i];
                        $shift->save();
                    }
                }
            }
            
            DB::commit();
            alert()->success('Company Update Successfully!');
            return redirect(route('companies.index'))->with('success', 'Company Update Successfully!');
        }
        
        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public
        function destroy (Request $request, $id) {
            $company = company::findOrFail($request->company_id);
            $company->status = '0';
            $company->update();
            return redirect(route('companies.index'))->with('success', 'Company Deleted Successfully!');
        }
        
        public
        function logo_update (Request $request, $id) {
            $this->validate($request, [
                'profile_image' => 'required'
            ]);
            if ($request->hasFile('profile_image')) {
                $filenameWithExt_br = $request->file('profile_image')->getClientOriginalName();
                $profile_image = pathinfo($filenameWithExt_br, PATHINFO_FILENAME);
                $extension_pi = $request->file('profile_image')->getClientOriginalExtension();
                $fileNameToStore = $profile_image . '_' . time() . '.' . $extension_pi;
                $request->file('profile_image')->storeAs('public/company', $fileNameToStore);
            } else {
                $fileNameToStore_br = null;
            }
            $company = company::findOrFail($id);
            $company_id = $company->regno;
            $company->logo = $fileNameToStore;
            $company->update();
            return redirect(route('companies.show', $company_id))->with('success', 'Company Logo Update Successfully!');
        }
        
        public
        function profile_conatcts_update (Request $request, $id) {
            $this->validate($request, [
                'con_name' => 'required',
                'con_desig' => 'required',
                'con_tele' => 'required',
                'con_email' => 'required'
            ]);
            $company = company::findOrFail($id);
            $company->contactperson_name = $request->con_name;
            $company->contactperson_designation = $request->con_desig;
            $company->contactperson_telephone = $request->con_tele;
            $company->contactperson_email = $request->con_email;
            $company->update();
            return redirect(route('companies.show', $company->regno))->with('success', 'Contact Person info Updated !');
        }
        
        public
        function bank_update (Request $request, $id) {
            $this->validate($request, [
                'bank_code' => 'required',
                'branch_code' => 'required',
                'barnch_address' => 'required'
            ]);
            $company = company::findOrFail($id);
            $company->bank_code = $request->bank_code;
            $company->branch_code = $request->branch_code;
            $company->branch_address = $request->barnch_address;
            $company->update();
            return redirect(route('companies.show', $company->regno))->with('success', 'Company Bank Info Updated !');
        }
        
        public
        function tax_details_update (Request $request, $id) {
            $company = company::findOrFail($id);
            $company->TINno = $request->titNo;
            $company->PAYEno = $request->titNo;
            $company->EPFno = $request->epfNo;
            $company->EPFrate = $request->epfRate;
            $company->update();
            return redirect(route('companies.show', $company->regno))->with('success', 'Company Bank Info Updated !');
        }
        
        public
        function leave_details_update (Request $request, $id) {
            $per1 = LeaveType::where('company_id', '=', $id)->where('type', '=', 'permanent')->where('name', '=', 'Short Leave')->first();
            $per1->entitlement = $request->Short_Leave;
            $per1->update();
            $per2 = LeaveType::where('company_id', '=', $id)->where('type', '=', 'permanent')->where('name', '=', 'Medical')->first();
            $per2->entitlement = $request->Medical;
            $per2->update();
            $per3 = LeaveType::where('company_id', '=', $id)->where('type', '=', 'permanent')->where('name', '=', 'Lieu Leave')->first();
            $per3->entitlement = $request->Lieu_Leave;
            $per3->update();
            $pro1 = LeaveType::where('company_id', '=', $id)->where('type', '=', 'probation')->where('name', '=', 'Short Leave')->first();
            $pro1->entitlement = $request->Short_Leave2;
            $pro1->update();
            $pro2 = LeaveType::where('company_id', '=', $id)->where('type', '=', 'probation')->where('name', '=', 'Medical')->first();
            $pro2->entitlement = $request->Medical2;
            $pro2->update();
            return redirect(route('companies.show', $id))->with('success', 'Leave Entitlements Updated !');
        }
        
        public
        function assignUserToCompany (Request $request) {
            $company = company::findOrFail($request->company);
            $company->user_id = $request->user;
            $company->update();
            
            $user = User::findOrFail($request->user);
            if (!$user->roles->first()->name == "super-admin")
                $user->syncRoles(['company-admin']);
            
            return redirect(route('companies.index'))->with('success', 'Company Update Successfully!');
        }
        
        public function destroyShift(Request $request){
            $shift = Shift::findOrFail($request->shift_id[0]);
            $shift->forceDelete();
    
            alert()->success('Shift deleted successfully.');
            return redirect()->back();
        }
    }
