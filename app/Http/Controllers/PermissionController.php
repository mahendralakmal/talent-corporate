<?php

namespace App\Http\Controllers;

use App\company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        if(Auth::user()->getRoleNames()->first() == "super-admin") {
            $companies = company::where('status', '=', true)->get();
            $users = User::all();
        } else {
            $companies = company::where([['status', true],['user_id', Auth::user()->id]])->get();
            $users = User::all();
        }
        return view('permissions.index', compact('permissions', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('permissions.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'permission_name' => 'required|max:40',
//            'roles' => 'required|array|min:1',
        ]);

        $permission = new Permission();
        $permission->name = $request->permission_name;
        $roles = $request->roles;
        $permission->save();

//        if(!empty($request->roles)){
//
//            foreach ($roles as $role) {
//
//                $r = Role::where('id', '=', $role)->firstOrFail();
//
//                $permission = Permission::where('name', '=', $request->permission_name)->first();
//
//                $r->givePermissionTo($permission);
//            }
//        }
    
        alert()->success('Permission Created Successfully!');
        return redirect(route('roles.index'))->with('success', 'Permission Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'permission_name' => 'required|max:40'
        ]);

        $permission = Permission::findOrFail($id);

        $permission->name = $request->permission_name;

        $permission->update();

        return redirect(route('permissions.index'))->with('success', 'Permission Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $permission->delete();

        return redirect(route('permissions.index'))->with('success', 'Permission Deleted!');
    }
    
    public function updateRolesPermissions(Request $request){
        $role = Role::findOrFail($request->role_id);
        $role->syncPermissions($request->role_permission);
        return back();
    }
}
