<?php

namespace App\Http\Controllers;

use App\bank;
use App\company;
use App\Employee;
use App\Employee_Attendance;
use App\paysheet;
use App\Salary;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Component\HttpFoundation\File\File;

class bankfileController extends Controller {
    /**
     * bankfileController constructor.
     */
    public function __construct () { $this->middleware('auth'); }
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index () {
        
        return view('reports.bankfile');
    }
    
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create () {
    }
    
    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request) {
        $this->validate($request, [
            'year' => 'required|not_in:0',
            'month' => 'required|not_in:0',
        ]);
        $month = Carbon::parse($request->month)->month;
        $mon = $request->month;
        $company = company::where('regno', session('company_id'))->first();
        $count = paysheet::where('month', '=', $month)->where('year', '=', $request->year)->where('company_id', session('company_id'))->count();
        $logo = company::where('regno', session('company_id'))->value('logo');
        $year = $request->year;
        if (!$count == 0) {
            $bankfile = paysheet::where([
                ['month', date("m", mktime(0, 0, 0, $month, 1))],
                ['year', $request->year],
                ['approvalStatus', true],
                ['company_id', session('company_id')]
            ])->get();
            
            return view('reports.bankfile_report', compact('bankfile', 'company', 'logo', 'year', 'month', 'mon'));
        }
    }
    
    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id) {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id) {
        //
    }
    
    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id) {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id) {
        //
    }
    
    public function downloadBankFileText (Request $request) {
        $company = company::where('regno', session('company_id'))->first();
        $bankfile = paysheet::where(
            [
                ['month', date("m", mktime(0, 0, 0, $request->month, 1))],
                ['year', $request->year],
                ['approvalStatus', true],
                ['company_id', session('company_id')]
            ])->get();
        $totalAmount = paysheet::where(
            [
                ['month', date("m", mktime(0, 0, 0, $request->month, 1))],
                ['year', $request->year],
                ['approvalStatus', true],
                ['company_id', session('company_id')]
            ])->sum('netPay');
        $totalAccNo = Employee::where([['company_id', auth()->user()->company_id]])->sum('account_no');
        $data=null;
        if(!empty($bankfile)){
            $data .= substr($company->name,0, 20);
            $data .= str_pad(round($totalAmount) ."00",11,"0",STR_PAD_LEFT);
            $data .= str_pad($company->account_number,12,0,STR_PAD_LEFT);
            $data .= Carbon::parse($request->transaction_date)->format('ymd');
            $data .= str_pad($totalAccNo,14,0,STR_PAD_LEFT);
            $data .= str_pad($bankfile->count(),5,0,STR_PAD_LEFT);
            $data .= $company->bank_code;
            $data .= $company->branch_code;
            $data .= "223";
            $data .= "\r\n";
            foreach ($bankfile as $key=>$value){
                $data .= str_pad($key+1, 8, "0", STR_PAD_LEFT);
                $data .= str_pad(substr(Employee::where('user_id',$value->emp_id)->first()->name_with_initials,0,20), 20, " ", STR_PAD_RIGHT);;
                $data .= Employee::where('user_id',$value->emp_id)->first()->bank_code;
                $data .= Employee::where('user_id',$value->emp_id)->first()->branch_code;
                $data .= Employee::where('user_id',$value->emp_id)->first()->account_no;
                $data .= '023';
                $data .= str_pad(round($value->netPay,0). "00", 11, 0, STR_PAD_LEFT);
                $data .= Carbon::parse('2019')->format('y');
                $data .= '09';//date("m", mktime(0, 0, 0, $request->month, 1));
                $data .= '23';//$company->salaryDate+1 ;
                $data .= 'Sal ' .date("M", mktime(0, 0, 0, $request->month, 1))."'".Carbon::parse('2019')->format('y');
                $data .= "\r\n";
            }
            $data .= "00000000000000000000000000000000000000000000000000000000000000000000000000000000";
        }
        $file = '/public/files/bank_file/'.Carbon::now()->toDateTimeString().str_replace(" ", "_", $company->name). '_bank_file.txt';
        Storage::disk('local')->put($file, $data);
        return Storage::download($file);
    }
}
