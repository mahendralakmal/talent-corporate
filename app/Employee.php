<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    public function salary(){
        return $this->hasOne('App\Salary','employee_id','user_id');
    }

    public function company(){
        return $this->belongsTo('App\Company','regno','company_id');
    }
    
    public function attend(){
        return $this->hasMany('App\Employee_Attendance','user_id','user_id')->where('companyId', session('company_id'));
    }
    
    public function allowance(){
        return $this->hasOne('App\Allowance','company_id','company_id');
    }
    
    public function myShift(){
        return $this->hasOne('App\Shift','id','shift');
    }
    
    public function departments(){
        return $this->hasOne('App\Department','id','department');
    }
    
    public function leaves(){
        return $this->hasMany('App\Leave', 'user_id', 'user_id');
    }
    
    public function leaveBalance(){
        return $this->hasMany('App\Leave_balance', 'user_id', 'user_id');
    }
}
