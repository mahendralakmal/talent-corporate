<?php

namespace App\Imports;

use App\Employee_Attendance;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AttendanceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Employee_Attendance([
            'user_id' => $row['user_id'],
            'uin' => Carbon::parse($row['uin'])->timestamp,
            'uout' => Carbon::parse($row['uout'])->timestamp,
            'today' => Carbon::parse($row['today'])->format('Y-m-d'),
            'companyId' => session('company_id'),
            'uin_string' => Carbon::parse($row['uin'])->format('h:m:i a'),
            'uout_string' => Carbon::parse($row['uout'])->format('h:m:i a')
        ]);
    }
    
    public function batchSize(): int
    {
        $size = env('IMPORT_BATCH_SIZE', 100);
        return $size;
    }
    
    public function chunkSize(): int
    {
        $size = env('IMPORT_CHUNK_SIZE', 100);
        return $size;
    }
}
