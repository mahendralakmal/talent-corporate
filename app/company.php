<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class company extends Model
{
    public function allowance(){
        return $this->hasOne('App\Allowance','company_id','regno');
    }
    
    public function employees(){
        return $this->hasMany('App\Employee','company_id', 'regno');
    }
    
    public function employeesWithoutResign(){
        return $this->hasMany('App\Employee','company_id', 'regno')->where('date_of_resign', null);
    }
    
    public function getAttendance() {
        return $this->belongsToMany('App\Emplyee', 'attendance','user_id','user_id');
    }
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function departments(){
        return $this->hasMany(Department::class);
    }
    
    public function leaveTypes(){
        return $this->hasMany('App\LeaveType', 'company_id', 'regno');
    }
    
    public function shifts(){
        return $this->hasMany('App\Shift', 'company_id', 'regno');
    }
    
    public function salaries(){
        return $this->hasMany('App\Salary', 'company_id', 'regno');
    }
    
    public function leaveBalance(){
        return $this->hasMany('App\Leave_balance', 'company_id', 'regno');
    }
    
    public function leaves(){
        return $this->hasMany('App\Leave', 'company_id', 'regno');
    }
}
