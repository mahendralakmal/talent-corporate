<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_Attendance extends Model
{
    protected $fillable = ['user_id', 'uin', 'uout', 'today','companyId','uin_string','uout_string'];
    
    protected $table = 'attendances';
    
    public function user(){
        return $this->belongsTo('App\User','user_id','user_id');
    }
    
    public function employee(){
        return $this->belongsTo('App\Employee','user_id','user_id');
    }
}
