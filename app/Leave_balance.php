<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave_balance extends Model
{
    public function employee(){
        return $this->hasMany('App\Employee','user_id','user_id');
    }
    
    public function leaveType(){
        return $this->hasOne('App\LeaveType', 'id', 'leave_type');
    }
}
