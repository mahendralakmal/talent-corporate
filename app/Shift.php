<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    public function company(){
        return $this->hasOne('App\company', 'regno', 'company_id');
    }
}
