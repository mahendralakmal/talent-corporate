<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class employee_creation extends Mailable
{
    use Queueable, SerializesModels;

    var $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company_id)
    {
        $this->id = $company_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->markdown('email.employee_creation', ['email' => $request->contact_email, 'password' => $this->id, 'name' => $request->contact_person])->subject("Talent email")->to($request->contact_email)->from('noreply@client.corporatesolutions.lk');
    }
}
