<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    public function employee(){
        return $this->belongsTo('App\Employee', 'user_id', 'user_id');
    }
    
    public function company(){
        return $this->belongsTo('App\company', 'regno', 'company_id');
    }
    
    public function type(){
        return $this->hasOne('App\LeaveTypes', 'id', 'type');
    }
}
