<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function company(){
        return $this->belongsTo('App\company');
    }
    
    public function employees(){
        return $this->hasMany('App\Employee', 'department', 'department_name');
    }
}
