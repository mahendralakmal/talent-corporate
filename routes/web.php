<?php
    
    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    Route::group(['middleware' => 'prevent-back-history'], function () {
        Auth::routes();
        Route::get('/home', 'HomeController@index');
    });
    
    Route::get('/', 'HomeController@index')->name('home');
    
    Auth::routes();
    
    Route::get('/home', 'HomeController@index')->name('home');


//use Illuminate\Support\Facades\Artisan;
//use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//    return view('auth.login');
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
    
    Route::group(['middleware' => ['role_or_permission:super-admin']], function () {
        
        Route::post('updateRolesPermissions', 'PermissionController@updateRolesPermissions');
        Route::resource('users', 'UserController');
        Route::resource('permissions', 'PermissionController');
        Route::resource('roles', 'RoleController');
        
        
    });
    Route::resource('/companies', 'CompanyController');
    
    
    Route::put('/paysheet/paysheetCancel/{id}', 'paysheetController@paysheetCancel')->name('paysheet.paysheetCancel');
    Route::put('/paysheet/paysheetApprove/{id}', 'paysheetController@paysheetApprove')->name('paysheet.paysheetApprove');
    
    Route::post('/paysheetCancelAll', 'paysheetController@paysheetCancelAll')->name('paysheet.paysheetCancelAll');
    Route::put('/paysheetCancelAll', 'paysheetController@paysheetCancelAll')->name('paysheet.paysheetCancelAll');
    
    Route::post('/paysheetApproveAll', 'paysheetController@paysheetApproveAll')->name('paysheet.paysheetApproveAll');
    Route::put('/paysheetApproveAll', 'paysheetController@paysheetApproveAll')->name('paysheet.paysheetApproveAll');
    
    /*------------------------------------------------------------------*/
    /*                      System Routes
    /*------------------------------------------------------------------*/
    Route::post('/attendances/generateAttendanceReport', 'AttendanceController@generateAttendanceReport')->name('attendance.generateAttendanceReport');
    Route::get('/attendances/getAttendanceReport', 'AttendanceController@getAttendanceReport')->name('attendance.getAttendanceReport');
    Route::get('/attendances/getSampleAttendance', 'AttendanceController@getSampleAttendance')->name('getSampleAttendance');
    
    Route::post('/employees/image/{id}', 'employeeController@profile')->name('employees.profile');
    Route::get('/employees/getbranches', 'employeeController@getbranches')->name('employees.getbranches');
    Route::get('/employee-list/{id}', 'employeeController@emp_list')->name('employees.emp_list');
    Route::get('/employees/filter', 'employeeController@filter')->name('employees.filter');
    Route::get('/employees/export', 'employeeController@export')->name('employees.export');
    Route::get('/employees/profile', 'employeeController@emp_profile')->name('employees.emp_profile');
    Route::resource('/employees', 'employeeController');
    Route::post('/employees/saveEmployee', 'employeeController@store')->name('saveEmployee');
    Route::post('/employees/updateEmployee', 'employeeController@update')->name('updateEmployee');
    Route::post('selected/company', 'HomeController@setCompany')->name('setCompany');
    
    Route::post('/leaves_balance/getBalance', 'LeaveBalanceController@getBalance')->name('leaves_balance.getBalance');
    Route::resource('/leaves', 'LeaveController');
    Route::resource('/leaves_balance', 'LeaveBalanceController');
    
    Route::post('/payroll-approval', 'SalaryController@approve')->name('payroll.approve');
    
    Route::get('/payroll-approval', 'SalaryController@payrole_approval')->name('payroll.approval');
    
    Route::resource('/salary', 'SalaryController');
    
    Route::resource('/events', 'EventsController');
    
    Route::post('/paysheet/print', 'paysheetController@print_sheet')->name('paysheet.print');
    
    Route::post('/paysheet/payrole_with_file', 'paysheetController@payrole_with_file')->name('paysheet.payrole_with_file');
    
    Route::resource('/paysheet', 'paysheetController');
    
    Route::resource('/payslip', 'payslipController');
    
    Route::get('/employee-payslip', 'payslipController@employee_payslip')->name('payslip.employee_payslip');
    
    Route::resource('/payadvice', 'payadviceController');
    
    Route::resource('/bankfile', 'bankfileController');
    
    Route::resource('/epfcform', 'epfcformController');
    
    Route::resource('/wht', 'whtController');
    
    Route::get('importExport', 'AttendanceController@importExport');
    
    Route::post('/paysheet/payRole', 'AttendanceController@payRole')->name('attendance.payRole');
    
    Route::get('downloadExcel/{type}', 'AttendanceController@downloadExcel');
    
    Route::post('importExcel', 'AttendanceController@importExcel');
    
    Route::resource('/attendances', 'AttendanceController');
    
    Route::resource('/employee_loans', 'LoanController');
    
    Route::get('/department', 'SettingController@department')->name('settings.department');
    
    Route::post('/department', 'SettingController@store_department')->name('settings.store_department');
    
    Route::get('/department_edit', 'SettingController@edit_department')->name('settings.edit_department');
    
    Route::put('/department_update', 'SettingController@update_department')->name('settings.update_department');
    
    Route::delete('/department_delete/{id}', 'SettingController@delete_department')->name('settings.delete_department');
    
    Route::get('/theme', 'SettingController@theme')->name('settings.theme');
    
    Route::put('/companies/logo_update/{id}', 'CompanyController@logo_update')->name('companies.logo_update');
    
    Route::put('/companies/bank_update/{id}', 'CompanyController@bank_update')->name('companies.bank_update');
    
    Route::put('/companies/tax_details_update/{id}', 'CompanyController@tax_details_update')->name('companies.tax_details_update');
    
    Route::put('/companies/profile_conatcts_update/{id}', 'CompanyController@profile_conatcts_update')->name('companies.profile_conatcts_update');
    
    Route::put('/companies/leave_details_update/{id}', 'CompanyController@leave_details_update')->name('companies.leave_details_update');
    Route::post('/companies/assignUserToCompany', 'CompanyController@assignUserToCompany');
    
    Route::get('/clear-cache', function () {
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('config:cache');
        $exitCode = Artisan::call('view:clear');
        $exitCode = Artisan::call('route:clear');
        return 'DONE';
    });
    
    
    Route::get('/storage', function () {
        
        symlink('/home/clientcorporates/ERP/storage/app/public', '/home/clientcorporates/public_html/storage');
        
        echo "Storage Linked";
    });
    
    
    Route::get('/mail', 'MailController@index')->name('Mail.index');
    
    Route::post('/send', 'MailController@send_mail')->name('Mail.send');
    
    Route::post('/bankfile/getBnkFile', 'bankfileController@downloadBankFileText')->name('GetBankFile');
    Route::post('/bankfile/getEpfFile', 'epfcformController@downloadEpfFileText')->name('GetEpfFile');
    Route::post('/bankfile/getEtfFile', 'epfcformController@downloadEtfFileText')->name('GetEtfFile');
    Route::post('/attendances/getAttendance', 'AttendanceController@filterAttendance')->name('GetAttendance');
    
    Route::get('/employees/getReportingManager', 'employeeController@getReportingManager')->name('employees.getReportingManager');
    Route::post('/shifts/destroyShift','CompanyController@destroyShift')->name('destroyShift');
    
    Route::get('/temp/{id}', 'SettingController@temp')->name('settings.temp');

