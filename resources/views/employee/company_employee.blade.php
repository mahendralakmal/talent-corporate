@extends('layouts.app')

@section('title')
    <title>Talent | Employee</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Employee</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <div class="pull-right">
                </div>
            </h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">Emp ID</th>
                    <th class="search">Name</th>
                    <th class="search">E-mail</th>
                    <th class="search">NIC</th>
                    <th class="search">Contact Number</th>
                    <th class="search">Gender</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody id="table">
                @foreach($employees as $employee)

                    <tr>
                        <td>{{ $employee->user_id }}</td>
                        <td>{{ $employee->sname }} {{ $employee->fname }} {{ $employee->lname }}</td>
                        <td>{{ $employee->personal_email }}</td>
                        <td>{{ $employee->nic }}</td>
                        <td>{{ $employee->per_mobileno }}</td>
                        <td>{{ $employee->gender }}</td>
                        <td>

                            @can('view employees')
                                <a class="btn btn-circle btn-info btn-sm" href="{{ route('employees.show', $employee->id) }}" data-toggle="tooltip" title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                            @endcan

                            @can('edit employees')
                                <a class="btn btn-circle btn-success btn-sm" href="{{ route('employees.edit', $employee->id) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            @endcan

                            @can('delete employees')
                                <button class="btn btn-danger btn-sm" data-employeeid="{{$employee->id}}" data-toggle="modal" data-target="#delete"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        <div class="row">

            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>


    <!-- delete modal -->
    <div id="delete" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                </div>
                <h1>Are you Sure ?</h1>

                <form action="{{ route('employees.destroy', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <div class="modal-body">
                        <input type="hidden" name="employee_id" value="" id="employee_id">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')
    <script>
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('employeeid');
            console.log(id);
            var modal = $(this)
            modal.find('#employee_id').val(id);
        });


        $('#to').change(function(event) {
            console.log("inside");
            var fromEmp =  $("#from :selected").val();

            if(from!='Select From'){
                var toEmp = $('#to').val();
                console.log(to);

                console.log(from);

                {{--$.ajax({--}}
                {{--    url:"{{ route('employees.filter') }}",--}}
                {{--    method: "get",--}}
                {{--    data: {from:fromEmp,to:toEmp},--}}
                {{--    datatype: "json",--}}
                {{--    success:function (data) {--}}

                {{--        $('#table').html(data.html);--}}

                {{--    }--}}
                {{--});--}}
            }

        });


    </script>
@endsection
