@extends('layouts.app')

@section('title')
    <title>Talent | View Employee</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Profile</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Employee Profile
                <small class="pull-right">
                    <?php $joinDate = $employee->date_of_join;
                    $parts = explode('.', $joinDate);

                    if($parts<date("Y")){
                    ?>
                    <button class="btn btn-success btn-sm" disabled data-toggle="modal" data-target="#new">Leave</button>
                    <?php }else{ ?>
                    <button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#new">Leave</button>
                    <?php } ?>
                </small>
            </h3>
        </div><!--card-header-->

        <div class="card-body">

            <div class="row">

                <div class="col-3">
                    <!--employee-profile-->
                    <div class="card">
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><img src="{{ url('storage/company/employees/'. $employee->user_img) }}" style="width:180px; border-radius: 50%;"></li>
                                <li class="list-group-item">{{$employee->sname}} {{$employee->fname}} {{$employee->lname}}</li>
                            </ul>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item" style="align:center;">Notice Board</li>
                            </ul>
                        </div>
                    </div>
                </div><!--col-3-->

                <div class="col-9">

                    <!--employee-image-->
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#Persoanl_details" data-toggle="tab">Personal</a></li>

                                @if($employee->spouse_name)
                                    <li class="nav-item"><a class="nav-link" href="#Spouse_details" data-toggle="tab">Spouse</a></li>
                                @endif

                                @if(!$kids->isEmpty())
                                    <li class="nav-item"><a class="nav-link" href="#Kids_details" data-toggle="tab">Kids</a></li>
                                @endif

                                @if(!$salaries->isEmpty())
                                    <li class="nav-item"><a class="nav-link" href="#Salary_details" data-toggle="tab">Salary</a></li>
                                @endif

                                @if(!$banks->isEmpty() || $bank_branches->isEmpty())
                                    <li class="nav-item"><a class="nav-link" href="#Bank_details" data-toggle="tab">Bank</a></li>
                                @endif

                                @if(!$loan->isEmpty())
                                    <li class="nav-item"><a class="nav-link" href="#loan_details" data-toggle="tab">Loans</a></li>
                                @endif

                                <li class="nav-item"><a class="nav-link" href="#doc_details" data-toggle="tab">Documents</a></li>
                                {{--                                <li class="nav-item"><a class="nav-link" href="#attendance" data-toggle="tab">Attendance</a></li>--}}
                                <li class="nav-item"><a class="nav-link" href="#history" data-toggle="tab">History</a></li>
                                <li class="nav-item"><a class="nav-link" href="#attendance" data-toggle="tab">Attendance</a></li>
                            </ul>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">

                                <div class="tab-pane" id="attendance">
{{--                                    {{ App\Employee_Attendance::where('user_id',$employee->user_id)->get() }}--}}

                                    <div class="tab-pane" id="attendance">
                                        <ul class="list-group">
                                            @foreach($attendYears as $aYear)
                                                <li class="list-group-item" data-toggle="collapse"
                                                    data-target="#{{ $aYear->year }}" aria-expanded="false"
                                                    aria-controls="{{ $aYear->year }}">{{ $aYear->year }}</li>
                                                <li class="collapse list-group-item" id="{{ $aYear->year }}">
                                                    <ul class="list-group">
                                                        @foreach($attendMonth as $aMonth)
                                                            @if($aMonth->year === $aYear->year)
                                                                <li class="list-group-item" data-toggle="collapse"
                                                                    data-target="#{{ $aYear->year }}{{ $aMonth->month }}"
                                                                    aria-expanded="false"
                                                                    aria-controls="{{ $aMonth->month }}">{{ $aMonth->month }}</li>
                                                                <li class="collapse list-group-item"
                                                                    id="{{ $aYear->year }}{{ $aMonth->month }}">
                                                                    <table class="table table-striped">
                                                                        <tr>
                                                                            <th>Department</th>
                                                                            {{--                                                                        <th>Year</th>--}}
                                                                            {{--                                                                        <th>Month</th>--}}
                                                                            <th>Date</th>
                                                                            <th>In</th>
                                                                            <th>Out</th>
                                                                        </tr>
                                                                        @foreach($employee->attend as $attendance)
                                                                            @if((\Carbon\Carbon::parse($attendance->today)->format('Y') === $aYear->year) &&
                                                                            (\Carbon\Carbon::parse($attendance->today)->format('F') === $aMonth->month))
                                                                                <tr>
                                                                                    <td>{{ App\Department::findOrFail($employee->department)->department_name }}</td>
                                                                                    {{--                                                                                <td>{{ \Carbon\Carbon::parse($attendance->today)->format('Y') }}</td>--}}
                                                                                    {{--                                                                                <td>{{ \Carbon\Carbon::parse($attendance->today)->format('F') }}</td>--}}
                                                                                    <td>{{ \Carbon\Carbon::parse($attendance->today)->format('Y-m-d') }}</td>
                                                                                    <td>{{ \Carbon\Carbon::parse($attendance->uin_string)->format('H:i:s') }}</td>
                                                                                    <td>{{ \Carbon\Carbon::parse($attendance->uout_string)->format('H:i:s') }}</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    </table>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </li>

                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="history">
                                    <ul class="list-group list-group-flush">

                                        @foreach($employee_history as $history)
                                            @if($history->employee_id == $employee->user_id)
                                                <li class="list-group-item"><i class="fa fa-check" aria-hidden="true"> </i> {{$history->description}} in {{$history->date}}</li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="active tab-pane" id="Persoanl_details">
                                    <div class="form-row">
                                        <div class="col">
                                            <label>Employee ID</label>
                                            <input type="text" readonly="readonly" class="form-control" name="user_id" value="{{ $employee->user_id }}" style="width:40%">
                                        </div>
                                    </div>

                                    <div class="form-row">


                                        <div class="col">
                                            <label>First Name</label>
                                            <input type="text" disabled class="form-control" name="fname" value="{{ $employee->fname }}">
                                        </div>

                                        <div class="col">
                                            <label>Middle Name</label>
                                            <input type="text" disabled class="form-control" name="mname" value="{{ $employee->mname }}">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Last Name</label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->lname }}">
                                        </div>

                                        <div class="col">
                                            <label>Other Names</label>
                                            <input type="text" class="form-control" name="oname" readonly="readonly" placeholder="Other names" value="{{ $employee->oname }}">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Birthday</label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->bday }}">
                                        </div>

                                        <div class="col">
                                            <label>NIC</label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->nic }}">
                                        </div>

                                        <div class="col">
                                            <label>Passport No</label>
                                            <input type="text" class="form-control" name="passNo" readonly="readonly" placeholder="Passport No" value="{{ $employee->passportNo }}" >
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Official E-mail</label>
                                            <input type="email" class="form-control" name="email" readonly="readonly"  value="{{ $employee->off_email }}" placeholder="E-mail">
                                        </div>

                                        <div class="col">
                                            <label>Personal E-mail</label>
                                            <input type="email" class="form-control" name="personal_email" readonly="readonly" placeholder="Personal E-mail" value="{{ $employee->personal_email }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Permenet Address</label>
                                        <div class="form-row">
                                            <div class="col">
                                                <label>Line 1</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('address_line1') ? ' is-invalid' : ''}}" value="{{ $employee->p_line01 }}" name="p_address_line1" placeholder="Address Line 1" >

                                            </div>

                                            <div class="col">
                                                <label>Line 2</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('address_line2') ? ' is-invalid' : ''}}" value="{{ $employee->p_line02 }}" name="p_address_line2" placeholder="Address Line 2" >

                                            </div>

                                            <div class="col">
                                                <label>Street</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('street') ? ' is-invalid' : ''}}"  name="p_street" value="{{ $employee->p_street }}" placeholder="Street" >

                                            </div>
                                        </div>

                                        <div class="form-row">


                                            <div class="col">
                                                <label>City</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('City') ? ' is-invalid' : ''}}" value="{{ $employee->p_city }}" name="c_city" placeholder="City" >

                                            </div>

                                            <div class="col">
                                                <label>Postal Code</label>
                                                <input maxlength="5" readonly="readonly" class="form-control{{$errors->has('p_postalcode') ? ' is-invalid' : ''}}" value="{{ $employee->p_postal_code }}" name="c_postal_code" placeholder="Postal Code" >

                                            </div>

                                            <div class="col">
                                                <label>Country</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('country') ? ' is-invalid' : ''}}" value="{{ $employee->p_country }}" name="p_country" placeholder="Country" >

                                            </div>

                                            <div class="col">
                                                <label>Country Code</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('country_code') ? ' is-invalid' : ''}}" value="{{ $employee->p_country_code }}" name="p_country_code" placeholder="Address" >

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>Current Address</label>
                                        <div class="form-row">
                                            <div class="col">
                                                <label>Line 1</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('address_line1') ? ' is-invalid' : ''}}" value="{{ $employee->c_line01 }}" name="c_address_line1" placeholder="Address Line 1" >

                                            </div>

                                            <div class="col">
                                                <label>Line 2</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('address_line2') ? ' is-invalid' : ''}}" value="{{ $employee->c_line02 }}" name="c_address_line2" placeholder="Address Line 2" >

                                            </div>
                                            <div class="col">
                                                <label>Street</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('street') ? ' is-invalid' : ''}}" value="{{ $employee->c_street }}" name="c_street" placeholder="Street" >

                                            </div>
                                        </div>

                                        <div class="form-row">

                                            <div class="col">
                                                <label>City </label>
                                                <input readonly="readonly" class="form-control{{$errors->has('City') ? ' is-invalid' : ''}}" value="{{ $employee->c_city }}" name="c_city" placeholder="City" >

                                            </div>

                                            <div class="col">
                                                <label>Postal Code </label>
                                                <input maxlength="5" readonly="readonly" class="form-control{{$errors->has('p_postalcode') ? ' is-invalid' : ''}}" value="{{ $employee->c_postal_code }}" name="c_postal_code" placeholder="Postal Code" >

                                            </div>

                                            <div class="col">
                                                <label>Country</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('country') ? ' is-invalid' : ''}}" value="{{ $employee->c_country }}" name="c_country" placeholder="Country" >
                                                @if ($errors->has('country'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('country')}}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="col">
                                                <label>Country Code</label>
                                                <input readonly="readonly" class="form-control{{$errors->has('country_code') ? ' is-invalid' : ''}}" value="{{ $employee->c_country_code }}" name="c_country_code" placeholder="Address" >
                                                @if ($errors->has('country_code'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('country_code')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Official Mobile Number</label>
                                            <input type="text" class="form-control" readonly="readonly" name="officialmobileno"  value="{{ $employee->off_mobileno }}" placeholder="Mobile Number">
                                        </div>

                                        <div class="col">
                                            <label>Personal Home Number</label>
                                            <input type="text" class="form-control" readonly="readonly" name="personalmobileno" placeholder="Personal Number" value="{{ $employee->off_mobileno }}">
                                        </div>

                                        <div class="col">
                                            <label>Home Number</label>
                                            <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $employee->homeno }}" placeholder="Home Number" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col">
                                                <label>Department </label>
                                                <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $employee->department }}" placeholder="Home Number" >
                                            </div>

                                            <div class="col">
                                                <label>Reporting Manager</label>
                                                @foreach($employees as $emp)
                                                    @if($emp->company_id == auth()->user()->company_id )
                                                        @if($emp->user_id ==$employee->reporting_manager)
                                                            <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $emp->fname }} {{ $emp->mname }}" placeholder="Home Number" >
                                                        @endif
                                                    @endif
                                                @endforeach

                                            </div>

                                            <div class="col">
                                                <label>Employee Category</label>
                                                <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $employee->employee_cat }}" placeholder="Home Number" >
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-4">
                                                <label>Electorate </label>
                                                <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $employee->electorate }}" placeholder="Home Number" >


                                            </div>

                                            <div class="col-4">
                                                <label>Religion </label>
                                                <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $employee->religion }}" placeholder="Home Number" >
                                            </div>

                                            <div class="col" >
                                                <label>Skill Category <span class="required">*</span></label>

                                                @foreach($skills as $skill_category)
                                                    @if( $skill_category->cat_id==$employee->skill_cate )
                                                        <input type="text" class="form-control" readonly="readonly" name="homeno"  value="{{ $skill_category->cat_name }}" placeholder="Home Number" >
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Nationality</label>
                                            <input type="text" class="form-control" name="nationality" readonly="readonly" placeholder="Nationality" value="{{ $employee->nationality }}">
                                        </div>

                                        <div class="col">
                                            <label>Civil Status </label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->civil_status }}">
                                        </div>

                                        <div class="col">
                                            <label>Gender</label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->gender }}">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Blood Group</label>
                                            <input type="text" class="form-control" disabled  value="{{ $employee->blod}}">
                                        </div>

                                        <div class="col">
                                            <label>Designation</label>
                                            <input type="text" disabled class="form-control" name="sname" value="{{ $employee->designation }}">
                                        </div>

                                        @foreach($employee_history as $history)
                                            @if($history->employee_id == $employee->user_id)
                                                @if($history->designation == $employee->designation)
                                                    <div class="col">
                                                        <label>Designated Date</label>
                                                        <input type="date" class="form-control" disabled name="designated_date" value="{{ $history->date }}">
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach

                                    </div>

                                    <div class="form-row">
                                        <div class="col-8">
                                            <form action="{{route('employees.profile',$employee->id)}}" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputFile">Profile Image</label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="profile_image" id="exampleInputFile">
                                                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-group">
                                                    <button type="submit" class="btn btn-primary">Upload</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="Spouse_details">
                                    <div class="form-row">
                                        <div class="col">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="spouse_name" readonly="readonly" value="{{ $employee->spouse_name }}" placeholder="Official E-mail">
                                        </div>

                                        <div class="col">
                                            <label>NIC</label>
                                            <input type="text" class="form-control" name="spouse_nic" readonly="readonly" value="{{ $employee->spouse_nic }}" placeholder="Personal E-mail">
                                        </div>

                                        <div class="col">
                                            <label>Gender</label>
                                            <input type="text" class="form-control" name="spouse_nic" readonly="readonly" value="{{ $employee->spouse_gender }}" placeholder="Personal E-mail">

                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Date of Birth</label>
                                            <input type="date" class="form-control" name="spouse_dob" readonly="readonly" value="{{ $employee->spouse_dob }}" placeholder="Official E-mail">
                                        </div>

                                        <div class="col">
                                            <label>Telephone Number</label>
                                            <input type="text" class="form-control" name="spouse_teleNo" readonly="readonly" value="{{ $employee->spouse_teleNo }}" placeholder="Personal E-mail">
                                        </div>

                                        <div class="col">
                                            <label>Blood Group</label>
                                            <input type="text" class="form-control" disabled  value="{{ $employee->spouse_blood}}">
                                            @if ($errors->has('spouse_blood'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('spouse_blood')}}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="Kids_details">
                                    @foreach($kids as $kid)
                                        @if($kid->company_id==$employee->company_id && $kid->employee_id==$employee->user_id)
                                            <div class="form-row">
                                                <div class="col">
                                                    <label>Name</label>
                                                    <input type="text" class="form-control" disabled name="kid1_name" value="{{ $kid->name }}" placeholder="Official E-mail">
                                                </div>

                                                <div class="col">
                                                    <label>Date of Birth</label>
                                                    <input type="date" class="form-control" disabled name="kid1_dob" value="{{ $kid->dob }}" placeholder="Personal E-mail">
                                                </div>

                                                <div class="col">
                                                    <label>Blood Group</label>
                                                    <input type="text" class="form-control" disabled  value="{{ $kid->blood}}">
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="tab-pane" id="Salary_details">
                                    @foreach($salaries as $salary)
                                        @if($salary->company_id==$employee->company_id && $salary->employee_id==$employee->user_id)

                                            <div class="form-row">
                                                <div class="col">
                                                    <label>Basic salary</label>
                                                    <input  type="text"  id="demo" disabled class="form-control" name="basic_salary"  value="{{ $salary->basic_salary }}">
                                                </div>



                                                <div class="col">
                                                    @if($salary->basic_PAYE==1)
                                                        <label>PAYE</label>
                                                        <input type="checkbox" name="paye_calc" checked disabled class="icheckbox_flat-green">
                                                    @else
                                                        <label>PAYE</label>
                                                        <input type="checkbox" name="paye_calc" disabled class="icheckbox_flat-green">

                                                    @endif
                                                </div>

                                                <div class="col">
                                                    @if($salary->basic_EPF==1)
                                                        <label>EPF</label>
                                                        <input type="checkbox" name="epf_calc" checked disabled class="icheckbox_flat-green">
                                                    @else
                                                        <label>EPF</label>
                                                        <input type="checkbox" name="epf_calc" disabled class="icheckbox_flat-green">
                                                    @endif
                                                </div>

                                            </div>


                                            <?php if($allowance->allowance01!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance01}}</label>
                                                    <input type="text" class="form-control" name="allow01" disabled placeholder="{{$allowance->allowance01}}" value="{{$salary->allowance01}}" id="allow1">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance02!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance02}}</label>
                                                    <input type="text" class="form-control" name="allow02" disabled placeholder="{{$allowance->allowance02}}" value="{{$salary->allowance02}}" id="allow2">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance03!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance03}}</label>
                                                    <input type="text" class="form-control" name="allow03" disabled placeholder="{{$allowance->allowance03}}" value="{{$salary->allowance03}}" id="allow3">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance04!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance04}}</label>
                                                    <input type="text" class="form-control" name="allow04" disabled placeholder="{{$allowance->allowance04}}" value="{{$salary->allowance04}}" id="allow0">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance05!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance05}}</label>
                                                    <input type="text" class="form-control" name="allow05" disabled placeholder="{{$allowance->allowance05}}" value="{{$salary->allowance05}}" id="allow5">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance06!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance06}}</label>
                                                    <input type="text" class="form-control" name="allow06" disabled placeholder="{{$allowance->allowance06}}" value="{{$salary->allowance06}}" id="allow6">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance07!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance07}}</label>
                                                    <input type="text" class="form-control" name="allow07" disabled placeholder="{{$allowance->allowance07}}" value="{{$salary->allowance07}}" id="allow7">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance08!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance08}}</label>
                                                    <input type="text" class="form-control" name="allow08" disabled placeholder="{{$allowance->allowance08}}" value="{{$salary->allowance08}}" id="allow8">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance09!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance09}}</label>
                                                    <input type="text" class="form-control" name="allow09" disabled placeholder="{{$allowance->allowance09}}" value="{{$salary->allowance09}}" id="allow9">
                                                </div>
                                            </div>
                                            <?php }
                                            if($allowance->allowance10!=null){ ?>
                                            <div class="form-row">
                                                <div class="col-5">
                                                    <label>{{$allowance->allowance10}}</label>
                                                    <input type="text" class="form-control" name="allow10" disabled placeholder="{{$allowance->allowance10}}" value="{{$salary->allowance10}}" id="allow10">
                                                </div>
                                            </div>
                                            <?php } ?>
                                            @break;
                                        @endif
                                    @endforeach
                                </div>

                                <div class="tab-pane" id="Bank_details">

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Bank name</label>
                                            @foreach($banks as $bank)
                                                @if($bank->bank_code==$employee->bank_code)
                                                    <input type="text" class="form-control" disabled name="account_no" value="{{ $bank->bank_name }}" >
                                                @endif
                                            @endforeach
                                        </div>

                                        <div class="col">
                                            <label>Branch name</label>
                                            @foreach($bank_branches as $bank_branch)
                                                @if($bank_branch->bank_code == $employee->bank_code)
                                                    @if($bank_branch->branch_code == $employee->branch_code)
                                                        <input type="text" class="form-control" disabled name="account_no" value="{{ $bank_branch->bank_branch }}" >
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <label>Account Number</label>
                                            <input type="text" class="form-control" disabled name="account_no" value="{{ $employee->account_no }}" >
                                        </div>

                                        <div class="col">
                                            <label>Name as for bank</label>
                                            <input type="text" class="form-control" name="bankaccount_name" readonly="readonly" placeholder="Account Name" value="{{ $employee->account_name }}" >
                                            @if ($errors->has('bankaccount_name'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('bankaccount_name')}}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="loan_details">
                                    <table class="table table-bordered table-striped" id="example1">
                                        <thead>
                                        <tr>
                                            <td>Description</td>
                                            <td>Amount</td>
                                            <td>Installments</td>
                                            <td>Duration</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($loan as $loanX)
                                            <tr>
                                                @if($loanX->company_id == auth()->user()->company_id)
                                                    @if($loanX->user_id == $employee->user_id)

                                                        <td>{{$loanX->loan_type}}</td>
                                                        <td>{{$loanX->loan_total}}</td>
                                                        <td>Rs .{{$loanX->monthly_installment}} x {{$loanX->no_installments}}</td>
                                                        <td>{{$loanX->start_date}} - {{$loanX->end_date}}</td>
                                                        <td>
                                                            @if($loanX->no_installments == $loanX->payed_installments)
                                                                <span class="badge badge-success">Paid</span>
                                                            @else
                                                                <span class="badge badge-warning">Pending</span>
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>

                                <div class="tab-pane" id="doc_details">

                                    <ul class="list-group">

                                        @if($employee->nic_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                NIC
                                                <a href="../storage/company/employees/{{$employee->nic_copy}}" download="{{$employee->nic_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->passport_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Passport
                                                <a href="../storage/company/employees/{{$employee->passport_copy}}" download="{{$employee->passport_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->spouse_nic_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                spouse NIC
                                                <a href="../storage/company/employees/{{$employee->spouse_nic_copy}}" download="{{$employee->spouse_nic_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->joining_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                joining letter
                                                <a href="../storage/company/employees/{{$employee->joining_copy}}" download="{{$employee->joining_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->account_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Bank Account
                                                <a href="../storage/company/employees/{{$employee->account_copy}}" download="{{$employee->account_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->agreement)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Agreement
                                                <a href="../storage/company/employees/{{$employee->agreement}}" download="{{$employee->agreement}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->dob_copy)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Birth Certificate
                                                <a href="../storage/company/employees/{{$employee->dob_copy}}" download="{{$employee->dob_copy}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->additional_doc1)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Additional Document 1
                                                <a href="../storage/company/employees/{{$employee->additional_doc1}}" download="{{$employee->additional_doc1}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->additional_doc2)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Additional Document 2
                                                <a href="../storage/company/employees/{{$employee->additional_doc2}}" download="{{$employee->additional_doc2}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                        @if($employee->additional_doc3)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Additional Document 3
                                                <a href="../storage/company/employees/{{$employee->additional_doc3}}" download="{{$employee->additional_doc3}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </li>
                                        @endif

                                    </ul>

                                </div>

                            </div>
                        </div>

                        <div class="card-footer">
                            <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        </div>
                    </div>

                </div><!--col-9-->

            </div><!--row-->

        </div><!--card-body-->

    </div>

    <!-- create model -->
    <div class="modal fade" id="new" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add a new Leave</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('leaves.store')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        @include('leave.form')
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add Leave</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end create model -->
@endsection
