@extends('layouts.app')

@section('title')
    <title>Talent | Skills & Talents</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Employee</a></li>
    <li class="breadcrumb-item active">Skills and talents</li>
@endsection

@section('content')
<div class="row justify-content-center">
<div class="col-md-9">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><b>Skills and talents</b></h3>
        </div><!--card-header-->

        <div class="card-body">
        <form action="{{ route('employee_skills.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                
                <div class="form-row">
                    <div class="col">
                        <label>Employee ID</label>
                        <select class="custom-select" name="emp_id">
                            <option selected>Select the Employee</option>
                            @foreach($employees as $employee)
                                @if($employee->company_id == auth()->user()->company_id )
                                    <option value="{{ $employee->user_id }}">{{ $employee->sname }}({{ $employee->user_id }})</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <label>Skill type</label>
                        <select class="custom-select" name="skill_type">
                            <option selected>Select the skill type</option>
                            <option value="Academic" >Academic</option>
                            <option value="Non-Academic">Non-Academic</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label>Skill or talent</label>
                        <input type="text" class="form-control" name="skill" placeholder="Skill or talent">
                    </div>
               </div>
               <div class="form-row">
                    <div class="col">
                        <label>Description</label>
                        <textarea class="form-control" rows="3" name="desp" placeholder="Description" ></textarea>
                
                    </div>
               </div>


                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Add</button>
                </div><!--modal-footer-->

            </form>
            </div>


        </div><!--card-body -->
    </div>
</div>
</div>
@endsection
