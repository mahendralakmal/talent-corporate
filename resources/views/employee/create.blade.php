@extends('layouts.app')

@section('title')
    <title>Talent | New Employee</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Employee</a></li>
    <li class="breadcrumb-item active">New Employee</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <form action="{{ route('saveEmployee') }}" id="formEmployeeDetails" enctype="multipart/form-data"
                      method="POST">
                    {{ csrf_field() }}
                    <h3>Personal Details</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col-2">
                                <div class="form-group">
                                    <input type="hidden" name="company_id" id="company_id"
                                           value="{{auth()->user()->company_id}}">
                                    <label>Employee ID <span class="required">*</span></label>

{{--                                    {{ dd(count($employees)) }}--}}
                                    @if((isset($employees)) && (!empty($employees)))
                                        <input type="text" required
                                               class="form-control{{$errors->has('user_id') ? ' is-invalid' : ''}}"
                                               name="user_id" value="{{ (count($employees) > 0)?$employees->last()->user_id + 1:1 }}">
                                    @endif
                                    @if ($errors->has('user_id'))
                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('user_id')}}</strong>
                                                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>EPF No. <span class="required">*</span></label>
                                    @if((isset($employees)) && (!empty($employees)))
                                    <input type="text" required
                                           class="text-md-right form-control{{$errors->has('epf_no') ? ' is-invalid' : ''}}"
                                           name="epf_no" maxlength="5" value="{{ (count($employees) > 0)?$employees->last()->user_id + 1:1 }}">
                                    @endif

                                @if ($errors->has('epf_no'))
                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('epf_no')}}</strong>
                                                                            </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Title <span class="required">*</span></label>
                                    <select class="form-control{{$errors->has('title') ? ' is-invalid' : ''}}"
                                            required
                                            name="title">
                                        <option selected value="">Select Title</option>
                                        <option value="Mr." @if("Mr."==old('title')) selected @endif>Mr.</option>
                                        <option value="Mrs." @if("Mrs."==old('title')) selected @endif>Mrs.</option>
                                        <option value="Ms." @if("Ms."==old('title')) selected @endif>Ms.</option>
                                        <option value="Dr." @if("Dr."==old('title')) selected @endif>Dr.</option>
                                    </select>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('title')}}</strong>
                                                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>First Name <span class="required">*</span></label>
                                    <input type="text" required
                                           class="noSpace form-control {{$errors->has('fname') ? ' is-invalid' : ''}}"
                                           name="fname" value="{{old('fname')}}" placeholder="First Name">
                                    @if ($errors->has('fname'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('fname')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">

                                <div class="form-group">
                                    <label>Last Name <span class="required">*</span></label>
                                    <input type="text" required
                                           class="noSpace form-control {{$errors->has('lname') ? ' is-invalid' : ''}}"
                                           name="lname" value="{{old('lname')}}" placeholder="Last Name">
                                    @if ($errors->has('lname'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('lname')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Full Name <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('full_name') ? ' is-invalid' : ''}}"
                                           value="{{old('full_name')}}" name="full_name" placeholder="Full name">
                                    @if ($errors->has('full_name'))
                                        <span class="invalid-feedback" role="alert">
                                                                                                <strong>{{ $errors->first('full_name')}}</strong>
                                                                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Name With Initials <span class="required">*</span>   </label>
                                    <input type="text"
                                           class="form-control {{$errors->has('name_with_initials') ? ' is-invalid' : ''}}"
                                           name="name_with_initials" value="{{old('name_with_initials')}}"
                                           placeholder="Name With Initials">
                                    @if ($errors->has('name_with_initials'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('name_with_initials')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <label>Permenet Address</label>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Line 1 <span class="required">*</span></label>
                                    <input required
                                           class="form-control{{$errors->has('p_address_line1') ? ' is-invalid' : ''}}"
                                           name="p_address_line1" value="{{old('p_address_line1')}}"
                                           id="p_address_line1"
                                           placeholder="Address Line 1">
                                    @if ($errors->has('p_address_line1'))
                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('p_address_line1')}}</strong>
                                                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Line 2 <span class="required"></span></label>
                                    <input
                                        class="form-control{{$errors->has('p_address_line2') ? ' is-invalid' : ''}}"
                                        name="p_address_line2" value="{{old('p_address_line2')}}"
                                        id="p_address_line2"
                                        placeholder="Address Line 2">
                                    @if ($errors->has('p_address_line2'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_address_line2')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Street <span class="required"></span></label>
                                    <input class="form-control{{$errors->has('p_street') ? ' is-invalid' : ''}}"
                                           name="p_street"
                                           id="p_street" value="{{old('p_street')}}" placeholder="Street">
                                    @if ($errors->has('p_street'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_street')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>City <span class="required">*</span></label>
                                    <input class="form-control{{$errors->has('p_city') ? ' is-invalid' : ''}}"
                                           name="p_city" required
                                           id="p_city" placeholder="City" value="{{old('p_city')}}">
                                    @if ($errors->has('p_city'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_city')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Postal Code </label>
                                    <input class="form-control{{$errors->has('p_postal_code') ? ' is-invalid' : ''}}"
                                           name="p_postal_code" id="p_postal_code" value="{{old('p_postal_code')}}"
                                           placeholder="Postal Code" maxlength="5">
                                    @if ($errors->has('p_postal_code'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_postal_code')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Country <span class="required">*</span></label>
                                    <select class="form-control{{$errors->has('p_country') ? ' is-invalid' : ''}}"
                                            name="p_country" id="p_country" placeholder="Country" required
                                            value="{{old('fname')}}">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->name}}-{{$country->code}}"
                                                    @if($country->name.'-'.$country->code==old('p_country')) selected @endif>
                                                {{$country->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('p_country'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_country')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Country Code <span class="required">*</span></label>
                                    <input required
                                           class="form-control{{$errors->has('p_country_code') ? ' is-invalid' : ''}}"
                                           name="p_country_code" id="p_country_code" placeholder="Country Code"
                                           value="{{old('p_country_code')}}">
                                    @if ($errors->has('p_country_code'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('p_country_code')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <label>Postal Address
                                <button id="same" type="button" class="btn btn-success btn-sm" data-toggle="tooltip"
                                        data-placement="top" title="Copy Address"><i class="fa fa-clone"
                                                                                     aria-hidden="true"></i>
                                </button>&nbsp;<button id="clear" type="button" class="btn btn-warning btn-sm"
                                                       data-toggle="tooltip" data-placement="top" title="Clear"><i
                                        class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </label>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Line 1 <span class="required">*</span></label>
                                    <input required
                                           class="form-control{{$errors->has('c_address_line1') ? ' is-invalid' : ''}}"
                                           value="{{old('c_address_line1')}}" name="c_address_line1"
                                           id="c_address_line1"
                                           placeholder="Address Line 1">
                                    @if ($errors->has('c_address_line1'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_address_line1')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Line 2 <span class="required"></span></label>
                                    <input
                                        class="form-control{{$errors->has('c_address_line2') ? ' is-invalid' : ''}}"
                                        value="{{old('c_address_line2')}}" name="c_address_line2"
                                        id="c_address_line2"
                                        placeholder="Address Line 2">
                                    @if ($errors->has('c_address_line2'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_address_line2')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Street <span class="required"></span></label>
                                    <input class="form-control{{$errors->has('c_street') ? ' is-invalid' : ''}}"
                                           name="c_street"
                                           value="{{old('c_street')}}" id="c_street" placeholder="Street">
                                    @if ($errors->has('c_street'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_street')}}</strong>
                                                                    </span>
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>City <span class="required">*</span></label>
                                    <input class="form-control{{$errors->has('c_city') ? ' is-invalid' : ''}}"
                                           name="c_city" required
                                           value="{{old('c_city')}}" id="c_city" placeholder="City">
                                    @if ($errors->has('c_city'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_city')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Postal Code </label>
                                    <input class="form-control{{$errors->has('c_postal_code') ? ' is-invalid' : ''}}"
                                           name="c_postal_code" value="{{old('c_postal_code')}}" id="c_postal_code"
                                           placeholder="Postal Code" maxlength="5">
                                    @if ($errors->has('c_postal_code'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_postal_code')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Country <span class="required">*</span></label>
                                    <select class="form-control{{$errors->has('c_country') ? ' is-invalid' : ''}}"
                                            name="c_country" id="c_country" value="{{old('fname')}}" required
                                            placeholder="Country">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->name}}-{{$country->code}}"
                                                    @if($country->name.'-'.$country->code==old('c_country')) selected @endif>
                                                {{$country->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('c_country'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_country')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label>Country Code <span class="required">*</span></label>
                                    <input required
                                           class="form-control{{$errors->has('c_country_code') ? ' is-invalid' : ''}}"
                                           name="c_country_code" value="{{old('c_country_code')}}" id="c_country_code"
                                           placeholder="Country Code">
                                    @if ($errors->has('c_country_code'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('c_country_code')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <label>Nationality <span class="required">*</span></label>
                                    <input type="text" required
                                           class="form-control{{$errors->has('nationality') ? ' is-invalid' : ''}}"
                                           name="nationality" value="{{old('nationality')}}"
                                           placeholder="Nationality">
                                    @if ($errors->has('nationality'))
                                        <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $errors->first('nationality')}}</strong>
                                                                  </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>National Identity Card No <span class="required">*</span></label>
                                    <input type="text" required maxlength="12"
                                           class="form-control{{$errors->has('nic') ? ' is-invalid' : ''}}"
                                           name="nic" placeholder="NIC No" value="{{old('nic')}}">
                                    @if ($errors->has('nic'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('nic')}}</strong>
                                                                     </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">

                                    <label>Passport No</label>
                                    <input type="text" class="form-control" name="passNo" placeholder="Passport No"
                                           value="{{old('passNo')}}">

                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <label>Official E-mail</label>
                                    <input type="email" class="form-control" name="official_email"
                                           placeholder="Official E-mail"
                                           value="{{old('official_email')}}">

                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">

                                    <label>Personal E-mail <span class="required">*</span></label>
                                    <input type="email" required
                                           class="form-control{{$errors->has('personal_email') ? ' is-invalid' : ''}}"
                                           name="personal_email" value="{{old('personal_email')}}"
                                           placeholder="Personal E-mail">
                                    @if ($errors->has('personal_email'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('personal_email')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <label>Official Mobile Number</label>
                                    <input type="text" class="form-control" value="{{old('officialmobileno')}}"
                                           name="officialmobileno" data-inputmask='"mask": "+99 99 999-9999"'
                                           data-mask
                                           value=""
                                           placeholder="Official Number">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">

                                    <label>Personal Mobile Number <span class="required">*</span></label>
                                    <input type="text" required
                                           class="form-control{{$errors->has('personalmobileno') ? ' is-invalid' : ''}}"
                                           value="{{old('personalmobileno')}}" name="personalmobileno"
                                           data-inputmask='"mask": "+99 99 999-9999"' value="" data-mask
                                           placeholder="Personal Number">
                                    @if ($errors->has('personalmobileno'))
                                        <span class="invalid-feedback" role="alert">
                                                                         <strong>{{ $errors->first('personalmobileno')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Home Number <span class="required">*</span></label>
                                    <input type="text" required
                                           class="form-control{{$errors->has('homeno') ? ' is-invalid' : ''}}"
                                           name="homeno" value="{{old('homeno')}}"
                                           data-inputmask='"mask": "+99 99 999-9999"'
                                           data-mask placeholder="Home Number" value="">
                                    @if ($errors->has('homeno'))
                                        <span class="invalid-feedback" role="alert">
                                                                         <strong>{{ $errors->first('homeno')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <div class="form-group">
                                        <label>Birthday <span class="required">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                                                            <span
                                                                                                class="input-group-text"><i
                                                                                                    class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="date" required
                                                   class="form-control{{$errors->has('bday') ? ' is-invalid' : ''}}"
                                                   name="bday" value="{{old('bday')}}" placeholder="Birthday">
                                            @if ($errors->has('bday'))
                                                <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bday')}}</strong>
                                                                       </span>
                                            @endif
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">

                                    <label>Civil Status <span class="required">*</span></label>
                                    <select required
                                            class="form-control{{$errors->has('civil_status') ? ' is-invalid' : ''}}"
                                            name="civil_status">
                                        <option value="">Select Status</option>
                                        <option value="Married"
                                                @if("Married"==old('civil_status')) selected @endif>
                                            Married
                                        </option>
                                        <option value="Not married"
                                                @if("Not married"==old('civil_status')) selected @endif>
                                            Unmarried
                                        </option>
                                    </select>
                                    @if ($errors->has('civil_status'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('civil_status')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Gender <span class="required">*</span></label>
                                    <select class="form-control{{$errors->has('gender') ? ' is-invalid' : ''}}" required
                                            name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="Male" @if("Male"==old('gender')) selected @endif>Male
                                        </option>
                                        <option value="Female" @if("Female"==old('gender')) selected @endif>
                                            Female
                                        </option>
                                        <option value="Other" @if("Other"==old('gender')) selected @endif>Other
                                        </option>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('gender')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <label>Blood Group </label>
                                    <select class="form-control{{$errors->has('blod') ? ' is-invalid' : ''}}"
                                            name="blod">
                                        <option value="">Select Blood Group</option>
                                        <option value="O negative"
                                                @if("O negative"==old('blod')) selected @endif>O
                                            negative
                                        </option>
                                        <option value="O positive"
                                                @if("O positive"==old('blod')) selected @endif >O
                                            positive
                                        </option>
                                        <option value="A negative"
                                                @if("A negative"==old('blod')) selected @endif>A
                                            negative
                                        </option>
                                        <option value="A positive"
                                                @if("A positive"==old('blod')) selected @endif>A
                                            positive
                                        </option>
                                        <option value="B negative"
                                                @if("B negative"==old('blod')) selected @endif>B
                                            negative
                                        </option>
                                        <option value="B positive"
                                                @if("B positive"==old('blod')) selected @endif>B
                                            positive
                                        </option>
                                        <option value="AB negative"
                                                @if("AB negative"==old('blod')) selected @endif>
                                            AB
                                            negative
                                        </option>
                                        <option value="AB positive"
                                                @if("AB positive"==old('blod')) selected @endif>
                                            AB
                                            positive
                                        </option>
                                    </select>
                                    @if ($errors->has('blod'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('blod')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Designation <span class="required">*</span></label>
                                    <input type="text" required
                                           class="form-control{{$errors->has('designation') ? ' is-invalid' : ''}}"
                                           value="{{old('designation')}}" name="designation"
                                           placeholder="Designation">
                                    @if ($errors->has('designation'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('designation')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <div class="form-group">

                                    <label>Date of join <span class="required">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="date" required
                                               class="form-control{{$errors->has('designated_date') ? ' is-invalid' : ''}}"
                                               value="{{old('designated_date')}}" name="designated_date">
                                        @if ($errors->has('designated_date'))
                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('designated_date')}}</strong>
                                                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="col">--}}
                            {{--                                <div class="form-group">--}}

                            {{--                                    <label>Date of Separation <span class="required"></span></label>--}}
                            {{--                                    <div class="input-group">--}}
                            {{--                                        <div class="input-group-prepend">--}}
                            {{--                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>--}}
                            {{--                                        </div>--}}
                            {{--                                        <input type="date"--}}
                            {{--                                               class="form-control{{$errors->has('separated_date') ? ' is-invalid' : ''}}"--}}
                            {{--                                               value="{{old('separated_date')}}" name="separated_date">--}}
                            {{--                                        @if ($errors->has('separated_date'))--}}
                            {{--                                            <span class="invalid-feedback" role="alert">--}}
                            {{--                                                                                <strong>{{ $errors->first('separated_date')}}</strong>--}}
                            {{--                                                                            </span>--}}
                            {{--                                        @endif--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col">--}}
                            {{--                                <div class="form-group">--}}

                            {{--                                    <label>Remarks for separation <span class="required"></span></label>--}}
                            {{--                                    <div class="input-group">--}}
                            {{--                                                                        <textarea name="remarks_for_separation"--}}
                            {{--                                                                                  id="remarks_for_separation" rows="2"--}}
                            {{--                                                                                  class="form-control"></textarea>--}}
                            {{--                                        @if ($errors->has('remarks_for_separation'))--}}
                            {{--                                            <span class="invalid-feedback" role="alert">--}}
                            {{--                                                                                <strong>{{ $errors->first('remarks_for_separation')}}</strong>--}}
                            {{--                                                                            </span>--}}
                            {{--                                        @endif--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">

                                    <label>Department <span class="required">*</span></label>
                                    <select required
                                            class="form-control{{$errors->has('department') ? ' is-invalid' : ''}}"
                                            name="department">
                                        <option value="">Select Department</option>
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}"
                                                    @if($department->department_name==old('department')) selected @endif>
                                                {{$department->department_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('department'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('department')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Reporting Manager <span class="required"></span></label>
                                    <select class="form-control" name="reporting_manager">
                                        <option value="">Select Reporting Manager</option>
                                        @foreach($employees as $employee)
{{--                                            @if($employee->company_id == auth()->user()->company_id )--}}
                                                <option value="{{ $employee->user_id }}"
                                                        @if($employee->user_id==old('reporting_manager')) selected @endif>
                                                    {{
                                                    $employee->user_id }}
                                                    - {{ $employee->name_with_initials }}</option>
{{--                                            @endif--}}
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">

                                    <label>Employee Category <span class="required">*</span></label>
                                    <select required
                                            class="form-control{{$errors->has('employee_cat') ? ' is-invalid' : ''}}"
                                            name="employee_cat">
                                        <option value="">Select Employee Category</option>
                                        <option value="Probation"
                                                @if("Probation"==old('employee_cat')) selected @endif>
                                            Probation
                                        </option>
                                        <option value="Intern"
                                                @if("Intern"==old('employee_cat')) selected @endif>
                                            Intern
                                        </option>
                                        <option value="Permanent"
                                                @if("Permanent"==old('employee_cat')) selected @endif>
                                            Permanent
                                        </option>
                                        <option value="Contract"
                                                @if("Contract"==old('employee_cat')) selected @endif>
                                            Contract
                                        </option>
                                    </select>
                                    @if ($errors->has('employee_cat'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('employee_cat')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <div class="form-group">

                                    <label>Electorate </label>
                                    <select class="form-control{{$errors->has('electorate') ? ' is-invalid' : ''}}"
                                            name="electorate">
                                        <option value="">Select Electorate</option>
                                        @foreach($electorates as $electorate)
                                            <option value="{{$electorate->polling_division}}"
                                                    @if($electorate->polling_division==old('electorate')) selected @endif>
                                                {{$electorate->polling_division}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('electorate'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('electorate')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">

                                    <label>Religion </label>
                                    <select class="form-control{{$errors->has('religion') ? ' is-invalid' : ''}}"
                                            name="religion">
                                        <option value="">Select Religion</option>
                                        <option value="Buddhism"
                                                @if("Buddhism"==old('religion')) selected @endif>
                                            Buddhism
                                        </option>
                                        <option value="Hinduism"
                                                @if("Hinduism"==old('religion')) selected @endif>
                                            Hinduism
                                        </option>
                                        <option value="Islam" @if("Islam"==old('religion')) selected @endif>
                                            Islam
                                        </option>
                                        <option value="Christianity"
                                                @if("Christianity"==old('religion')) selected @endif>
                                            Christianity
                                        </option>
                                    </select>
                                    @if ($errors->has('religion'))
                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('religion')}}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">

                                    <label>Skill Category <span class="required">*</span></label>
                                    <select required
                                            class="form-control{{$errors->has('skill_cat') ? ' is-invalid' : ''}}"
                                            name="skill_cat" id="skill_cat">
                                        <option value="">Select Skill Category</option>
                                        @foreach($skill_categoryies as $skill_category)
                                            <option value="{{ $skill_category->cat_id }}"
                                                    @if($skill_category->cat_id==old('skill_cat')) selected @endif>
                                                {{
                                                $skill_category->cat_name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('skill_cat'))
                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('skill_cat')}}</strong>
                                                                        </span>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            @php
                                $company = App\company::findOrFail(session('company'));
                            @endphp
                            @if($company->shift_apl)
                                <div class="col-4">
                                    <div class="form-group">

                                        <label>Shift <span class="required">*</span></label>
                                        <select required
                                                class="form-control{{$errors->has('shift') ? ' is-invalid' : ''}}"
                                                name="shift">
                                            <option value="">Select Shift</option>
                                            @foreach($company->shifts as $shift)
                                                <option value="{{ $shift->id }}"> {{ $shift->start }} - {{ $shift->end }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('employee_cat'))
                                            <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('employee_cat')}}</strong>
                                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    </fieldset>
                    {{--                    SPOUSE DETAILS --}}
                    <h3>Spouse Details (Optional)</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Name </label>
                                    <input type="text" class="form-control" name="spouse_name"
                                           placeholder="Spouse Name"
                                           value="{{old('spouse_name')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>NIC </label>
                                    <input type="text" class="form-control" name="spouse_nic"
                                           placeholder="Spouse NIC"
                                           value="{{old('spouse_nic')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Gender </label>
                                    <select class="form-control" name="spouse_gender">
                                        <option selected value="">Select Gender</option>
                                        <option value="Male" @if("Male"==old('spouse_gender')) selected @endif>
                                            Male
                                        </option>
                                        <option value="Female" @if("Female"==old('spouse_gender')) selected @endif>
                                            Female
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Date of Birth </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="date" class="form-control" name="spouse_dob"
                                               placeholder="Spouse Birthday" value="{{old('spouse_dob')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Telephone Number </label>
                                    <input type="text" class="form-control" name="spouse_teleNo"
                                           data-inputmask='"mask": "99 999-9999"' data-mask
                                           value="{{old('spouse_teleNo')}}"
                                           placeholder="Spouse Telephone" value="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Blood Group </label>
                                    <select class="form-control" name="spouse_blood">
                                        <option selected value="">Select Blood Group</option>
                                        <option value="O negative"
                                                @if("O negative"==old('spouse_blood')) selected @endif>O
                                            negative
                                        </option>
                                        <option value="O positive"
                                                @if("O positive"==old('spouse_blood')) selected @endif>O
                                            positive
                                        </option>
                                        <option value="A negative"
                                                @if("A negative"==old('spouse_blood')) selected @endif>A
                                            negative
                                        </option>
                                        <option value="A positive"
                                                @if("A positive"==old('spouse_blood')) selected @endif>A
                                            positive
                                        </option>
                                        <option value="B negative"
                                                @if("B negative"==old('spouse_blood')) selected @endif>B
                                            negative
                                        </option>
                                        <option value="B positive"
                                                @if("B positive"==old('spouse_blood')) selected @endif>B
                                            positive
                                        </option>
                                        <option value="AB negative"
                                                @if("AB negative"==old('spouse_blood')) selected @endif>
                                            AB negative
                                        </option>
                                        <option value="AB positive"
                                                @if("AB positive"==old('spouse_blood')) selected @endif>
                                            AB positive
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    {{--                    KIDS DETAILS --}}
                    <h3>Kids Details (Optional)</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Name(kid 01)</label>
                                    <input type="text" class="form-control" name="kid1_name" placeholder="Name"
                                           value="{{old('kid1_name')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="date" class="form-control" name="kid1_dob"
                                               placeholder="Birthday"
                                               value="{{old('kid1_dob')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Blood Group</label>
                                    <select class="form-control" name="kid1_blood">
                                        <option selected value="">Select Blood Group</option>
                                        <option value="O negative"
                                                @if("O negative"==old('kid1_blood')) selected @endif>O
                                            negative
                                        </option>
                                        <option value="O positive"
                                                @if("O positive"==old('kid1_blood')) selected @endif>O
                                            positive
                                        </option>
                                        <option value="A negative"
                                                @if("A negative"==old('kid1_blood')) selected @endif>A
                                            negative
                                        </option>
                                        <option value="A positive"
                                                @if("A positive"==old('kid1_blood')) selected @endif>A
                                            positive
                                        </option>
                                        <option value="B negative"
                                                @if("B negative"==old('kid1_blood')) selected @endif>B
                                            negative
                                        </option>
                                        <option value="B positive"
                                                @if("B positive"==old('kid1_blood')) selected @endif>B
                                            positive
                                        </option>
                                        <option value="AB negative"
                                                @if("AB negative"==old('kid1_blood')) selected @endif>AB
                                            negative
                                        </option>
                                        <option value="AB positive"
                                                @if("AB positive"==old('kid1_blood')) selected @endif>AB
                                            positive
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Name(kid 02)</label>
                                    <input type="text" class="form-control" name="kid2_name"
                                           placeholder="Name"
                                           value="{{old('kid2_name')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="date" class="form-control" name="kid2_dob"
                                               placeholder="Birthday"
                                               value="{{old('kid2_dob')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Blood Group</label>
                                    <select class="form-control" name="kid2_blood">
                                        <option selected value="">Select Blood Group</option>
                                        <option value="O negative"
                                                @if("O negative"==old('kid2_blood')) selected @endif>O
                                            negative
                                        </option>
                                        <option value="O positive"
                                                @if("O positive"==old('kid2_blood')) selected @endif>O
                                            positive
                                        </option>
                                        <option value="A negative"
                                                @if("A negative"==old('kid2_blood')) selected @endif>A
                                            negative
                                        </option>
                                        <option value="A positive"
                                                @if("A positive"==old('kid2_blood')) selected @endif>A
                                            positive
                                        </option>
                                        <option value="B negative"
                                                @if("B negative"==old('kid2_blood')) selected @endif>B
                                            negative
                                        </option>
                                        <option value="B positive"
                                                @if("B positive"==old('kid2_blood')) selected @endif>B
                                            positive
                                        </option>
                                        <option value="AB negative"
                                                @if("AB negative"==old('kid2_blood')) selected @endif>AB
                                            negative
                                        </option>
                                        <option value="AB positive"
                                                @if("AB positive"==old('kid2_blood')) selected @endif>AB
                                            positive
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Name(kid 03)</label>
                                    <input type="text" class="form-control" name="kid3_name"
                                           placeholder="Name"
                                           value="{{old('kid3_name')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="date" class="form-control" name="kid3_dob"
                                               placeholder="Birthday"
                                               value="{{old('kid3_dob')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Blood Group</label>
                                    <select class="form-control" name="kid3_blood">
                                        <option selected value="">Select Blood Group</option>
                                        <option value="O negative"
                                                @if("O negative"==old('kid3_blood')) selected @endif>O
                                            negative
                                        </option>
                                        <option value="O positive"
                                                @if("O positive"==old('kid3_blood')) selected @endif>O
                                            positive
                                        </option>
                                        <option value="A negative"
                                                @if("A negative"==old('kid3_blood')) selected @endif>A
                                            negative
                                        </option>
                                        <option value="A positive"
                                                @if("A positive"==old('kid3_blood')) selected @endif>A
                                            positive
                                        </option>
                                        <option value="B negative"
                                                @if("B negative"==old('kid3_blood')) selected @endif>B
                                            negative
                                        </option>
                                        <option value="B positive"
                                                @if("B positive"==old('kid3_blood')) selected @endif>B
                                            positive
                                        </option>
                                        <option value="AB negative"
                                                @if("AB negative"==old('kid3_blood')) selected @endif>AB
                                            negative
                                        </option>
                                        <option value="AB positive"
                                                @if("AB positive"==old('kid3_blood')) selected @endif>AB
                                            positive
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    {{--                    SALARY DETAILS --}}
                    <h3>Salary Details</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Basic salary <span class="required">*</span></label>
                                    <input type="text" id="basic" required value="{{ old('basic_salary') }}"
                                           class="form-control{{$errors->has('basic_salary') ? ' is-invalid' : ''}}"
                                           name="basic_salary" placeholder="0.00">
                                    @if ($errors->has('basic_salary'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('basic_salary')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-0">
                                <div class="form-check">

{{--                                    {{ $allowance }}--}}
                                    <input type="checkbox" name="epf_calc" class="form-check-input" value="{{old('epf_calc')}}"  @if($allowance->allowance01Epf) checked @endif><label>EPF</label>
                                </div>
                            </div>

                            <div class="col-1">
                                <div class="form-check">

                                    <input type="checkbox" name="paye_calc" class="form-check-input" value="{{old('paye_calc')}}"  @if($allowance->allowance01Epf) checked @endif><label>PAYE</label>
                                </div>
                            </div>
                        </div>
						<?php if($allowance->allowance01 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance01}}</label>
                                    <input type="text" class="form-control" name="allow01"
                                           placeholder="{{$allowance->allowance01}}" id="allow1" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance02 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance02}}</label>
                                    <input type="text" class="form-control" name="allow02"
                                           placeholder="{{$allowance->allowance02}}" id="allow2" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance03 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance03}}</label>
                                    <input type="text" class="form-control" name="allow03"
                                           placeholder="{{$allowance->allowance03}}" id="allow3" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance04 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance04}}</label>
                                    <input type="text" class="form-control" name="allow04"
                                           placeholder="{{$allowance->allowance04}}" id="allow0" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance05 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance05}}</label>
                                    <input type="text" class="form-control" name="allow05"
                                           placeholder="{{$allowance->allowance05}}" id="allow5" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance06 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance06}}</label>
                                    <input type="text" class="form-control" name="allow06"
                                           placeholder="{{$allowance->allowance06}}" id="allow6" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance07 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance07}}</label>
                                    <input type="text" class="form-control" name="allow07"
                                           placeholder="{{$allowance->allowance07}}" id="allow7" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance08 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance08}}</label>
                                    <input type="text" class="form-control" name="allow08"
                                           placeholder="{{$allowance->allowance08}}" id="allow8" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance09 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance09}}</label>
                                    <input type="text" class="form-control" name="allow09"
                                           placeholder="{{$allowance->allowance09}}" id="allow9" value="0">
                                </div>
                            </div>
                        </div>
						<?php }
						if($allowance->allowance10 != null){ ?>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>{{$allowance->allowance10}}</label>
                                    <input type="text" class="form-control" name="allow10"
                                           placeholder="{{$allowance->allowance10}}" id="allow10" value="0">
                                </div>
                            </div>
                        </div>
						<?php } ?>
                    </fieldset>
                    {{--                    BANK DETAILS --}}
                    <h3>Bank Details</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col">
                                <label>Bank name</label>
                                <select class="form-control{{$errors->has('bank_code') ? ' is-invalid' : ''}}"
                                        name="bank_code" id="bank_code">
                                    <option value="">Select Bank Name</option>
                                    @foreach($banks as $bank)
                                        <option value="{{ $bank->bank_code }}"
                                                @if($bank->bank_code==old('bank_code')) selected @endif>{{
                                            $bank->bank_name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('bank_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_code')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Branch name </label>
                                <select class="form-control{{$errors->has('branch_code') ? ' is-invalid' : ''}}"
                                        name="branch_code" id="branch_code">
                                    <option value="">Select Branch Name</option>
                                </select>
                                @if ($errors->has('branch_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('branch_code')}}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label>Account Number</label>
                                <input type="text" value="{{ old('account_no') }}"
                                       class="form-control {{$errors->has('account_no') ? ' is-invalid' : ''}}"
                                       name="account_no" id="account_no" placeholder="Account Number">
                                @if ($errors->has('account_no'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('account_no')}}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Name as for bank</label>
                                <input type="text"
                                       class="form-control{{$errors->has('bankaccount_name') ? ' is-invalid' : ''}}"
                                       value="{{old('bankaccount_name')}}" name="bankaccount_name"
                                       placeholder="Account Name">
                                @if ($errors->has('bankaccount_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bankaccount_name')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {{--                    EMPLOYEE DETAILS --}}
                    <h3>Employee Contract</h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Agreement <span
                                            class="required">*</span></label>
                                    <input type="file" required
                                           class="form-control{{$errors->has('emp_agreement') ? ' is-invalid' : ''}}"
                                           value="{{old('emp_agreement')}}" name="emp_agreement">
                                    @if ($errors->has('emp_agreement'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('emp_agreement')}}</strong>/span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">NIC Copy</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="nic_copy"
                                                   value="{{old('nic_copy')}}">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Birth Certificate</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="birth_copy"
                                                   value="{{old('birth_copy')}}">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Passport Copy</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="passport_copy"
                                                   value="{{old('passport_copy')}}">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Spouse NIC copy</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="spouse_nic_copy"
                                                   value="{{old('spouse_nic_copy')}}">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Bank Account copy</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="bank_copy"
                                                   value="{{old('bank_copy')}}">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Joining Letter </label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file"
                                                   class="form-control{{$errors->has('joining_letter') ? ' is-invalid' : ''}}"
                                                   value="{{old('joining_letter')}}" name="joining_letter">
                                            {{--                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>--}}
                                            @if ($errors->has('joining_letter'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('joining_letter')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Additional Document 1</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="additional_doc1"
                                                   value="{{old('additional_doc1')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Additional Document 2</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="additional_doc2"
                                                   value="{{old('additional_doc2')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputFile">Additional Document 3</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="additional_doc3"
                                                   value="{{old('additional_doc3')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        var form = $("#formEmployeeDetails").show();
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                user_id: {required: true,},
                epf_no: {required: true,},
            }
        });

        $("#formEmployeeDetails").steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "slideLeft",
            autoFocus: true,
            cssClass: "tabcontrol",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                $("#formEmployeeDetails").submit();
            }
        });
    </script>
    <script>
        $('#account_no').blur(function (e) {
            $('#account_no').val(('000000000000' + $('#account_no').val()).slice(-12));
        });

        $('#bank_code').change(function (event) {
            var bank_code = $(this).val();
            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {
                    $('#branch_code').html(data.html);
                }
            });
        });
        $('#bank_code').ready(function (event) {
            var bank_code = $(this).val();
            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {
                    $('#branch_code').html(data.html);
                }
            });
        });

        $('#same').click(function () {
            $("#c_address_line1").val($("#p_address_line1").val());
            $("#c_address_line2").val($("#p_address_line2").val());
            $("#c_street").val($("#p_street").val());
            $("#c_city").val($("#p_city").val());
            $("#c_postal_code").val($("#p_postal_code").val());
            $("#c_country").val($("#p_country").val());
            $("#c_country_code").val($("#p_country_code").val());
        });

        $('#clear').click(function () {
            $("#c_address_line1").val("");
            $("#c_address_line2").val("");
            $("#c_street").val("");
            $("#c_city").val("");
            $("#c_postal_code").val("");
            $("#c_country").val("");
            $("#c_country_code").val("");
        });

        $('#p_country').change(function () {

            var id = $("#p_country :selected").val();

            console.log(id);

            var arr = id.split('-');

            console.log(arr[0]);

            $("#p_country_code").val(arr[1]);
        });

        $('#c_country').change(function () {

            var id = $("#c_country :selected").val();

            console.log(id);

            var arr = id.split('-');

            console.log(arr[0]);

            $("#c_country_code").val(arr[1]);
        });
    </script>
@endsection
