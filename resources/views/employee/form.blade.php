<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label>Company Id</label>
            <input type="text"  class="form-control{{$errors->has('company_id') ? ' is-invalid' : ''}}" name="company_id" value="{{auth()->user()->company_id}}"  readonly="readonly">
            @if ($errors->has('company_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('company_id')}}</strong>
                </span>
            @endif
        </div>

        <div class="col">
            <label>Employee ID</label>
            <input type="text"  class="form-control{{$errors->has('user_id') ? ' is-invalid' : ''}}" name="user_id" value="COM<?php echo(rand(100000,999999)) ?>"  readonly="readonly">
            @if ($errors->has('user_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('user_id')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Surname</label>
            <input type="text" class="form-control{{$errors->has('sname') ? ' is-invalid' : ''}}" name="sname" placeholder="Surname">
            @if ($errors->has('sname'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('sname')}}</strong>
            </span>
            @endif
        </div>

        <div class="col">
            <label>First Name</label>
            <input type="text" class="form-control{{$errors->has('fname') ? ' is-invalid' : ''}}" name="fname" placeholder="First Name">
            @if ($errors->has('fname'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('fname')}}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Last Name</label>
            <input type="text" class="form-control{{$errors->has('lname') ? ' is-invalid' : ''}}" name="lname" placeholder="Last Name">
            @if ($errors->has('lname'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('lname')}}</strong>
                </span>
            @endif
        </div>
        <div class="col">
            <label>Other Names</label>
            <input type="text" class="form-control{{$errors->has('oname') ? ' is-invalid' : ''}}" name="oname" placeholder="Other names">
            @if ($errors->has('oname'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('oname')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Birthday</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
                <input type="date" class="form-control{{$errors->has('bday') ? ' is-invalid' : ''}}" name="bday"  placeholder="Birthday">
                @if ($errors->has('bday'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('bday')}}</strong>
                </span>
                @endif
            </div>
            <!-- /.input group -->
        </div>
        <div class="col">
            <label>Nationl Identity Card No</label>
            <input type="text" class="form-control{{$errors->has('nic') ? ' is-invalid' : ''}}" name="nic" placeholder="NIC No" >
            @if ($errors->has('nic'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('nic')}}</strong>
                </span>
            @endif
        </div>
        <div class="col">
            <label>Passport No</label>
            <input type="text" class="form-control{{$errors->has('passNo') ? ' is-invalid' : ''}}" name="passNo" placeholder="Passport No" >
            @if ($errors->has('passNo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('passNo')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Official E-mail</label>
            <input type="email" class="form-control{{$errors->has('official_email') ? ' is-invalid' : ''}}" name="official_email" placeholder="Official E-mail">
            @if ($errors->has('official_email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('official_email')}}</strong>
                </span>
            @endif
        </div>
        <div class="col">
            <label>Personal E-mail</label>
            <input type="email" class="form-control{{$errors->has('personal_email') ? ' is-invalid' : ''}}" name="personal_email" placeholder="Personal E-mail">
            @if ($errors->has('personal_email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('personal_email')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Official Mobile Number</label>
            <input type="text" class="form-control{{$errors->has('officialmobileno') ? ' is-invalid' : ''}}" name="officialmobileno" data-inputmask='"mask": "(999) 999-9999"' data-mask placeholder="Official Number">
            @if ($errors->has('officialmobileno'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('officialmobileno')}}</strong>
                </span>
            @endif
        </div>
        <div class="col">
            <label>Personal Mobile Number</label>
            <input type="text" class="form-control{{$errors->has('personalmobileno') ? ' is-invalid' : ''}}" name="personalmobileno" data-inputmask='"mask": "(999) 999-9999"' data-mask placeholder="Personal Number" >
            @if ($errors->has('personalmobileno'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('personalmobileno')}}</strong>
                </span>
            @endif
        </div>

        <div class="col">
            <label>Home Number</label>
            <input type="text" class="form-control{{$errors->has('homeno') ? ' is-invalid' : ''}}" name="homeno" data-inputmask='"mask": "(999) 999-9999"' data-mask placeholder="Home Number" >
            @if ($errors->has('homeno'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('homeno')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Nationality</label>
            <input type="text" class="form-control{{$errors->has('nationality') ? ' is-invalid' : ''}}" name="nationality" placeholder="Nationality">
            @if ($errors->has('nationality'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('nationality')}}</strong>
                </span>
            @endif
        </div>
        <div class="col">
            <label>Civil Status</label>
            <select class="custom-select{{$errors->has('civil_status') ? ' is-invalid' : ''}}" name="civil_status">
                <option selected>Select Status</option>
                <option value="Married">Married</option>
                <option value="Not married">Not married</option>
            </select>
            @if ($errors->has('civil_status'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('civil_status')}}</strong>
                </span>
            @endif
        </div>

        <div class="col">
            <label>Gender</label>
            <select class="custom-select{{$errors->has('gender') ? ' is-invalid' : ''}}" name="gender">
                <option selected>Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
            @if ($errors->has('gender'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('gender')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Blood Group</label>
            <select class="custom-select{{$errors->has('blod') ? ' is-invalid' : ''}}" name="blod">
                <option selected>Select Blood Group</option>
                <option value="O negative">O negative</option>
                <option value="O positive">O positive</option>
                <option value="A negative">A negative</option>
                <option value="A positive">A positive</option>
                <option value="B negative">B negative</option>
                <option value="B positive">B positive</option>
                <option value="AB negative">AB negative</option>
                <option value="AB positive">AB positive</option>
            </select>
            @if ($errors->has('blod'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('blod')}}</strong>
                </span>
            @endif
        </div>

        <div class="col">
            <label>Designation</label>
            <input type="text" class="form-control{{$errors->has('designation') ? ' is-invalid' : ''}}" name="designation" placeholder="Designation" >
            @if ($errors->has('designation'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('designation')}}</strong>
                </span>
            @endif
        </div>

        <div class="col">
            <label>Date of join</label>
            <input type="date" class="form-control{{$errors->has('designated_date') ? ' is-invalid' : ''}}" name="designated_date" >
            @if ($errors->has('designated_date'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('designated_date')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label>Address</label>
            <textarea class="form-control{{$errors->has('address') ? ' is-invalid' : ''}}" rows="3" name="address" placeholder="Address" ></textarea>
            @if ($errors->has('address'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('address')}}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

{{--Spouse Details (Optional)--}}
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Spouse Details (Optional)</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top" title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="form-row">
            <div class="col">
                <label>Name</label>
                <input type="text" class="form-control{{$errors->has('spouse_name') ? ' is-invalid' : ''}}" name="spouse_name" placeholder="Spouse Name">
                @if ($errors->has('spouse_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('spouse_name')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>NIC</label>
                <input type="text" class="form-control{{$errors->has('spouse_nic') ? ' is-invalid' : ''}}" name="spouse_nic" placeholder="Spouse NIC">
                @if ($errors->has('spouse_nic'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('spouse_nic')}}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label>Date of Birth</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input type="date" class="form-control{{$errors->has('spouse_dob') ? ' is-invalid' : ''}}" name="spouse_dob" placeholder="Spouse Birthday">
                    @if ($errors->has('spouse_dob'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('spouse_dob')}}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col">
                <label>Telephone Number</label>
                <input type="text" class="form-control{{$errors->has('spouse_teleNo') ? ' is-invalid' : ''}}" name="spouse_teleNo" data-inputmask='"mask": "(999) 999-9999"' data-mask placeholder="Spouse Telephone">
                @if ($errors->has('spouse_teleNo'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('spouse_teleNo')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Blood Group</label>
                <select class="custom-select{{$errors->has('spouse_blood') ? ' is-invalid' : ''}}" name="spouse_blood">
                    <option selected>Select Blood Group</option>
                    <option value="O negative">O negative</option>
                    <option value="O positive">O positive</option>
                    <option value="A negative">A negative</option>
                    <option value="A positive">A positive</option>
                    <option value="B negative">B negative</option>
                    <option value="B positive">B positive</option>
                    <option value="AB negative">AB negative</option>
                    <option value="AB positive">AB positive</option>
                </select>
                @if ($errors->has('spouse_blood'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('spouse_blood')}}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
{{--End Spouse Details (Optional)--}}

{{--Kids Details (Optional)--}}
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Kids Details (Optional)</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top" title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="form-row">
            <div class="col">
                <label>Name(kid 01)</label>
                <input type="text" class="form-control{{$errors->has('kid1_name') ? ' is-invalid' : ''}}" name="kid1_name" placeholder="Name">
                @if ($errors->has('kid1_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid1_name')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Date of Birth</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input type="date" class="form-control{{$errors->has('kid1_dob') ? ' is-invalid' : ''}}" name="kid1_dob" placeholder="Birthday">
                    @if ($errors->has('kid1_dob'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('kid1_dob')}}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col">
                <label>Blood Group</label>
                <select class="custom-select{{$errors->has('kid1_blood') ? ' is-invalid' : ''}}" name="kid1_blood">
                    <option selected>Select Blood Group</option>
                    <option value="O negative">O negative</option>
                    <option value="O positive">O positive</option>
                    <option value="A negative">A negative</option>
                    <option value="A positive">A positive</option>
                    <option value="B negative">B negative</option>
                    <option value="B positive">B positive</option>
                    <option value="AB negative">AB negative</option>
                    <option value="AB positive">AB positive</option>
                </select>
                @if ($errors->has('kid1_blood'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid1_blood')}}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label>Name(kid 02)</label>
                <input type="text" class="form-control{{$errors->has('kid2_name') ? ' is-invalid' : ''}}" name="kid2_name" placeholder="Name">
                @if ($errors->has('kid2_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid2_name')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Date of Birth</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input type="date" class="form-control{{$errors->has('kid2_dob') ? ' is-invalid' : ''}}" name="kid2_dob" placeholder="Birthday">
                    @if ($errors->has('kid2_dob'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('kid2_dob')}}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col">
                <label>Blood Group</label>
                <select class="custom-select{{$errors->has('kid2_blood') ? ' is-invalid' : ''}}" name="kid2_blood">
                    <option selected>Select Blood Group</option>
                    <option value="O negative">O negative</option>
                    <option value="O positive">O positive</option>
                    <option value="A negative">A negative</option>
                    <option value="A positive">A positive</option>
                    <option value="B negative">B negative</option>
                    <option value="B positive">B positive</option>
                    <option value="AB negative">AB negative</option>
                    <option value="AB positive">AB positive</option>
                </select>
                @if ($errors->has('kid2_blood'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid2_blood')}}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label>Name(kid 03)</label>
                <input type="text" class="form-control{{$errors->has('kid3_name') ? ' is-invalid' : ''}}" name="kid3_name" placeholder="Name">
                @if ($errors->has('kid3_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid3_name')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Date of Birth</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input type="date" class="form-control{{$errors->has('kid3_dob') ? ' is-invalid' : ''}}" name="kid3_dob" placeholder="Birthday">
                    @if ($errors->has('kid3_dob'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('kid3_dob')}}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col">
                <label>Blood Group</label>
                <select class="custom-select{{$errors->has('kid3_blood') ? ' is-invalid' : ''}}" name="kid3_blood">
                    <option selected>Select Blood Group</option>
                    <option value="O negative">O negative</option>
                    <option value="O positive">O positive</option>
                    <option value="A negative">A negative</option>
                    <option value="A positive">A positive</option>
                    <option value="B negative">B negative</option>
                    <option value="B positive">B positive</option>
                    <option value="AB negative">AB negative</option>
                    <option value="AB positive">AB positive</option>
                </select>
                @if ($errors->has('kid3_blood'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kid3_blood')}}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
{{--End Kids Details (Optional)--}}

<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Salary Details</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top" title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="form-row">
            <div class="col">
                <label>Basic salary</label>
                <input type="number" class="form-control{{$errors->has('basic_salary') ? ' is-invalid' : ''}}" name="basic_salary" placeholder="Basic salary">
                @if ($errors->has('basic_salary'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('basic_salary')}}</strong>
                    </span>
                @endif
            </div>

            <div class="col">
                <br>
                <div class="checkbox">
                    <label>EPF calculation &nbsp
                        <input type="checkbox" name="epf_calc" >
                    </label>
                </div>

            </div>
        </div>
        <div class="form-row">
            <div class="col" >
                <label>Allowance detail</label>
                <input type="text" class="form-control{{$errors->has('allowancesDetail01') ? ' is-invalid' : ''}}" name="allowancesDetail01" placeholder="Allowance" >
                @if ($errors->has('allowancesDetail01'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesDetail01')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col" >
                <label>Allowance Amount</label>
                <input type="number" class="form-control{{$errors->has('allowancesAmount01') ? ' is-invalid' : ''}}" name="allowancesAmount01" placeholder="Amount" >
                @if ($errors->has('allowancesAmount01'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesAmount01')}}</strong>
                    </span>
                @endif
            </div>

        </div>
        <div class="form-row">

            <div class="col" >
                <label>Allowance detail</label>
                <input type="text" class="form-control{{$errors->has('allowancesDetail02') ? ' is-invalid' : ''}}" name="allowancesDetail02" placeholder="Allowance" >
                @if ($errors->has('allowancesDetail02'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesDetail02')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Allowance Amount</label>
                <input type="number" class="form-control{{$errors->has('allowancesAmount02') ? ' is-invalid' : ''}}" name="allowancesAmount02" placeholder="Amount" >
                @if ($errors->has('allowancesAmount02'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesAmount02')}}</strong>
                    </span>
                @endif
            </div>

        </div>
        <div class="form-row">
            <div class="col">
                <label>Allowance detail</label>
                <input type="text" class="form-control{{$errors->has('allowancesDetail03') ? ' is-invalid' : ''}}" name="allowancesDetail03" placeholder="Allowance" >
                @if ($errors->has('allowancesDetail03'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesDetail03')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Allowance Amount</label>
                <input type="number" class="form-control{{$errors->has('allowancesAmount03') ? ' is-invalid' : ''}}" name="allowancesAmount03" placeholder="Amount" >
                @if ($errors->has('allowancesAmount03'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesAmount03')}}</strong>
                    </span>
                @endif
            </div>


        </div>
        <div class="form-row">

            <div class="col">
                <label>Allowance detail</label>
                <input type="text" class="form-control{{$errors->has('allowancesDetail04') ? ' is-invalid' : ''}}" name="allowancesDetail04" placeholder="Allowance" >
                @if ($errors->has('allowancesDetail04'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesDetail04')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Allowance Amount</label>
                <input type="number" class="form-control{{$errors->has('allowancesAmount04') ? ' is-invalid' : ''}}" name="allowancesAmount04" placeholder="Amount" >
                @if ($errors->has('allowancesAmount04'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('allowancesAmount04')}}</strong>
                    </span>
                @endif
            </div>

        </div>
    </div>
    <!-- /.card-body -->
</div>


<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Bank Details</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top" title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="form-row">
            <div class="col">

                <label>Bank name</label>
                <select class="custom-select{{$errors->has('bank_code') ? ' is-invalid' : ''}}" name="bank_code">
                    <option selected>Select Bank Name</option>
                    @foreach($banks as $bank)
                        <option value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('bank_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bank_name')}}</strong>
                    </span>
                @endif
            </div>

            <div class="col">
                <label>Branch name</label>
                <select class="custom-select{{$errors->has('branch_code') ? ' is-invalid' : ''}}" name="branch_code">
                    <option selected>Select Branch Name</option>
                    @foreach($bank_branches as $bank_branch)
                        @if($bank_branch->bank_code == "GV7787GY7" )
                            <option value="{{ $bank_branch->bank_branch }}">{{ $bank_branch->bank_branch }}</option>
                        @endif
                    @endforeach
                </select>
                @if ($errors->has('branch_code'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('branch_code')}}</strong>
                    </span>
                @endif
            </div>

        </div>

        <div class="form-row">
            <div class="col">
                <label>Account Number</label>
                <input type="text" class="form-control{{$errors->has('account_no') ? ' is-invalid' : ''}}" name="account_no" placeholder="Account Number" >
                @if ($errors->has('account_no'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('account_no')}}</strong>
                    </span>
                @endif
            </div>
            <div class="col">
                <label>Name as for bank</label>
                <input type="text" class="form-control{{$errors->has('bankaccount_name') ? ' is-invalid' : ''}}" name="bankaccount_name" placeholder="Account Name" >
                @if ($errors->has('bankaccount_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bankaccount_name')}}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
