@extends('layouts.app')

@section('title')
    <title>Talent | Employee</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Employee</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <div class="pull-left">

                </div>
                <div class="pull-right">
                    {{--                    @can('create employees')--}}
                    <a href="{{ route('employees.create') }}" class="btn btn-success btn-sm" data-toggle="tooltip"
                       data-placement="top" title="New"><i class="fa fa-plus"></i></a>
                    {{--                    @endcan--}}
                </div>
            </h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">Emp ID</th>
                    <th class="search">Name</th>
                    <th class="search">E-mail</th>
                    {{--                    <th class="search">NIC</th>--}}
                    <th class="search">Contact Number</th>
                    {{--                    <th class="search">Gender</th>--}}
                    <th class="search">Date Of Join</th>
                    <th class="search">Date Of Seperation</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody id="table">
                @if(!is_null($employees))
                    @foreach($employees as $employee)
                        <tr>
                            @if(!is_null($employee->date_of_resign))
                                <td class="resign">{{ $employee->user_id }}</td>
                                <td class="resign">{{ $employee->name_with_initials }}</td>
                                <td class="resign">{{ $employee->personal_email }}</td>
                                <td class="resign">{{ $employee->per_mobileno }}</td>
                                <td class="resign">{{ $employee->date_of_join }}</td>
                                <td class="resign">{{ $employee->date_of_resign }}</td>
                                <td class="resign">

                                    @can('view employees')
                                        <a class="btn btn-circle btn-info btn-sm"
                                           href="{{ route('employees.show', $employee->id) }}" data-toggle="tooltip"
                                           title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('edit employees')
                                        <a class="btn btn-circle btn-success btn-sm"
                                           href="{{ route('employees.edit', $employee->id) }}" data-toggle="tooltip"
                                           title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('delete employees')
                                        <button class="btn btn-danger btn-sm" data-employeeid="{{$employee->id}}"
                                                data-toggle="modal" data-target="#delete"><i class="fa fa-trash"
                                                                                             aria-hidden="true"
                                                                                             data-toggle="tooltip"
                                                                                             title="Delete"></i>
                                        </button>
                                    @endcan
                                    @if($employee->date_of_resign == null)
                                        <button class="btn btn-warning btn-sm" data-employeeid="{{$employee->id}}"
                                                data-toggle="modal" data-target="#separate"><i class="fa fa-sign-out"
                                                                                               aria-hidden="true"
                                                                                               data-toggle="tooltip"
                                                                                               title="Separate"></i>
                                        </button>
                                    @endif
                                </td>
                            @else
                                <td>{{ $employee->user_id }}</td>
                                <td>{{ $employee->name_with_initials }}</td>
                                <td>{{ $employee->personal_email }}</td>
                                <td>{{ $employee->per_mobileno }}</td>
                                <td>{{ $employee->date_of_join }}</td>
                                <td>{{ $employee->date_of_resign }}</td>
                                <td>

                                    @can('view employees')
                                        <a class="btn btn-circle btn-info btn-sm"
                                           href="{{ route('employees.show', $employee->id) }}" data-toggle="tooltip"
                                           title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('edit employees')
                                        <a class="btn btn-circle btn-success btn-sm"
                                           href="{{ route('employees.edit', $employee->id) }}" data-toggle="tooltip"
                                           title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('delete employees')
                                        <button class="btn btn-danger btn-sm" data-employeeid="{{$employee->id}}"
                                                data-toggle="modal" data-target="#delete"><i class="fa fa-trash"
                                                                                             aria-hidden="true"
                                                                                             data-toggle="tooltip"
                                                                                             title="Delete"></i>
                                        </button>
                                    @endcan
                                    @if($employee->date_of_resign == null)
                                        <button class="btn btn-warning btn-sm" data-employeeid="{{$employee->id}}"
                                                data-toggle="modal" data-target="#separate"><i class="fa fa-sign-out"
                                                                                               aria-hidden="true"
                                                                                               data-toggle="tooltip"
                                                                                               title="Separate"></i>
                                        </button>
                                    @endif
                                </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>

            </table>
        </div>

        <div class="row">

            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>


    <div id="separate" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    @php $company = session('company'); @endphp
                    @if(isset($company))
                        <h4>Separation with {{ App\company::find(session('company'))->name }}</h4>
                    @endif
                </div>
                <form action="{{ route('employees.update', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                    <div class="modal-body">
                        <div class="form-group">
                            <lable>Separation Date</lable>
                            <input type="date" id="separation_date" name="separation_date" class="form-control">
                        </div>
                        <div class="form-group">
                            <lable>Remarks</lable>
                            <textarea name="remarks_for_separation" id="remarks_for_separation" cols="20"
                                      class="form-control" rows="5"></textarea>
                        </div>
                        <input type="hidden" name="employee_id" value="" id="employee_id">
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Separate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div id="delete" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                </div>
                <h1>Are you Sure ?</h1>

                <form action="{{ route('employees.destroy', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <div class="modal-body">
                        <input type="hidden" name="employee_id" value="" id="employee_id">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')
    <script>
        $('#separate').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('employeeid');
            console.log(id);
            var modal = $(this)
            modal.find('#employee_id').val(id);
        });

        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('employeeid');
            console.log(id);
            var modal = $(this)
            modal.find('#employee_id').val(id);
        });


        $('#to').change(function (event) {
            console.log("inside");
            var fromEmp = $("#from :selected").val();

            if (from != 'Select From') {
                var toEmp = $('#to').val();
                console.log(to);

                console.log(from);

                {{--$.ajax({--}}
                {{--    url:"{{ route('employees.filter') }}",--}}
                {{--    method: "get",--}}
                {{--    data: {from:fromEmp,to:toEmp},--}}
                {{--    datatype: "json",--}}
                {{--    success:function (data) {--}}

                {{--        $('#table').html(data.html);--}}

                {{--    }--}}
                {{--});--}}
            }

        });


    </script>
@endsection
