@extends('layouts.app')

@section('title')
    <title>Talent | Update Employee</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Employee Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Employee</a></li>
    <li class="breadcrumb-item active">Edit Employee</li>
@endsection

@section('content')

    <div class="card">

        <div class="card-body">
            <form action="{{ route('updateEmployee', $employee->id ) }}" method="POST" id="formEmployeeDetails"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="emp_id" id="emp_id" value="{{ $employee->id }}">
                {{ method_field('PUT') }}

                <h3 class="card-title">Update Employee</h3>
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Employee ID <span class="required">*</span></label>
                                <input type="text" readonly="readonly" class="form-control" name="company_id"
                                       value="{{ $employee->company_id }}" hidden style="width:40%">
                                <input type="text" readonly="readonly"
                                       class="form-control {{$errors->has('user_id') ? ' is-invalid' : ''}}"
                                       name="user_id" value="{{ $employee->user_id }}">
                                @if ($errors->has('user_id'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('user_id')}}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-2">
                                <label>Title <span class="required">*</span></label>
                                <select required class="form-control {{$errors->has('title') ? ' is-invalid' : ''}}"
                                        name="title">
                                    <option value="{{$employee->title}}" selected>{{$employee->title}}</option>
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Ms.">Ms.</option>
                                    <option value="Dr.">Dr.</option>
                                </select>
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>First Name <span class="required">*</span></label>
                                <input type="text" class="noSpace form-control {{$errors->has('fname') ? ' is-invalid' : ''}}"
                                       name="fname" id="fname" value="{{ $employee->fname }}"
                                       placeholder="First Name">
                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fname')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Last Name</label>
                                <input type="text" class="noSpace form-control {{$errors->has('lname') ? ' is-invalid' : ''}}"
                                       name="lname" id="lname" value="{{ $employee->lname }}"
                                       placeholder="Last Name">
                                @if ($errors->has('lname'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lname')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">

                            <div class="col">
                                <label>Full Name </label>
                                <input type="text"
                                       class="form-control {{$errors->has('full_name') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->full_name }}" name="full_name" placeholder="Full name">
                                @if ($errors->has('full_name'))
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('full_name')}}</strong>
                                                        </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Name With Initials</label>
                                <input type="text"
                                       class="form-control {{$errors->has('name_with_initials') ? ' is-invalid' : ''}}"
                                       name="name_with_initials" value="{{ $employee->name_with_initials }}"
                                       placeholder="Name With Initials">
                                @if ($errors->has('name_with_initials'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name_with_initials')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label>Birthday <span class="required">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="date"
                                           class="form-control {{$errors->has('bday') ? ' is-invalid' : ''}}"
                                           name="bday" value="{{ $employee->bday }}">
                                    @if ($errors->has('bday'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('bday')}}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>

                            <div class="col">
                                <label>National Identity Card No <span class="required">*</span></label>
                                <input type="text" class="form-control {{$errors->has('nic') ? ' is-invalid' : ''}}"
                                       name="nic" value="{{ $employee->nic }}" placeholder="NIC No">
                                @if ($errors->has('nic'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('nic')}}</strong>
                                  </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Passport No</label>
                                <input type="text" class="form-control" name="passNo"
                                       placeholder="Passport No" value="{{ $employee->passportNo }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Official E-mail</label>
                                <input type="email" class="form-control" name="official_email"
                                       value="{{ $employee->off_email }}" placeholder="E-mail">
                            </div>
                            <div class="col">
                                <label>Personal E-mail <span class="required">*</span></label>
                                <input type="email"
                                       class="form-control{{$errors->has('personal_email') ? ' is-invalid' : ''}}"
                                       name="personal_email" placeholder="Personal E-mail"
                                       value="{{ $employee->personal_email }}">
                                @if ($errors->has('personal_email'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('personal_email')}}</strong>
                                  </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Permenet Address</label>
                        <div class="row">
                            <div class="col">
                                <label>Line 1 <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('p_address_line1') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->p_line01 }}" name="p_address_line1"
                                       placeholder="Address Line 1">
                                @if ($errors->has('p_address_line1'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('p_address_line1')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Line 2 <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('p_address_line2') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->p_line02 }}" name="p_address_line2"
                                       placeholder="Address Line 2">
                                @if ($errors->has('p_address_line2'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('p_address_line2')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Street <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('p_street') ? ' is-invalid' : ''}}"
                                       name="p_street" value="{{ $employee->p_street }}" placeholder="Street">
                                @if ($errors->has('p_street'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('p_street')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>City <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('p_city') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->p_city }}" name="p_city" placeholder="City">
                                @if ($errors->has('p_city'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('p_city')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Postal Code <span class="required">*</span></label>
                                <input maxlength="5"
                                       class="form-control{{$errors->has('p_postalcode') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->p_postal_code }}" name="p_postal_code"
                                       placeholder="Postal Code">
                                @if ($errors->has('p_postalcode'))
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $errors->first('p_postalcode')}}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Country <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('p_country') ? ' is-invalid' : ''}}"
                                        name="p_country" id="p_country" placeholder="Country">
                                    @foreach($countries as $country)
                                        <option value="{{$country->name}}-{{$country->code}}"
                                                @if($country->name==$employee->p_country) selected @endif >
                                            {{$country->name}}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('p_country'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('p_country')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Country Code <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('p_country_code') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->p_country_code }}" name="p_country_code" id="p_country_code"
                                       placeholder="Address">
                                @if ($errors->has('p_country_code'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('p_country_code')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Postal Address</label>
                        <div class="row">
                            <div class="col">
                                <label>Line 1 <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('c_address_line1') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_line01 }}" name="c_address_line1"
                                       placeholder="Address Line 1">
                                @if ($errors->has('c_address_line1'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('c_address_line1')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Line 2 <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('c_address_line2') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_line02 }}" name="c_address_line2"
                                       placeholder="Address Line 2">
                                @if ($errors->has('c_address_line2'))
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $errors->first('c_address_line2')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Street <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('c_street') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_street }}" name="c_street" placeholder="Street">
                                @if ($errors->has('c_street'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('c_street')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>City <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('c_city') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_city }}" name="c_city" placeholder="City">
                                @if ($errors->has('c_city'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('c_city')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Postal Code <span class="required">*</span></label>
                                <input maxlength="5"
                                       class="form-control{{$errors->has('c_postal_code') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_postal_code }}" name="c_postal_code"
                                       placeholder="Postal Code">
                                @if ($errors->has('c_postal_code'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('c_postal_code')}}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Country <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('c_country') ? ' is-invalid' : ''}}"
                                        name="c_country" id="c_country" placeholder="Country">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->name}}-{{$country->code}}"
                                                @if($country->name==$employee->c_country) selected @endif>
                                            {{$country->name}}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('c_country'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('c_country')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Country Code <span class="required">*</span></label>
                                <input class="form-control{{$errors->has('c_country_code') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->c_country_code }}" name="c_country_code" id="c_country_code"
                                       placeholder="Address">
                                @if ($errors->has('c_country_code'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('c_country_code')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Official Mobile Number <span class="required">*</span></label>
                                <input type="text" class="form-control" name="officialmobileno"
                                       value="{{ $employee->off_mobileno }}" data-inputmask='"mask": "+99 99 999-9999"'
                                       data-mask placeholder="Mobile Number">
                            </div>
                            <div class="col">
                                <label>Personal Home Number <span class="required">*</span></label>
                                <input type="text"
                                       class="form-control {{$errors->has('personalmobileno') ? ' is-invalid' : ''}}"
                                       data-inputmask='"mask": "+99 99 999-9999"' data-mask name="personalmobileno"
                                       placeholder="Personal Number" value="{{ $employee->per_mobileno }}">
                                @if ($errors->has('personalmobileno'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('personalmobileno')}}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Home Number <span class="required">*</span></label>
                                <input type="text" class="form-control {{$errors->has('homeno') ? ' is-invalid' : ''}}"
                                       name="homeno" data-inputmask='"mask": "+99 99 999-9999"' data-mask
                                       value="{{ $employee->homeno }}" placeholder="Home Number">
                                {{$errors->has('homeno') ? ' is-invalid' : ''}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Nationality <span class="required">*</span></label>
                                <input type="text"
                                       class="form-control {{$errors->has('nationality') ? ' is-invalid' : ''}}"
                                       value="{{ $employee->nationality }}" name="nationality"
                                       placeholder="Nationality">
                                @if ($errors->has('nationality'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('nationality')}}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Civil Status <span class="required">*</span></label>
                                <select class="form-control" required name="civil_status">
                                    @if($employee->civil_status === "Married")
                                        <option value="Married" selected>Married</option>
                                        <option value="Not married">Not married</option>
                                    @else
                                        <option value="Married">Married</option>
                                        <option value="Not married" selected>Not married</option>
                                    @endif
                                </select>
                            </div>

                            <div class="col">
                                <label>Gender <span class="required">*</span></label>
                                <input type="text" class="form-control" name="gender"
                                       value="{{ $employee->gender }}" placeholder="Home Number">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Department <span class="required">*</span></label>
                                <select required class="form-control{{$errors->has('department') ? ' is-invalid' : ''}}"
                                        name="department">
                                    @foreach($departments as $department)
                                        <option value="{{$department->id}}"
										        <?php if($employee->department == $department->id){ ?>selected<?php } ?> >
                                            {{$department->department_name}}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('department'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('department')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Reporting Manager <span class="required">*</span></label>
                                <select class="form-control" name="reporting_manager">
                                    <option value="">Select Reporting Manager</option>
                                    @foreach($employees as $emp)
                                        <option value="{{ $emp->user_id }}"
                                                @if($emp->user_id=== $employee->reporting_manager) selected @endif>
                                            {{
                                            $emp->user_id }}
                                            - {{ $emp->name_with_initials }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label>Employee Category <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('employee_cat') ? ' is-invalid' : ''}}"
                                        name="employee_cat">
                                    <option value="Probation"
                                            @if($employee->employee_cat=="Probation") selected @endif >Probation
                                    </option>
                                    <option value="Intern" @if($employee->employee_cat=="Intern") selected @endif>
                                        Intern
                                    </option>
                                    <option value="Permanent" @if($employee->employee_cat=="Permanent") selected @endif>
                                        Permanent
                                    </option>
                                    <option value="Contract" @if($employee->employee_cat=="Contract") selected @endif>
                                        Contract
                                    </option>
                                </select>
                                @if ($errors->has('employee_cat'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('employee_cat')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Electorate <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('blod') ? ' is-invalid' : ''}}"
                                        name="electorate">
                                    @foreach($electorates as $electorate)
                                        <option value="{{$electorate->polling_division}}"
                                                @if($electorate->polling_division==$employee->electorate) selected @endif >
                                            {{$electorate->polling_division}}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('electorate'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('electorate')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col-4">
                                <label>Religion <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('religion') ? ' is-invalid' : ''}}"
                                        name="religion">
                                    <option value="{{$employee->religion}}" selected>{{$employee->religion}}</option>
                                    <option value="Probation">Intern</option>
                                    <option value="Intern">Intern</option>
                                    <option value="Permanent">Permanent</option>
                                    <option value="Contract">Contract</option>
                                </select>
                                @if ($errors->has('religion'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('religion')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col-4">
                                <label>Skill Category <span class="required">*</span></label>
                                <select class="form-control{{$errors->has('skill_cat') ? ' is-invalid' : ''}}"
                                        name="skill_cat" id="skill_cat">

                                    @foreach($skills as $skill_category)
                                        @if( $skill_category->cat_id==$employee->skill_cate )
                                            <option value="{{ $skill_category->cat_id }}"
                                                    selected>{{ $skill_category->cat_name }}</option>
                                        @else
                                            <option
                                                value="{{ $skill_category->cat_id }}">{{ $skill_category->cat_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('skill_cat'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('skill_cat')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Blood Group <span class="required">*</span></label>
                                <input type="text" class="form-control" name="blod"
                                       value="{{ $employee->blod }}" placeholder="Home Number">

                            </div>

                            <div class="col">
                                <label>Designation <span class="required">*</span></label>
                                <input type="text" hidden class="form-control" name="previous_designation"
                                       value="{{ $employee->designation }}" placeholder="Designation">

                                <input type="text"
                                       class="form-control {{$errors->has('designation') ? ' is-invalid' : ''}}"
                                       name="designation" value="{{ $employee->designation }}"
                                       placeholder="Designation">
                                @if ($errors->has('designation'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('designation')}}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col">
                                <label>Date Of Join <span class="required">*</span></label>
                                <input type="text" hidden class="form-control" name="date_of_join"
                                       value="{{ $employee->date_of_join }}" placeholder="Date Of Join">

                                <input type="text"
                                       class="form-control {{$errors->has('date_of_join') ? ' is-invalid' : ''}}"
                                       name="date_of_join" value="{{ $employee->date_of_join }}"
                                       placeholder="Date Of Join">
                                @if ($errors->has('date_of_join'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('date_of_join')}}</strong>
                                </span>
                                @endif
                            </div>

                            @php
                                $company = App\company::findOrFail(session('company'));
                            @endphp
                            @if($company->shift_apl)
                                <div class="col">
                                    <div class="form-group">

                                        <label>Shift <span class="required">*</span></label>
                                        <select required
                                                class="form-control{{$errors->has('shift') ? ' is-invalid' : ''}}"
                                                name="shift">
                                            <option value="">Select Shift</option>
                                            @foreach($company->shifts as $shift)
                                                @if($shift->id === $employee->shift)
                                                    <option value="{{ $shift->id }}" selected> {{ $shift->start }}
                                                        - {{ $shift->end }}</option>
                                                @else
                                                    <option value="{{ $shift->id }}"> {{ $shift->start }}
                                                        - {{ $shift->end }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('employee_cat'))
                                            <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('employee_cat')}}</strong>
                                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @foreach($employee_history as $history)
                                @if($history->employee_id == $employee->user_id)
                                    @if($history->designation == $employee->designation)
                                        <div class="col">
                                            <label>Date of Join <span class="required">*</span></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="date" class="form-control" name="designated_date"
                                                       value="{{ $history->date }}">
                                            </div>
                                        </div>
                                        @break
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>


                </fieldset>

                <h3 class="card-title"><b>Spouse Details</b></h3>

                <fieldset>
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label>Name </label>
                                <input type="text" class="form-control" name="spouse_name"
                                       value="{{ $employee->spouse_name }}">
                            </div>
                            <div class="col">
                                <label>NIC </label>
                                <input type="text" class="form-control" name="spouse_nic"
                                       value="{{ $employee->spouse_nic }}">
                            </div>
                            <div class="col">
                                <label>Gender </label>
								<?php if($employee->spouse_gender == "Male"){?>
                                <select class="form-control{{$errors->has('spouse_gender') ? ' is-invalid' : ''}}"
                                        name="spouse_gender">
                                    <option value="Male" selected>Male</option>
                                    <option value="Female">Female</option>
                                </select>
								<?php }else if ($employee->spouse_gender == "Female"){ ?>
                                <select class="form-control{{$errors->has('spouse_gender') ? ' is-invalid' : ''}}"
                                        name="spouse_gender">
                                    <option selected>Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female" selected>Female</option>
                                </select>
								<?php }else{ ?>
                                <select class="form-control{{$errors->has('spouse_gender') ? ' is-invalid' : ''}}"
                                        name="spouse_gender">
                                    <option selected>Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
								<?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Date of Birth </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="date" class="form-control" name="spouse_dob"
                                           value="{{ $employee->spouse_dob }}">
                                </div>
                            </div>
                            <div class="col">
                                <label>Telephone Number </label>
								<?php if($employee->spouse_teleNo != null){?>
                                <input type="text" class="form-control" name="spouse_teleNo"
                                       value="{{ $employee->spouse_teleNo }}">
								<?php }else{ ?>
                                <input type="text" class="form-control" name="spouse_teleNo" value=""
                                       data-inputmask='"mask": "99 999-9999"' data-mask>
								<?php }?>
                            </div>
                            <div class="col">
                                <label>Blood Group </label>
                                <select class="form-control{{$errors->has('spouse_blood') ? ' is-invalid' : ''}}"
                                        name="spouse_blood">

									<?php
									echo($employee->spouse_blood);
									if($employee->spouse_blood == null){ ?>
                                    <option selected value="">Select Blood Group</option>
									<?php } ?>
                                    <option value="O negative"
									        <?php if($employee->spouse_blood == "O negative"){ ?> selected <?php } ?> >
                                        O negative
                                    </option>
                                    <option value="O positive"
									        <?php  if($employee->spouse_blood == "O positive"){ ?> selected <?php } ?> >
                                        O positive
                                    </option>
                                    <option value="A negative"
									        <?php  if($employee->spouse_blood == "A negative"){ ?> selected <?php } ?> >
                                        A negative
                                    </option>
                                    <option value="A positive"
									        <?php  if($employee->spouse_blood == "A positive"){ ?> selected <?php } ?> >
                                        A positive
                                    </option>
                                    <option value="B negative"
									        <?php  if($employee->spouse_blood == "B negative"){ ?> selected <?php } ?> >
                                        B negative
                                    </option>
                                    <option value="B positive"
									        <?php  if($employee->spouse_blood == "B positive"){ ?> selected <?php } ?> >
                                        B positive
                                    </option>
                                    <option value="AB negative"
									        <?php if($employee->spouse_blood == "AB negative"){ ?> selected <?php } ?> >
                                        AB negative
                                    </option>
                                    <option value="AB positive"
									        <?php  if($employee->spouse_blood == "AB positive"){ ?> selected <?php } ?> >
                                        AB positive
                                    </option>

                                </select> @if ($errors->has('spouse_blood'))
                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('spouse_blood')}}</strong>
                                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>


                <h3 class="card-title"><b>Kids Details (Optional)</b></h3>

                <fieldset>
                    @foreach($kids as $kid)
                        @if($kid->company_id==$employee->company_id && $kid->employee_id==$employee->user_id)
                            <div class="form-group">

                                <div class="row">
                                    <div class="col">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="kid1_name"
                                               value="{{ $kid->name }}" placeholder="Official E-mail">
                                    </div>
                                    <div class="col">
                                        <label>Date of Birth</label>
                                        <input type="date" class="form-control" name="kid1_dob"
                                               value="{{ $kid->dob }}" placeholder="Personal E-mail">
                                    </div>
                                    <div class="col">
                                        <label>Blood Group</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" class="form-control"
                                                   name="kid1_blood" placeholder="Passport No"
                                                   value="{{ $kid->blood }}">
                                        </div>

                                        @if ($errors->has('spouse_blood'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('spouse_blood')}}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                    <div class="from-group">
                        <div class="row">
                            <div class="col">
                                <label>Name</label>
                                <input type="text" class="form-control" name="new_kid_name" placeholder="Name">
                            </div>
                            <div class="col">
                                <label>Date of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="date" class="form-control" name="new_kid_dob"
                                           placeholder="Date of Birth">
                                </div>
                            </div>
                            <div class="col">
                                <label>Blood Group</label>
                                <select class="form-control" name="new_kid_blood">
                                    <option selected>Select Blood Group</option>
                                    <option value="O negative">O negative</option>
                                    <option value="O positive">O positive</option>
                                    <option value="A negative">A negative</option>
                                    <option value="A positive">A positive</option>
                                    <option value="B negative">B negative</option>
                                    <option value="B positive">B positive</option>
                                    <option value="AB negative">AB negative</option>
                                    <option value="AB positive">AB positive</option>
                                </select>
                                @if ($errors->has('spouse_blood'))
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('spouse_blood')}}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>


                <h3 class="card-title"><b>Salary Details</b></h3>

                <fieldset>
                    @if(!empty($salary))
                        <div class="form-group">

                            <div class="row">
                                <div class="col">
                                    <label>Basic salary <span class="required">*</span></label>
                                    <input type="hidden"  class="form-control" name="salaryId"
                                           value="{{ $salary->id }}">

                                    <input type="number" id="basic"
                                           class="form-control{{$errors->has('basic_salary') ? ' is-invalid' : ''}}"
                                           name="basic_salary" value="{{ $salary->basic_salary }}">
                                    @if ($errors->has('basic_salary'))
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('basic_salary')}}</strong>
                                                    </span>
                                    @endif
                                </div>

                                <div class="col">
                                    @if($salary->basic_PAYE==1)
                                        <label>PAYE</label>
                                        <input type="checkbox" name="paye_calc" checked hidden>
                                        <input type="checkbox" checked disabled class="icheckbox_flat-green">
                                    @else
                                        <label>PAYE</label>
                                        <input type="checkbox" name="paye_calc" class="icheckbox_flat-green">

                                    @endif


                                    @if($salary->basic_EPF==1)
                                        <label>EPF</label>
                                        <input type="checkbox" name="epf_calc" checked hidden>
                                        <input type="checkbox" checked disabled class="icheckbox_flat-green">
                                    @else
                                        <label>EPF</label>
                                        <input type="checkbox" name="epf_calc" class="icheckbox_flat-green">

                                    @endif

                                </div>
                            </div>

							<?php if($allowance->allowance01 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance01}}</label>
                                    <input type="text" class="form-control" name="allow01"
                                           placeholder="{{$allowance->allowance01}}"
                                           value="{{$salary->allowance01}}" id="allow1">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance02 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance02}}</label>
                                    <input type="text" class="form-control" name="allow02"
                                           placeholder="{{$allowance->allowance02}}"
                                           value="{{$salary->allowance02}}" id="allow2">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance03 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance03}}</label>
                                    <input type="text" class="form-control" name="allow03"
                                           placeholder="{{$allowance->allowance03}}"
                                           value="{{$salary->allowance03}}" id="allow3">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance04 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance04}}</label>
                                    <input type="text" class="form-control" name="allow04"
                                           placeholder="{{$allowance->allowance04}}"
                                           value="{{$salary->allowance04}}" id="allow0">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance05 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance05}}</label>
                                    <input type="text" class="form-control" name="allow05"
                                           placeholder="{{$allowance->allowance05}}"
                                           value="{{$salary->allowance05}}" id="allow5">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance06 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance06}}</label>
                                    <input type="text" class="form-control" name="allow06"
                                           placeholder="{{$allowance->allowance06}}"
                                           value="{{$salary->allowance06}}" id="allow6">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance07 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance07}}</label>
                                    <input type="text" class="form-control" name="allow07"
                                           placeholder="{{$allowance->allowance07}}"
                                           value="{{$salary->allowance07}}" id="allow7">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance08 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance08}}</label>
                                    <input type="text" class="form-control" name="allow08"
                                           placeholder="{{$allowance->allowance08}}"
                                           value="{{$salary->allowance08}}" id="allow8">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance09 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance09}}</label>
                                    <input type="text" class="form-control" name="allow09"
                                           placeholder="{{$allowance->allowance09}}"
                                           value="{{$salary->allowance09}}" id="allow9">
                                </div>
                            </div>
							<?php }
							if($allowance->allowance10 != null){ ?>
                            <div class="row">
                                <div class="col-5">
                                    <label>{{$allowance->allowance10}}</label>
                                    <input type="text" class="form-control" name="allow10"
                                           placeholder="{{$allowance->allowance10}}"
                                           value="{{$salary->allowance10}}" id="allow10">
                                </div>
                            </div>
							<?php } ?>
                        </div>
                    @endif
                </fieldset>

                <h3 class="card-title"><b>Bank Details</b></h3>

                <fieldset>
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label>Bank name</label>
                                <input type="text" class="form-control" hidden name="previous_bank_code"
                                       value="{{ $employee->bank_code }}">

                                <select class="form-control" name="bank_code" id="bank_code">
                                    @foreach($banks as $bank)
                                        @if($bank->bank_code==$employee->bank_code)
                                            <option value="{{ $bank->bank_code }}" selected
                                                    id="bank_code">{{ $bank->bank_name }}</option>
                                        @else
                                            <option value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col">
                                <label>Branch name</label>
                                <input type="text" class="form-control" hidden name="previous_branch_code"
                                       value="{{ $employee->branch_code }}">

                                <select class="form-control" name="branch_code" id="branch_code">
                                    @foreach($bank_branches as $bank_branch)
                                        @if($bank_branch->bank_code == $employee->bank_code)
                                            @if($bank_branch->branch_code == $employee->branch_code)
                                                <option value="{{ $bank_branch->branch_code }}"
                                                        selected>{{ $bank_branch->bank_branch }}</option>
                                            @else
                                                <option
                                                    value="{{ $bank_branch->branch_code }}">{{ $bank_branch->bank_branch }}</option>
                                            @endif
                                        @endif
                                    @endforeach

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Account Number</label>
                                <input type="text"
                                       class="form-control {{$errors->has('account_no') ? ' is-invalid' : ''}}"
                                       name="account_no" id="account_no" placeholder="Account Number"
                                       value="{{ $employee->account_no }}">
                                @if ($errors->has('account_no'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('account_no')}}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="col">
                                <label>Name as for bank</label>
                                <input type="text"
                                       class="form-control {{$errors->has('bankaccount_name') ? ' is-invalid' : ''}}"
                                       name="bankaccount_name" placeholder="Account Name"
                                       value="{{ $employee->account_name }}">
                                @if ($errors->has('bankaccount_name'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('bankaccount_name')}}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>


    </div><!--card-body -->


@endsection

@section('custom-jquery')
    <script>
        var form = $("#formEmployeeDetails").show();
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                user_id: {required: true,},
                epf_no: {required: true,},
            }
        });

        $("#formEmployeeDetails").steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "slideLeft",
            autoFocus: true,
            cssClass: "tabcontrol",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                $("#formEmployeeDetails").submit();
            }
        });
    </script>

    <script>
        $('#account_no').blur(function (e) {
            $(this).val(('000000000000' + $(this).val()).slice(-12));
        });

        $('#bank_code').change(function (event) {
            var bank_code = $(this).val();
            console.log(bank_code);
            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {
                    $('#branch_code').html(data.html);
                }
            });
        });

        $('#p_country').change(function () {

            var id = $("#p_country :selected").val();

            console.log(id);

            var arr = id.split('-');

            console.log(arr[0]);

            $("#p_country_code").val(arr[1]);
        });

        $('#c_country').change(function () {

            var id = $("#c_country :selected").val();

            console.log(id);

            var arr = id.split('-');

            console.log(arr[0]);

            $("#c_country_code").val(arr[1]);
        });
    </script>
@endsection
