<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--title-->
    @yield('title')
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- title-image -->
    <link rel="icon" href="{{asset('dist/img/favicon.png')}}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme -->
    <link rel="stylesheet" href="{{asset('dist/css/nova.css')}}">

    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">

    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap4.min.css')}}">


    <!--select2-->
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/all.css')}}">

    <!--custom-css-->
    <link rel="stylesheet" href="{{asset('css/modal.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/app.css')}}">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">

    <!-- Font awasome -->
    <script src="https://kit.fontawesome.com/2a731be542.js"></script>

    <!-- brand name -->
    <link href="https://fonts.googleapis.com/css?family=Lato:700,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('dist/css/jquery.steps.css') }}">
    @yield('styles')
</head>
<body class="hold-transition sidebar-mini">

<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- jQuery -->
<div class="wrapper">
    <!-- Create Post Form -->
    <!--navbar-->
    <nav class="main-header navbar navbar-expand bg-blue navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>

            @php   $company = \App\company::where('regno', session('company_id'))->first();
            @endphp
            @role('super-admin|company-admin')
            @php
                if(Auth::user()->getRoleNames()->first() == "super-admin") {
                    $companies = App\company::where('status', '=', true)->get();
                } else {
                    $companies = App\company::where([['status', true],['user_id', Auth::user()->id]])->get();
                }
            @endphp
            @if($companies->count()>1)
                <li class="nav-item dropdown">
                    <span class="nav-link dropdown-toggle" role="button" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" v-pre>Companies</span>
                    <div class="dropdown-menu dropdown-menu-left">

                        @foreach($companies as $key=>$company)
                            <form action="{{ route('setCompany') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="company_id" id="company_id" value="{{ $company->id }}">
                                <button class="dropdown-item">
                                    <img src="{{url('storage/company/'. $company->logo  )}}"
                                         class="rounded-circle"
                                         style="width:35px; height:35px;"> {{ $company->name }}
                                </button>
                            </form>
                        @endforeach

                    </div>
                </li>
            @endif
            @php $regno = session('company_id') @endphp
            @if(isset($regno))
                <li class="nav-item nav-link">
                    <img
                        src="{{url('storage/company/'. App\Company::where('regno',session('company_id'))->first()->logo  )}}"
                        class="rounded-circle loged-user"> {{ App\Company::where('regno',session('company_id'))->first()->name }}
                </li>
            @endif
            @else
                <li class="nav-item d-none d-sm-inline-block">
                    <img src="{{url('storage/company/'. $company->logo  )}}" class="rounded-circle"
                         style="width:35px; height:35px;">
                </li>

                <li class="nav-item d-none d-sm-inline-block">
                    <a class="nav-link">{{$company->name}}</a>
                </li>
                @endrole
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" v-pre>
                    <img src="{{url('storage/company/employees/'. auth()->user()->image )}}"
                         class="img-circle loged-user">
                    {{ Auth::user()->name }}
                    <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    @role('company-admin|employee')
                    <a class="dropdown-item" href="{{route('employees.emp_profile')}}"
                       class="nav-link {{ Request::is('employees/profile') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-user"></i> My Profile
                    </a>
                    <hr>
                    @endrole
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </div>
            </li>

            @role('super-admin')
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                    <i class="fa fa-th-large"></i>
                </a>
            </li>
            @endrole

        </ul>
    </nav>
    <!--End navbar-->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('home') }}" class="brand-link">
            <img src="{{asset('dist/img/mainlogo.png')}}" class="brand-image img-circle">
            <span class="brand-text font-weight-light">
                    <b style="font-family: 'Lato', sans-serif;">{{ config('app.name', 'Laravel') }}</b>
                </span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">

                    <li class="nav-item">
                        <a href="{{ '/home' }}" class="nav-link {{ Request::is('home') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    @can('view employees')
                        <li class="nav-item">
                            <a href="{{ route('employees.index') }}"
                               class="nav-link {{ Request::is('employees', 'employees/create', 'employees/*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user-friends"></i>
                                <p>Employee</p>
                            </a>
                        </li>
                        {{--                    @endrole--}}
                    @endcan
                    @can('view loan')
                        {{--                    @role('super-admin|company-admin')--}}
                        <li class="nav-item">
                            <a href="{{ route('employee_loans.index') }}"
                               class="nav-link {{ Request::is('employee_loans*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                                <p>Loan</p>
                            </a>
                        </li>
                        {{--                    @endrole--}}
                    @endcan
                    @can('view company')
                        {{--                    @role('super-admin')--}}
                        <li class="nav-item">
                            <a href="{{ route('companies.index') }}"
                               class="nav-link {{ Request::is('companies*') ? 'active' : '' }}">
                                <i class="nav-icon fa fa-building"></i>
                                <p>Company</p>
                            </a>
                        </li>
                        {{--                    @endrole--}}
                    @endcan
                    @can('view event')
                        {{--                    @role('super-admin|company-admin')--}}
                        <li class="nav-item">
                            <a href="{{ route('events.index') }}"
                               class="nav-link {{ Request::is('events') ? 'active' : '' }}">
                                <i class="nav-icon fa fa-calendar-check-o"></i>
                                <p>Calendar</p>
                            </a>
                        </li>
                        {{--                    @endrole--}}
                    @endcan


                    {{--                    @can('create company')--}}

                    {{--                    @endcan--}}
                    @can('view attendance')
                        <li class="nav-item">
                            <a href="{{ route('attendances.index')}}"
                               class="nav-link {{ Request::is('attendances') ? 'active' : '' }}">
                                <i class="nav-icon fa fa-check-square-o"></i>
                                <p>Attendance</p>
                            </a>
                        </li>
                    @endcan

                    {{--                    @role('super-admin|company-admin')--}}
                    @can('process payroll')
                        <li class="nav-item has-treeview {{ Request::is('attendances', 'salary', 'payroll-approval') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-comments-dollar"></i>
                                <p>Payroll <i class="right fa fa-angle-left"></i></p>
                            </a>

                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('salary.index')}}"
                                       class="nav-link {{ Request::is('salary') ? 'active' : '' }}">
                                        <i class="nav-icon fas fa-dollar-sign"></i>
                                        <p>Payroll Creation</p>
                                    </a>
                                </li>

                                {{--                            <li class="nav-item">--}}
                                {{--                                <a href="{{ route('attendances.index')}}"--}}
                                {{--                                   class="nav-link {{ Request::is('attendances') ? 'active' : '' }}">--}}
                                {{--                                    <i class="nav-icon fa fa-check-square-o"></i>--}}
                                {{--                                    <p>Attendance</p>--}}
                                {{--                                </a>--}}
                                {{--                            </li>--}}

                                <li class="nav-item">
                                    <a href="{{ route('payroll.approval') }}"
                                       class="nav-link {{ Request::is('payroll-approval') ? 'active' : '' }}">
                                        <i class="nav-icon fa fa-thumbs-up"></i>
                                        <p>Payroll Approvals</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endcan
                    {{--                    @endrole--}}
                    @can('view leave')
                        {{--                    @role('super-admin|company-admin')--}}
                        <li class="nav-item has-treeview {{ Request::is('leaves', 'leaves_balance') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-exchange"></i>
                                <p>Leaves <i class="right fa fa-angle-left"></i></p>
                            </a>

                            <ul class="nav nav-treeview">
                                @can('view leave')
                                    @role('employee')
                                        @if(!auth()->user()->user_id == null)
                                            <li class="nav-item">
                                                <a href="{{route('leaves.show', auth()->user()->user_id)}}"
                                                   class="nav-link {{ Request::is('leaves/*') ? 'active' : '' }}">
                                                    <i class="nav-icon fa fa-exchange"></i>
                                                    <p>Leaves</p>
                                                </a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="nav-item">
                                            <a href="{{route('leaves_balance.index')}}"
                                               class="nav-link {{ Request::is('leaves_balance') ? 'active' : '' }}">
                                                <i class="fas fa-weight nav-icon"></i>
                                                <p>Leave Balance</p>
                                            </a>
                                        </li>
                                    @endrole
                                @endcan
                                @can('approve leave')
                                    <li class="nav-item">
                                        <a href="{{route('leaves.index')}}"
                                           class="nav-link {{ Request::is('leaves') ? 'active' : '' }}">
                                            <i class="fas fa-align-justify nav-icon"></i>
                                            <p>Leave Creation</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                        {{--                    @endrole--}}
                    @endcan

                    {{--                    @role('super-admin|company-admin')--}}
                    @can('view reports')
                        <li class="nav-item has-treeview {{ Request::is('paysheet', 'payslip', 'payadvice', 'bankfile', 'epfcform', 'wht', 'employee-payslip') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-pie-chart"></i>
                                <p>
                                    Reports
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                @can('view attendance report')
                                    <li class="nav-item">
                                        <a href="{{route('attendance.getAttendanceReport')}}"
                                           class="nav-link {{ Request::is('attendance') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>Attendance</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('view pay_sheet report')
                                    <li class="nav-item">
                                        <a href="{{route('paysheet.index')}}"
                                           class="nav-link {{ Request::is('paysheet') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>Paysheet</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('view payslip report')
                                    @role('employee')
                                    <li class="nav-item">
                                        <a href="{{route('payslip.employee_payslip')}}"
                                           class="nav-link {{ Request::is('employee-payslips') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>Payslip</p>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a href="{{route('payslip.index')}}"
                                           class="nav-link {{ Request::is('payslip') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>Payslip</p>
                                        </a>
                                    </li>
                                    @endrole
                                @endcan

                                @can('view pay_advice report')
                                    <li class="nav-item">
                                        <a href="{{route('payadvice.index')}}"
                                           class="nav-link {{ Request::is('payadvice') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>PayAdvice</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('view bank file')
                                    <li class="nav-item">
                                        <a href="{{route('bankfile.index')}}"
                                           class="nav-link {{ Request::is('bankfile') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>Bank File</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('view epf cform')
                                    <li class="nav-item">
                                        <a href="{{route('epfcform.index')}}"
                                           class="nav-link {{ Request::is('epfcform') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>EPF C Form</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('view wht certificate')
                                    <li class="nav-item">
                                        <a href="{{route('wht.index')}}"
                                           class="nav-link {{ Request::is('wht') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>WHT Certificate</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan

                    @role('super-admin')
                    <li class="nav-item has-treeview {{ Request::is('users', 'users/create', 'roles', 'roles/create', 'permissions', 'permissions/create') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-lock"></i>
                            <p>Authentication <i class="right fa fa-angle-left"></i></p>
                        </a>

                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('users.index')}}"
                                   class="nav-link {{ Request::is('users', 'users/create') ? 'active' : '' }}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Users</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('roles.index')}}"
                                   class="nav-link {{ Request::is('roles', 'roles/create') ? 'active' : '' }}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>Roles & Permissions</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role('super-admin|company-admin')
                    <li class="nav-item has-treeview {{ Request::is('companies/*', 'department', 'theme') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog"></i>
                            <p>Settings <i class="right fa fa-angle-left"></i></p>
                        </a>

                        <ul class="nav nav-treeview">

                            @role('company-admin')
                            <li class="nav-item">
                                <a href="{{route('companies.show', auth()->user()->company_id)}}"
                                   class="nav-link {{ Request::is('companies/*') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-user"></i>
                                    <p>Profile</p>
                                </a>
                            </li>
                            @endrole

                            <li class="nav-item">
                                <a href="{{route('settings.department')}}"
                                   class="nav-link {{ Request::is('department') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-cog"></i>
                                    <p>Department</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('settings.theme')}}"
                                   class="nav-link {{ Request::is('theme') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-adjust"></i>
                                    <p>Theme</p>
                                </a>
                            </li>

                        </ul>

                    </li>
                    @endrole
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <!-- End main Sidebar Container -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <!-- Create Post Form -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        @yield('page_header')
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @yield('pagenation')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                @include('sweetalert::alert')
                @yield('content')
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- End content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            <p style="display: inline">v1.0</p>
            {{--                <a href="http://novasolutions.lk" target="_blank"><object type="image/svg+xml"  data="{{asset('svg/nova.svg')}}" style="width: 60px;">Your browser does not support SVG</object></a>--}}
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy;
            <script type="text/javascript">
                var d = new Date();
                document.write(d.getFullYear());
            </script>
            <a href="http://corporatesolutions.lk" target="_blank">Corporate Business Solutions Pvt. Ltd</a>.</strong>
        All rights reserved.
    </footer>
    <!--End footer-->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>


<script src="{{asset('js/app.js')}}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!--App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('dist/js/plugins/jquery.steps/jquery.steps.min.js') }}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<!-- InputMask -->
<script src="{{asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{asset('plugins/input-mask/jquery.maskMoney.js')}}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{ asset('js/jquery.maskedinput.js') }}" type="text/javascript"></script><!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>

<!-- arithmetic -->
<script src="{{asset('plugins/jQuery-basic-arithmetic-plugin/jquery.basic_arithmetics.js')}}"></script>

<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

<!-- bootstrap time picker -->
<script src="{{asset('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

<!-- Global site tag -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143550755-1"></script>

<!-- jquery validator -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!--vue cdn-->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.min.js"></script>


<!--chart.js-->
<script src="{{asset('plugins/chart.js/dist/Chart.js')}}"></script>

<!-- NOVA for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>


<!-- js pdf -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>--}}

<script>
    $(function(){
        $('.noSpace').bind('input', function(){
            $(this).val(function(_, v){
                return v.replace(/\s+/g, '');
            });
        });
    });

    Vue.use(VeeValidate, {
        classes: true,
        classNames: {
            valid: 'is-valid',
            invalid: 'is-invalid'
        },
        useConstraintAttrs: false
    });
</script>

<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-143550755-1');
</script>

<script>
    $.validate({
        modules: 'date, security, file',
        borderColorOnError: '#F93260',
        validateOnBlur: true,
        addValidClassOnAll: true
    });
</script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<script>
    $(document).ready(function () {
        $("#example1").DataTable({
            "paging": true,
        });

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $("#example4").DataTable({
            "paging": true,
        });

        $("#example6").DataTable({
            "paging": true,
        });
    });
    $(function () {
        $('#example3').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        $("#example5").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<script>
    $(function () {
        $("#percent").mask("99.99%");
    });

    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    })
</script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

    })


    $("#demo1").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#basic").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow1").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow2").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow3").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow4").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow5").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow6").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow8").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow7").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow9").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $("#allow10").maskMoney({

        // The symbol to be displayed before the value entered by the user
        prefix: '',

        // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
        suffix: "",

        // Delay formatting of text field until focus leaves the field
        formatOnBlur: false,

        // Prevent users from inputing zero
        allowZero: false,

        // Prevent users from inputing negative values
        allowNegative: true,

        // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
        allowEmpty: false,

        // Select text in the input on double click
        doubleClickSelection: true,

        // Select all text in the input when the element fires the focus event.
        selectAllOnFocus: false,

        // The thousands separator
        thousands: ',',

        // The decimal separator
        decimal: '.',

        // How many decimal places are allowed
        precision: 2,

        // Set if the symbol will stay in the field after the user exits the field.
        affixesStay: false,

        // Place caret at the end of the input on focus
        bringCaretAtEndOnFocus: true

    });

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    })

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    })
</script>

@yield('custom-jquery')

</body>
</html>
