@component('mail::message')

# Congratulations!

    Dear {{ $name }},

    You are successfully registered with Talent System.

    These are your login credentials.

@component('mail::panel')
    **User Name : **{{ $email }}<br>
    **Password  : **{{ $password }}
@endcomponent

If you have any concerns please email to talent@corporatesolutions.lk.

@component('mail::button', ['url' => route('login')])
Login
@endcomponent

Regards,<br>
Talent Management Team.
@endcomponent
