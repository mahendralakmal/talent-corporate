@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', __('Page Not Found'))

@section('heading', __("Look like you're lost"))

@section('message', __('the page you are looking for not available!'))
