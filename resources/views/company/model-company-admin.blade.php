<div class="modal fade" id="company-admins{{$company->id}}">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Company Admins</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="example5">
                        <thead>
                        <tr>
                            <th class="search">Id</th>
                            <th class="search">User Name</th>
                            <th class="search">E-mail</th>
                            <th class="search">company</th>
                            <th class="search">Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="table">
                        @foreach($users as $key=>$user)
                            <tr>
                                <td>{{ $key }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
{{--                                @if($user->company_id != 'All')--}}
{{--                                    <td>{{ App\company::where('regno',$user->company_id)->first()->name }}</td>--}}
{{--                                @else--}}
                                    <td>{{ $user->company_id }}</td>
{{--                                @endif--}}
                                <td>{{ $user->getRoleNames()->first() }}</td>
                                <td>
                                    <form action="/companies/assignUserToCompany" method="post"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="user" id="user" value="{{ $user->id }}">
                                        <input type="hidden" name="company" id="company" value="{{$company->id}}">
                                        <button class="btn btn-circle btn-info btn-sm" data-toggle="tooltip"
                                                title="Assign Company Admin"><i class="fa fa-check-circle"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
