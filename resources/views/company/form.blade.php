<!--company details-->
<div class="card">

    <div class="card-header">
        <h3 class="card-title">Company Details</h3>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Company Name <span class="required">*</span></label>
                    <input type="text" class="form-control {{$errors->has('company_name') ? ' is-invalid' : ''}}"
                           name="company_name" value="{{ old('company_name') }}" id="company_name"
                           placeholder="Company Name">
                    @if ($errors->has('company_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('company_name')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label>Address <span class="required">*</span></label>
                    <input type="text" class="form-control {{$errors->has('address') ? ' is-invalid' : ''}}"
                           name="address" value="{{ old('address') }}" id="address" placeholder="Address">
                    @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('address')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-6">
                <div class="form-group">
                    <label>Business Nature <span class="required">*</span></label>
                    <input type="text" class="form-control {{$errors->has('business_name') ? ' is-invalid' : ''}}"
                           name="business_name" value="{{ old('business_name') }}" id="business_name"
                           placeholder="Business Name">
                    @if ($errors->has('business_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('business_name')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label>Entity Type <span class="required">*</span></label>
                    <select name="entity_type" id="entity_type"
                            class="form-control{{$errors->has('entity_type') ? ' is-invalid' : ''}}">
                        <option value="">Select Entity Type</option>
                        <option value="Proprietorship" @if("Proprietorship"==old('entity_type')) selected @endif>
                            Proprietorship
                        </option>
                        <option value="Partnership" @if("Partnership"==old('entity_type')) selected @endif>Partnership
                        </option>
                        <option value="Company" @if("Company"==old('entity_type')) selected @endif>Company</option>
                        <option value="Other" @if("Other"==old('entity_type')) selected @endif>Other</option>
                    </select>
                    @if ($errors->has('entity_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('entity_type')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Telephone No <span class="required">*</span></label>
                    <input type="text" class="form-control {{$errors->has('telephone_no') ? ' is-invalid' : ''}}"
                           name="telephone_no" value="{{ old('telephone_no') }}" id="telephone_no"
                           placeholder="+94 xx xxx-xxx" data-inputmask='"mask": "(+99) 99 999-9999"' data-mask>
                    @if ($errors->has('telephone_no'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telephone_no')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label>E-mail <span class="required">*</span></label>
                    <input type="text" class="form-control {{$errors->has('company_email') ? ' is-invalid' : ''}}"
                           name="company_email" value="{{ old('company_email') }}" id="email" placeholder="E-mail">
                    @if ($errors->has('company_email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('company_email')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Website</label>
                    <input type="text" class="form-control {{$errors->has('website') ? ' is-invalid' : ''}}"
                           name="website" value="{{ old('website') }}" id="website" placeholder="Website">
                    @if ($errors->has('website'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('website')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col basic-time">
                <div class="form-group">
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label>Start Time <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <input type="text"
                                       class="form-control timepicker {{$errors->has('start_time') ? ' is-invalid' : ''}}"
                                       name="start_time" value="{{ old('start_time') }}">
                                @if ($errors->has('start_time'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_time')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col basic-time">
                <div class="form-group">
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label>End Time <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <input type="text"
                                       class="form-control timepicker {{$errors->has('end_time') ? ' is-invalid' : ''}}"
                                       name="end_time" value="{{ old('end_time') }}">
                                @if ($errors->has('end_time'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_time')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <label>Leaves Applicability</label>
                <div class="input-group">
                    <div class="input-group">
                        <input type="checkbox" name="leaves" id="leaves" @if(old('leaves')=='1') checked
                               @endif value="1">
                    </div>
                </div>
            </div>

            <div class="col">
                <label>Shift Applicability</label>
                <div class="input-group">
                    <div class="input-group">
                        <input type="checkbox" name="shift_apl" id="shift_apl" @if(old('shift_apl')=='1') checked
                               @endif value="1">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end company details-->

<!--leave details-->
<div class="card card-default collapsed-card" id="leave_details_div" style="display: none;">
    <div class="card-header">
        <h3 class="card-title">Leave Details</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">

        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Permanent
                </h3>
            </div>

            <div class="card-body">

                <div class="row">

                    <div class="col-4">
                        <div class="form-group">
                            <label>Annual <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('annual') ? ' is-invalid' : ''}}"
                                   name="annual" placeholder="Annual" value="{{ old('annual') }}">
                            @if ($errors->has('annual'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('annual')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Casual <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('casual1') ? ' is-invalid' : ''}}"
                                   name="casual1" placeholder="Casual" value="{{ old('casual1') }}">
                            @if ($errors->has('casual1'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('casual1')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Medical <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('medical1') ? ' is-invalid' : ''}}"
                                   name="medical1" placeholder="Medical" value="{{ old('medical1') }}">
                            @if ($errors->has('medical1'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medical1')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Short Leave <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('shortLeave1') ? ' is-invalid' : ''}}"
                                   name="shortLeave1" placeholder="Short Leave" value="{{ old('shortLeave1') }}">
                            @if ($errors->has('shortLeave1'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shortLeave1')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Lieu Leave <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('lieuLeave') ? ' is-invalid' : ''}}"
                                   name="lieuLeave" placeholder="Lieu Leave" value="{{ old('lieuLeave') }}">
                            @if ($errors->has('lieuLeave'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lieuLeave')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="card">

            <div class="card-header">
                <h3 class="card-title">
                    Probation
                </h3>
            </div>

            <div class="card-body">

                <div class="row">

                    <div class="col-4">
                        <div class="form-group">
                            <label>Casual <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('casual2') ? ' is-invalid' : ''}}"
                                   name="casual2" placeholder="Casual" value="{{ old('casual2') }}">
                            @if ($errors->has('casual2'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('casual2')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Medical <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('medical2') ? ' is-invalid' : ''}}"
                                   name="medical2" placeholder="Medical" value="{{ old('medical2') }}">
                            @if ($errors->has('medical2'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medical2')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Short Leave <span class="required">*</span></label>
                            <input type="text" class="form-control {{$errors->has('shortLeave2') ? ' is-invalid' : ''}}"
                                   name="shortLeave2" placeholder="Short Leave" value="{{ old('shortLeave2') }}">
                            @if ($errors->has('shortLeave2'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shortLeave2')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end leave details-->

<!--Shift details-->
<div class="card card-default collapsed-card" id="shift_details_div" style="display: none;">
    <div class="card-header">
        <h3 class="card-title">Shift Details</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-8 shifts-allocation">
                <div class="shifts-allocation-contents">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <label>Start Time <span class="required">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control timepicker {{$errors->has('start_time') ? ' is-invalid' : ''}}"
                                                   name="start_time[]" value="{{ old('start_time') }}">
                                            @if ($errors->has('start_time'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_time')}}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <label>End Time <span class="required">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control timepicker {{$errors->has('end_time') ? ' is-invalid' : ''}}"
                                                   name="end_time[]" value="{{ old('end_time') }}">
                                            @if ($errors->has('end_time'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_time')}}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <lable>&nbsp;</lable>
                <button class="btn btn-danger btn-sm" type="button" id="add-shift"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
<!--end Shift details-->

<!--bank details-->
<div class="card card-default collapsed-card">

    <div class="card-header">
        <h3 class="card-title">Bank Details</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label>Bank name <span class="required">*</span></label>
                    <select class="form-control{{$errors->has('bank_code') ? ' is-invalid' : ''}}" name="bank_code"
                            id="bank_code" data-validation="required">
                        <option selected value="">Select Bank Name</option>
                        @foreach($banks as $bank)
                            <option value="{{ $bank->bank_code }}"
                                    @if($bank->bank_code==old('bank_code')) selected @endif>{{ $bank->bank_name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('bank_code'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('bank_code')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Branch name <span class="required">*</span></label>
                    <select class="form-control{{$errors->has('branch_code') ? ' is-invalid' : ''}}" name="branch_code"
                            id="branch_code" data-validation="required">
                        <option value="">{{ trans('Select Branch') }}</option>
                    </select>
                    @if ($errors->has('branch_code'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('branch_code')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Branch Address <span class="required">*</span></label>
                    <input type="text" name="barnch_address"
                           class="form-control{{$errors->has('barnch_address') ? ' is-invalid' : ''}}"
                           placeholder="Branch Address" value="{{ old('barnch_address') }}">
                    @if ($errors->has('barnch_address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('barnch_address')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label>Account Number <span class="required">*</span></label>
                    <input type="text" max="12" name="account_number" id="account_no"
                           class="form-control{{$errors->has('account_number') ? ' is-invalid' : ''}}"
                           placeholder="Account Number" value="{{ old('account_number') }}">
                    @if ($errors->has('account_number'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('account_number')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Account Name <span class="required">*</span></label>
                    <input type="text" name="account_name"
                           class="form-control{{$errors->has('account_name') ? ' is-invalid' : ''}}"
                           placeholder="Account Name" value="{{ old('account_name') }}">
                    @if ($errors->has('account_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('account_name')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Salary Processing Date <span class="required">*</span></label>
                    <select type="number" name="salaryDate"
                            class="form-control{{$errors->has('salaryDate') ? ' is-invalid' : ''}}"
                            placeholder="Salary Processing Date">
                        <option value="">Select Salary Processing Date</option>
                        <option value="1" @if("1"==old('salaryDate')) selected @endif>1</option>
                        <option value="2" @if("2"==old('salaryDate')) selected @endif>2</option>
                        <option value="3" @if("3"==old('salaryDate')) selected @endif>3</option>
                        <option value="4" @if("4"==old('salaryDate')) selected @endif>4</option>
                        <option value="5" @if("5"==old('salaryDate')) selected @endif>5</option>
                        <option value="6" @if("6"==old('salaryDate')) selected @endif>6</option>
                        <option value="7" @if("7"==old('salaryDate')) selected @endif>7</option>
                        <option value="8" @if("8"==old('salaryDate')) selected @endif>8</option>
                        <option value="9" @if("9"==old('salaryDate')) selected @endif>9</option>
                        <option value="10" @if("10"==old('salaryDate')) selected @endif>10</option>
                        <option value="11" @if("11"==old('salaryDate')) selected @endif>12</option>
                        <option value="13" @if("13"==old('salaryDate')) selected @endif>13</option>
                        <option value="14" @if("14"==old('salaryDate')) selected @endif>14</option>
                        <option value="15" @if("15"==old('salaryDate')) selected @endif>15</option>
                        <option value="16" @if("16"==old('salaryDate')) selected @endif>16</option>
                        <option value="17" @if("17"==old('salaryDate')) selected @endif>17</option>
                        <option value="18" @if("18"==old('salaryDate')) selected @endif>18</option>
                        <option value="19" @if("19"==old('salaryDate')) selected @endif>19</option>
                        <option value="20" @if("20"==old('salaryDate')) selected @endif>20</option>
                        <option value="21" @if("21"==old('salaryDate')) selected @endif>21</option>
                        <option value="22" @if("22"==old('salaryDate')) selected @endif>22</option>
                        <option value="23" @if("23"==old('salaryDate')) selected @endif>23</option>
                        <option value="24" @if("24"==old('salaryDate')) selected @endif>24</option>
                        <option value="25" @if("25"==old('salaryDate')) selected @endif>25</option>
                        <option value="26" @if("26"==old('salaryDate')) selected @endif>26</option>
                        <option value="27" @if("27"==old('salaryDate')) selected @endif>27</option>
                        <option value="28" @if("28"==old('salaryDate')) selected @endif>28</option>
                        <option value="29" @if("29"==old('salaryDate')) selected @endif>29</option>
                        <option value="30" @if("30"==old('salaryDate')) selected @endif>30</option>
                        <option value="31" @if("31"==old('salaryDate')) selected @endif>31</option>
                    </select>
                    @if ($errors->has('salaryDate'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('salaryDate')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

    </div>
</div>
<!--end bank details-->

<!--Tax details-->
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Tax Details</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label>TIN No</label>
                    <input type="text" class="form-control" name="tin_no" id="tin_no" placeholder="TIN no"
                           value="{{ old('tin_no') }}">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>EPF Registration No </label>
                    <input type="text" class="form-control" name="epf_registration_no" id="epf_registration_no"
                           placeholder="EPF Registration No" value="{{ old('epf_registration_no') }}">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>EPF Rate</label>
                    <input type="text" class="form-control" name="epf_rate" id="epf_rate" placeholder="EPF rate"
                           value="{{ old('epf_rate') }}">
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Stamp Duty Compound <span class="required">*</span></label><br>
                    <input type="radio" name="stamp_duty" class="flat-red" checked>&nbsp;Yes
                    <input type="radio" name="stamp_duty" class="flat-red">&nbsp;No
                </div>
            </div>
        </div>

    </div>
</div>
<!--end Tax details-->

<!--contact person details-->
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Contact Person</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label>Contact Person <span class="required">*</span></label>
                    <input type="text" class="form-control{{$errors->has('contact_person') ? ' is-invalid' : ''}}"
                           name="contact_person" value="{{ old('contact_person') }}" id="contact_person"
                           placeholder="Contact Person">
                    @if ($errors->has('contact_person'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_person')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Designation <span class="required">*</span></label>
                    <input type="text"
                           class="form-control{{$errors->has('contact_person_designation') ? ' is-invalid' : ''}}"
                           name="contact_person_designation" value="{{ old('contact_person_designation') }}"
                           id="contact_person_designation" placeholder="Designation">
                    @if ($errors->has('contact_person_designation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_person_designation')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>E-mail <span class="required">*</span></label>
                    <input type="text" class="form-control{{$errors->has('contact_email') ? ' is-invalid' : ''}}"
                           name="contact_email" value="{{ old('contact_email') }}" id="email" placeholder="E-mail">
                    @if ($errors->has('contact_email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_email')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label>Mobile<span class="required">*</span> </label>
                    <input type="text" class="form-control{{$errors->has('mobile') ? ' is-invalid' : ''}}"
                           data-inputmask='"mask": "(+99) 99 999-9999"' placeholder="+94 xx xxx-xxx" data-mask
                           name="mobile" value="{{ old('mobile') }}" id="mobile" placeholder="Mobile">
                    @if ($errors->has('mobile'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label>Landline <span class="required">*</span></label>
                    <input type="text" class="form-control{{$errors->has('fixed') ? ' is-invalid' : ''}}" name="fixed"
                           value="{{ old('fixed') }}" id="fixed" placeholder="+94 xx xxx-xxx"
                           data-inputmask='"mask": "(+99) 99 999-9999"' data-mask>
                    @if ($errors->has('fixed'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fixed')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>Ext</label>
                    <input type="text" class="form-control" name="ext" id="ext" placeholder="Ext"
                           value="{{ old('ext') }}">
                </div>
            </div>
        </div>

    </div>

</div>
<!--end contact person details-->

<!--payroll details-->
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Payroll Details</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 1</label>
                    <input type="text" class="form-control" name="allowance01" placeholder="allowance"
                           value="{{ old('allowance01') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance01Epf')=='1')
                                <input type="checkbox" name="allowance01Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance01Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance01Paye')=='1')
                                <input type="checkbox" name="allowance01Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance01Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 2</label>
                    <input type="text" class="form-control" name="allowance02" placeholder="allowance"
                           value="{{ old('allowance02') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance02Epf')=='1')
                                <input type="checkbox" name="allowance02Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance02Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance02Paye')=='1')
                                <input type="checkbox" name="allowance02Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance02Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 3</label>
                    <input type="text" class="form-control" name="allowance03" placeholder="allowance"
                           value="{{ old('allowance03') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance03Epf')=='1')
                                <input type="checkbox" name="allowance03Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance03Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance03Paye')=='1')
                                <input type="checkbox" name="allowance03Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance03Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 4</label>
                    <input type="text" class="form-control" name="allowance04" placeholder="allowance"
                           value="{{ old('allowance04') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance04Epf')=='1')
                                <input type="checkbox" name="allowance04Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance04Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance04Paye')=='1')
                                <input type="checkbox" name="allowance04Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance04Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 5</label>
                    <input type="text" class="form-control" name="allowance05" placeholder="allowance"
                           value="{{ old('allowance05') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance05Epf')=='1')
                                <input type="checkbox" name="allowance05Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance05Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance05Paye')=='1')
                                <input type="checkbox" name="allowance05Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance05Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 6</label>
                    <input type="text" class="form-control" name="allowance06" placeholder="allowance"
                           value="{{ old('allowance06') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance06Epf')=='1')
                                <input type="checkbox" name="allowance06Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance06Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance06Paye')=='1')
                                <input type="checkbox" name="allowance06Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance06Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 7</label>
                    <input type="text" class="form-control" name="allowance07" placeholder="allowance"
                           value="{{ old('allowance07') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance07Epf')=='1')
                                <input type="checkbox" name="allowance07Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance07Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance07Paye')=='1')
                                <input type="checkbox" name="allowance07Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance07Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 8</label>
                    <input type="text" class="form-control" name="allowance08" placeholder="allowance"
                           value="{{ old('allowance08') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance08Epf')=='1')
                                <input type="checkbox" name="allowance08Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance08Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance08Paye')=='1')
                                <input type="checkbox" name="allowance08Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance08Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 9</label>
                    <input type="text" class="form-control" name="allowance09" placeholder="allowance"
                           value="{{ old('allowance09') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance09Epf')=='1')
                                <input type="checkbox" name="allowance09Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance09Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance09Paye')=='1')
                                <input type="checkbox" name="allowance09Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance09Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Allowance 10</label>
                    <input type="text" class="form-control" name="allowance10" placeholder="allowance"
                           value="{{ old('allowance10') }}">
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>EPF </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance10Epf')=='1')
                                <input type="checkbox" name="allowance10Epf" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance10Epf" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-1">
                <div class="form-group">
                    <label>PAYE </label>
                    <div class="input-group">
                        <div class="input-group">
                            @if(old('allowance10Paye')=='1')
                                <input type="checkbox" name="allowance10Paye" checked class="flat-red" value="1">
                            @else
                                <input type="checkbox" name="allowance10Paye" class="flat-red" value="1">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--end payroll details-->

<!--company doc details-->
<div class="card card-default collapsed-card">
    <div class="card-header">
        <h3 class="card-title">Company Document</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-placement="top"
                    title="Expandable"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="exampleInputFile">BR Copy <span class="required">*</span></label>
                    <input type="file" class="form-control{{$errors->has('br_copy') ? ' is-invalid' : ''}}"
                           name="br_copy" value="{{ old('br_copy') }}">
                    @if ($errors->has('br_copy'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('br_copy')}}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="exampleInputFile">TIN Document </label>
                    <input type="file" class="form-control" name="tin_document">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="exampleInputFile">EPF Document</label>
                    <input type="file" class="form-control" name="epf_document">
                </div>
            </div>

        </div>

    </div>
</div>
<!--end company doc details-->
