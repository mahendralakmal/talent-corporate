@extends('layouts.app')

@section('title')
    <title>Corporate Solutions | Dashboard</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Company Settings</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('companies.index')}}">Comapny</a></li>
    <li class="breadcrumb-item active">Edit Company</li>
@endsection

@section('content')

    <form action="{{ route('companies.update', $company->id) }}" method="post">

    {{ csrf_field() }}

    {{ method_field('put') }}

    <!--company details-->
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Company Details</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Company Name <span class="required">*</span></label>
                            <input type="text" class="form-control{{$errors->has('company_name') ? ' is-invalid' : ''}}"
                                   name="company_name" id="company_name" placeholder="Company Name"
                                   value="{{$company->name}}">
                            @if ($errors->has('company_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_name')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>Address <span class="required">*</span></label>
                            <input type="text" class="form-control{{$errors->has('address') ? ' is-invalid' : ''}}"
                                   name="address" id="address" placeholder="Address" value="{{$company->address}}">
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-6">
                        <div class="form-group">
                            <label>Business Nature <span class="required">*</span></label>
                            <input type="text"
                                   class="form-control{{$errors->has('business_name') ? ' is-invalid' : ''}}"
                                   name="business_name" id="business_name" placeholder="Business Name"
                                   value="{{$company->business_nature}}">
                            @if ($errors->has('business_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('business_name')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>Entity Type <span class="required">*</span></label>
                            <select name="entity_type" id="entity_type"
                                    class="form-control{{$errors->has('entity_type') ? ' is-invalid' : ''}}">
                                <option value="{{$company->entity_type}}" selected>{{$company->entity_type}}</option>
                                <option value="Proprietorship">Proprietorship</option>
                                <option value="Partnership">Partnership</option>
                                <option value="Company">Company</option>
                                <option value="Other">Other</option>
                            </select>
                            @if ($errors->has('entity_type'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('entity_type')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-6">
                        <div class="form-group">
                            <label>Telephone No <span class="required">*</span></label>
                            <input type="text" class="form-control{{$errors->has('telephone_no') ? ' is-invalid' : ''}}"
                                   name="telephone_no" id="telephone_no" placeholder="Telephone No"
                                   data-inputmask='"mask": "(+99) 99 999-9999"' data-mask
                                   value="{{$company->telephone}}">
                            @if ($errors->has('telephone_no'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telephone_no')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>E-mail <span class="required">*</span></label>
                            <input type="text"
                                   class="form-control{{$errors->has('company_email') ? ' is-invalid' : ''}}"
                                   name="company_email" id="email" placeholder="E-mail" value="{{$company->email}}">
                            @if ($errors->has('company_email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_email')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="col-6">
                        <div class="form-group">
                            <label>Website <span class="required">*</span></label>
                            <input type="text" class="form-control{{$errors->has('website') ? ' is-invalid' : ''}}"
                                   name="website" id="website" placeholder="Website" value="{{$company->website}}">
                            @if ($errors->has('website'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('website')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col basic-time">
                        <div class="form-group">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Start Time <span class="required">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        <input type="text"
                                               class="form-control timepicker{{$errors->has('start_time') ? ' is-invalid' : ''}}"
                                               name="start_time" value="{{$company->startTime}}">
                                        @if ($errors->has('start_time'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('start_time')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col basic-time">
                        <div class="form-group">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>End Time <span class="required">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        <input type="text"
                                               class="form-control timepicker{{$errors->has('end_time') ? ' is-invalid' : ''}}"
                                               name="end_time" value="{{$company->endTime}}">
                                        @if ($errors->has('end_time'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('end_time')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <label>Leaves Applicability</label>
                        <div class="input-group">
                            <div class="input-group">
                                <input type="checkbox" name="leaves" id="leaves"
                                       @if(old('leaves')=='1' || $company->leaveStatus == 1) checked @endif value="1">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <label>Shift Applicability</label>
                        <div class="input-group">
                            <div class="input-group">
                                <input type="checkbox" name="shift_apl" id="shift_apl"
                                       @if(old('shift_apl')=='1' || $company->shift_apl) checked
                                       @endif value="1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end company details-->


        <!--leave details-->
        <div class="card card-default" id="leave_details_div"
             @if(!$company->leaveStatus == 1)style="display: none;"@endif>
            <div class="card-header">
                <h3 class="card-title">Leave Details</h3>
            </div>
            <div class="card-body">

                <div class="card">

                    <div class="card-header">
                        <h3 class="card-title">
                            Permanent
                        </h3>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Annual <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('annualLeave') ? ' is-invalid' : ''}}"
                                           name="annualLeave" placeholder="Annual"
                                           value="{{ old('annualLeave') }}@if(!empty($leaves[5])){{ $leaves[5]->entitlement }}@endif">
                                    @if ($errors->has('annualLeave'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('annualLeave')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Casual <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('casualLeave1') ? ' is-invalid' : ''}}"
                                           name="casualLeave1" placeholder="Casual"
                                           value="{{ old('casualLeave1') }}@if(!empty($leaves[6])){{ $leaves[6]->entitlement }}@endif">
                                    @if ($errors->has('casualLeave1'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('casualLeave1')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Medical <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('medical1') ? ' is-invalid' : ''}}"
                                           name="medical1" placeholder="Medical"
                                           value="{{ old('medical1') }}@if(!empty($leaves[1])){{ $leaves[1]->entitlement }}@endif">
                                    @if ($errors->has('medical1'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medical1')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Short Leave <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('shortLeave1') ? ' is-invalid' : ''}}"
                                           name="shortLeave1" placeholder="Short Leave"
                                           value="{{ old('shortLeave1') }}@if(!empty($leaves[0])){{ $leaves[0]->entitlement }}@endif">
                                    @if ($errors->has('shortLeave1'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shortLeave1')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Lieu Leave <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('lieuLeave') ? ' is-invalid' : ''}}"
                                           name="lieuLeave" placeholder="Lieu Leave"
                                           value="{{ old('lieuLeave') }}@if(!empty($leaves[2])){{ $leaves[2]->entitlement }}@endif">
                                    @if ($errors->has('lieuLeave'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lieuLeave')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="card">

                    <div class="card-header">
                        <h3 class="card-title">
                            Probation
                        </h3>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Casual <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('casual2') ? ' is-invalid' : ''}}"
                                           name="casual2" placeholder="Casual"
                                           value="{{ old('casual2') }}@if(!empty($leaves[7])){{ $leaves[7]->entitlement }}@endif">
                                    @if ($errors->has('casual2'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('casual2')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Medical <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('medical2') ? ' is-invalid' : ''}}"
                                           name="medical2" placeholder="Medical"
                                           value="{{ old('medical2') }}@if(!empty($leaves[4])){{ $leaves[4]->entitlement }}@endif">
                                    @if ($errors->has('medical2'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medical2')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label>Short Leave <span class="required">*</span></label>
                                    <input type="text"
                                           class="form-control {{$errors->has('shortLeave2') ? ' is-invalid' : ''}}"
                                           name="shortLeave2" placeholder="Short Leave"
                                           value="{{ old('shortLeave2') }}@if(!empty($leaves[3])){{ $leaves[3]->entitlement }}@endif">
                                    @if ($errors->has('shortLeave2'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shortLeave2')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
        <!--end leave details-->

        <!--Shift details-->
        <div class="card card-default" id="shift_details_div"
             @if(!$company->shift_apl == 1)style="display: none;"@endif>
            <div class="card-header">
                <h3 class="card-title">Shift Details</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip"
                            data-placement="top"
                            title="Expandable"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-8 shifts-allocation">
                        @if(is_null($shifts->first()))
                            <div class="shifts-allocation-contents">
                                <div class="row">
                                    <input type="hidden" name="shift_id[]">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>Start Time <span class="required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control timepicker {{$errors->has('start_time') ? ' is-invalid' : ''}}"
                                                               name="start_time[]"
                                                               value="{{ old('start_time') }}">
                                                        @if ($errors->has('start_time'))
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_time')}}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>End Time <span class="required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control timepicker {{$errors->has('end_time') ? ' is-invalid' : ''}}"
                                                               name="end_time[]"
                                                               value="{{ old('end_time') }}">
                                                        @if ($errors->has('end_time'))
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_time')}}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <button class="btn btn-sm btn-danger btn-close" type="button"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        @else
                            @foreach($shifts as $key=>$shift)
                                <div class="shifts-allocation-contents">
                                    <div class="row">
                                        <input type="hidden" name="shift_id[]" value="{{$shift->id}}">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>Start Time <span class="required">*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-clock-o"></i></span>
                                                            </div>
                                                            <input type="text"
                                                                   class="form-control timepicker {{$errors->has('start_time') ? ' is-invalid' : ''}}"
                                                                   name="start_time[]"
                                                                   value="{{ old('start_time') }}{{$shift->start}}">
                                                            @if ($errors->has('start_time'))
                                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_time')}}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>End Time <span class="required">*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-clock-o"></i></span>
                                                            </div>
                                                            <input type="text"
                                                                   class="form-control timepicker {{$errors->has('end_time') ? ' is-invalid' : ''}}"
                                                                   name="end_time[]"
                                                                   value="{{ old('end_time') }}{{$shift->end}}">
                                                            @if ($errors->has('end_time'))
                                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_time')}}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-danger btn-close" type="button"><i
                                                    class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-2">
                        <lable>&nbsp;</lable>
                        <button class="btn btn-danger btn-sm" type="button" id="add-shift"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--end Shift details-->

        <!--bank details-->
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Bank Details</h3>
            </div>


            <div class="card-body">

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Bank name <span class="required">*</span></label>
                            <select class="form-control{{$errors->has('bank_code') ? ' is-invalid' : ''}}"
                                    name="bank_code" id="bank_code">
                                @foreach($banks as $bank)
                                    @if($bank->bank_code==$company->bank_code)
                                        <option value="{{ $bank->bank_code }}" selected
                                                id="bank_code">{{ $bank->bank_name }}</option>
                                    @else
                                        <option value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('bank_code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('bank_code')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Branch name <span class="required">*</span></label>
                            <select class="form-control{{$errors->has('branch_code') ? ' is-invalid' : ''}}"
                                    name="branch_code" id="branch_code">
                                @foreach($bank_branches as $bank_branch)
                                    @if($bank_branch->bank_code == $company->bank_code)
                                        @if($bank_branch->branch_code == $company->branch_code)
                                            <option value="{{ $bank_branch->branch_code }}"
                                                    selected>{{ $bank_branch->bank_branch }}</option>
                                        @else
                                            <option
                                                value="{{ $bank_branch->branch_code }}">{{ $bank_branch->bank_branch }}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('branch_code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('branch_code')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Branch Address <span class="required">*</span></label>
                            <input type="text" name="barnch_address"
                                   class="form-control{{$errors->has('barnch_address') ? ' is-invalid' : ''}}"
                                   placeholder="Branch Address" value="{{$company->branch_address}}">
                            @if ($errors->has('barnch_address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('barnch_address')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Account Number <span class="required">*</span></label>
                            <input type="text" name="account_number"
                                   class="form-control{{$errors->has('account_number') ? ' is-invalid' : ''}}"
                                   placeholder="Account Number" value="{{$company->account_number}}">
                            @if ($errors->has('account_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('account_number')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Account Name <span class="required">*</span></label>
                            <input type="text" name="account_name"
                                   class="form-control{{$errors->has('account_name') ? ' is-invalid' : ''}}"
                                   placeholder="Account Name" value="{{$company->account_name}}">
                            @if ($errors->has('account_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('account_name')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Salary Processing Date <span class="required">*</span></label>
                            <select type="number" name="salaryDate"
                                    class="form-control{{$errors->has('salaryDate') ? ' is-invalid' : ''}}"
                                    placeholder="Salary Processing Date" value="{{$company->salaryDate}}">
                                <option value="">Select Salary Processing Date</option>
                                <option value="1" <?php if($company->salaryDate == "1"){ ?>selected <?php } ?> >1
                                </option>
                                <option value="2" <?php if($company->salaryDate == "2"){ ?>selected <?php } ?> >2
                                </option>
                                <option value="3" <?php if($company->salaryDate == "3"){ ?>selected <?php } ?> >3
                                </option>
                                <option value="4" <?php if($company->salaryDate == "4"){ ?>selected <?php } ?> >4
                                </option>
                                <option value="5" <?php if($company->salaryDate == "5"){ ?>selected <?php } ?> >5
                                </option>
                                <option value="6" <?php if($company->salaryDate == "6"){ ?>selected <?php } ?> >6
                                </option>
                                <option value="7" <?php if($company->salaryDate == "7"){ ?>selected <?php } ?> >7
                                </option>
                                <option value="8" <?php if($company->salaryDate == "8"){ ?>selected <?php } ?> >8
                                </option>
                                <option value="9" <?php if($company->salaryDate == "9"){ ?>selected <?php } ?> >9
                                </option>
                                <option value="10" <?php if($company->salaryDate == "10"){ ?>selected <?php } ?> >10
                                </option>
                                <option value="11" <?php if($company->salaryDate == "12"){ ?>selected <?php } ?> >12
                                </option>
                                <option value="13" <?php if($company->salaryDate == "13"){ ?>selected <?php } ?> >13
                                </option>
                                <option value="14" <?php if($company->salaryDate == "14"){ ?>selected <?php } ?> >14
                                </option>
                                <option value="15" <?php if($company->salaryDate == "15"){ ?>selected <?php } ?> >15
                                </option>
                                <option value="16" <?php if($company->salaryDate == "16"){ ?>selected <?php } ?> >16
                                </option>
                                <option value="17" <?php if($company->salaryDate == "17"){ ?>selected <?php } ?> >17
                                </option>
                                <option value="18" <?php if($company->salaryDate == "18"){ ?>selected <?php } ?> >18
                                </option>
                                <option value="19" <?php if($company->salaryDate == "19"){ ?>selected <?php } ?> >19
                                </option>
                                <option value="20" <?php if($company->salaryDate == "20"){ ?>selected <?php } ?> >20
                                </option>
                                <option value="21" <?php if($company->salaryDate == "21"){ ?>selected <?php } ?> >21
                                </option>
                                <option value="22" <?php if($company->salaryDate == "22"){ ?>selected <?php } ?> >22
                                </option>
                                <option value="23" <?php if($company->salaryDate == "23"){ ?>selected <?php } ?> >23
                                </option>
                                <option value="24" <?php if($company->salaryDate == "24"){ ?>selected <?php } ?> >24
                                </option>
                                <option value="25" <?php if($company->salaryDate == "25"){ ?>selected <?php } ?> >25
                                </option>
                                <option value="26" <?php if($company->salaryDate == "26"){ ?>selected <?php } ?> >26
                                </option>
                                <option value="27" <?php if($company->salaryDate == "27"){ ?>selected <?php } ?> >27
                                </option>
                                <option value="28" <?php if($company->salaryDate == "28"){ ?>selected <?php } ?> >28
                                </option>
                                <option value="29" <?php if($company->salaryDate == "29"){ ?>selected <?php } ?> >29
                                </option>
                                <option value="30" <?php if($company->salaryDate == "30"){ ?>selected <?php } ?> >30
                                </option>
                                <option value="31" <?php if($company->salaryDate == "31"){ ?>selected <?php } ?> >31
                                </option>
                            </select>
                            @if ($errors->has('salaryDate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('salaryDate')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end bank details-->

        <!--Tax details-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tax Details</h3>
            </div>

            <div class="card-body">

                <div class="row">

                    <div class="col-4">
                        <div class="form-group">
                            <label>TIN no</label>
                            <input type="text" class="form-control" name="tin_no" id="tin_no" placeholder="TIN no"
                                   value="{{$company->TINno}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>EPF Registration No </label>
                            <input type="text" class="form-control" name="epf_registration_no" id="epf_registration_no"
                                   placeholder="EPF Registration No" value="{{$company->EPFno}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>EPF rate</label>
                            <input type="text" class="form-control" name="epf_rate" id="epf_rate" placeholder="EPF rate"
                                   value="{{$company->EPFrate}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Stamp Duty Compound <span class="required">*</span></label><br>
                            @if($company->stamp_duty == 'on')
                                <input type="radio" name="stamp_duty" class="flat-red" checked>&nbsp;Yes
                                <input type="radio" name="stamp_duty" class="flat-red">&nbsp;No
                            @elseif($company->stamp_duty == 'off')
                                <input type="radio" name="stamp_duty" class="flat-red">&nbsp;Yes
                                <input type="radio" name="stamp_duty" class="flat-red" checked>&nbsp;No
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--end Tax details-->

        <!--contact person details-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Contact Person</h3>
            </div>

            <div class="card-body">

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Contact Person <span class="required">*</span></label>
                            <input type="text"
                                   class="form-control{{$errors->has('contact_person') ? ' is-invalid' : ''}}"
                                   name="contact_person" id="contact_person" placeholder="Contact Person"
                                   value="{{$company->contactperson_name}}">
                            @if ($errors->has('contact_person'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('contact_person')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Designation <span class="required">*</span></label>
                            <input type="text"
                                   class="form-control{{$errors->has('contact_person_designation') ? ' is-invalid' : ''}}"
                                   name="contact_person_designation" id="contact_person_designation"
                                   placeholder="Designation" value="{{$company->contactperson_designation}}">
                            @if ($errors->has('contact_person_designation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('contact_person_designation')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>E-mail <span class="required">*</span></label>
                            <input type="text"
                                   class="form-control{{$errors->has('contact_email') ? ' is-invalid' : ''}}"
                                   name="contact_email" id="email" placeholder="E-mail"
                                   value="{{$company->contactperson_email}}">
                            @if ($errors->has('contact_email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('contact_email')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Mobile<span class="required">*</span> </label>
                            <input type="text" class="form-control{{$errors->has('mobile') ? ' is-invalid' : ''}}"
                                   data-inputmask='"mask": "(+99) 99 999-9999"' data-mask name="mobile" id="mobile"
                                   placeholder="Mobile" value="{{$company->contactperson_telephone}}">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Landline <span class="required">*</span></label>
                            <input type="text" class="form-control{{$errors->has('fixed') ? ' is-invalid' : ''}}"
                                   name="fixed" id="fixed" placeholder="Fixed"
                                   data-inputmask='"mask": "(+99) 99 999-9999"' data-mask
                                   value="{{$company->contactperson_fixedphone}}">
                            @if ($errors->has('fixed'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fixed')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Ext</label>
                            <input type="text" class="form-control" name="ext" id="ext" placeholder="Fixed"
                                   value="{{$company->landline_extention}}">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end contact person details-->

        <!--payroll details-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Payroll Details</h3>
            </div>

            <div class="card-body">

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 1</label>
                            <input type="text" class="form-control" name="allowance01" placeholder="allowance"
                                   value="{{$allowance->allowance01}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance01Epf==1)
                                        <input type="checkbox" name="allowance01Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance01Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance01Paye==1)
                                        <input type="checkbox" name="allowance01Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance01Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 2</label>
                            <input type="text" class="form-control" name="allowance02" placeholder="allowance"
                                   value="{{$allowance->allowance02}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance02Epf==1)
                                        <input type="checkbox" name="allowance02Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance02Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance02Paye==1)
                                        <input type="checkbox" name="allowance02Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance02Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 3</label>
                            <input type="text" class="form-control" name="allowance03" placeholder="allowance"
                                   value="{{$allowance->allowance03}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance03Epf==1)
                                        <input type="checkbox" name="allowance03Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance03Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance03Paye==1)
                                        <input type="checkbox" name="allowance03Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance03Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 4</label>
                            <input type="text" class="form-control" name="allowance04" placeholder="allowance"
                                   value="{{$allowance->allowance04}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance04Epf==1)
                                        <input type="checkbox" name="allowance04Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance04Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance04Paye==1)
                                        <input type="checkbox" name="allowance04Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance04Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 5</label>
                            <input type="text" class="form-control" name="allowance05" placeholder="allowance"
                                   value="{{$allowance->allowance05}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance05Epf==1)
                                        <input type="checkbox" name="allowance05Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance05Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance05Paye==1)
                                        <input type="checkbox" name="allowance05Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance05Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 6</label>
                            <input type="text" class="form-control" name="allowance06" placeholder="allowance"
                                   value="{{$allowance->allowance06}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance06Epf==1)
                                        <input type="checkbox" name="allowance06Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance06Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance06Paye==1)
                                        <input type="checkbox" name="allowance06Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance06Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 7</label>
                            <input type="text" class="form-control" name="allowance07" placeholder="allowance"
                                   value="{{$allowance->allowance07}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance07Epf==1)
                                        <input type="checkbox" name="allowance07Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance07Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance07Paye==1)
                                        <input type="checkbox" name="allowance07Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance07Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 8</label>
                            <input type="text" class="form-control" name="allowance08" placeholder="allowance"
                                   value="{{$allowance->allowance08}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance08Epf==1)
                                        <input type="checkbox" name="allowance08Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance08Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance08Paye==1)
                                        <input type="checkbox" name="allowance08Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance08Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 9</label>
                            <input type="text" class="form-control" name="allowance09" placeholder="allowance"
                                   value="{{$allowance->allowance09}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance09Epf==1)
                                        <input type="checkbox" name="allowance09Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance09Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance09Paye==1)
                                        <input type="checkbox" name="allowance09Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance09Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Allowance 10</label>
                            <input type="text" class="form-control" name="allowance10" placeholder="allowance"
                                   value="{{$allowance->allowance10}}">
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>EPF </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance10Epf==1))
                                    <input type="checkbox" name="allowance10Epf" class="flat-red" value="1" checked>
                                    @else
                                        <input type="checkbox" name="allowance10Epf" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-1">
                        <div class="form-group">
                            <label>PAYE </label>
                            <div class="input-group">
                                <div class="input-group">
                                    @if($allowance->allowance10Paye==1)
                                        <input type="checkbox" name="allowance10Paye" class="flat-red" value="1"
                                               checked>
                                    @else
                                        <input type="checkbox" name="allowance10Paye" class="flat-red" value="1">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--end payroll details-->

        <!--company doc details-->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Company Document</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="exampleInputFile">TIN Document </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file"
                                           class="form-control{{$errors->has('tin_document') ? ' is-invalid' : ''}}"
                                           name="tin_document">
                                    @if ($errors->has('tin_document'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('tin_document')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="exampleInputFile">EPF Document</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file"
                                           class="form-control{{$errors->has('epf_document') ? ' is-invalid' : ''}}"
                                           name="epf_document">
                                    @if ($errors->has('epf_document'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('epf_document')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end company doc details-->

        <div class="card-footer">
            <small class="pull-right">
                <a href="{{url()->previous()}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
                <button type="submit" class="btn btn-primary btn-sm">Update Company</button>
            </small>
        </div>

    </form>

@endsection

@section('custom-jquery')
    <script>
        $('#bank_code').change(function (event) {
            var bank_code = $(this).val();
            console.log(bank_code);
            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {
                    $('#branch_code').html(data.html);
                }
            });
        });

        $('#leaves').change(function (event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#leaves').is(":checked"))
                $('#leave_details_div').show();
            else
                $('#leave_details_div').hide();
        });

        $('#shift_apl').change(function (event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#shift_apl').is(":checked")) {
                // it is checked
                $('.basic-time').hide();
                $('#shift_details_div').show();
            } else {
                $('.basic-time').show();
                $('#shift_details_div').hide();
            }
        });

        $('#shift_apl').ready(function (event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#shift_apl').is(":checked")) {
                // it is checked
                $('.basic-time').hide();
                $('#shift_details_div').show();
            } else {
                $('.basic-time').show();
                $('#shift_details_div').hide();
            }
        });

        $(function () {
            $("#add-shift").on('click', function () {
                $(".shifts-allocation-contents:last").clone().prependTo($(".shifts-allocation-contents:last"));
            });

            $(".btn-close").on('click', function () {
                $(this).closest(".shifts-allocation-contents").append('@csrf');
                $(this).closest(".shifts-allocation-contents").wrap('<form id="formRemoveShift" action="/shifts/destroyShift" method="POST" enctype="application/x-www-form-urlencoded">');
                $(this).closest("#formRemoveShift").submit();
            });
        });
    </script>
@endsection
