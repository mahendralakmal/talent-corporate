<div class="modal fade" id="{{$company->id}}">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ $company->name }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="example5">
                        <thead>
                        <tr>
                            <th class="search">Emp ID</th>
                            <th class="search">Name</th>
                            <th class="search">E-mail</th>
                            <th class="search">NIC</th>
                            <th class="search">Contact Number</th>
                            <th class="search">Gender</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="table">
                        @foreach($company->employees as $employee)
                            <tr>
                                <td>{{ $employee->user_id }}</td>
                                <td>{{ $employee->sname }} {{ $employee->fname }} {{ $employee->lname }}</td>
                                <td>{{ $employee->personal_email }}</td>
                                <td>{{ $employee->nic }}</td>
                                <td>{{ $employee->per_mobileno }}</td>
                                <td>{{ $employee->gender }}</td>
                                <td>

                                    @can('view employees')
                                        <a class="btn btn-circle btn-info btn-sm" href="{{ route('employees.show', $employee->id) }}" data-toggle="tooltip" title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('edit employees')
                                        <a class="btn btn-circle btn-success btn-sm" href="{{ route('employees.edit', $employee->id) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    @endcan

                                    @can('delete employees')
                                        <button class="btn btn-danger btn-sm" data-employeeid="{{$employee->id}}" data-toggle="modal" data-target="#delete"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete"></i></button>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
