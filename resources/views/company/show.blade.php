@extends('layouts.app')

@section('title')
    <title>Corporate Solutions | Dashboard</title>

    <style>
        #image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }

        .middle {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 110px;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);

        }

        #container:hover #image {
            opacity: 0.3;
        }

        #container:hover .middle {
            opacity: 1;
        }

        .text {
            color: #000000;
            font-weight: bold;
            font-size: 16px;
            text-align: center;
        }
    </style>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Company Settings</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('companies.index')}}">Company</a></li>
    <li class="breadcrumb-item active">View Company</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Profile</h3>
                </div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" id="container">
                            <div class="text-center">
                                <img src="{{ url('storage/company/'. $company->logo) }}" id="image" class="rounded"
                                     style="width:180px;">
                            </div>
                            <div class="middle">
                                <div class="text" id="img_text">Change Logo</div>
                            </div>
                        </li>

                        <li class="list-group-item"><label>Reg No</label><br>{{$company->regno}}</li>

                        <li class="list-group-item"><label>Address</label><br>{{$company->address}}</li>

                        <li class="list-group-item"><label>Telephone</label><br>{{$company->telephone}}</li>

                        <li class="list-group-item"><label>E-mail</label><br><a
                                href="mailto:{{$company->email}}">{{$company->email}}</a></li>

                        <li class="list-group-item"><label>Web</label><br><a href="//{{$company->website}}"
                                                                             target="_blank">{{$company->website}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#epf" data-toggle="tab">EPF Details</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#contacts" data-toggle="tab">Contacts</a></li>
                        <li class="nav-item"><a class="nav-link" href="#bank" data-toggle="tab">Bank Details</a></li>
                        <li class="nav-item"><a class="nav-link" href="#settings" id="logo" data-toggle="tab">Profile
                                Image</a></li>
{{--                        @if($company->leaveStatus == 1)--}}
                            <li class="nav-item"><a class="nav-link" href="#leave" id="logo" data-toggle="tab">Leave</a>
                            </li>
{{--                        @endif--}}
                    </ul>
                </div>

                <div class="card-body">
                    <div class="tab-content">

                        <div class="active tab-pane" id="epf">
                            <form action="{{ route('companies.tax_details_update', $company->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>TIN Number</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('titNo') ? ' is-invalid' : ''}}"
                                                   name="titNo" value="{{$company->TINno}}" placeholder="TIN Number">
                                            @if ($errors->has('titNo'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('titNo')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>PAYE Reg No</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('payNo') ? ' is-invalid' : ''}}"
                                                   name="payNo" value="{{$company->PAYEno}}" placeholder="PAYE Reg No">
                                            @if ($errors->has('payNo'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('payNo')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>EPF Number</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('epfNo') ? ' is-invalid' : ''}}"
                                                   name="epfNo" value="{{$company->EPFno}}" placeholder="EPF Number">
                                            @if ($errors->has('epfNo'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('epfNo')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>EPF Rate</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('epfRate') ? ' is-invalid' : ''}}"
                                                   name="epfRate" value="{{$company->EPFrate}}" placeholder="EPF Rate">
                                            @if ($errors->has('epfRate'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('epfRate')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}"
                                           data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"
                                                                                 aria-hidden="true"></i> Back</a>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip"
                                            title="Update">Update
                                    </button>
                                </div><!--modal-footer-->
                            </form>
                        </div>

                        <div class="tab-pane" id="contacts">
                            <form action="{{ route('companies.profile_conatcts_update', $company->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Person Name</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('con_name') ? ' is-invalid' : ''}}"
                                                   name="con_name" value="{{$company->contactperson_name}}"
                                                   placeholder="Person Name">
                                            @if ($errors->has('con_name'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('con_name')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Designation</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('con_desig') ? ' is-invalid' : ''}}"
                                                   name="con_desig" value="{{$company->contactperson_designation}}"
                                                   placeholder="Designation">
                                            @if ($errors->has('con_desig'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('con_desig')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Telephone Number</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('con_tele') ? ' is-invalid' : ''}}"
                                                   name="con_tele" value="{{$company->contactperson_telephone}}"
                                                   placeholder="Telephone Number">
                                            @if ($errors->has('con_tele'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('con_tele')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text"
                                                   class="form-control {{$errors->has('con_email') ? ' is-invalid' : ''}}"
                                                   name="con_email" value="{{$company->contactperson_email}}"
                                                   placeholder="Email">
                                            @if ($errors->has('con_email'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('con_email')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}"
                                           data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"
                                                                                 aria-hidden="true"></i> Back</a>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip"
                                            title="Update">Update
                                    </button>
                                </div><!--modal-footer-->
                            </form>
                        </div>

                        <div class="tab-pane" id="settings">
                            <form action="{{route('companies.logo_update', $company->id)}}" method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Company Logo</label>
                                            <input type="file"
                                                   class="form-control {{$errors->has('profile_image') ? ' is-invalid' : ''}}"
                                                   name="profile_image" id="exampleInputFile">
                                            @if ($errors->has('profile_image'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('profile_image')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}"
                                           data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"
                                                                                 aria-hidden="true"></i> Back</a>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm">Upload</button>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="bank">

                            <form action="{{ route('companies.bank_update', $company->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Bank </label>
                                            <select
                                                class="form-control {{$errors->has('bank_code') ? ' is-invalid' : ''}}"
                                                name="bank_code" id="bank_code">
                                                @foreach($banks as $bank)
                                                    @if($bank->bank_code==$company->bank_code)
                                                        <option value="{{ $bank->bank_code }}" selected
                                                                id="bank_code">{{ $bank->bank_name }}</option>
                                                    @else
                                                        <option
                                                            value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('bank_code'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('bank_code')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Account No </label>
                                            <input type="text" name="barnch_address"
                                                   class="form-control {{$errors->has('barnch_address') ? ' is-invalid' : ''}}"
                                                   placeholder="Branch Address" value="{{$company->account_number}}">
                                            @if ($errors->has('barnch_address'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('barnch_address')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Branch Branch </label>
                                            <select
                                                class="form-control {{$errors->has('branch_code') ? ' is-invalid' : ''}}"
                                                name="branch_code" id="branch_code">
                                                @foreach($bank_branches as $bank_branch)
                                                    @if($bank_branch->bank_code == $company->bank_code)
                                                        @if($bank_branch->branch_code == $company->branch_code)
                                                            <option value="{{ $bank_branch->branch_code }}"
                                                                    selected>{{ $bank_branch->bank_branch }}</option>
                                                        @else
                                                            <option
                                                                value="{{ $bank_branch->branch_code }}">{{ $bank_branch->bank_branch }}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('branch_code'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('branch_code')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Branch Address </label>
                                            <input type="text" name="barnch_address"
                                                   class="form-control {{$errors->has('barnch_address') ? ' is-invalid' : ''}}"
                                                   placeholder="Branch Address" value="{{$company->branch_address}}">
                                            @if ($errors->has('barnch_address'))
                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('barnch_address')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}"
                                           data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"
                                                                                 aria-hidden="true"></i> Back</a>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip"
                                            title="Update">Update
                                    </button>
                                </div><!--modal-footer-->

                            </form>

                        </div>

                        <div class="tab-pane" id="leave">

                            <form action="{{ route('companies.leave_details_update', $company->regno) }}" method="post">
                                {{ csrf_field() }}
                                {{method_field('PUT')}}
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Permanent</h3>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
{{--                                                <div class="col">--}}
{{--                                                    <label>{{\App\LeaveType::where('company_id', '=', $company->regno)->where('type', '=', 'permanent')->where('name', '=', 'Short Leave')->first()->name}}</label></l>--}}
{{--                                                    <input type="text" class="form-control" name="Short_Leave"--}}
{{--                                                           value="{{$per->entitlement}}" placeholder="{{$per->name}}">--}}
{{--                                                </div>--}}

{{--                                                <div class="col">--}}
{{--                                                    <label>{{\App\LeaveType::where('company_id', '=', $company->regno)->where('type', '=', 'permanent')->where('name', '=', 'Medical')->first()->name}}</label></l>--}}
{{--                                                    <input type="text" class="form-control" name="Medical"--}}
{{--                                                           value="{{$per->entitlement}}" placeholder="{{$per->name}}">--}}
{{--                                                </div>--}}
{{--                                                <div class="col">--}}
{{--                                                    <label>{{\App\LeaveType::where('company_id', '=', $company->regno)->where('type', '=', 'permanent')->where('name', '=', 'Lieu Leave')->first()->name}}</label></l>--}}
{{--                                                    <input type="text" class="form-control" name="Lieu_Leave"--}}
{{--                                                           value="{{$per->entitlement}}" placeholder="{{$per->name}}">--}}
{{--                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="card">

                                    <div class="card-header">
                                        <h3 class="card-title">Probation</h3>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">

{{--                                                <div class="col">--}}
{{--                                                    <label>{{\App\LeaveType::where('company_id', '=', $company->regno)->where('type', '=', 'probation')->where('name', '=', 'Short Leave')->first()->name}}</label></l>--}}
{{--                                                    <input type="text" class="form-control" name="Short_Leave2"--}}
{{--                                                           value="{{$per->entitlement}}" placeholder="{{$per->name}}">--}}
{{--                                                </div>--}}

{{--                                                <div class="col">--}}
{{--                                                    <label>{{\App\LeaveType::where('company_id', '=', $company->regno)->where('type', '=', 'probation')->where('name', '=', 'Medical')->first()->name}}</label></l>--}}
{{--                                                    <input type="text" class="form-control" name="Medical2"--}}
{{--                                                           value="{{$per->entitlement}}" placeholder="{{$per->name}}">--}}
{{--                                                </div>--}}


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}"
                                           data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"
                                                                                 aria-hidden="true"></i> Back</a>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip"
                                            title="Update">Update
                                    </button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('custom-jquery')
    <script>
        $(document).ready(function () {
            $('#img_text').click(function () {
                $('#logo').tab('show');
            })
        });

        $('#bank_code').change(function (event) {
            var bank_code = $(this).val();
            console.log(bank_code);
            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {
                    $('#branch_code').html(data.html);
                }
            });
        });
    </script>

@endsection
