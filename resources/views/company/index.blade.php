@extends('layouts.app')

@section('title')
    <title>Corporate Solutions | Company</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Super Admin - Company creation</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Company</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <div class="pull-right">
                    <div class="input-group">
                        @can('create company')
                            <a href="{{ route('companies.create') }}" class="btn btn-success btn-sm"
                               data-toggle="tooltip" data-placement="top" title="New"><i class="fa fa-plus"></i></a>
                        @endcan
                    </div>
                </div>
            </h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example4">
                <thead>
                <tr>
                    <th>RegNo</th>
                    <th>Business Name</th>
                    <th>Telephone</th>
                    <th>E-mail</th>
                    <th>Admin</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->regno}}</td>
                        <td>{{$company->name}}</td>
                        <td>{{$company->telephone}}</td>
                        <td>{{$company->email}}</td>
                        @if(!is_null($company->user_id))
                            <td>{{ $company->user->name }}</td>
                            <td>{{ $company->user->getRoleNames()->first() }}</td>
                        @else
                            <td> - </td>
                            <td> - </td>
                        @endif
                        <td>
                            <a class="btn btn-circle btn-warning btn-sm"
                               href="{{ route('employees.emp_list', $company->regno) }}" data-toggle="tooltip"
                               title="Employees"><i class="fas fa-user-friends"></i></a>

                            @can('view company')
                                <a class="btn btn-circle btn-info btn-sm"
                                   href="{{ route('companies.show', $company->regno) }}" data-toggle="tooltip"
                                   title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                            @endcan

                            @can('edit company')
                                <a class="btn btn-circle btn-success btn-sm"
                                   href="{{ route('companies.edit', $company->id) }}" data-toggle="tooltip"
                                   title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            @endcan

                            @can('delete company')
                                <button class="btn btn-danger btn-sm" data-companyid="{{$company->id}}"
                                        data-toggle="modal" data-target="#delete"><i class="fa fa-trash"
                                                                                     aria-hidden="true"
                                                                                     data-toggle="tooltip"
                                                                                     title="Delete"></i></button>
                            @endcan
                            <button class="btn btn-circle btn-sm" style="color: #3490dc; border: 1px solid silver;"
                                    title="View Employees" data-toggle="modal" data-company="{{$company->id}}"
                                    data-target="#{{$company->id}}"><i class="fa fa-eye" data-toggle="tooltip"
                                                                       aria-hidden="true"></i></button>

                            <button class="btn btn-circle btn-sm" style="color: #3490dc; border: 1px solid silver;"
                                    title="Assign Company Admin" data-toggle="modal" data-company="{{$company->id}}"
                                    data-target="#company-admins{{$company->id}}"><i class="fa fa-user" data-toggle="tooltip"
                                                                       aria-hidden="true"></i></button>
                            @include('company.model-employee')
                            @include('company.model-company-admin')

                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        <div class="row">

            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>


    <!-- employee model -->
    <!--end delete model-->

    <!-- delete modal -->
    <div id="delete" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog-lg modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                </div>
                <h1>Are you Sure ?</h1>

                <form action="{{ route('companies.destroy', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <div class="modal-body">
                        <input type="hidden" name="company_id" value="" id="company_id">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end delete model-->
@endsection

@section('custom-jquery')

    <script>
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('companyid');
            console.log(id);
            var modal = $(this)
            modal.find('#company_id').val(id);
        });
    </script>

@endsection
