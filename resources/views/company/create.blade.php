@extends('layouts.app')

@section('title')
    <title>Corporate Solutions | Company</title>
    <style>
        .bootstrap-timepicker-widget table td a {
            border: 1px transparent solid;
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 8px 0;
            outline: 0;
            color: #333;
            background-color: #bab9b6;
            border-radius: 4px;
        }
    </style>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Company Settings</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('companies.index')}}">Company</a></li>
    <li class="breadcrumb-item active">Create Company</li>
@endsection

@section('content')

    <form action="{{ route('companies.store') }}" method="post" enctype="multipart/form-data" id="company">

        {{ csrf_field() }}

        @include('company.form')

        <div class="card-footer">
            <small class="pull-right">
                <a href="{{url()->previous()}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
                <button type="submit" class="btn btn-primary btn-sm">Add Company</button>
            </small>
        </div>

    </form>

@endsection

@section('custom-jquery')

    <script>
        $('#account_no').blur(function (e) {
            $('#account_no').val(('000000000000' + $('#account_no').val()).slice(-12));
        });

        $('#bank_code').change(function (event) {
            var bank_code = $(this).val();
            console.log(bank_code);

            $.ajax({
                url: "{{ route('employees.getbranches') }}",
                method: "get",
                data: {bank_code: bank_code},
                datatype: "json",
                success: function (data) {

                    $('#branch_code').html(data.html);

                }
            });
        });

        $('#leaves').change(function (event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#leaves').is(":checked")) {
                // it is checked
                $('#leave_details_div').show();
            } else {
                $('#leave_details_div').hide();
            }
        });
        $('#leaves').ready(function (event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#leaves').is(":checked")) {
                // it is checked
                $('#leave_details_div').show();
            } else {
                $('#leave_details_div').hide();
            }
        });

        $('#shift_apl').change(function(event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#shift_apl').is(":checked")) {
                // it is checked
                $('.basic-time').hide();
                $( '#shift_details_div' ).show();
            }else{
                $('.basic-time').show();
                $( '#shift_details_div' ).hide();
            }
        });
        $('#shift_apl').ready(function(event) {
            var leave = $("input[type='checkbox']").val();
            if ($('#shift_apl').is(":checked")) {
                // it is checked
                $('.basic-time').hide();
                $( '#shift_details_div' ).show();
            }else{
                $('.basic-time').show();
                $( '#shift_details_div' ).hide();
            }
        });

        $(function () {
            $("#add-shift").on('click', function () {
                $( ".shifts-allocation-contents:last" ).clone().appendTo($(".shifts-allocation:last"));
            });
        });
    </script>


@endsection
