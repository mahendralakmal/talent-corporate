@extends('layouts.app')

@section('title')
    <title>Talent | Roles</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Roles</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Roles</li>
    <li class="breadcrumb-item active">All Roles</li>
@endsection

@section('content')

    {{--    <div class="card">--}}
    {{--        <div class="card-header">--}}
    {{--            Available Roles & Permissions--}}
    {{--            <small class="pull-right">--}}
    {{--                <a href="{{route('roles.create')}}" class="btn btn-success btn-sm" data-toggle="tooltip"--}}
{{--    data-placement="top" title="New"><i class="fas fa-plus"></i></a>--}}
    {{--            </small>--}}
    {{--        </div>--}}
    <div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Create New Role</div>
                    <div class="card-body">
                        <form action="{{route('roles.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <input type="text" class="form-control {{$errors->has('role_name') ? ' is-invalid' : ''}}" value="{{ old('role_name') }}" name="role_name" placeholder="Role Name">
                                        @if ($errors->has('role_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('role_name')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm pull-right" type="submit">Add</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Create New Permission</div>
                    <div class="card-body">
                        <form action="{{route('permissions.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Permission Name</label>
                                        <input type="text" class="form-control {{$errors->has('permission_name') ? ' is-invalid' : ''}}" value="{{ old('permission_name') }}" name="permission_name" placeholder="Permission Name">
                                        @if ($errors->has('permission_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permission_name')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm pull-right" type="submit">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Roles</div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($roles as $role)
                                <li class="list-group-item" data-toggle="collapse" data-target="#{{ $role->name }}"
                                    aria-expanded="false" aria-controls="{{ $role->name }}">{{ $role->role_name }}
                                    <i class="fa fa-plus-circle pull-right"></i>
                                </li>
                                <div class="role collapse" id="{{ $role->name }}">
                                    <form action="updateRolesPermissions" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="role_id" id="role_id" value="{{ $role->id }}">
                                        <ul class="list-group">
                                            @foreach($permissions as $key=>$permission)
                                                <li class="list-group-item">{{ $permission->name }}
                                                    <input type="checkbox" name="role_permission[]"
                                                           id="{{ $permission->id }}"
                                                           value="{{ $permission->id }}"
                                                           class="pull-right"
                                                           @if(in_array($permission->id,$role->permissions->pluck('id')->toArray())) checked @endif>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <button class="btn btn-primary pull-right" type="submit">Update</button>
                                    </form>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Permissions</div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($permissions as $key=>$permission)
                                <li class="list-group-item">{{ $permission->name }}
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>

@endsection
