
@extends('layouts.app')

@section('title')
    <title>Talent | Permissions</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Permissions</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
    <li class="breadcrumb-item active">Update Permission</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <b>Edit Role</b>
                    </div>

                    <div class="card-body">
                        <form action="{{route('roles.update', $role->id)}}" method="post">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <input type="text" class="form-control" name="role_name" placeholder="Role Name" value="{{$role->name}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Assign Permissions</label>
                                        <ul class="list-group">
                                            @foreach($permissions as $permission)
                                                <?php $per = $role->permissions()->pluck('name')->implode(',');
                                                    $parts = explode(',', $per);
                                                    $status = false;
                                                    foreach($parts as $part){
                                                        if($part==$permission->name) {
                                                             $status= true;
                                                        }
                                                    }
                                                    if($status) {?>
                                                    <li class="list-group-item"><input type="checkbox" name="permissions[]" checked value="{{$permission->id}}"> {{$permission->name}}</li>
                                                    <?php }else{
                                                    ?>
                                                    <li class="list-group-item"><input type="checkbox" name="permissions[]" value="{{$permission->id}}"> {{$permission->name}}</li>
                                                    <?php
                                                    }?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <small class="pull-right">
                                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                                <button class="btn btn-primary btn-sm" type="submit">Update</button>
                            </small>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
