
@extends('layouts.app')

@section('title')
    <title>Talent | Roles</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Roles</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
    <li class="breadcrumb-item active">New Role</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <b>Add Role</b>
                    </div>

                    <div class="card-body">

                        <form action="{{route('roles.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <input type="text" class="form-control {{$errors->has('role_name') ? ' is-invalid' : ''}}" value="{{ old('role_name') }}" name="role_name" placeholder="Role Name">
                                        @if ($errors->has('role_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('role_name')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Assign Permissions</label>
                                        <ul class="list-group {{$errors->has('permissions[]') ? ' is-invalid' : ''}}">
                                            @if(!$permissions->isEmpty())
                                                @foreach($permissions as $permission)
                                                    <li class="list-group-item"><input type="checkbox" name="permissions[]" value="{{$permission->id}}"> {{$permission->name}}</li>
                                                @endforeach
                                            @else
                                                <h3>No Permissions Found</h3>
                                            @endif
                                        </ul>
                                        @if ($errors->has('permissions[]'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permissions[]')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <small class="pull-right">
                                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                                <button class="btn btn-primary btn-sm" type="submit">Add</button>
                            </small>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
