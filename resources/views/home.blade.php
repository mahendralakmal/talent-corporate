@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Dashboard</title>
@endsection

@section('content')
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome {{ Auth::user()->name }} !</div>

                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">Birthday</div>
                                <div class="card-body">
                                    @if($birthdays->count() > 0)
                                        <ul class="list-group">
                                            @foreach($birthdays as $birthday)
                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{$birthday->fname}} {{$birthday->lname}}
                                                    <form action="" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="email" value="{{$birthday->per_email}}">
                                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Wish Here"><i class="fa fa-birthday-cake" aria-hidden="true" ></i></button>
                                                    </form>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <h3>No Birthday to show</h3>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">Leaves</div>

                                <div class="card-body">
                                    <h3>No Leaves to Approve</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-jquery')

@endsection
