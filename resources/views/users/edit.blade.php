@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit User</div>

                    <div class="card-body">
                        <form action="{{route('users.update', $user->id)}}" method="post">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Name"
                                               value="{{$user->name}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="text" name="email" class="form-control" placeholder="E-mail"
                                               value="{{$user->email}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Roles</label>
                                        <ul class="list-group">
                                            @foreach($roles as $role)
													<?php if($user->roles()->pluck('name')->implode(' ') == $role->name) {?>
                                                    <li class="list-group-item"><input type="radio" name="roles"
                                                                                       id="roles" checked
                                                                                       value="{{$role->id}}"> {{$role->role_name}}
                                                    </li>
													<?php }else{
													?>
                                                    <li class="list-group-item"><input type="radio" name="roles"
                                                                                       id="roles"
                                                                                       value="{{$role->id}}"> {{$role->role_name}}
                                                    </li>

													<?php
													} ?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control"
                                               placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-sm">Update</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
