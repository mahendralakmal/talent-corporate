@extends('layouts.app')

@section('page_header')
    <h5 class="m-0 text-dark">Authentication</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('users.create') }}">New Users</a></li>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New User</div>

                <div class="card-body">
                    <form action="{{route('users.store')}}" method="post">
                        {{csrf_field()}}

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control {{$errors->has('name') ? ' is-invalid' : ''}}" placeholder="Name">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="text" name="email" class="form-control {{$errors->has('email') ? ' is-invalid' : ''}}" placeholder="E-mail">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Roles</label>
                                    <ul class="list-group">
                                        @foreach($roles as $role)

                                            <li class="list-group-item"><input type="radio" name="roles" id="roles" value="{{$role->id}}" @if($role->id ==4) checked @endif> {{$role->name}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control {{$errors->has('password') ? ' is-invalid' : ''}}" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="form-control {{$errors->has('password_confirmation') ? ' is-invalid' : ''}}" placeholder="Confirm Password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <small class="pull-right">
                            <a href="{{url()->previous()}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                            <button type="submit" class="btn btn-primary btn-sm">Add User</button>
                        </small>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
