@extends('layouts.app')

@section('title')
    <title>Talent | Users</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Users</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">All Users</li>
@endsection

@section('content')

    <div class="card">

        <div class="card-header">Users
            <small class="pull-right">
                <a href="{{route('users.create')}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="New"><i class="fas fa-plus"></i></a>
            </small>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php $id = 1; ?>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$id++}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td><span class="badge badge-success"> {{$user->roles()->pluck('name')->implode(' ')}} </span></td>
                            <td>
{{--                                @if($user->name == "Super Admin")--}}
                                @can('delete user')
                                    <form action="{{route('users.destroy',$user->id)}}" method="post" style="display: inline">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete" >
                                            <i class="fa fa-trash"
                                               aria-hidden="true"
                                               data-toggle="tooltip"
                                               title="Delete"></i>
                                        </button>
                                    </form>
                                @endcan
                                @can('edit user')
                                        <a type="submit" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit" href="/users/{{$user->id}}/edit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>

@endsection
