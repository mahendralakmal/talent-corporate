@extends('layouts.app')

@section('title')
    <title>Talent | leaves</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Leave Balance Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Leave Balance</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <form action="{{ route('leaves_balance.getBalance')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <select name="type" class="form-control" id="type" required>
                                <option value="" selected>Select the type</option>
                                @foreach($leaves_type as $type)
                                    <option value="{{$type->id}}">{{$type->name}} ({{$type->type}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-2">
                        <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-search" aria-hidden="true"
                                                                                data-toggle="tooltip" title="New"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">Id</th>
                    <th class="search">Employee ID</th>
                    <th class="search">Employee Name</th>
                    <th class="search">Leave Type</th>
                    <th class="search">Total</th>
                    <th class="search">Used</th>
                    <th class="search">Available</th>
                </tr>
                </thead>

                <tbody id="balanceTable">
                @foreach($leaves_balance as $key=>$Lblance)
                    <tr>
                        <th class="search">{{$key+1}}</th>
                        <th class="search">{{$Lblance->user_id}}</th>
                        <th class="search">{{App\Employee::where('user_id',$Lblance->user_id)->first()['name_with_initials']}}</th>
                        <th class="search">{{App\LeaveType::findOrFail($Lblance->leave_type)->name}}</th>
                        <th class="search">{{ (float)$Lblance->used+(float)$Lblance->remaining }}</th>
                        <th class="search">{{$Lblance->used}}</th>
                        <th class="search">{{$Lblance->remaining}}</th>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>


    <!-- create model -->
{{--    <div class="modal fade" id="new" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel"--}}
{{--         aria-hidden="true">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title">Add a new Leave Balance</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}

{{--                <form action="{{ route('leaves_balance.store')}}" method="POST">--}}
{{--                    {{ csrf_field() }}--}}
{{--                    <div class="modal-body">--}}
{{--                        <div class="form-group">--}}
{{--                            <div class="form-row">--}}
{{--                                <label for="studentFirstName">Employee Name</label>--}}
{{--                                <select class="form-control" name="emp_name">--}}
{{--                                    <option selected>Select Employee</option>--}}
{{--                                    @foreach ($employees as $employee)--}}
{{--                                        <option--}}
{{--                                            value="{{$employee->id}}">{{$employee->fname}} {{$employee->lname}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="form-row">--}}
{{--                                <div class="col">--}}
{{--                                    <label for="studentFirstName">Total Leaves</label>--}}
{{--                                    <input type="number" class="form-control" name="total_leaves"--}}
{{--                                           placeholder="Total Leaves">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="form-row">--}}
{{--                                <div class="col">--}}
{{--                                    <label for="studentFirstName">Used Leaves</label>--}}
{{--                                    <input type="number" class="form-control" name="approved_leaves"--}}
{{--                                           placeholder="Available Leaves">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>--}}
{{--                        <button type="submit" class="btn btn-primary btn-sm">Add Leave Balance</button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- end create model -->


@endsection

@section('custom-jquery')
    <script>

        {{--$('#type').change(function (event) {--}}
        {{--    var type = $(this).val();--}}
        {{--    console.log(type);--}}

        {{--    $.ajax({--}}
        {{--        url: "{{ route('leaves_balance.getBalance') }}",--}}
        {{--        method: "get",--}}
        {{--        data: {type: type},--}}
        {{--        datatype: "json",--}}
        {{--        success: function (data) {--}}
        {{--            console.log(data);--}}
        {{--            $('#balanceTable').html(data.html);--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}
    </script>
@endsection

