@extends('layouts.app')

@section('title')
    <title>Talent | leaves</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">leave Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Leave Details</li>
@endsection

@section('content')

    <div class="row">

        <div class="col-4">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Leave Balance</h3>
                </div>

                <div class="card-body">
                    <canvas id="myChart" width="300" height="300"></canvas>
                </div>
            </div>

        </div>

        <div class="col-8">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Leave Details
                        <small class="pull-right">
                            @if(!is_null($remainingLeaves))
                                    @if($remainingLeaves == 0)
                                        <button data-toggle="modal" data-target="#new" class="btn btn-success btn-sm"
                                                disabled><i class="fa fa-plus" aria-hidden="true" data-toggle="tooltip"
                                                            title="Your Remaining Balance is Over"></i></button>
                                    @else
                                        <button data-toggle="modal" data-target="#new" class="btn btn-success btn-sm"><i
                                                class="fa fa-plus" aria-hidden="true" data-toggle="tooltip"
                                                title="New"></i>
                                        </button>
                                    @endif
                            @endif
                        </small>
                    </h3>
                </div>

                <div class="card-body">
                    <div>
                        <ul class="list-group list-group-horizontal-lg col-12">
                            @foreach($leave_balance as $lBalance)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{$lBalance->leaveType->name}}&nbsp;&nbsp;<span class="badge badge-primary badge-pill">{{$lBalance->remaining}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                    <table class="table table-bordered table-striped" id="example1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($leaves as $leave)
                            <tr>
                                <td>{{$leave->id}}</td>
                                <td>{{$leave->date}}</td>
                                <td>{{$leave->reason}}</td>
                                <td>@if($leave->approved_status == 'pending')
                                        <span class="badge badge-warning">{{$leave->approved_status}}</span>
                                    @elseif($leave->approved_status == 'accepted')
                                        <span class="badge badge-success">{{$leave->approved_status}}</span>
                                    @elseif($leave->approved_status == 'rejected')
                                        <span class="badge badge-danger">{{$leave->approved_status}}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>


    <!-- create model -->
    <div class="modal fade" id="new" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add a new Leave</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('leaves.store')}}" method="POST" id="leave">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        @include('leave.form')
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Add Leave</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end create model -->

@endsection

@section('custom-jquery')

    <script>
        var ctx = document.getElementById('myChart');

        var myChart = new Chart(ctx, {
            type: 'pie',
            data: data = {
                datasets: [{
                    data: [@if(!is_null($leave_balance)){{$usedLeaves}}@endif,
                        @if(!is_null($leave_balance)){{$remainingLeaves}}@endif],
                    backgroundColor: [
                        'rgb(245, 105, 84)',
                        'rgb(60, 141, 188)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)'
                    ],
                    borderWidth: 1
                }],
                labels: ['Used', 'Remaining']
            },
        });


    </script>

@endsection
