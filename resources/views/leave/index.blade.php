@extends('layouts.app')

@section('title')
    <title>Talent | leaves</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">leave Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Leave Details</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <small class="pull-right">
                    @can('create leave')
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#new"><i
                                class="fa fa-plus" aria-hidden="true" data-toggle="tooltip" title="New"></i></button>
                    @endcan
                </small>
            </h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">Id</th>
                    <th class="search">Employee Name</th>
                    <th class="search">Leave type</th>
                    <th class="search">Date</th>
                    <th class="search">Category</th>
                    <th class="search">Reason</th>
                    <th class="search">Approve</th>
                    <th class="search">Action</th>
                </tr>
                </thead>

                <tbody>
                @if(!is_null($leaves))
                    @foreach($leaves as $leave)
                        {{--                    {{ dd($leave) }}--}}
                        <tr>
                            <td>{{$leave->user_id}}</td>
                            @role('employee')
                            <td>{{ $employees->name_with_initials }}</td>
                            {{--                        @endrole--}}
                            {{--                        @role('super-admin|company-admin')--}}
                            @else
                                @php
                                    $employee = App\Employee::where('user_id', $leave->user_id)->first();
                                @endphp
                                <td>{{ $employee->name_with_initials }}</td>
                                @endrole
                                @php
                                    $type = App\LeaveType::findOrFail($leave->type);
                                @endphp
                                <td>{{$type->name}}</td>
                                <td>{{$leave->date}}</td>
                                <td>{{$type->type}}</td>
                                <td>{{$leave->reason}}</td>
                                @if($leave->approved_status=='pending')
                                    <td><span class="badge badge-warning">Pending</span></td>
                                @elseif($leave->approved_status=='accepted')
                                    <td><span class="badge badge-success">Approved</span></td>
                                @else
                                    <td><span class="badge badge-danger">Rejected</span></td>
                                @endif
                                <td>
                                    <form action="{{ route('leaves.update',$leave->id) }}" method="post">
                                        {{csrf_field()}}
                                        {{ method_field('PUT') }}
                                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"
                                                                                                data-toggle="tooltip"
                                                                                                data-placement="top"
                                                                                                title="Approve"
                                                                                                aria-hidden="true"
                                                                                                style="color: #fff;"></i>
                                        </button>
                                    </form>
                                </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>

            </table>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
            </div>
        </div>

    </div>


    <!-- create model -->
    <div class="modal fade" id="new" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add a new Leave</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('leaves.store')}}" method="POST" id="leave">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        @include('leave.form')
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Add Leave</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end create model -->


    <!-- delete modal -->
    <div id="delete" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                </div>
                <h1>Are you Sure ?</h1>

                <form action="{{ route('leaves.destroy', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <div class="modal-body">
                        <input type="hidden" name="leave_id" value="" id="leave_id">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')
    <script>
        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('leaveid');
            console.log(id);
            var modal = $(this)
            modal.find('#leave_id').val(id);
        });
    </script>


@endsection
