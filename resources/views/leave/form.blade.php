<div class="form-group">
    <div class="form-row">
        <div class="col">
            @role('super-admin|company-admin|hr-manager|operation-manager')
            <label for="studentFirstName">Employee(s)</label>
                @php
                    $setCompany = session('company');
                    if(isset($setCompany))
                        $employees =\ App\company::findOrFail(session('company'))->employees;
                @endphp
                <select class="form-control" name="user_id" id="select_user_id">
                    <option value="" selected>Select the employee</option>
                    @if(isset($setCompany))
                        @foreach(\App\company::findOrFail(session('company'))->employees as $key=>$employee)
                            @if(is_null($employee->date_of_resign))
                                <option value="{{ $employee->user_id }}">{{ $employee->user_id }}-{{
                                    $employee->name_with_initials }}
                                </option>
                            @endif
                        @endforeach
                    @endif
                </select>
                <input type="hidden" class="form-control" name="company_id" id="company_id"
                       value="{{ session('company_id') }}">
                <input type="hidden" class="form-control" name="reporting_manager" id="reporting_manager">
            @endrole
            @role('employee')
                <input type="text" class="form-control {{$errors->has('user_id') ? ' is-invalid' : ''}}"
                       name="employee_name" id="employee_name" placeholder="Leave ID"
                       value="{{ auth()->user()->employee->user_id }}-{{ auth()->user()->employee->name_with_initials }}">
                <input type="hidden" class="form-control" name="user_id" id="user_id"
                       value="{{ auth()->user()->employee->user_id }}">
                <input type="hidden" class="form-control" name="company_id" id="company_id"
                       value="{{ auth()->user()->employee->company_id }}">
            @endrole

            @if ($errors->has('user_id'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('user_id')}}</strong>
                    </span>
            @endif

        </div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col">
			<?php $leaves = \App\LeaveType::where('company_id', '=', 'All')->orWhere('company_id', session('company_id'))->get(); ?>
            <label for="studentFirstName">Leave Type</label>
            <select class="form-control {{$errors->has('type') ? ' is-invalid' : ''}}" name="type" id="type">
                <option value="" selected>Select the type</option>
				<?php foreach ($leaves as $leave){
				?>
                <option
                    value="<?php echo($leave->id) ?>"><?php echo($leave->name . ' (' . $leave->type . ')') ?></option>
				<?php
				} ?>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('type')}}</strong>
                        </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label for="studentFirstName">Reason</label>
            <textarea class="form-control {{$errors->has('reason') ? ' is-invalid' : ''}}" name="reason" id="reason"
                      placeholder="Reason"></textarea>
            @if ($errors->has('reason'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('reason')}}</strong>
                    </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label for="studentFirstName">Date</label>
            <input type="date" class="form-control {{$errors->has('date') ? ' is-invalid' : ''}}" name="date" id="date">
            @if ($errors->has('date'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date')}}</strong>
                    </span>
            @endif
        </div>

    </div>
</div>
