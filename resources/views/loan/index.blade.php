@extends('layouts.app')

@section('title')
    <title>Talent | Loan</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Loan Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Loan</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">

                <div class="row">

                    <div class="col">
                            <button id="btnExport" class="btn btn-warning btn-sm" onclick="fnExcelReport();" data-toggle="tooltip" data-placement="top" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                 Export</button>
                    </div>

                    <div class="col">
                        <small class="pull-right">

                                <a href="{{ route('employee_loans.create') }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="New" style="float: right;"><i class="fa fa-plus"></i></a>
                        </small>
                    </div>
                </div>
                
            </h3>
        </div>


        <div class="card-body">
    
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                    <tr>
                        <th class="search">Employee ID</th>
                        <th class="search">Employee Name</th>
                        <th class="search">Loan Amount</th>
                        <th class="search">Type</th>
                        <th class="search">Installment</th>
                        <th class="search">Payed Amount</th>
                        <th class="search">Loan Balance</th>
                        <th class="search">Installment Due</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($loans as $loan)
                        @if($loan->company_id == auth()->user()->company_id)
                        <tr>
                            <td>{{ $loan->user_id }}</td>

                            @foreach($employee as $emp)
                                @if($emp->user_id == $loan->user_id)
                                    <td>{{ $emp->fname }} {{ $emp->mname }}</td>
                                @endif
                            @endforeach

                            <td>{{ $loan->loan_total }}</td>
                            <td>{{ $loan->loan_type }}</td>
                            <td>{{ $loan->monthly_installment }}</td>
                            <td>{{ $loan->payed_installments*$loan->monthly_installment }}</td>
                            <td>{{ ($loan->no_installments-$loan->payed_installments)*$loan->monthly_installment }}</td>
                            <td>{{ $loan->no_installments-$loan->payed_installments }}</td>

                        </tr>
                        @endif
                    @endforeach
                </tbody>

            </table>

        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>


    <div style="display: none">

        <table class="table table-bordered table-striped" id="loanTable">
            <thead>
            <tr>
                <td>S.N</td>
                <th class="search">Emp No</th>
                <th class="search">Emp Name</th>
                <th class="search">Loan Description</th>
                <th class="search">Loan Amount</th>
                <th class="search">Loan Granted date</th>
                <th class="search">Installment</th>
                <th class="search">Amount Payed</th>
                <th class="search">Loan Balance</th>
                <th class="search">Installment Due</th>
            </tr>
            </thead>

            <tbody>
            <?php
                    $id =1;
            ?>
            @foreach($loans as $loan)
                @if($loan->company_id == auth()->user()->company_id)
                    <tr>
                        <td><?php echo($id++) ?></td>
                        <td>{{ $loan->user_id }}</td>
                        @foreach($employee as $emp)
                            @if($emp->user_id == $loan->user_id)
                                <td>{{ $emp->fname }} {{ $emp->mname }}</td>
                                @break
                            @endif
                        @endforeach
                        <td>{{ $loan->loan_type }}</td>
                        <td>{{ $loan->loan_total }}</td>
                        <td>{{ $loan->start_date }}</td>
                        <td>{{ $loan->monthly_installment }}</td>
                        <td>{{ $loan->payed_installments*$loan->monthly_installment }}</td>
                        <td>{{ ($loan->no_installments-$loan->payed_installments)*$loan->monthly_installment }}</td>
                        <td>{{ $loan->no_installments-$loan->payed_installments }}</td>

                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>


    </div>
@endsection


@section('custom-jquery')
    <script>
        function fnExcelReport()
        {
            var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j=0;
            tab = document.getElementById('loanTable'); // id of table

            for(j = 0 ; j < tab.rows.length ; j++)
            {
                tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text=tab_text+"</table>";
            tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
            tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html","replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

    </script>

@endsection
