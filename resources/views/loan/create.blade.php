@extends('layouts.app')

@section('title')
    <title>Talent | Loan</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Loan Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('employee_loans.index') }}">Loan</a></li>
    <li class="breadcrumb-item active">Add Loan</li>
@endsection

@section('content')

    <div class="card">

        <form action="{{ route('employee_loans.store') }}" method="POST" id="loan">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Employee ID <span class="required">*</span></label>
                            <input type="text" name="company_id" hidden value="{{auth()->user()->company_id}}">
                            <select class="form-control{{$errors->has('emp_id') ? ' is-invalid' : ''}}" id="id" name="emp_id" >
                                <option value="">Select the Employee</option>
                                @foreach($employees as $employee)
                                    @if($employee->company_id == auth()->user()->company_id )
                                        <option  value="{{ $employee->user_id }}-{{ $employee->fname }} {{ $employee->mname }}">{{ $employee->user_id }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('emp_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('emp_id')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>Employee Name <span class="required">*</span></label>
                            <input type="text" id="name" class="form-control{{$errors->has('emp_name') ? ' is-invalid' : ''}}" name="emp_name" col="5" placeholder="Employee Name" v-validate>
                            @if ($errors->has('emp_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('emp_name')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Loan description <span class="required">*</span></label>
                            <textarea type="text" class="form-control{{$errors->has('loan_desc') ? ' is-invalid' : ''}}"  name="loan_desc" col="5" placeholder="Loan description"></textarea>
                            @if ($errors->has('loan_desc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('loan_desc')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Loan Amount <span class="required">*</span></label>
                            <input type="text" id="demo"  class="form-control{{$errors->has('loan_amount') ? ' is-invalid' : ''}}" name="loan_amount" placeholder="Loan Amount">
                            @if ($errors->has('loan_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('loan_amount')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>No of installments in months<span class="required">*</span></label>
                            <input type="text" id="count" class="form-control{{$errors->has('no_of_months') ? ' is-invalid' : ''}}" name="no_of_months" placeholder="No of installments">
                            @if ($errors->has('no_of_months'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_of_months')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Interest rate <span class="required">*</span></label>
                            <input type="text" id="intrest"  placeholder="00%" class="form-control{{$errors->has('interest_rate') ? ' is-invalid' : ''}}" name="interest_rate" id="percent">
                            @if ($errors->has('interest_rate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('interest_rate')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>Monthly installment <span class="required">*</span></label>
                            <input type="text"  id="demo1"  class="form-control{{$errors->has('monthly_inst') ? ' is-invalid' : ''}}" name="monthly_inst" placeholder="Monthly installment">
                            @if ($errors->has('monthly_inst'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('monthly_inst')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Payed Installments <span class="required">*</span></label>
                            <input type="text"  class="form-control{{$errors->has('payed_installments') ? ' is-invalid' : ''}}" name="payed_installments" placeholder="Payed installments" value="0">
                            @if ($errors->has('payed_installments'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('payed_installments')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label>Start Date <span class="required">*</span></label>
                            <input type="date"  class="form-control{{$errors->has('start_date') ? ' is-invalid' : ''}}" name="start_date" id="start_date">
                            @if ($errors->has('start_date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_date')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <input type="hidden"  class="form-control{{$errors->has('end_date') ? ' is-invalid' : ''}}" name="end_date" id="end_date">
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <a href="{{url()->previous()}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                <button type="submit" :disabled="'errors.any()'" class="btn btn-primary btn-sm">Add</button>
            </div>
        </form>
    </div>

@endsection

@section('custom-jquery')
    <script>


                $("#percent").mask("99.99%");

                $('#demo').maskMoney({

                    // The symbol to be displayed before the value entered by the user
                    prefix:'',

                    // The suffix to be displayed after the value entered by the user(example: "1234.23 €").
                    suffix: "",

                    // Delay formatting of text field until focus leaves the field
                    formatOnBlur: false,

                    // Prevent users from inputing zero
                    allowZero:false,

                    // Prevent users from inputing negative values
                    allowNegative:true,

                    // Allow empty input values, so that when you delete the number it doesn't reset to 0.00.
                    allowEmpty: false,

                    // Select text in the input on double click
                    doubleClickSelection: true,

                    // Select all text in the input when the element fires the focus event.
                    selectAllOnFocus: false,

                    // The thousands separator
                    thousands: ',',

                    // The decimal separator
                    decimal: '.' ,

                    // How many decimal places are allowed
                    precision: 2,

                    // Set if the symbol will stay in the field after the user exits the field.
                    affixesStay : false,

                    // Place caret at the end of the input on focus
                    bringCaretAtEndOnFocus: true

                });

                $('#intrest').change(function(){

                    if($("#intrest").val()=="" ||  $("#demo").val()=="" || $("#count").val()=="" ){

                    }else{
                        var count = $("#count").val();
                        var amount = $("#demo").val();
                        //parseNumberCustom(amount);

                        var removed = amount.replace(",", "");
                        var removed1 = removed.replace(",", "");
                        var removed2 = removed1.replace(",", "");
                        var removed3 = removed2.replace(",", "");
                        var removed4 = removed3.replace(",", "");

                        console.log(removed4);
                        var arr = removed4.split('.');
                        var new_amount =   arr[0];
                        var intrest = $("#intrest").val();


                        console.log(count);
                        console.log(arr[0]);
                        console.log(new_amount);
                        console.log(intrest);

                        var inter = new_amount*intrest/100;
                        console.log(inter);
                        var total = parseInt(new_amount)+parseInt(inter);
                        console.log(total);
                        var installment = total/parseInt(count);
                        console.log(installment);

                        var newInstallment = Math.round(installment*100.0)/100;
                        console.log(newInstallment);

                        $("#demo1").val(newInstallment);
                    }

                });

                $('#demo').change(function(){

                    if($("#intrest").val()=="" ||  $("#demo").val()=="" || $("#count").val()=="" ){

                    }else{
                        var count = $("#count").val();
                        var amount = $("#demo").val();
                        //parseNumberCustom(amount);

                        var removed = amount.replace(",", "");
                        var removed1 = removed.replace(",", "");
                        var removed2 = removed1.replace(",", "");
                        var removed3 = removed2.replace(",", "");
                        var removed4 = removed3.replace(",", "");

                        console.log(removed4);
                        var arr = removed4.split('.');
                        var new_amount =   arr[0];
                        var intrest = $("#intrest").val();


                        console.log(count);
                        console.log(arr[0]);
                        console.log(new_amount);
                        console.log(intrest);

                        var inter = new_amount*intrest/100;
                        console.log(inter);
                        var total = parseInt(new_amount)+parseInt(inter);
                        console.log(total);
                        var installment = total/parseInt(count);
                        console.log(installment);

                        var newInstallment = Math.round(installment*100.0)/100;
                        console.log(newInstallment);

                        $("#demo1").val(newInstallment);
                    }

                });

                $('#count').change(function(){

                    if($("#intrest").val()=="" ||  $("#demo").val()=="" || $("#count").val()=="" ){

                    }else{
                        var count = $("#count").val();
                        var amount = $("#demo").val();
                        //parseNumberCustom(amount);

                        var removed = amount.replace(",", "");
                        var removed1 = removed.replace(",", "");
                        var removed2 = removed1.replace(",", "");
                        var removed3 = removed2.replace(",", "");
                        var removed4 = removed3.replace(",", "");

                        console.log(removed4);
                        var arr = removed4.split('.');
                        var new_amount =   arr[0];
                        var intrest = $("#intrest").val();


                        console.log(count);
                        console.log(arr[0]);
                        console.log(new_amount);
                        console.log(intrest);

                        var inter = new_amount*intrest/100;
                        console.log(inter);
                        var total = parseInt(new_amount)+parseInt(inter);
                        console.log(total);
                        var installment = total/parseInt(count);
                        console.log(installment);

                        var newInstallment = Math.round(installment*100.0)/100;
                        console.log(newInstallment);

                        $("#demo1").val(newInstallment);
                    }


                });

                function parseNumberCustom(number_string) {
                    var new_number = parseInt(number_string.indexOf(',') >= 3 ? number_string.split(',')[0] : number_string.replace(/[^0-9\.]/g, ''));
                    console.log('Before==>' + number_string + ', After==>' + new_number);
                    return new_number;
                }

                $('#id').change(function(){
                    var id = $("#id :selected").val();

                    console.log(id);
                    var arr = id.split('-');

                    console.log(arr[0]);
                    $("#name").val(arr[1]);
                });

                $('#start_date').change(function () {

                    var start_date = $("#start_date").val();

                    console.log(start_date)

                    var array = start_date.split('-');

                    var Y = array[0];
                    var M = array[1];
                    var D = array[2];

                    console.log(Y);
                    console.log(M);
                    console.log(D);

                    var no_of_month = $("#count").val();



                    // var year = $.add(Y,$.divide($.add(parseInt(M), no_of_month),12));

                    var monthSum = parseInt(M)+parseInt(no_of_month);

                    var monthArray = (monthSum/12).toString().split('.');

                    var monthModulus = monthSum%12;

                    //var newYear = parseInt(Y)+

                    //
                    //
                    console.log(monthArray[0])
                    console.log(monthModulus);

                    var newYear = parseInt(Y)+parseInt(monthArray[0]);

                    var end_date = newYear.toString() + '-' + monthModulus.toString() + '-' + D;

                    console.log(end_date);


                    $('#end_date').val(end_date);
                });


    </script>


@endsection
