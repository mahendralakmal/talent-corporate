@extends('layouts.app')

@section('title')
    <title>Corporate Solutions | Events</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Events Management</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Events</li>
@endsection

@section('content')


    <div class="card">
        <div class="card-body">
            <div class="row">
            <div class="col-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Create Event</h3>
                    </div>

                    <div class="card-body" >
                        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                            <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                            <ul class="fc-color-picker" id="color-chooser">
                                <li><a class="text-primary" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-warning" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-success" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-danger" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                            </ul>
                        </div>

                        <form action="{{route('events.store')}}" method="post" id="app2" >
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Event</label>
                                <input type="text" name="event_name" class="form-control {{$errors->has('event_name') ? ' is-invalid' : ''}}" value="{{ old('event_name') }}" placeholder="Event Title">
                                @if ($errors->has('event_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('event_name')}}</strong>
                                     </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Date</label>
                                <input type="date" class="form-control {{$errors->has('date') ? ' is-invalid' : ''}}" value="{{ old('date') }}" name="date" >
                                @if ($errors->has('date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="hidden" name="color" id="color" value="#0062cc">

                            <div class="form-group">
                                <button type="submit" id="add-new-event" class="btn btn-primary btn-flat">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-9">
                <div class="card">

                    <div class="card-body">
                        <div id="calender" >
                            {!! $calendar->calendar() !!}
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')

    {!! $calendar->script() !!}

    <script>

        $(function () {

            /* ADDING EVENTS */
            var currColor = '#3c8dbc' //Red by default

            var colorChooser = $('#color-chooser-btn')


            $('#color-chooser > li > a').click(function (e) {
                e.preventDefault()
                //Save color
                currColor = $(this).css('color')

               $('#color').val(rgb2hex(currColor));

                //Add color effect to button
                $('#add-new-event').css({
                    'background-color': currColor,
                    'border-color'    : currColor
                })

                console.log(rgb2hex(currColor))
            })

            function rgb2hex(rgb) {
                rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                function hex(x) {
                    return ("0" + parseInt(x).toString(16)).slice(-2);
                }
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
            }

            $('#add-new-event').click(function (e) {
                //e.preventDefault()

                var val = $('#new-event').val()

                // if (val.length == 0) {
                //     return
                // }

                //Create events
                var event = $('<div />')
                event.css({
                    'background-color': currColor,
                    'border-color'    : currColor,
                    'color'           : '#fff'
                }).addClass('external-event')
                event.html(val)

                $('#external-events').prepend(event)

                //Remove event from text input
                $('#new-event').val('')
            })
        })

    </script>

    <script>
        var app2 = new Vue({
            el: '#app2',
            data: {
                event_name: null,
                date: null
            }
        })
    </script>

@endsection
