@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Home</a></li>
    <li class="breadcrumb-item">Reports</li>
    <li class="breadcrumb-item active">WHT</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                All WHT's
            </h3>

            <br>

            <form action="{{ route('wht.store') }}" method="post">
                {{ csrf_field() }}

                <div class="row">

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Year</label>
                            <select class="form-control{{$errors->has('year') ? ' is-invalid' : ''}}" name="year">
                                <option value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>
                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">.</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
                                    data-placement="top" title="Calculate"><i class="fa fa-search"
                                                                              aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </form>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                        <td>S.N</td>
                        <td>EMP No</td>
                        <td>EPF No.</td>
                        <td>Name</td>
                        <td>Designation</td>
                        <td>Approval</td>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection
