
@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Reports</li>
    <li class="breadcrumb-item active">Payslip</li>
@endsection

@section('content')
<div class="card">
        <div class="card-header">
            <h5 class="card-title">All Payslips</h5>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                    <tr>
                        <th class="search">Employee ID</th>
                        <th class="search">Name</th>
                        <th class="search">EPF No</th>
                        <th class="search">Designation</th>
                        <th class="search">NetPay</th>
                        <th class="search">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($payslips as $payslip)
                       <tr>
                           <td>{{$payslip->year}}</td>
                           <td>{{date("F", mktime(0, 0, 0, $payslip->month,1))}}</td>
                           <td>{{$payslip->emp_id}}</td>
                           <td>{{$payslip->name}}</td>
                           <td>{{$payslip->epf_no}}</td>
                           <td>{{$payslip->designation}}</td>
                           <td>{{$payslip->netPay}}</td>
                           <td><a href="{{route('payslip.show',$payslip->id)}}" id="download" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                       </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        // var doc = new jsPDF();
        // var specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        //
        // $('#cmd').click(function () {
        //     doc.fromHTML($('#content').html(), 15, 15, {
        //         'width': 170,
        //         'elementHandlers': specialElementHandlers
        //     });
        //     doc.save('sample-file.pdf');
        // });
    </script>
@endsection
