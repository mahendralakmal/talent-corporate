@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>

    <style>
        #report table, td, th {

            padding: 9px;
        }

        #report .left ul {
            list-style: none;
        }

        #report table thead {
            font-weight: bold;
            background-color: #E2EFDA;
        }

        @media print {
            #hilightcell {
                background-color: #D9D9D9;
            }
            .pull-right  style=text-align: right;{
                float: right;
                text-align: right;
            }
            #report table, td, th {

                padding: 9px;
            }

            #report .left ul {
                list-style: none;
            }

            #report table thead {
                font-weight: bold;
                background-color: #E2EFDA;
            }

            @page {
                size: auto;   /* auto is the initial value */
                margin: 0mm;  /* this affects the margin in the printer settings */
            }
        }
    </style>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Payslip</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{'/home'}}">Payslip</a></li>
    <li class="breadcrumb-item active">Payslip Report</li>
@endsection

@section('content')

    <div class="card" id="printableDiv">

        <div class="card-header">
            <small class="pull-right" style="text-align: right;">
                <button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Print"><i
                        class="fa fa-print" aria-hidden="true" id="print"></i></button>
            </small>
        </div>

		<?php

		$BasicSalary = 0;
		$Allowance1 = 0;
		$Allowance2 = 0;
		$Allowance3 = 0;
		$Allowance4 = 0;
		$Allowance5 = 0;
		$Allowance6 = 0;
		$Allowance7 = 0;
		$Allowance8 = 0;
		$Allowance9 = 0;
		$Allowance10 = 0;
		$Bonus = 0;
		$Commission = 0;
		$OverTime = 0;
		$LeavePay = 0;
		$Arrears = 0;
		$Adjustment = 0;
		$TotalGross = 0;
		$EPF8 = 0;
		$noPay = 0;
		$PAYE = 0;
		$Advance = 0;
		$Loan = 0;
		$SD = 0;
		$Deduction1 = 0;
		$Deduction2 = 0;
		$TotalDeduction = 0;
		$NetPay = 0;
		$EPF12 = 0;
		$ETF3 = 0;
		$Gratuity = 0;
		$CTC = 0;
		$countAllowance1 = 0;
		$countAllowance2 = 0;
		$countAllowance3 = 0;
		$countAllowance4 = 0;
		$countAllowance5 = 0;
		$countAllowance6 = 0;
		$countAllowance7 = 0;
		$countAllowance8 = 0;
		$countAllowance9 = 0;
		$countAllowance10 = 0;

		$countBasic = 0;
		$countBonus = 0;
		$countCommission = 0;
		$countOverTime = 0;
		$countLeavePay = 0;
		$countArrears = 0;
		$countAdjustment = 0;
		$countTotalGross = 0;
		$countNoPay = 0;
		$countEPF8 = 0;
		$countPAYE = 0;
		$countAdvance = 0;
		$countLoan = 0;
		$countSD = 0;
		$countDeduction1 = 0;
		$countDeduction2 = 0;
		$countTotalDeduction = 0;
		$countNetPay = 0;
		$countEPF12 = 0;
		$countETF3 = 0;
		$countGratuity = 0;
		$countCTC = 0;

		?>

        @foreach($payadvice as $paysheet)
            <tr>

				<?php $BasicSalary += $paysheet->basic_salary;
				$countBasic++;
				?>
				<?php if ($paysheet->allowance01 != 0) {
					$Allowance1 += $paysheet->allowance01;
					$countAllowance1++;
				}?>
				<?php if ($paysheet->allowance02 != 0) {
					$Allowance2 += $paysheet->allowance02;
					$countAllowance2++;
				}?>
				<?php if ($paysheet->allowance03 != 0) {
					$Allowance3 += $paysheet->allowance03;
					$countAllowance3++;
				}?>
				<?php if ($paysheet->allowance04 != 0) {
					$Allowance4 += $paysheet->allowance04;
					$countAllowance4++;
				}?>

				<?php if ($paysheet->allowance05 != 0) {
					$Allowance5 += $paysheet->allowance05;
					$countAllowance5++;
				}?>
				<?php if ($paysheet->allowance06 != 0) {
					$Allowance6 += $paysheet->allowance06;
					$countAllowance6++;
				}?>
				<?php if ($paysheet->allowance07 != 0) {
					$Allowance7 += $paysheet->allowance07;
					$countAllowance7++;
				}?>
				<?php if ($paysheet->allowance08 != 0) {
					$Allowance8 += $paysheet->allowance08;
					$countAllowance8++;
				}?>

				<?php if ($paysheet->allowance09 != 0) {
					$Allowance9 += $paysheet->allowance09;
					$countAllowance9++;
				}?>
				<?php if ($paysheet->allowance10 != 0) {
					$Allowance10 += $paysheet->allowance10;
					$countAllowance10++;
				}?>

				<?php if ($paysheet->performanceBonus != 0) {
					$Bonus += $paysheet->performanceBonus;
					$countBonus++;
				}?>
				<?php
				if ($paysheet->commission != 0) {
					$Commission += $paysheet->commission;
					$countCommission++;
				} ?>
				<?php
				if ($paysheet->commission != 0) {
					$OverTime += $paysheet->OT;
					$countCommission++;
				} ?>
				<?php
				if ($paysheet->leave_pay != 0) {
					$LeavePay += $paysheet->leave_pay;
					$countLeavePay++;
				} ?>
				<?php
				if ($paysheet->arrears != 0) {
					$Arrears += $paysheet->arrears;
					$countArrears++;
				}  ?>
				<?php
				if ($paysheet->adjustments != 0) {
					$Adjustment += $paysheet->adjustments;
					$countAdjustment++;
				}  ?>
				<?php
				if ($paysheet->gross_remuneration != 0) {
					$TotalGross += $paysheet->gross_remuneration;
					$countTotalGross++;
				}  ?>
				<?php
				if ($paysheet->noPay != 0) {
					$noPay += $paysheet->noPay;
					$countNoPay++;
				} ?>
				<?php
				if ($paysheet->epf08 != 0) {
					$EPF8 += $paysheet->epf08;
					$countEPF8++;
				} ?>
				<?php
				if ($paysheet->paye != 0) {
					$PAYE += $paysheet->paye;
					$countPAYE++;
				} ?>
				<?php
				if ($paysheet->advance != 0) {
					$Advance += $paysheet->advance;
					$countAdvance++;
				} ?>
				<?php
				if ($paysheet->loan != 0) {
					$Loan += $paysheet->loan;
					$countLoan++;
				} ?>
				<?php
				if ($paysheet->SD != 0) {
					$SD += $paysheet->SD;
					$countSD++;
				} ?>
				<?php
				if ($paysheet->otherDeduction01 != 0) {
					$Deduction1 += $paysheet->otherDeduction01;
					$countDeduction1++;
				}  ?>
				<?php
				if ($paysheet->otherDeduction02 != 0) {
					$Deduction2 += $paysheet->otherDeduction02;
					$countDeduction2++;
				}  ?>
				<?php
				if ($paysheet->totalDeduction != 0) {
					$TotalDeduction += $paysheet->totalDeduction;
					$countTotalDeduction++;
				} ?>
				<?php
				if ($paysheet->netPay != 0) {
					$NetPay += $paysheet->netPay;
					$countNetPay++;
				} ?>
				<?php
				if ($paysheet->epf12 != 0) {
					$EPF12 += $paysheet->epf12;
					$countEPF12++;
				} ?>
				<?php
				if ($paysheet->etf != 0) {
					$ETF3 += $paysheet->etf;
					$countETF3++;
				}  ?>
				<?php
				if ($paysheet->gratuity != 0) {
					$Gratuity += $paysheet->gratuity;
					$countGratuity++;
				}  ?>
				<?php
				if ($paysheet->CTC != 0) {
					$CTC += $paysheet->CTC;
					$countCTC++;
				} ?>
            </tr>
        @endforeach

        <div class="card-body">
            <style>
                table, td, th {

                    padding: 9px;
                }

                .left ul {
                    list-style: none;
                }

                table thead {
                    font-weight: bold;
                    background-color: #E2EFDA;
                }

                @media print {
                    #hilightcell {
                        background-color: #D9D9D9;
                    }

                    #report table, td, th {

                        padding: 9px;
                    }

                    #report .left ul {
                        list-style: none;
                    }

                    #report table thead {
                        font-weight: bold;
                        background-color: #E2EFDA;
                    }

                    @page {
                        size: auto;   /* auto is the initial value */
                        margin: 0mm;  /* this affects the margin in the printer settings */
                    }
                }
            </style>
            <div class="left">
                <ul>
                    <li><b>Pay Advise</b></li>
                    <br>
                    <li class="com">{{$company->name}}</li>
                    <li>EPF Reg. No.{{$company->EPFno}} | PAYE Reg. No. {{$company->PAYEno}}</li>
                    <br>
                    <li>Pay Advise for the month of {{ $mon }} 2019</li>

                    <br>

                    <table>
                        <thead>
                        <tr>
                            <td><b>Description</b></td>
                            <td><b>No.of Emps.</b></td>
                            <td><b>Amount(Rs.)</b></td>
                            <td><b>Due Date</b></td>
                            <td><b>Payee</b></td>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td><b>Pay Details:</b></td>
                        </tr>

                        <tr>
                            <td>Basic Salary</td>
                            <td><?php echo($countBasic) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($BasicSalary,2)) ?></td>
                        </tr>


						<?php if($countAllowance1 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance01}}</td>
                            <td><?php echo($countAllowance1) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance1, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance2 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance02}}</td>
                            <td><?php echo($countAllowance2) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance2, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance3 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance03}}</td>
                            <td><?php echo($countAllowance3) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance3, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance4 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance04}}</td>
                            <td><?php echo($countAllowance4) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance4, 2)) ?></td>
                        </tr>
						<?php
						}
						?>

						<?php if($countAllowance5 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance05}}</td>
                            <td><?php echo($countAllowance5) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance5, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance6 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance06}}</td>
                            <td><?php echo($countAllowance6) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance6, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance7 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance07}}</td>
                            <td><?php echo($countAllowance7) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance7, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance8 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance08}}</td>
                            <td><?php echo($countAllowance8) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance8, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance9 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance09}}</td>
                            <td><?php echo($countAllowance9) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance9, 2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countAllowance10 != 0){?>
                        <tr>
                            <td>{{$allowance->allowance10}}</td>
                            <td><?php echo($countAllowance10) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Allowance10,2)) ?></td>
                        </tr>
						<?php
						}
						?>




						<?php if($countBonus != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countBonus) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Bonus,2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countCommission != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countCommission) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Commission,2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countOverTime != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countOverTime) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($OverTime,2)) ?></td>
                        </tr>
						<?php
						}
						?>
						<?php if($countLeavePay != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countLeavePay) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($LeavePa,2)) ?></td>
                        </tr>
						<?php
						}
						?>

						<?php if($countArrears != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countArrears) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Arrears,2)) ?></td>
                        </tr>
						<?php
						}
						?>

						<?php if($countAdjustment != 0){?>
                        <tr>
                            <td>Performance/Bonus</td>
                            <td><?php echo($countAdjustment) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($Adjustment,2)) ?></td>
                        </tr>
						<?php
						}
						?>


                        <tr>
                            <td><b>Total Gross Remuneration</b></td>
                            <td><?php echo($countTotalGross) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($TotalGross,2)) ?></td>
                        </tr>

                        <tr>
                            <td></td>
                        </tr>

                        <tr>
                            <td>No Pay</td>
                            <td><?php echo($countNoPay) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($noPay,2)) ?></td>
                        </tr>
                        <tr>
                            <td>EPF Employee`s Contribution</td>
                            <td><?php echo($countEPF8) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($EPF8,2)) ?></td>
                        </tr>

                        <tr>
                            <td>PAYE Tax</td>
                            <td><?php echo($countPAYE) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($PAYE,2)) ?></td>
                        </tr>

                        <tr>
                            <td>Stamp Duty</td>
                            <td><?php echo($countSD) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($SD,2)) ?></td>
                        </tr>

                        <tr>
                            <td><b>Total Deductions</b></td>
                            <td><?php echo($countTotalDeduction) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($TotalDeduction,2)) ?></td>
                        </tr>

                        <tr>
                            <td><b>Net Pay</b></td>
                            <td>{{ $countNetPay }}</td>
                            <td>{{ number_format($NetPay,2) }}</td>
                            <td>{{ App\company::findOrFail(session('company'))->salaryDate }}/{{ $month }}/{{ $year }}</td>
                            <td>Employees</td>
                        </tr>

                        <tr class="row1">
                            <td>Cost to Company(CTC)</td>
                            <td><?php echo($countCTC) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($CTC,2)) ?></td>
                        </tr>

                        <tr>
                            <td><b><u>Statutory Payments:</u></b></td>
                        </tr>

                        <tr>
                            <td>EPF Employer&#39;s Contribution</td>
                            <td><?php echo($countEPF12) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($EPF12,2)) ?></td>
                        </tr>

                        <tr>
                            <td>Total EPF Contribution for the month</td>
                            <td></td>
                            <td></td>
                            <td><b>25/{{ $month }}/{{ $year }}</b></td>
                            <td>Employees Provident Fund</td>
                        </tr>

                        <tr>
                            <td>ETF Employer`s Contribution</td>
                            <td><?php echo($countETF3) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($ETF3,2)) ?></td>
                            <td><b>25/{{ $month }}/{{ $year }}</b></td>
                            <td>Employees Trust Fund</td>
                        </tr>

                        <tr>
                            <td>Stamp Duty</td>
                            <td><?php echo($countSD) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($SD,2)) ?></td>
                            <td><b>10/{{ $month }}/{{ $year }}</b></td>
                            <td>Reimburse to CBS</td>
                        </tr>

                        <tr>
                            <td>PAYE Tax</td>
                            <td><?php echo($countPAYE) ?></td>
                            <td class="pull-right" style="text-align: right;"><?php echo(number_format($PAYE,2)) ?></td>
                            <td><b>10/{{ $month }}/{{ $year }}</b></td>
                            <td>Department of Inland Revenue</td>
                        </tr>

                        <tr>
                            <td>Gratuity</td>
                            <td><?php echo($countGratuity) ?></td>
                            <td class="pull-right" style="text-align: right; color: red"><?php echo(number_format($Gratuity,2)) ?></td>
                        </tr>
                        </tbody>
                    </table>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')

@endsection
