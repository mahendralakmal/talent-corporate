@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Reports</li>
    <li class="breadcrumb-item active">Payslip</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                All Payslips
            </h3>
            <br>

            <form action="{{ route('payslip.store') }}" method="post">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">EmpID</label>
                            <select class="form-control{{$errors->has('emp_id') ? ' is-invalid' : ''}}" name="emp_id">
                                <option value="">Select Employee ID</option>
                                @if(!is_null($employees))
                                    @foreach($employees as $employee)
                                        <option
                                            value="{{$employee->user_id}}">{{$employee->user_id}} {{$employee->name_with_initials}}</option>
                                    @endforeach
                                @endif
                            </select>

                            @if ($errors->has('emp_id'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('emp_id')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Year</label>
                            <select class="form-control{{$errors->has('year') ? ' is-invalid' : ''}}" name="year">
                                <option value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>

                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('year')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Month</label>
                            <select class="form-control{{$errors->has('month') ? ' is-invalid' : ''}}" name="month">
                                <option selected value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>

                            @if ($errors->has('month'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('month')}}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">file</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
                                    data-placement="top" title="Find"><i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">Employee ID</th>
                    <th class="search">Name</th>
                    <th class="search">EPF No</th>
                    <th class="search">Designation</th>
                    <th class="search">NetPay</th>
                    <th class="search">Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($payslips as $payslip)
                    <tr>
                        <td>{{$payslip->emp_id}}</td>
                        <td>{{$payslip->name}}</td>
                        <td>{{$payslip->epf_no}}</td>
                        <td>{{$payslip->designation}}</td>
                        <td>{{$payslip->netPay}}</td>
                        <td><a href="{{route('payslip.show','0')}}" id="download" class="btn btn-success btn-sm"
                               data-toggle="tooltip" data-placement="top" title="Download"><i class="fa fa-download"
                                                                                              aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        // var doc = new jsPDF();
        // var specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        //
        // $('#cmd').click(function () {
        //     doc.fromHTML($('#content').html(), 15, 15, {
        //         'width': 170,
        //         'elementHandlers': specialElementHandlers
        //     });
        //     doc.save('sample-file.pdf');
        // });
    </script>
@endsection
