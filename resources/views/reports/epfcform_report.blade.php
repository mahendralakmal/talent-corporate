@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>

    <style type="text/css">
        .left ul {
            list-style: none;
        }

        .left {
            float: left;

        }

        .right {
            float: right;
            margin-bottom: 20px;
        }

        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table thead {
            font-weight: bold;
        }

        .first {
            margin-bottom: 20px;
        }

        .content {
            text-align: center;
        }

        .content-left {
            text-align: left;
        }

        .content-right {
            text-align: right;
        }

        .content tr td {
            padding-left: 5px;
            padding-right: 5px;
        }

        .hide {
            color: #fff;
        }

        .foot-content ul {
            list-style: none;
        }

        .foot-left {
            float: left;
            margin-top: 20px;
        }

        .foot-right {
            float: right;
            margin-top: 25px;
        }

        .foot-left ul li {
            margin-bottom: 20px;
        }

        .foot-left ul {
            float: left;
        }

        .foot-right ul li {
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">EPF C Form File</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Home</a></li>
    <li class="breadcrumb-item active">EPF C Form</li>
@endsection

@section('content')
    <div class="card" id="printableDiv">
        <style type="text/css">
            @media print {
            #frmEpfCForm, #frmEtfCForm { display: none; }
            .left ul {list-style: none;}
            .left {float: left;}
            .right {float: right;margin-bottom: 20px;}
            table, td, th {border: 1px solid black;}
            table {border-collapse: collapse;width: 100%;}
            table thead {font-weight: bold;}
            .first {margin-bottom: 20px;}
            .content {text-align: center;}
            .content-left {text-align: left;}
            .content-right {text-align: right;}
            .content tr td {padding-left: 5px;padding-right: 5px;}
            .hide {color: #fff;}
            .foot-content ul {list-style: none;}
            .foot-left {float: left;margin-top: 20px;}
            .foot-right {float: right;margin-top: 25px;}
            .foot-left ul li {margin-bottom: 20px;}
            .foot-left ul {float: left;}
            .foot-right ul li {margin-bottom: 10px;}
            }
        </style>
        <div class="card-header">
            <small class="row pull-right">
                <a class="btn btn-success" id="print" data-toggle="tooltip" data-placement="top"
                   title="Print"><i class="fa fa-print" aria-hidden="true"></i></a>
                <form action="{{ route('GetEpfFile') }}" id="frmEpfCForm" method="post">
                    @csrf
                    <input type="hidden" id="year" name="year" value="{{$year}}">
                    <input type="hidden" id="month" name="month" value="{{$month}}">
                    <button class="btn btn-success" data-toggle="tooltip" id="downloadEpf" data-placement="top"
                            title="download EPF">EPF <i class="fa fa-cloud-download" aria-hidden="true"></i></button>
                </form>
                <form action="{{ route('GetEtfFile') }}" id="frmEtfCForm" method="post">
                    @csrf
                    <input type="hidden" id="year" name="year" value="{{$year}}">
                    <input type="hidden" id="month" name="month" value="{{$month}}">
                    <button class="btn btn-success end" data-toggle="tooltip" id="downloadEtf" data-placement="top"
                            title="download ETF">ETF <i class="fa fa-cloud-download" aria-hidden="true"></i></button>
                </form>
            </small>
        </div>

		<?php

		$Total = 0;
		$epf08 = 0;
		$epf12 = 0;
		$sn = 1;

		?>

        @foreach($epfData as $epf)
			<?php
			$Total += $epf->epf08;
			$epf08 += $epf->epf08;
			$Total += $epf->epf12;
			$epf12 += $epf->epf12;

			?>
        @endforeach

        <div class="card-body" id="report">
            <div class="right" style="min-width: 31rem;">
                <table class="first">
                    <tr>
                        <td class="content-left">Summary of Remittance for Employment No</td>
                        <td  style="width: 40%">{{$count}}</td>
                    </tr>

                    <tr>
                        <td class="content-left">Contributions</td>
						<?php
						$price_text = (string)$Total; // convert into a string
						$arr = str_split($price_text, "3"); // break string in 3 character sets

						$price_new_text = implode(",", $arr);
						?>
                        <td><?php echo(number_format($Total,2)); ?></td>
                    </tr>

                    <tr>
                        <td class="content-left">Surcharges</td>
                        <td>-</td>
                    </tr>

                    <tr>
                        <td class="content-left">Other Payments</td>
                        <td>-</td>
                    </tr>

                    <tr>
                        <td class="content-left">Total Remittance</td>
                        <td><?php echo(number_format($Total,2)); ?></td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td class="content-left">Cheque No</td>
                        <td style="width: 75%"></td>
                    </tr>

                    <tr>
                        <td class="content-left">Bank &amp; Branch</td>
                        <td></td>
                    </tr>
                </table>
            </div>

            <div class="left">
                <ul style="float: left; text-align: left; ">
                    <li>{{$company->name}}</li>
					<?php
					$address = explode(',', $company->address);

					foreach($address as $value) {
					?>
                    <li><?php echo($value) ?></li>

					<?php } ?>
                </ul>
            </div>
            <table class="content">
                <tr>
                    <td colspan="2" style="text-align: left;"><b>C FORM</b></td>
                    <td colspan="3">E. P. F. Contribution for: {{ $mon }} <b>{{ $year }}</b></td>
                    <td colspan="3">E. P. F. Registration <b>No: {{ App\company::findOrFail(session('company'))->EPFno }}</b></td>
                </tr>

                <tr>
                    <td rowspan="2">S.N</td>
                    <td rowspan="2">Employee`s Name</td>
                    <td rowspan="2">NIC No.</td>
                    <td rowspan="2">Sex</td>
                    <td rowspan="2">EPF No.</td>
                    <td colspan="3">Contribution</td>
                </tr>

                <tr>
                    <td>Total</td>
                    <td>Employer</td>
                    <td>Employee</td>
                </tr>

                <tbody>
                @foreach($epfData as $key=>$data)
					<?php if($data->epf_no > 0){?>
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td class="content-left">{{ App\Employee::where('user_id',$data->emp_id)->first()->name_with_initials }}</td>
                        <td class="content-left">{{$data->nic}}</td>
						<?php
						$gender = \App\Employee::where('user_id', '=', $data->emp_id)->where('company_id', '=', $data->company_id)->value('gender');
						?>
                        <td class="content-left"><?php echo($gender) ?></td>

                        <td>{{$data->epf_no}}</td>
						<?php
						$sum = $data->epf08 + $data->epf12;
						?>
                        <td class="content-right"><?php echo(number_format($sum, 2))?></td>
                        <td class="content-right"><?php echo(number_format($data->epf08, 2))?></td>
                        <td class="content-right"><?php echo(number_format($data->epf12, 2))?></td>
                    </tr>
					<?php } ?>
                @endforeach


                </tbody>

                <tfoot>
                <tr>
                    <td colspan="1"
                        style="border-bottom-style: hidden;border-left-style: hidden; border-right-style: hidden;"></td>
                    <td style="text-align: left; border-bottom-style: hidden;border-right-style: hidden;">Total</td>
                    <td colspan="3" style="border-bottom-style: hidden;"></td>
                    <td class="content-right"><?php echo(number_format($Total, 2)) ?></td>
                    <td class="content-right"><?php echo(number_format($epf08, 2))?></td>
                    <td class="content-right"><?php echo(number_format($epf12, 2)) ?></td>

                </tr>
                </tfoot>
            </table>

            <div class="foot-content">

                <div class="foot-right">
                    <ul>
                        <li>……………………………………………….</li>
                        <li>Signature of Employer</li>
                    </ul>
                </div>

                <div class="foot-left">
                    <ul>
                        <li>I certify that the information given above is correct.</li>
                        <li>TP No:{{$company->telephone}}</li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        // $('#downloadEpf').on('click', function () {
        //     $('#frmEpfCForm').submit(function (e) {
        //         e.preventDefault();
        //         var year = $('#year').val();
        //         var month = $('#month').val();
        //         // alert(year + '-' + month);
        //
        //         $.ajax({
        //             url: "/bankfile/getEpfFile",
        //             method: "post",
        //             data: {year: year, month: month, _token: $('meta[name="csrf-token"]').attr('content')},
        //             datatype: "json",
        //             success: function (data) {
        //                 console.log(data);
        //
        //                 // alert("Succes->" + data);
        //             },
        //             error: function (data) {
        //                 // alert("Error->" + data);
        //             }
        //         })
        //     })
        // });

        function print(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        // function print(){
        //     let doc = new jsPDF('p','pt','a4');
        //
        //     doc.addHTML($("#report"),function() {
        //         doc.save('html.pdf');
        //     });
        //
        // }

    </script>
@endsection
