@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>

    <style type="text/css">
        .left ul li {
            list-style: none;
            width: 600px;
        }

        .colord {
            display: inline;
            color: #D31B33;
            font-weight: bold;
        }

        .deco {
            display: inline;
            color: #000;
            font-weight: bold;
            text-decoration: underline;
        }

        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 600px;
        }

        table thead {
            font-weight: bold;
        }

        table tr td {
            text-align: center;
        }

        .col-width1 {
            width: 10%;
        }

        .col-width2 {
            width: 4%;
        }

        .col-width3 {
            width: 10%;
        }

        .col-width4 {
            width: 30%;
        }

        .col-width5 {
            width: 20%;
        }

        .col-width6 {
            width: 15%;
        }

        .foot {
            margin-top: 20px;
        }

        .sign {
            margin-top: 60px;
        }
    </style>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Bank File</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Home</a></li>
    <li class="breadcrumb-item active">Payslip</li>
@endsection

@section('content')
    <div class="card" id="printableDiv">
        <div class="card-header">
            <small class="pull-right">
                <a class="btn btn-success" id="print" data-toggle="tooltip" data-placement="top"
                   title="Print"><i class="fa fa-print" aria-hidden="true"></i></a>
                <button class="btn btn-success"
                        data-toggle="modal" data-target="#bnkFilegenerate" title="download"><i
                        class="fa fa-cloud-download"
                        aria-hidden="true"></i></button>
            </small>
        </div>

        <style>
            #hilightcell { background-color: #D9D9D9; }
            #report table, td, th { border: 1px solid silver; }
            #report .left ul { list-style: none; }
            #report table thead { font-weight: bold; }
            .modal{ display: none; }
            @media print {
                #hilightcell { background-color: #D9D9D9; }
                #report table, td, th { border: 1px solid silver; }
                #report .left ul { list-style: none; }
                #report table thead { font-weight: bold; }
                .modal{ display: none; }
                @page
                {
                    size: auto;   /* auto is the initial value */
                    margin: 0mm;  /* this affects the margin in the printer settings */
                }
            }
        </style>

		<?php

		$Total = 0;
		$sn = 1;

		?>

        @foreach($bankfile as $bank)
			<?php
			$Total += $bank->netPay;
			?>
        @endforeach

        <div class="card-body" id="report">
            <div class="left">
                <ul>
                    <li><?php echo(date('d/m/Y')); ?></li>
                    <br>
                    <li><b>The Manager</b></li>
					<?php $bankName = \App\bank::where('bank_code', '=', $company->bank_code)->value('bank_name'); ?>
                    <li><?php echo($bankName) ?> Bank</li>
					<?php $branchName = \App\Bank_Branch::where('bank_code', '=', $company->bank_code)->where('branch_code', '=', $company->branch_code)->value('bank_branch'); ?>
                    <li><?php echo($branchName) ?></li>
                    <br>
                    <li>Ref: Current Account No. {{$company->account_number}}</li>
                    <br>
                    <li>Dear Sir/Madam,</li>
                    <br>
                    <li><b>Request for Fund Transfer –Employees A/C.</b></li>
                    <br>
					<?php
					$price_text = (string)$Total; // convert into a string
					$arr = str_split($price_text, "3"); // break string in 3 character sets

					$price_new_text = implode(",", $arr);
					?>
                    @php $f = new \NumberFormatter( locale_get_default(), \NumberFormatter::SPELLOUT ); @endphp
                    <li><p>It is kindly request to transfer sum of Rs. <?php echo(number_format($Total)) ?>/-
                            (Rupees {{ $f->format(round($Total,0)) }} Only) being
                            the salary of the staff for the <span class="deco">month of {{$mon}} {{$year}}.</span></p>
                    </li>
                    <br>
                    <li><b>The value date of the transfer should be {{ App\company::findOrFail(session('company'))->salaryDate }} {{$mon}} {{$year}}</b></li>
                    <br>

                    <table style="width: 60rem !important;">
                        <thead>
                        <tr>
                            <td class="col-width2">SN</td>
                            <td class="col-width5">Name</td>
                            <td class="col-width6">Bank & No</td>
                            <td class="col-width6">Branch & No</td>
                            <td class="col-width3">Account No.</td>
                            <td class="col-width3">Amount (Rs.)</td>
                            <td class="col-width4">Amount in word (Rs.)</td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($bankfile as $key=>$bank)
                            <tr>
                                @php $emp = App\Employee::where('user_id', $bank->emp_id)->first(); @endphp
	                            @php $Branch = App\Bank_Branch::where([['bank_code', $emp->bank_code],['branch_code', $emp->branch_code]])->first(); @endphp
                                <td>{{ $key+1 }}</td>
                                <td style="text-align: left">{{ $emp->account_name }}</td>
                                <td style="text-align: left">{{ App\bank::where('bank_code',$emp->bank_code)->first()->bank_name }} {{ $emp->bank_code }}</td>
                                <td style="text-align: left">{{ $Branch->bank_branch }} {{ $Branch->branch_code }}</td>
                                <td style="text-align: left">{{ $emp->account_no }}</td>
								<?php
								$price_text = (string)$bank->netPay; // convert into a string
								$arr = str_split($price_text, "3"); // break string in 3 character sets

								$price_new_text = implode(",", $arr);
								?>
                                <td style="text-align: right">{{ number_format($bank->netPay,2) }}</td>
                                <td style="text-align: left"><p>Rupees {{ $f->format(round($bank->netPay,0)) }} Only</p></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="foot">
                        <li>Please be acknowledged the above request.</li>
                        <br>
                        <li>For {{$company->name}}</li>
                        <li class="sign">Authorized Signatory</li>
                    </div>
                </ul>
            </div>

        </div>


        <div id="bnkFilegenerate" class="modal fade" role=".modal-dialog-centered">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Generate Bank File</h4>
                    </div>
                    <form action="{{ route('GetBankFile') }}" method="post" id="frmgenerate">
                        @csrf
                        <input type="hidden" id="year" name="year" value="{{$year}}">
                        <input type="hidden" id="month" name="month" value="{{$month}}">
                        {{--                        {{ method_field('PUT')}}--}}
                        <div class="modal-body">
                            <div class="form-group">
                                <lable>Transaction Date</lable>
                                <input type="date" id="transaction_date" name="transaction_date" class="form-control"
                                       required>
                                @if ($errors->has('transaction_date'))
                                    <span class="invalid-feedback"
                                          role="alert"><strong>{{ $errors->first('transaction_date')}}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                        <div class="row justify-content-md-center">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                            <button type="submit" id="generate" class="btn btn-success">Generate</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        $('#frmgenerate').submit(function (e) {
            $('#bnkFilegenerate').modal('toggle'); //or  $('#IDModal').modal('hide');
            $('#frmgenerate').submit();
            return false;
        });

        function print(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        // function print(){
        //     let doc = new jsPDF('p','pt','a4');
        //
        //     doc.addHTML($("#report"),function() {
        //         doc.save('html.pdf');
        //     });
        //
        // }

    </script>
@endsection

@section('custom-jquery')
    <script>


        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
        var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        function inWords(num) {
            if ((num = num.toString()).length > 9) return 'overflow';
            n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
            if (!n) return;
            var str = '';
            str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
            str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
            str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
            str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
            str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
            return str;
        }

        document.getElementById('number').onload = function () {
            console.log("inside");
            document.getElementById('words').innerHTML = inWords(document.getElementById('number').value);
        };

    </script>
@endsection
