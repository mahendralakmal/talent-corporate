@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Reports</li>
    <li class="breadcrumb-item active">Pay sheet</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                All Paysheets for the month of {{ $last_month }}
            </h3>

            <br>

            <div class="row">
                <div class="col-3">
                    <form action="{{ route('paysheet.print') }}" method="post" id="paysheet">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputFile">Year</label>
                            <select class="form-control {{$errors->has('year') ? ' is-invalid' : ''}}" name="year">
                                <option selected value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>

                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year')}}</strong>
                                </span>
                            @endif
                        </div>
                </div>

                <div class="col-3">
                    <div class="form-group">
                        <label for="exampleInputFile">Month</label>
                        <select class="form-control {{$errors->has('month') ? ' is-invalid' : ''}}" name="month">
                            <option selected value="">Select Month</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                        @if ($errors->has('month'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('month')}}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="col">
                    {{--                    <label for="exampleInputFile" style="color: #fff;">file</label>--}}
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top"
                                title="Calculate"><i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>

                </form>
            </div>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                        <td>S.N</td>
                        <td>EMP No</td>
                        <td>EPF No.</td>
                        <td>Name</td>
                        <td>Designation</td>
                        <td>DOJ</td>
                        <td>DOR</td>
                        <td>No.of days work</td>
                        <td>Total Gross remuneration</td>
                        <td>Total Deduction</td>
                        <td>Net pay</td>
                        <td>CTC</td>
                        <td>Approval</td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($paysheets as $paysheet)
                        <tr>
                            <td>{{$paysheet->SN}}</td>
                            <td>{{$paysheet->emp_id}}</td>
                            <td>{{$paysheet->epf_no}}</td>
                            <td>{{$paysheet->name}}</td>
                            <td>{{$paysheet->designation}}</td>
                            <td>{{$paysheet->doj}}</td>
                            <td>{{$paysheet->dor}}</td>
                            <td>{{$paysheet->noofdays_work}}</td>
                            <td>{{$paysheet->gross_remuneration}}</td>
                            <td>{{$paysheet->totalDeduction}}</td>
                            <td>{{$paysheet->netPay}}</td>
                            <td>{{$paysheet->CTC}}</td>
                            <td><a href="" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                                   title="Download"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection


