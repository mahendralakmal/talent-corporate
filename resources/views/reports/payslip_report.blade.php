@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>



    <style type="text/css">
        table, td, th {
            /*border: 1px solid black;*/
            border-bottom: 1px solid silver;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        #hilightcell {
            background-color: #D9D9D9;
        }

        #hilightcell_1 {
            background-color: #D9D9D9;
            padding-right: 10px;
            text-align: right;
        }

        table tr td {
            padding-left: 10px;
            padding-right: 10px;
        }

        .report_header {
            text-align: center;
            margin-bottom: 20px;
        }

        img {
            width: 80px;
        }

        #values {
            text-align: right;
            padding-right: 10px;
        }

        .values {
            text-align: right;
        }

        #report {
            -webkit-print-color-adjust: exact !important;
        }

        #right {
            text-align: right;
            padding-right: 10px;
        }

        @media print {
            #hilightcell {
                background-color: #D9D9D9;
            }

            @page {
                size: auto;   /* auto is the initial value */
                margin: 0mm;  /* this affects the margin in the printer settings */
            }
        }
    </style>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Payslip</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{'/home'}}">Payslip</a></li>
    <li class="breadcrumb-item active">Payslip Report</li>
@endsection

@section('content')
    <div class="card" id="printableDiv">
        <style>
            table, td, th {border-bottom: 1px solid silver;}

            table { border-collapse: collapse; width: 100%; }

            #hilightcell { background-color: #D9D9D9; }

            #hilightcell_1 { background-color: #D9D9D9; padding-right: 10px; text-align: right; }

            table tr td { padding-left: 10px; padding-right: 10px; }

            .report_header { text-align: center; margin-bottom: 20px; }

            img { width: 80px; }

            #values { text-align: right; padding-right: 10px; }

            .values { text-align: right; }

            #report { -webkit-print-color-adjust: exact !important; }

            #right { text-align: right; padding-right: 10px; }
        </style>

        <div class="card-header">
            <small class="pull-right">
                <button class="btn btn-success"  data-toggle="tooltip" data-placement="top"
                        title="Print" id="print"><i class="fa fa-print" aria-hidden="true"></i></button>
            </small>
        </div>

        <div class="card-body" id="report">

            <table style="width: 1000px;" id="tablePayslip">
                <tbody>
                <tr>
                    <td>
                        <div class="report_header">
                            <img src="{{url('/storage/company/'. $logo)}}" alt="">
                            <h3><b>{{$company->name}}</b></h3>
                            <h4><b>Pay Slip for the month of {{ $mon }} {{$payslip->year}}</b></h4>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="border: 1px solid silver">
                        @php
                            $income = array(
                                array('Basic Salary', $payslip->basic_salary),
                                array($allowance->allowance01, $payslip->allowance01),
                                array($allowance->allowance02, $payslip->allowance02),
                                array($allowance->allowance03, $payslip->allowance03),
                                array($allowance->allowance04, $payslip->allowance04),
                                array($allowance->allowance05, $payslip->allowance05),
                                array($allowance->allowance06, $payslip->allowance06),
                                array($allowance->allowance07, $payslip->allowance07),
                                array($allowance->allowance08, $payslip->allowance08),
                                array($allowance->allowance09, $payslip->allowance09),
                                array($allowance->allowance10, $payslip->allowance10),
                                array("Performance/Bonus", $payslip->performanceBonus),
                                array("Commission", $payslip->commission),
                                array("Over Time", $payslip->OT),
                                array("Arrears", $payslip->arrears),
                                array("Adjustment", $payslip->adjustments)
                            );
                            $incomeLength = count($income);
                            $key = 0;
                            //dd($incomeLength);
                        @endphp

                        <table style="width: 100%; " className="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="25%"><b>Name</b><span style="margin-right: 5rem; float: right;">:</span></td>
                                <td width="75%"><b>{{$payslip->name}}</b></td>
                            </tr>
                            {{--                            <tr>--}}
                            {{--                                <td width="25%"><b>Paysheet Reference</b></td>--}}
                            {{--                                <td width="75%" width="100px">--}}
                            {{--                                    <b>{{$payslip->year}}{{$payslip->month}}{{$payslip->SN}}</b>--}}
                            {{--                                </td>--}}
                            {{--                            </tr>--}}
                            <tr>
                                <td width="25%"><b>Emp No.</b><span style="margin-right: 5rem; float: right;">:</span>
                                </td>
                                <td width="75%">{{$payslip->emp_id}}</td>
                            </tr>
                            <tr>
                                <td width="25%"><b>E.P.F. No.</b><span
                                        style="margin-right: 5rem; float: right;">:</span></td>
                                <td width="75%">{{$payslip->epf_no}}</td>
                            </tr>
                            <tr>
                                <td width="25%"><b>Designation</b><span
                                        style="margin-right: 5rem; float: right;">:</span></td>
                                <td width="75%">{{$payslip->designation}}</td>
                            </tr>
                            <tr>
                                <td width="25%"><b>Department</b><span
                                        style="margin-right: 5rem; float: right;">:</span></td>
{{--                                <td width="75%">{{$payslip->department}}</td>--}}
                                <td width="75%">{{App\Department::findOrFail($payslip->department)->department_name}}</td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: 100%;" className="table table-bordered">
                            <tbody>
                            <tr>
                                <td></td>
                                <td id="values">Currency (LKR)</td>
                            </tr>
                            <tr>
                                <td width="75%" id="hilightcell"><b>Earnings</b></td>
                                <td width="25%" id="hilightcell"></td>
                            </tr>
                            {{--                BASIC SALARY--}}
                            <tr>
                                <td width="75%">{{$income[$key][0] }}</td>
                                <td id="values">{{ number_format($income[$key++][1],2) }}</td>
                            </tr>
                            {{--                PERFORMANCE / BONUS--}}
                            @php
                                for($i = $key; $i < $incomeLength; $i++){
                                    if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1],2)) @endphp</td>

                            </tr>
                            @php   break;}
                                    else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1],2)) @endphp</td>

                            </tr>
                            @php break;
                                    }else if($i > 10 && $income[$i][1] == 0){
                            @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>

                            </tr>

                            @php break;
                                    }else {
                                        $key++;
                                    }
                                }
                            @endphp
                            {{--                COMMITION--}}
                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                    if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">{{$income[$key][0]}}</td>
                                <td id="values">{{number_format($income[$key++][1], 2)}}</td>
                            </tr>
                            @php   break;}
                    else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                    }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
                    }else {
                        $key++;
                    }
                }
                            @endphp
                            {{--                OVETIME --}}
                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                    if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>

                            </tr>
                            @php   break;}
                    else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>

                            </tr>
                            @php break;
                    }else if($i > 10 && $income[$i][1] == 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>

                            </tr>
                            @php break;
                    }else {
                        $key++;
                    }
                }
                            @endphp
                            {{--                ARREARS--}}
                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                    if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                    else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                    }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                    }else {
                        $key++;
                    }
                }
                            @endphp

                            {{--                ADJUSTMENTS--}}
                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp


                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>

                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp





                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp

                            @php

                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
                }else {
	                $key++;

                }
                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
                }else {
	                $key++;
                }
                }

                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
                }else {
	                $key++;
                }
                }
                            @endphp

                            @php

                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php   break;}
                else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%">@php echo($income[$key][0]) @endphp</td>
                                <td id="values">@php echo(number_format($income[$key++][1], 2)) @endphp</td>
                            </tr>
                            @php break;
                }else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%">@php echo($income[$key++][0]) @endphp</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp
                            <tr>
                                <td width="75%"><b>Total Earnings</b></td>
                                <td id="values"><b>@php echo(number_format($payslip->gross_remuneration, 2)) @endphp</b>
                                </td>

                            </tr>
                            </tbody>
                        </table>

                        <table style="width: 100%;" className="table table-bordered">
                            <tbody>
                            <tr>
                                <td width="75%" id="hilightcell"><b>Deductions</b></td>
                                <td id="hilightcell"><b></b></td>
                            </tr>
                            <tr>
                                <td width="75%">No Pay</td>
                                <td id="values">@php echo(number_format($payslip->noPay, 2)) @endphp</td>
                            </tr>
                            <tr>
                                @php
                                    if($payslip->advance != 0 ){
                                @endphp
                                <td width="75%">Salary Advance</td>
                                <td id="values">@php echo(number_format($payslip->advance, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Salary Advance</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            <tr>
                                @php
                                    if($payslip->epf08 != 0 ){
                                @endphp
                                <td width="75%">EPF Contribution</td>
                                <td id="values">@php echo(number_format($payslip->epf08, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">EPF Contribution</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            <tr>
                                @php
                                    if($payslip->loan != 0 ){
                                @endphp
                                <td width="75%">Loan Recovery</td>
                                <td id="values">@php echo(number_format($payslip->loan, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Loan Recovery</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            <tr>
                                @php
                                    if($payslip->paye != 0 ){
                                @endphp
                                <td width="75%">PAYE</td>
                                <td id="values">@php echo(number_format($payslip->paye, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">PAYE</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            <tr>
                                @php
                                    if($payslip->SD != 0 ){
                                @endphp
                                <td width="75%">Stamp Duty</td>
                                <td id="values">@php echo(number_format($payslip->SD, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Stamp Duty</td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            @php

                                if($key == $incomeLength){@endphp
                            <tr>
                                @php
                                    if($payslip->otherDeduction01 != 0 ){
                                @endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">@php echo(number_format($payslip->otherDeduction01, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">{{ number_format(0,2)}}</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            @php
                                }else{
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                @php
                                    if($payslip->otherDeduction01 != 0 ){
                                @endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">@php echo(number_format($payslip->otherDeduction01, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">{{ number_format(0,2)}}</td>
                                @php
                                    }
                                @endphp

                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                @php
                                    if($payslip->otherDeduction01 != 0 ){
                                @endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">@php echo(number_format($payslip->otherDeduction01, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">{{ number_format(0,2)}}</td>
                                @php
                                    }
                                @endphp

                            </tr>
                            @php break;
							}elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td id="values">{{ number_format(0,2)}}/td>
                                @php
                                    if($payslip->otherDeduction01 != 0 ){
                                @endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">@php echo(number_format($payslip->otherDeduction01, 2)) @endphp</td>
                                @php
                                    }else{@endphp
                                <td width="75%">Other deductions</td>
                                <td id="values">{{ number_format(0,2)}}</td>
                                @php
                                    }
                                @endphp
                            </tr>

                            @php break;
							}else {
								$key++;
							}
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>

                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){
                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){
                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}elseif($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){
                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){
                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id="values">@php echo(number_format(0, 2)) @endphp</td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php break;
							}else {
								$key++;
							}
							}
                            @endphp

                            @php

                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%"></td>
                                <td></td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%"></td>
                                <td></td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td id=""></td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp

                            @php
                                for($i = $key; $i < $incomeLength; $i++){

                                if($income[$i][1] != 0 && $i > 0 && $i < 11){ @endphp
                            <tr>
                                <td width="75%"></td>
                                <td></td>
                            </tr>
                            @php   break;}
							else if($i > 10 && $income[$i][1] != 0){ @endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php break;
							}else if($i > 10 && $income[$i][1] == 0){@endphp
                            <tr>
                                <td width="75%" id=""></td>
                                <td id=""></td>
                            </tr>
                            @php
                                }else {
                                    $key++;
                                }
                                }
                            @endphp

                            <tr>
                                <td width="75%"><b>Total Deduction</b></td>
                                <td id="values">
                                    <b>@php echo(number_format($payslip->totalDeduction, 2)) @endphp</b></td>
                            </tr>

                            <tr id="hilightcell">
                                <td width="75%"><b>Net Salary | LKR</b></td>
                                <td id="values"><b>@php echo(number_format($payslip->netPay, 2)) @endphp</b></td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: 100%;" className="table table-bordered">
                            <tbody>
                            <tr>
                                <th colspan="2" id="hilightcell_1"><i style="float: left;">Employer's Contribution.</i>
                                </th>
                                <th id="hilightcell"></th>
                            </tr>


                            <tr>
                                <td width="70%">EPF Contribution.</td>
                                <td width="5%" id="valuse" class="values">12%</td>
                                <td width="25%" id="values">@php echo(number_format($payslip->epf12 , 2)) @endphp</td>
                            </tr>

                            <tr>
                                <td width="70%">ETF Contribution.</td>
                                <td width="5%" id="valuse" class="values">3%</td>
                                <td width="25%" id="values">@php echo(number_format($payslip->etf , 2)) @endphp</td>
                            </tr>
                            <tr>
                                <td colspan="2" id="hilightcell"><strong>Total Employer's Contribution</strong></td>
                                <td id="hilightcell" class="values">
                                    <strong>@php echo(number_format($payslip->epf12 + $payslip->etf , 2))@endphp</strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" id="hilightcell"><strong>Total Contribution for E.P.F.</strong></td>
                                <td id="hilightcell" class="values">
                                    <strong>@php echo(number_format($payslip->epf08 + $payslip->epf12 , 2)) @endphp</strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: 100%;" className="table table-bordered">
                            <tbody>
                            <tr>
                                <td colspan="4">Salary Bank</td>
                                <td class="values">{{\App\bank::where('bank_code', '=', $employee->bank_code)->first()->bank_name}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">Bank Branch</td>
                                <td class="values">{{\App\Bank_Branch::where('branch_code',$employee->branch_code)->first()->bank_branch}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">Bank Account No</td>
                                <td class="values">{{$employee->account_no}}</td>
                            </tr>

                            <tr>
                                <td colspan="6" id="hilightcell" height="20px"></td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <p style="text-align: center;">This is a computer generated document and hence not requires a
                            manual
                            signature</p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="editor"></div>
    </div>
@endsection

@section('custom-jquery')
{{--    <script>--}}
{{--        function print(div) {--}}
{{--            var printContents = document.getElementById('tablePayslip').innerHTML;--}}
{{--            var originalContents = document.body.innerHTML;--}}
{{--            document.body.innerHTML = printContents;--}}
{{--            window.print();--}}
{{--            document.body.innerHTML = originalContents;--}}
{{--        };--}}

{{--    </script>--}}
@endsection
