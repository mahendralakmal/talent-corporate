@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Reports</li>
    <li class="breadcrumb-item active">Pay sheet</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                Attendance Report
            </h3>

            <br>

            <form action="{{ route('attendance.generateAttendanceReport') }}" id="generateAttendanceReport"
                  method="post">
                @csrf
                <div class="row">
                    <div class="col-2">
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" name="from" id="from" class="form-control"
                                   value="@if(!is_null($frm)){{$frm}}@endif">
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="form-group">
                            <label>To</label>
                            <input type="date" name="to" id="to" class="form-control"
                                   value="@if(!is_null($too)){{$too}}@endif">
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="form-group">
                            <label for="exampleInputFile">Department</label>
                            <select name="department" id="department" class="form-control">
                                <option value="">Select Department</option>
                                @if(isset($company))
                                    @foreach($company->departments as $key=>$department)
                                        @if($department->id == $dep)
                                            <option value="{{ $department->id }}"
                                                    selected>{{ $department->department_code }}
                                                - {{ $department->department_name }}</option>
                                        @else
                                            <option value="{{ $department->id }}">{{ $department->department_code }}
                                                - {{ $department->department_name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="form-group">
                            <label for="exampleInputFile">Employee</label>
                            <select name="employee" id="employee" class="form-control">
                                <option value="">Select Employee</option>
                                @if(isset($company))
                                    @foreach($company->employeesWithoutResign as $key=>$employee)
                                        @if($department->id == $dep)
                                            <option
                                                value="{{ $employee->id }}" selected>{{ $employee->user_id }}
                                                - {{ $employee->name_with_initials }}</option>
                                        @else
                                            <option
                                                value="{{ $employee->id }}">{{ $employee->user_id }}
                                                - {{ $employee->name_with_initials }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="form-group">
                            <label for="exampleInputFile">Attendance Type</label>
                            <select name="type" id="type" class="form-control">
                                <option value="">Select Report</option>
                                <option value="a" @if($typ == 'a') selected @endif>Attendance</option>
                                <option value="l" @if($typ == 'l') selected @endif>Late Attendance</option>
                                <option value="e" @if($typ == 'e') selected @endif>Absents</option>
                            </select>
                        </div>
                    </div>

                    <div class="col">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
                                    data-placement="top"
                                    title="Search"><i class="fa fa-search" aria-hidden="true"></i>
                            </button>


                        </div>
                    </div>
                </div>
            </form>
            <div>
                <button class="btn btn-primary" id="exportPaysheet" onclick="fnExcelReport();">Export Excel</button>
                <button class="btn btn-primary" id="exportPdf" onclick="fnPdfReport();">Export PDF</button>
            </div>


        </div>

        <div class="card-body" id="content">
            <div class="table-responsive">
                <table class="table table-bordered" id="attendance">
                    <thead>
                    <tr>
                        <td colspan="12" class="no-borders">
                            <p>
                            <h3>@if(!is_null($company)){{ $company->name }}@endif</h3></p><br>
                            <h4><p id="report">@if($typ == 'a')Attendance Report @elseif($typ == 'l')Late Attendance
                                    Report @elseif($typ == 'e')Absence Report @endif</p></h4>
                            <br>
                            <h6><p id="period">For the period of {{ $frm }} - {{ $too }}</p></h6>
                        </td>
                    </tr>
                    <tr>
                        <td>S.N</td>
                        <td>EMP No</td>
                        <td>EPF No.</td>
                        <td>Name</td>
                        <td>Department</td>
                        <td>Category</td>
                        <td>Designation</td>
                        <th>Date</th>
                        <th>In</th>
                        <th>Out</th>
                        @if($typ == 'l')
                            <th>Late</th>
                        @endif
                    </tr>
                    </thead>

                    <tbody>
                    @php $j = 1; @endphp
                    @if(!is_null($attendance))
                        @foreach($attendance as $key=>$attendanceEmp)
                            @if(is_null($attendanceEmp['employee']->date_of_resign))
                                @foreach($period as $keys=>$date)
                                    @if($date->format('l') != 'Sunday')
                                        <tr>
                                            @if($typ == 'l')
                                                @if($attendanceEmp['attendance'][$keys]['uin_string'] != "AB")
                                                    @if(Carbon\Carbon::parse($attendanceEmp['attendance'][$keys]['uin_string'])->isoFormat('HH:mm') > Carbon\Carbon::parse($company->startTime)->isoFormat('HH:mm'))
                                                        <td>{{$j++}}</td>
                                                        <td>{{$attendanceEmp['employee']->user_id}}</td>
                                                        <td>{{$attendanceEmp['employee']->epf_no}}</td>
                                                        <td>{{$attendanceEmp['employee']->name_with_initials}}</td>
                                                        <td>{{$attendanceEmp['employee']->departments['department_name']}}</td>
                                                        <td>{{$attendanceEmp['employee']->employee_cat}}</td>
                                                        <td>{{$attendanceEmp['employee']->designation}}</td>
                                                        <td>{{$date->format('Y-m-d')}}</td>
                                                        <td>{{$attendanceEmp['attendance'][$keys]['uin_string']}}</td>
                                                        <td>@if(!is_null($attendanceEmp['attendance'][$keys]['uout_string']))
                                                                {{$attendanceEmp['attendance'][$keys]['uout_string']}}
                                                            @endif</td>
                                                        <td>{{ gmdate('H:i:s', Carbon\Carbon::parse($attendanceEmp['attendance'][$keys]['uin_string'])->diffInSeconds(Carbon\Carbon::parse($company->startTime))) }}</td>
                                                    @endif
                                                @endif
                                            @elseif($typ == 'e')
                                                @if($attendanceEmp['attendance'][$keys]['uin_string'] == "AB")
                                                    <td>{{$j++}}</td>
                                                    <td>{{$attendanceEmp['employee']->user_id}}</td>
                                                    <td>{{$attendanceEmp['employee']->epf_no}}</td>
                                                    <td>{{$attendanceEmp['employee']->name_with_initials}}</td>
                                                    <td>{{$attendanceEmp['employee']->departments['department_name']}}</td>
                                                    <td>{{$attendanceEmp['employee']->employee_cat}}</td>
                                                    <td>{{$attendanceEmp['employee']->designation}}</td>
                                                    <td>{{$date->format('Y-m-d')}}</td>
                                                    <td>{{$attendanceEmp['attendance'][$keys]['uin_string']}}</td>
                                                    <td>@if(!is_null($attendanceEmp['attendance'][$keys]['uout_string']))
                                                            {{$attendanceEmp['attendance'][$keys]['uout_string']}}
                                                        @endif</td>
                                                @endif
                                            @else
                                                <td>{{$j++}}</td>
                                                <td>{{$attendanceEmp['employee']->user_id}}</td>
                                                <td>{{$attendanceEmp['employee']->epf_no}}</td>
                                                <td>{{$attendanceEmp['employee']->name_with_initials}}</td>
                                                <td>{{$attendanceEmp['employee']->departments['department_name']}}</td>
                                                <td>{{$attendanceEmp['employee']->employee_cat}}</td>
                                                <td>{{$attendanceEmp['employee']->designation}}</td>
                                                <td>{{$date->format('Y-m-d')}}</td>
                                                <td>{{$attendanceEmp['attendance'][$keys]['uin_string']}}</td>
                                                <td>{{$attendanceEmp['attendance'][$keys]['uout_string']}}</td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection



@section('custom-jquery')



    <script>

        $('#type').change(function () {
            $('#report').text($(this).find("option:selected").text() + " Report");
        })

        $('#from').change(function () {
            $('#period').text("For the period of " + $(this).val() + " - " + $('#to').val());
        })

        $('#to').change(function () {
            $('#period').text("For the period of " + $('#from').val() + " - " + $(this).val());
        })

        var company = "@if(!is_null($company)){{ $company->name }}@endif";

        function fnPdfReport() {
            var pdf = new jsPDF('l', 'pt', 'a4');
            pdf.autoTable({html: '#attendance'});
            pdf.save(company+ '-' + $('#report').text() + '-' + $('#period').text() + '.pdf');
        }

        function fnExcelReport() {
            var report = $('#type option:selected').text();
            var from = $('#from').val();
            var to = $('#to').val();

            var tab_text = "<table border='1'>";
            // var tab_text = "<table border='1'><tr bgcolor='#87AFC6'>";
            tab_text = tab_text + "<tr><td colspan='11' bgcolor='#87AFC6'><b style='font-size: 25px;'>@if(!is_null($company)){{ $company->name }}@endif</b><br><span style='font-size: 15px;'>" + report + " Report</span><br><span>For the period of " + from + ' - ' + to + " Report</span></td></tr>";

            var textRange;
            var j = 0;
            tab = document.getElementById('attendance'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                if (j != 0) {
                    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                }
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();

                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            } else {               //other browser not tested on IE 11
                //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text),download: "YourFileName.xls"));

                var sa = $("<a />", {
                    href: 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text),
                    download: company + '-' + $('#report').text() + '-' + $('#period').text()+ '.xls'
                })
                    .appendTo("body")
                    .get(0)
                    .click()
            }

            return (sa);
        }

    </script>

@endsection
