
@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>

    <style type="text/css">
        .left ul{
            list-style: none;
        }
        .underline {
            text-decoration: underline;
        }

        table, td, th {
            border: 1px solid black;
            padding: 9px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            margin-top: 25px;
        }
        table thead {
            font-weight: bold;
        }
    </style>
@endsection

@section('page_header')
    <h1 class="m-0 text-dark">WHT Certificate</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Home</a></li>
    <li class="breadcrumb-item active">WHT Certificate</li>
@endsection

@section('content')
    <div class="card">

        <div class="card-header">
            <small class="pull-right">
                <button class="btn btn-success" onClick="print()" data-toggle="tooltip" data-placement="top" title="Print"><i class="fa fa-print" aria-hidden="true"></i></button>
            </small>
        </div>

        <div class="card-body" id="report">
            <div class="left">
                <ul>
                    <li><?php echo(date('d/m/Y')); ?></li>
                    <br>
                    <li><b>{{$company}}</b></li>
                    <li><b class="underline">Issuing of WHT Certificate for the YA <?php echo($year-1); ?>/{{$year}}.</b></li>
                    <br><br>
                    <li>Please kindly acknowledge the receipt of your WHT Certificate for the Y/A <?php echo($year-1); ?>/{{$year}}</li>


                    <table>
                        <thead>
                        <tr>
                            <td>S.N</td>
                            <td>Emp. No.</td>
                            <td>Name</td>
                            <td>NIC/PP</td>
                            <td>Recipient Signature</td>
                            <td>Date of Receipt</td>
                        </tr>
                        </thead>
                        <?php $sn = 1; ?>
                        <tbody>
                        @foreach($employees as $emp)
                            <?php
                                $paye = \App\Salary::where('company_id','=',$emp->company_id)->where('employee_id','=',$emp->user_id)->value('basic_PAYE');
                                if($paye==1){?>
                            <tr>
                                <td><?php echo($sn++); ?></td>
                                <td>{{$emp->user_id}}</td>
                                <td>{{$emp->fname}} {{$emp->mname}} {{$emp->lname}}</td>
                                <td>{{$emp->nic}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php } ?>
                        @endforeach
                        </tbody>
                    </table>

                    <br>
                    <li>Thanking you,</li>
                    <br><br>
                    <li>Head - Compliance Assurance Services</li>
                    <li>Corporate Business Solutions Pvt. Ltd</li>
                </ul>
            </div>
        </div>

    </div>
@endsection

@section('custom-jquery')
    <script>
        function print(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }



        // function print(){
        //     let doc = new jsPDF('p','pt','a4');
        //
        //     doc.addHTML($("#report"),function() {
        //         doc.save('html.pdf');
        //     });
        //
        // }

    </script>
@endsection
