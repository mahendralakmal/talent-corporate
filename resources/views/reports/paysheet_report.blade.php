@extends('layouts.app')

@section('title')
    <title>Talent | Reports</title>

    <style>
        #colored {
            font-weight: bold;
        }
    </style>

@endsection

@section('page_header')
    <h1 class="m-0 text-dark">Reports</h1>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('paysheet.index')}}">Pay Sheet</a></li>
    <li class="breadcrumb-item active">Pay Sheet Report</li>
@endsection

@section('content')

    <button class="btn btn-primary" id="exportPaysheet" onclick="fnExcelReport();">Export</button>

    <input hidden value="{{$filename}}" id="filename">

    <div><b style="font-size: 25px;">{{$comapany->name}}</b></div>
    <div style=" font-weight: bold;"><h5>Paysheet for the month of {{ $mon }} {{$year}}</h5></div>

    <div class="table-Paysheet">
        <table class="table" id="paysheet">
            <thead>
            {{--            <tr >--}}
            {{--                <td colspan="17"><b style="font-size: 25px;">{{$comapany->name}}</b></td>--}}
            {{--            </tr>--}}
            {{--            <tr>--}}
            {{--                <td colspan="17" style=" font-weight: bold;"><h5>Paysheet for the month of {{ $mon }} {{$year}}</h5></td>--}}
            {{--            </tr>--}}

            {{--            <tr >--}}
            {{--                <td></td>--}}
            {{--            </tr>--}}

            <tr>
                <td>S.N</td>
                <td>EMP No</td>
                <td>EPF No</td>
                <td>Name</td>
                <td>Designation</td>

                <td>Department</td>
                <td>DOJ</td>
                <td>DOR</td>
                <td>No.of days work</td>
                <td>BasicSalary</td>
				<?php if($allowance->allowance01 != null){ ?>
                <td><?php echo($allowance->allowance01) ?></td>
				<?php } ?>
				<?php if($allowance->allowance02 != null){ ?>
                <td><?php echo($allowance->allowance02) ?></td>
				<?php } ?>
				<?php if($allowance->allowance03 != null){ ?>
                <td><?php echo($allowance->allowance03) ?></td>
				<?php } ?>
				<?php if($allowance->allowance04 != null){ ?>
                <td><?php echo($allowance->allowance04) ?></td>
				<?php } ?>
				<?php if($allowance->allowance05 != null){ ?>
                <td><?php echo($allowance->allowance05) ?></td>
				<?php } ?>
				<?php if($allowance->allowance06 != null){ ?>
                <td><?php echo($allowance->allowance06) ?></td>
				<?php } ?>
				<?php if($allowance->allowance07 != null){ ?>
                <td><?php echo($allowance->allowance07) ?></td>
				<?php } ?>
				<?php if($allowance->allowance08 != null){ ?>
                <td><?php echo($allowance->allowance08) ?></td>
				<?php } ?>
				<?php if($allowance->allowance09 != null){ ?>
                <td><?php echo($allowance->allowance09) ?></td>
				<?php } ?>
				<?php if($allowance->allowance10 != null){ ?>
                <td><?php echo($allowance->allowance10) ?></td>
				<?php } ?>
                <td>Performance/Bonus</td>
                <td>Commission</td>
                <td>OverTime</td>
                <td>LeavePay</td>
                <td>Arrears</td>
                <td>Adjustment</td>
                <td>TotalGross remuneration</td>
                <td>No Pay</td>
                <td>EPF8%</td>
                <td>PAYE</td>
                <td>Advance</td>
                <td>Loan</td>
                <td>SD</td>
                <td>OtderDeduction1</td>
                <td>OtderDeduction2</td>
                <td>TotalDeduction</td>
                <td>NetPay</td>
                <td>EPF 12%</td>
                <td>ETF 3%</td>
                <td>Gratuity</td>
                <td>CTC</td>
            </tr>
            </thead>

            <tbody>

			<?php

			$BasicSalary = 0;
			$Allowance1 = 0;
			$Allowance2 = 0;
			$Allowance3 = 0;
			$Allowance4 = 0;
			$Allowance5 = 0;
			$Allowance6 = 0;
			$Allowance7 = 0;
			$Allowance8 = 0;
			$Allowance9 = 0;
			$Allowance10 = 0;
			$Bonus = 0;
			$Commission = 0;
			$OverTime = 0;
			$LeavePay = 0;
			$Arrears = 0;
			$Adjustment = 0;
			$TotalGross = 0;
			$noPay = 0;
			$EPF8 = 0;
			$PAYE = 0;
			$Advance = 0;
			$Loan = 0;
			$SD = 0;
			$Deduction1 = 0;
			$Deduction2 = 0;
			$TotalDeduction = 0;
			$NetPay = 0;
			$EPF12 = 0;
			$ETF3 = 0;
			$Gratuity = 0;
			$CTC = 0;
			?>

            @foreach($paysheets as $paysheet)
                <tr>
                    <td>{{$paysheet->SN}}</td>
                    <td>{{$paysheet->emp_id}}</td>
                    <td>{{$paysheet->epf_no}}</td>
                    <td>{{$paysheet->name}}</td>
                    <td>{{$paysheet->designation}}</td>
                    <td>{{$paysheet->department}}</td>
                    <td>{{$paysheet->doj}}</td>
                    <td>{{$paysheet->dor}}</td>
                    <td>{{$paysheet->noofdays_work}}</td>
                    <td><?php echo(number_format($paysheet->basic_salary)) ?></td><?php $BasicSalary += $paysheet->basic_salary; ?>
					<?php if($allowance->allowance01 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance01)) ?></td><?php $Allowance1 += $paysheet->allowance01; ?>
					<?php } ?>
					<?php if($allowance->allowance02 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance02)) ?></td><?php $Allowance2 += $paysheet->allowance02; ?>
					<?php } ?>
					<?php if($allowance->allowance03 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance03)) ?></td><?php $Allowance3 += $paysheet->allowance03; ?>
					<?php } ?>
					<?php if($allowance->allowance04 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance04)) ?></td><?php $Allowance4 += $paysheet->allowance04; ?>
					<?php } ?>
					<?php if($allowance->allowance05 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance05)) ?></td><?php $Allowance5 += $paysheet->allowance05; ?>
					<?php } ?>
					<?php if($allowance->allowance06 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance06)) ?></td><?php $Allowance6 += $paysheet->allowance06; ?>
					<?php } ?>
					<?php if($allowance->allowance07 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance07)) ?></td><?php $Allowance7 += $paysheet->allowance07; ?>
					<?php } ?>
					<?php if($allowance->allowance08 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance08)) ?></td><?php $Allowance8 += $paysheet->allowance08; ?>
					<?php } ?>
					<?php if($allowance->allowance09 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance09)) ?></td><?php $Allowance9 += $paysheet->allowance09; ?>
					<?php } ?>
					<?php if($allowance->allowance10 != null){ ?>
                    <td><?php echo(number_format($paysheet->allowance10)) ?></td><?php $Allowance10 += $paysheet->allowance10; ?>
					<?php } ?>

                    <td><?php echo(number_format($paysheet->performanceBonus)) ?></td><?php $Bonus += $paysheet->performanceBonus; ?>
                    <td><?php echo(number_format($paysheet->commission)) ?></td><?php $Commission += $paysheet->commission; ?>
                    <td><?php echo(number_format($paysheet->OT)) ?></td><?php $OverTime += $paysheet->OT; ?>
                    <td><?php echo(number_format($paysheet->leave_pay)) ?></td><?php $LeavePay += $paysheet->leave_pay; ?>
                    <td><?php echo(number_format($paysheet->arrears)) ?></td><?php $Arrears += $paysheet->arrears; ?>
                    <td><?php echo(number_format($paysheet->adjustments)) ?></td><?php $Adjustment += $paysheet->adjustments; ?>
                    <td><?php echo(number_format($paysheet->gross_remuneration)) ?></td><?php $TotalGross += $paysheet->gross_remuneration; ?>
                    <td><?php echo(number_format($paysheet->noPay)) ?></td><?php $noPay += $paysheet->noPay; ?>
                    <td><?php echo(number_format($paysheet->epf08)) ?></td><?php $EPF8 += $paysheet->epf08; ?>
                    <td><?php echo(number_format($paysheet->paye)) ?></td><?php $PAYE += $paysheet->paye; ?>
                    <td><?php echo(number_format($paysheet->advance)) ?></td><?php $Advance += $paysheet->advance; ?>
                    <td><?php echo(number_format($paysheet->loan)) ?></td><?php $Loan += $paysheet->loan; ?>
                    <td><?php echo(number_format($paysheet->SD)) ?></td><?php $SD += $paysheet->SD; ?>
                    <td><?php echo(number_format($paysheet->otherDeduction01)) ?></td><?php $Deduction1 += $paysheet->otherDeduction01; ?>
                    <td><?php echo(number_format($paysheet->otherDeduction02)) ?></td><?php $Deduction2 += $paysheet->otherDeduction02; ?>
                    <td><?php echo(number_format($paysheet->totalDeduction)) ?></td><?php $TotalDeduction += $paysheet->totalDeduction; ?>
                    <td><?php echo(number_format($paysheet->netPay)) ?></td><?php $NetPay += $paysheet->netPay; ?>
                    <td><?php echo(number_format($paysheet->epf12)) ?></td><?php $EPF12 += $paysheet->epf12; ?>
                    <td><?php echo(number_format($paysheet->etf)) ?></td><?php $ETF3 += $paysheet->etf; ?>
                    <td><?php echo(number_format($paysheet->gratuity)) ?></td><?php $Gratuity += $paysheet->gratuity; ?>
                    <td><?php echo(number_format($paysheet->CTC)) ?></td><?php $CTC += $paysheet->CTC; ?>
                </tr>
            @endforeach
            <tr>
                <td id="colored" colspan="9">Total</td>

                <td id="colored"><?php echo(number_format($BasicSalary)) ?></td>
				<?php if($allowance->allowance01 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance1)) ?></td> <?php } ?>
				<?php if($allowance->allowance02 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance2)) ?></td> <?php } ?>
				<?php if($allowance->allowance03 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance3)) ?></td> <?php } ?>
				<?php if($allowance->allowance04 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance4)) ?></td> <?php } ?>
				<?php if($allowance->allowance05 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance5)) ?></td> <?php } ?>
				<?php if($allowance->allowance06 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance6)) ?></td> <?php } ?>
				<?php if($allowance->allowance07 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance7)) ?></td> <?php } ?>
				<?php if($allowance->allowance08 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance8)) ?></td> <?php } ?>
				<?php if($allowance->allowance09 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance9)) ?></td> <?php } ?>
				<?php if($allowance->allowance10 != null){ ?>
                <td id="colored"><?php echo(number_format($Allowance10)) ?></td> <?php } ?>

                <td id="colored"><?php echo(number_format($Bonus)) ?></td>
                <td id="colored"><?php echo(number_format($Commission)) ?></td>
                <td id="colored"><?php echo(number_format($OverTime)) ?></td>
                <td id="colored"><?php echo(number_format($LeavePay)) ?></td>
                <td id="colored"><?php echo(number_format($Arrears)) ?></td>
                <td id="colored"><?php echo(number_format($Adjustment)) ?></td>
                <td id="colored"><?php echo(number_format($TotalGross)) ?></td>
                <td id="colored"><?php echo(number_format($noPay)) ?></td>
                <td id="colored"><?php echo(number_format($EPF8)) ?></td>
                <td id="colored"><?php echo(number_format($PAYE)) ?></td>
                <td id="colored"><?php echo(number_format($Advance)) ?></td>
                <td id="colored"><?php echo(number_format($Loan)) ?></td>
                <td id="colored"><?php echo(number_format($SD)) ?></td>
                <td id="colored"><?php echo(number_format($Deduction1)) ?></td>
                <td id="colored"><?php echo(number_format($Deduction2)) ?></td>
                <td id="colored"><?php echo(number_format($TotalDeduction)) ?></td>
                <td id="colored"><?php echo(number_format($NetPay)) ?></td>
                <td id="colored"><?php echo(number_format($EPF12)) ?></td>
                <td id="colored"><?php echo(number_format($ETF3)) ?></td>
                <td id="colored"><?php echo(number_format($Gratuity)) ?></td>
                <td id="colored"><?php echo(number_format($CTC)) ?></td>

            </tr>
            </tbody>
        </table>
    </div>
    <hr>
    <div class="row">
        <div class="col"><p style="background-color: #F4B084; padding-left: 10px; padding-right: 10px;">Resingnation</p>
        </div>
        <div class="col"><p style="color: #8A30A0;">Increments</p></div>
        <div class="col"><p style="color: #66B070; padding-left: 10px;">No pay</p></div>
        <div class="col"><p style="color: #008AE0;">Promotion</p></div>
        <div class="col"><p style="background-color: #BFBFBF; padding-left: 10px;">New Recruits</p></div>
        <div class="col"><p style="color: #F6B143;">Promotion & Increments</p></div>
        <div class="col"><p style="color:#FFC066; padding-left: 10px;">Corrections</p></div>
        <div class="col"><p style="color: #D50000;">Confirmation & Increments</p></div>
    </div>

@endsection

@section('custom-jquery')



    <script>

        // $(document).ready(function () {
        //     $('#print').click(function () {
        //         console.log($("#filename").val());
        //     });
        // });

        function fnExcelReport() {
            var tab_text = "<table border='1'><tr bgcolor='#87AFC6'>";
            {{--tab_text = tab_text+"<tr><td colspan='17'><b style='font-size: 25px;'>{{$comapany->name}}</b></td><td colspan='17' style='font-weight: bold;'><h5>Paysheet for the month of {{ $mon }} {{$year}}</h5></td></tr>";--}}

            var textRange;
            var j = 0;
            tab = document.getElementById('paysheet'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();

                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            } else {               //other browser not tested on IE 11
                                   //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text),download: "YourFileName.xls"));

                var sa = $("<a />", {
                    href: 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text),
                    download: $("#filename").val() + '.xls'
                })
                    .appendTo("body")
                    .get(0)
                    .click()
            }

            return (sa);
        }

    </script>

@endsection
