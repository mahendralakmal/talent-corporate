
@extends('layouts.app')

@section('title')
    <title>Talent | Payroll</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Payroll</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Payroll</li>
    <li class="breadcrumb-item active">Payroll Edit</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
        </div>
        <form action="{{ route('paysheet.update', $paysheet->id ) }}" method="POST">

            {{ csrf_field() }}

            {{ method_field('put') }}

            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>SN</label>
                            <input type="text"  class="form-control" name="sn" placeholder="SN" value="{{$paysheet->SN}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Company ID</label>
                            <input type="text" class="form-control" name="company_id" placeholder="Company ID" value="{{$paysheet->company_id}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Employee ID</label>
                            <input type="text" class="form-control" name="emp_id" placeholder="Employee ID" value="{{$paysheet->emp_id}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Month</label>
                            <input type="text" class="form-control" name="month" placeholder="Month" value="{{$paysheet->month}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Year</label>
                            <input type="text" class="form-control" name="year" placeholder="Year" value="{{$paysheet->year}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>EPF No</label>
                            <input type="text" class="form-control" name="epf_no" placeholder="EPF No" value="{{$paysheet->epf_no}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{$paysheet->name}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Designation</label>
                            <input type="text" class="form-control" name="designation" placeholder="Designation" value="{{$paysheet->designation}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>NIC</label>
                            <input type="text" class="form-control" name="nic" placeholder="NIC" value="{{$paysheet->nic}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Date Of Join</label>
                            <input type="text" class="form-control" name="doj" placeholder="Date Of Join" value="{{$paysheet->doj}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>DOR</label>
                            <input type="text" class="form-control" name="dor" placeholder="DOR" value="{{$paysheet->dor}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Department</label>
                            <input type="text" class="form-control" name="department" placeholder="Department" value="{{$paysheet->department}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>No of Days Work</label>
                            <input type="text" class="form-control" name="noofdays_work" placeholder="No of Days Work" value="{{$paysheet->noofdays_work}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Basic Salary</label>
                            <input type="text" class="form-control" name="basic_salary" placeholder="Basic Salary" value="{{$paysheet->basic_salary}}">
                        </div>
                    </div>
                @if($allowance->allowance01!=null)
                    <div class="col-4">
                        <div class="form-group">
                            <label>Allowance 01</label>
                            <input type="text" class="form-control" name="allowance01" placeholder="Allowance 01" value="{{$paysheet->allowance01}}">
                        </div>
                    </div>
                @endif
                </div>

                <div class="row">
                    @if($allowance->allowance02!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 02</label>
                                <input type="text" class="form-control" name="allowance02" placeholder="Allowance 02" value="{{$paysheet->allowance02}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance03!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 03</label>
                                <input type="text" class="form-control" name="allowance03" placeholder="Allowance 03" value="{{$paysheet->allowance03}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance04!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 04</label>
                                <input type="text" class="form-control" name="allowance04" placeholder="Allowance 04" value="{{$paysheet->allowance04}}">
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    @if($allowance->allowance05!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 05</label>
                                <input type="text" class="form-control" name="allowance05" placeholder="Allowance 05" value="{{$paysheet->allowance05}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance06!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 06</label>
                                <input type="text" class="form-control" name="allowance06" placeholder="Allowance 06" value="{{$paysheet->allowance06}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance07!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 07</label>
                                <input type="text" class="form-control" name="allowance07" placeholder="Allowance 07" value="{{$paysheet->allowance07}}">
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    @if($allowance->allowance08!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 08</label>
                                <input type="text" class="form-control" name="allowance08" placeholder="Allowance 08" value="{{$paysheet->allowance08}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance09!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 09</label>
                                <input type="text" class="form-control" name="allowance09" placeholder="Allowance 09" value="{{$paysheet->allowance09}}">
                            </div>
                        </div>
                    @endif

                    @if($allowance->allowance10!=null)
                        <div class="col-4">
                            <div class="form-group">
                                <label>Allowance 10</label>
                                <input type="text" class="form-control" name="allowance10" placeholder="Allowance 10" value="{{$paysheet->allowance10}}">
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Performance Bonus</label>
                            <input type="text" class="form-control" name="performanceBonus" placeholder="Performance Bonus" value="{{$paysheet->performanceBonus}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Commission</label>
                            <input type="text" class="form-control" name="commission" placeholder="Commission" value="{{$paysheet->commission}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>OT</label>
                            <input type="text" class="form-control" name="OT" placeholder="OT" value="{{$paysheet->OT}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Leave Pay</label>
                            <input type="text" class="form-control" name="leave_pay" placeholder="Leave Pay" value="{{$paysheet->leave_pay}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Arrears</label>
                            <input type="text" class="form-control" name="arrears" placeholder="Arrears" value="{{$paysheet->arrears}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Adjustments</label>
                            <input type="text" class="form-control" name="adjustments" placeholder="Adjustments" value="{{$paysheet->adjustments}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Gross Remuneration</label>
                            <input type="text" class="form-control" name="gross_remuneration" placeholder="Gross Remuneration" value="{{$paysheet->gross_remuneration}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>EPF 08</label>
                            <input type="text" class="form-control" name="epf08" placeholder="EPF 08" value="{{$paysheet->epf08}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>No Pay</label>
                            <input type="text" class="form-control" name="noPay" placeholder="No Pay" value="{{$paysheet->noPay}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>PAYE</label>
                            <input type="text" class="form-control" name="paye" placeholder="PAYE" value="{{$paysheet->paye}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Advance</label>
                            <input type="text" class="form-control" name="advance" placeholder="Advance" value="{{$paysheet->advance}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Loan</label>
                            <input type="text" class="form-control" name="loan" placeholder="Loan" value="{{$paysheet->loan}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Stamp Duty</label>
                            <input type="text" class="form-control" name="SD" placeholder="SD" value="{{$paysheet->SD}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Other Deduction 01</label>
                            <input type="text" class="form-control" name="otherDeduction01" placeholder="Other Deduction 01" value="{{$paysheet->otherDeduction01}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Other Deduction 02</label>
                            <input type="text" class="form-control" name="otherDeduction02" placeholder="Other Deduction 02" value="{{$paysheet->otherDeduction02}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Total Deduction</label>
                            <input type="text" class="form-control" name="totalDeduction" placeholder="Total Deduction" value="{{$paysheet->totalDeduction}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Net Pay</label>
                            <input type="text" class="form-control" name="netPay" placeholder="Net Pay" value="{{$paysheet->netPay}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>EPF 12</label>
                            <input type="text" class="form-control" name="epf12" placeholder="EPF 12" value="{{$paysheet->epf12}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>ETF</label>
                            <input type="text" class="form-control" name="etf" placeholder="ETF" value="{{$paysheet->etf}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>Gratuity</label>
                            <input type="text" class="form-control" name="gratuity" placeholder="Gratuity" value="{{$paysheet->gratuity}}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label>CTC</label>
                            <input type="text" class="form-control" name="CTC" placeholder="CTC" value="{{$paysheet->ctc}}">
                        </div>
                    </div>
                </div>


            </div>

            <div class="card-footer">
                <small class="pull-right">
                    <a href="{{url()->previous()}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    <button type="submit" class="btn btn-primary btn-sm">Update Paysheet record</button>
                </small>
            </div>
        </form>
    </div>

@endsection
