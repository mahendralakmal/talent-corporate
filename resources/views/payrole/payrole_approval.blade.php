@extends('layouts.app')

@section('title')
    <title>Talent | Payroll</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Payroll Approvals</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Payroll</li>
    <li class="breadcrumb-item active">Approvals</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                All Paysheets
            </h3>
        </div>

        <div class="card-body">
            <form id="formPaysheetApproveAll" action="" method="POST" enctype="application/x-www-form-urlencoded">
                <div class="table-responsive">
                    @csrf
                    <div id="cancel-all" style="display: none"><input type="submit"
                                                                      class="float-right btn btn-danger cancel-all"
                                                                      value="Cancel All"/></div>
                    <div id="approve-all" style="display: none"><input type="submit"
                                                                       class="float-right btn btn-primary approve-all"
                                                                       value="Approve All"/></div>
                    <table class="table table-bordered table-striped" id="example1">
                        <thead>
                        <tr>
                            <td><input type="checkbox" id="chbCheckAll"></td>
                            <td>S.N</td>
                            <td>EMP No</td>
                            <td>Name</td>
                            <td>Designation</td>
                            <td>Year</td>
                            <td>month</td>
                            <td>No.of days work</td>
                            <td>Total Gross remuneration</td>
                            <td>Total Deduction</td>
                            <td>Net pay</td>
                            <td>Action</td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($paysheets as $key=>$paysheet)
                            <tr>
                                <td name="check_td">
                                    <input type="checkbox" name="check_{{$key}}" id="check">
                                    <input type="hidden" name="paysheet_id[]" id="paysheet_id"
                                           value="{{$paysheet->id}}">
                                </td>
                                <td>{{$key+1}}</td>
                                <td>{{$paysheet->emp_id}}</td>
                                <td>{{$paysheet->name}}</td>
                                <td>{{$paysheet->designation}}</td>
                                <td>{{$paysheet->year}}</td>
                                <td>{{date("F", mktime(0, 0, 0, $paysheet->month, 10))}}</td>
                                <td>{{$paysheet->noofdays_work}}</td>
                                <td>{{ number_format($paysheet->gross_remuneration,2)}}</td>
                                <td>{{ number_format($paysheet->totalDeduction,2)}}</td>
                                <td>{{ number_format($paysheet->netPay,2)}}</td>
                                <td>
                                    <a href="{{route('paysheet.edit', $paysheet->id)}}" class="btn btn-warning btn-sm"
                                       data-toggle="tooltip" title="Edit" style="display: inline-block;"><i class="fa fa-pencil-square-o"
                                                                             aria-hidden="true"></i></a>
                                    @if($key == 0)<form action="" method="post"></form>@endif
                                    <form action="{{route('paysheet.paysheetApprove', $paysheet->id)}}" method="post"
                                          style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip"
                                                title="Approve"><i class="fa fa-check" aria-hidden="true"></i></button>
                                    </form>

                                    <form action="{{route('paysheet.paysheetCancel', $paysheet->id)}}" method="post"
                                          style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip"
                                                title="Cancel"><i class="fa fa-close" aria-hidden="true"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('custom-jquery')
    <script>
        $('#chbCheckAll').click(function (e) {
            var table = $(e.target).closest('table');
            $('td input:checkbox', table).prop('checked', this.checked);

            if (this.checked) {
                $('#approve-all').show();
                $('#cancel-all').show();

            } else {
                $('#approve-all').hide();
                $('#cancel-all').hide();
            }
            {{--$(".table-responsive").append('@csrf');--}}
            // $(".table-responsive").wrap('<form id="formPaysheetApproveAll" action="/paysheet/paysheetApproveAll" method="POST" enctype="application/x-www-form-urlencoded">');
        });
        $('.approve-all').click(function (e) {
            e.preventDefault();
            $('#formPaysheetApproveAll').attr('action', '/paysheetApproveAll').submit();
            // var data = $("#formPaysheetApproveAll").serialize();
            // $.ajax({
            //     url: "/paysheet/paysheetApproveAll",
            //     data: data,
            //     method: "POST",
            //     datatype: "json",
            //     success: function (response) {
            //         // console.log('success');
            //         // console.log(response);
            //         // alert(response);
            //         return response;
            //     },
            //     error: function (response) {
            //         // console.log('error');
            //         // console.log(response);
            //         return response;
            //     }
            // });
        });

        $('.cancel-all').click(function (e) {
            e.preventDefault();
            $('#formPaysheetApproveAll').attr('action', '/paysheetCancelAll').submit();
        });
    </script>
@endsection
