
@extends('layouts.app')

@section('title')
    <title>talent | Payroll</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Payroll</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Payroll</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Without Attendance Salary Calculation</h5>
        </div>

        <div class="card-body">
            <form action="{{ route('paysheet.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Year</label>
                            <select class="form-control{{$errors->has('year1') ? ' is-invalid' : ''}}"  name="year1">
                                <option selected value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>
                            @if ($errors->has('year1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year1')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Month</label>
                            <select class="form-control{{$errors->has('month1') ? ' is-invalid' : ''}}"  name="month1">
                                <option selected value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                            @if ($errors->has('month1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('month1')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">.</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Calculate"><i class="fas fa-calculator"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">With Attendance Salary Calculation</h5>
        </div>

        <div class="card-body">
            <form action="{{ route('paysheet.payrole_with_file') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Year</label>
                            <select class="form-control{{$errors->has('year') ? ' is-invalid' : ''}}" name="year">
                                <option selected value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>
                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Month</label>
                            <select class="form-control{{$errors->has('month') ? ' is-invalid' : ''}}" name="month">
                                <option selected value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="Auguest">Auguest</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                            @if ($errors->has('month'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('month')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Payrole Excel</label>
                            <input type="file" class="form-control{{$errors->has('file') ? ' is-invalid' : ''}}" name="file">

                            @if ($errors->has('file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('file')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">.</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Upload"><i class="fa fa-upload" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{'/'}}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>
@endsection
