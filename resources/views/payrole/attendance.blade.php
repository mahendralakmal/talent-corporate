@extends('layouts.app')

@section('title')
    <title>Talent | Payrole</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Attendance</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Attendance</li>
@endsection

@section('content')
    @can('manual attendance upload')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Attendance Manual Upload</h3>
        </div>

        <div class="card-body">
            <form action="{{ route('attendances.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label>Year</label>
                            <select class="form-control{{$errors->has('year') ? ' is-invalid' : ''}}" name="year">
                                <option selected value="">Select Year</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                            </select>
                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>Month</label>
                            <select class="form-control{{$errors->has('year') ? ' is-invalid' : ''}}" name="month">
                                <option selected value="">Select Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="Auguest">Auguest</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                            @if ($errors->has('month'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('month')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label>Attendance Sheet</label>
                            <input type="file" class="form-control{{$errors->has('file') ? ' is-invalid' : ''}}"
                                   name="file">
                            @if ($errors->has('file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('file')}}</strong>
                                 </span>
                            @endif
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">.</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
                                    data-placement="top" title="Upload"><i class="fa fa-upload" aria-hidden="true"></i>
                            </button>
                            &nbsp;
                            <a href="{{ route('getSampleAttendance') }}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Download CSV template"><i class="fa fa-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
    @endcan


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Attendance Details</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('GetAttendance') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">

                    <div class="col-3">
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" name="from" id="from" class="form-control" value="{{old('from')}}">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label>To</label>
                            <input type="date" name="to" id="to" class="form-control" value="{{old('to')}}">
                        </div>
                    </div>

                    <div class="col">
                        <label for="exampleInputFile" style="color: #fff;">.</label>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip"
                                    data-placement="top" title="Search"><i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                    <div class="pullright">
                        <label for="">
                            <br>

                        </label>
                    </div>

                </div>
            </form>
            <div class="tab-pane" id="attendance">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Department</th>
                    </tr>
                    @if(!is_null($employees))
                        @foreach($employees as $key=>$employee)
                            <tr data-toggle="collapse" data-target="#{{$employee->id}}" aria-expanded="false"
                                aria-controls="{{$employee->id}}">
                                <td>{{ $employee->user_id }}</td>
                                <td>{{ $employee->name_with_initials }}</td>
                                <td>{{ $employee->departments['department_name'] }}<strong><i class="fa fa-angle-down pull-right"></i></strong></td>
                            </tr>
                            <tr class="collapse multi-collapse" id="{{ $employee->id }}">
                                <td colspan="3">
                                    <div id="inner">
                                        <table class="table table-bordered" id="example6">
                                            <tbody>
                                            <tr>
                                                <th>Year</th>
                                                <th>Month</th>
                                                <th>Date</th>
                                                <th>Day</th>
                                                <th>In</th>
                                                <th>Out</th>
                                            </tr>
                                            @if(!$filtered)
                                                @foreach($employee->attend as $attendance)
                                                    <tr>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->today)->format('Y') }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->today)->format('F') }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->today)->format('d') }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->today)->format('D') }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->uin_string)->format('H:i:s') }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($attendance->uout_string)->format('H:i:s') }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                @foreach($attendance as $key=>$value)
                                                    @if($employee->user_id == $value['employee']->user_id)
                                                        @foreach($value['attendance'] as $aKye=>$aValue)
                                                            <tr>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->today)->format('Y') }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->today)->format('F') }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->today)->format('d') }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->today)->format('D') }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->uin_string)->format('H:i:s') }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($aValue->uout_string)->format('H:i:s') }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

