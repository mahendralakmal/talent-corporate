
@extends('layouts.app')

@section('title')
    <title>Talent | Permissions</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Permissions</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Permissions</li>
    <li class="breadcrumb-item active">All Permissions</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Available Permissions
            <small class="pull-right">
                <a href="{{route('permissions.create')}}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="New"><i class="fas fa-plus"></i></a>
            </small>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">

                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Permissions</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i=0; ?>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{$permission->name}}</td>
                                <td>
                                    <a href="{{route('permissions.edit', $permission->id)}}" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>

                                    <form action="{{route('permissions.destroy',$permission->id)}}" method="post" style="display: inline">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
    </div>

@endsection
