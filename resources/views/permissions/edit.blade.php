
@extends('layouts.app')

@section('title')
    <title>Talent | Permissions</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Permissions</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permissions</a></li>
    <li class="breadcrumb-item active">Update Permission</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <b>Edit Permission</b>
                    </div>

                    <div class="card-body">

                        <form action="{{route('permissions.update', $permission->id)}}" method="post">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Permission Name</label>
                                        <input type="text" class="form-control {{$errors->has('permission_name') ? ' is-invalid' : ''}}" name="permission_name" placeholder="Permission Name" value="{{$permission->name}}">
                                        @if ($errors->has('permission_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permission_name')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <small class="pull-right">
                                <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                                <button class="btn btn-primary btn-sm" type="submit">Update</button>
                            </small>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
