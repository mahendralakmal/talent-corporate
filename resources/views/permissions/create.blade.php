
@extends('layouts.app')

@section('title')
    <title>Talent | Permissions</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Permission</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permissions</a></li>
    <li class="breadcrumb-item active">New Permission</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <b>Add Permission</b>
                    </div>

                    <div class="card-body">

                        <form action="{{route('permissions.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Permission Name</label>
                                        <input type="text" class="form-control {{$errors->has('permission_name') ? ' is-invalid' : ''}}" value="{{ old('permission_name') }}" name="permission_name" placeholder="Permission Name">
                                        @if ($errors->has('permission_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permission_name')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Assign Permission to Roles</label>
                                        <ul class="list-group">
                                            @if(!$roles->isEmpty())
                                                @foreach($roles as $role)
                                                    <li class="list-group-item"><input type="checkbox" name="roles[]" value="{{$role->id}}"> {{$role->name}}</li>
                                                @endforeach
                                            @else
                                                <h3>Roles Not Available</h3>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <small class="pull-right">
                                    <a class="btn btn-success btn-sm" href="{{url()->previous()}}" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                                    <button class="btn btn-primary btn-sm" type="submit">Add</button>
                            </small>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
