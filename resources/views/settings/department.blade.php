@extends('layouts.app')

@section('title')
    <title>Talent | Settings</title>
@endsection

@section('page_header')
    <h5 class="m-0 text-dark">Settings</h5>
@endsection

@section('pagenation')
    <li class="breadcrumb-item"><a href="{{'/home'}}">Dashboard</a></li>
    <li class="breadcrumb-item">Settings</li>
    <li class="breadcrumb-item active">Department</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <small class="pull-right">
                    <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#new"><i class="fa fa-plus"
                                                                                                     aria-hidden="true"
                                                                                                     data-toggle="tooltip"
                                                                                                     title="New"></i>
                    </button>
                </small>
            </h3>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                    <th class="search">ID</th>
                    @role('super-admin')
                    <th class="search">Company</th>
                    @endrole
                    <th class="search">Department Code</th>
                    <th class="search">Department Name</th>
                    <th class="search">Action</th>
                </tr>
                </thead>

                <tbody>

                <?php $i = 1; ?>

                @foreach($departments as $department)
                    <tr>
                        <td>{{$i++}}</td>
                        @role('super-admin')
                        <td>{{$department->company->name}}</td>
                        @endrole
                        <td>{{$department->department_code}}</td>
                        <td>{{$department->department_name}}</td>
                        <td>
                            <button class="btn btn-success btn-sm" data-departmentid="{{ $department->id }}"
                                    data-toggle="modal" data-target="#edit"><i class="fa fa-pencil-square-o"
                                                                               data-toggle="tooltip"
                                                                               data-placement="top" title="Edit"
                                                                               id="update" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-danger btn-sm" data-departmentid="{{ $department->id }}"
                                    data-toggle="modal" data-target="#delete"><i class="fa fa-trash-o"
                                                                                 data-toggle="tooltip"
                                                                                 data-placement="top" title="Delete"
                                                                                 aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>
        </div>

        <div class="row">
            <div class="col-3" style="margin-bottom: 15px; margin-left: 25px;">
                <a class="btn btn-success btn-sm" href="{{url()->previous()}}"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> Back</a>
            </div>
        </div>

    </div>


    <!-- create model -->
    <div class="modal fade" id="new" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add a new Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{ route('settings.store_department')}}" method="POST" >
                        {{ csrf_field() }}
                        <div class="modal-body">
                            @include('settings.department_form')
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add Department</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end create model -->

    <!-- edit model -->
    <div class="modal fade" id="edit" tabindex="-1" role=".modal-dialog-centered" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('settings.update_department','0')}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <input type="hidden" name="dept_id" value="" id="dept_id">
                        @include('settings.department_form')
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update Department</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit model -->

    <!-- delete modal -->
    <div id="delete" class="modal fade" role=".modal-dialog-centered">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                </div>
                <h1>Are you Sure ?</h1>

                <form action="{{ route('settings.delete_department', '0') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <div class="modal-body">
                        <input type="hidden" name="dept_id" value="" id="dept_id">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('custom-jquery')

    <script>
        $('#edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('departmentid');
            var modal = $(this)
            modal.find('#dept_id').val(id);
            $.ajax({
                url: "{{ route('settings.edit_department') }}",
                method: "get",
                data: {id: id},
                dataType: "json",
                success: function (data) {
                    modal.find('#department_id').val(data.department_code);
                    modal.find('#department_name').val(data.department_name);
                }
            });
        });

        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('departmentid');
            console.log(id);
            var modal = $(this)
            modal.find('#dept_id').val(id);
        });
    </script>

    <script>
        var department = new Vue({
            el: '#department',
            data: {
                department_name: null,
                department_id: null
            }
        })
    </script>
@endsection
