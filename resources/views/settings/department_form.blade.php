@role('super-admin')
<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label>Company</label>
            <select name="company" id="company" class="form-control">
                <option value="">Select Company</option>
                @foreach($companies as $company)
                    <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">
                <span class="help-block form-error">@{{ errors.first('department_name') }}</span>
            </div>
        </div>
    </div>
</div>
@endrole
@role('company-admin')
    <input type="hidden" name="company" id="company" value="{{ \App\company::where('regno',auth()->user()->company_id)->first()->id }}" >
@endrole

<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label>Department Code</label>
            <input type="text" maxlength="10" v-model="department_id" v-validate="'required'" data-vv-as="Department ID" class="form-control{{$errors->has('department_id') ? ' is-invalid' : ''}}" name="department_id" id="department_id">
            <div class="invalid-feedback">
                <span class="help-block form-error">@{{ errors.first('department_id') }}</span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col">
            <label>Department Description</label>
            <input type="text" maxlength="50" v-model="department_name" v-validate="'required|numeric'" data-vv-as="Department Name" class="form-control{{$errors->has('department_name') ? ' is-invalid' : ''}}" name="department_name" id="department_name">
            <div class="invalid-feedback">
                <span class="help-block form-error">@{{ errors.first('department_name') }}</span>
            </div>
        </div>
    </div>
</div>
