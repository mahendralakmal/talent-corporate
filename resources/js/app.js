/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('sweetalert');
// require('jspdf');
// require('html2canvas');
// require('rasterizehtml');
//
// window.Vue = require('vue');
window.jsPDF = require('jspdf');
window.jsPDFAutotable = require('jspdf-autotable');
window.html2canvas = require('html2canvas');
window.rasterizehtml = require('rasterizehtml');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
window.Vue = require('vue');
Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$(document).ready(function () {
    window.history.forward(1);

    $(function() {
        $('.alert').delay(5000).fadeOut('slow');
    });
});
$(document).ready(function () {
    var doc = new jsPDF('p', 'pt', 'a4');

    var specialElementHandlers = {
        '#print-btn': function (element, renderer) {
            return true;
        }
    };

    $('#print').click(function () {
        // var css = '<link rel="stylesheet" href="http://tallent.d/css/app.css">';
        //
        // var printContent = $('#report');
        //
        // var WinPrint = window.open('', '', 'width=900,height=650');
        // WinPrint.document.write(css+printContent.innerHTML);
        // WinPrint.document.head.innerHTML = css;

        var WinPrint = window.open();
        WinPrint.document.write($('#printableDiv').html());
        // WinPrint.document.close();
        // WinPrint.focus();
        WinPrint.print();
        WinPrint.close();
    });

});

