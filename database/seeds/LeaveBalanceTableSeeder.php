<?php
    
    use App\company;
    use App\Employee;
    use App\Leave_balance;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Auth;
    
    class LeaveBalanceTableSeeder extends Seeder {
        /**
         * Run the database seeds.
         * @return void
         */
        public function run () {
            $companies = company::all();
            foreach ($companies as $key => $company) {
                $types =$company->leaveTypes;
                foreach ($types as $leaveType) {
                    $employees = $company->employees;
                    foreach ($employees as $key => $employee) {
                        $leave_balance = new Leave_balance();
                        $leave_balance->company_id = $company->regno;
                        $leave_balance->user_id = $employee->user_id;
                        $leave_balance->leave_type = $leaveType->id;
                        $leave_balance->used = 0;
                        $leave_balance->remaining = $leaveType->entitlement;
                        $leave_balance->save();
                    }
                }
            }
        }
    }
